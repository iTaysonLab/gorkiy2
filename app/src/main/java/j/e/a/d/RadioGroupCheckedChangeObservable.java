package j.e.a.d;

import android.widget.RadioGroup;
import j.c.a.a.c.n.c;
import j.e.a.InitialValueObservable;
import l.b.Observer;
import l.b.r.MainThreadDisposable;

public final class RadioGroupCheckedChangeObservable extends InitialValueObservable<Integer> {
    public final RadioGroup b;

    public static final class a extends MainThreadDisposable implements RadioGroup.OnCheckedChangeListener {
        public final RadioGroup c;
        public final Observer<? super Integer> d;

        /* renamed from: e  reason: collision with root package name */
        public int f2599e = -1;

        public a(RadioGroup radioGroup, Observer<? super Integer> observer) {
            this.c = radioGroup;
            this.d = observer;
        }

        public void a() {
            this.c.setOnCheckedChangeListener(null);
        }

        public void onCheckedChanged(RadioGroup radioGroup, int i2) {
            if (!g() && i2 != this.f2599e) {
                this.f2599e = i2;
                this.d.b(Integer.valueOf(i2));
            }
        }
    }

    public RadioGroupCheckedChangeObservable(RadioGroup radioGroup) {
        this.b = radioGroup;
    }

    public void c(Observer<? super Integer> observer) {
        if (c.a((Observer<?>) observer)) {
            a aVar = new a(this.b, observer);
            this.b.setOnCheckedChangeListener(aVar);
            observer.a(aVar);
        }
    }

    public Object c() {
        return Integer.valueOf(this.b.getCheckedRadioButtonId());
    }
}
