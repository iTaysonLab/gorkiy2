package j.c.e;

import com.crashlytics.android.core.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.a.a.a.outline;
import j.c.e.ByteString;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public final class CodedInputStream {
    public final byte[] a;
    public final boolean b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f2587e;

    /* renamed from: f  reason: collision with root package name */
    public final InputStream f2588f;
    public int g;
    public boolean h = false;

    /* renamed from: i  reason: collision with root package name */
    public int f2589i;

    /* renamed from: j  reason: collision with root package name */
    public int f2590j = Integer.MAX_VALUE;

    /* renamed from: k  reason: collision with root package name */
    public int f2591k;

    /* renamed from: l  reason: collision with root package name */
    public int f2592l = 100;

    /* renamed from: m  reason: collision with root package name */
    public int f2593m = 67108864;

    /* renamed from: n  reason: collision with root package name */
    public a f2594n = null;

    public interface a {
        void a();
    }

    public CodedInputStream(byte[] bArr, int i2, int i3, boolean z) {
        this.a = bArr;
        this.c = i3 + i2;
        this.f2587e = i2;
        this.f2589i = -i2;
        this.f2588f = null;
        this.b = z;
    }

    public void a(int i2) {
        if (this.g != i2) {
            throw new InvalidProtocolBufferException("Protocol message end-group tag did not match expected tag.");
        }
    }

    public ByteString b() {
        ByteString byteString;
        int d2 = d();
        int i2 = this.c;
        int i3 = this.f2587e;
        if (d2 <= i2 - i3 && d2 > 0) {
            if (!this.b || !this.h) {
                byteString = new ByteString.g(ByteString.d.a(this.a, this.f2587e, d2));
            } else {
                byteString = ByteString.a(this.a, i3, d2);
            }
            this.f2587e += d2;
            return byteString;
        } else if (d2 == 0) {
            return ByteString.c;
        } else {
            return ByteString.b(c(d2));
        }
    }

    public byte c() {
        if (this.f2587e == this.c) {
            d(1);
        }
        byte[] bArr = this.a;
        int i2 = this.f2587e;
        this.f2587e = i2 + 1;
        return bArr[i2];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        if (r2[r3] < 0) goto L_0x006a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int d() {
        /*
            r5 = this;
            int r0 = r5.f2587e
            int r1 = r5.c
            if (r1 != r0) goto L_0x0007
            goto L_0x006a
        L_0x0007:
            byte[] r2 = r5.a
            int r3 = r0 + 1
            byte r0 = r2[r0]
            if (r0 < 0) goto L_0x0012
            r5.f2587e = r3
            return r0
        L_0x0012:
            int r1 = r1 - r3
            r4 = 9
            if (r1 >= r4) goto L_0x0018
            goto L_0x006a
        L_0x0018:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 7
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x0024
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            goto L_0x0070
        L_0x0024:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0031
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
        L_0x002f:
            r1 = r3
            goto L_0x0070
        L_0x0031:
            int r1 = r3 + 1
            byte r3 = r2[r3]
            int r3 = r3 << 21
            r0 = r0 ^ r3
            if (r0 >= 0) goto L_0x003f
            r2 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r2
            goto L_0x0070
        L_0x003f:
            int r3 = r1 + 1
            byte r1 = r2[r1]
            int r4 = r1 << 28
            r0 = r0 ^ r4
            r4 = 266354560(0xfe03f80, float:2.2112565E-29)
            r0 = r0 ^ r4
            if (r1 >= 0) goto L_0x002f
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0070
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002f
            int r1 = r3 + 1
            byte r3 = r2[r3]
            if (r3 >= 0) goto L_0x0070
            int r3 = r1 + 1
            byte r1 = r2[r1]
            if (r1 >= 0) goto L_0x002f
            int r1 = r3 + 1
            byte r2 = r2[r3]
            if (r2 >= 0) goto L_0x0070
        L_0x006a:
            long r0 = r5.e()
            int r1 = (int) r0
            return r1
        L_0x0070:
            r5.f2587e = r1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.e.CodedInputStream.d():int");
    }

    public boolean e(int i2) {
        int g2;
        int i3 = i2 & 7;
        if (i3 == 0) {
            int i4 = this.c;
            int i5 = this.f2587e;
            if (i4 - i5 >= 10) {
                byte[] bArr = this.a;
                int i6 = 0;
                while (true) {
                    if (i6 >= 10) {
                        break;
                    }
                    int i7 = i5 + 1;
                    if (bArr[i5] >= 0) {
                        this.f2587e = i7;
                        break;
                    }
                    i6++;
                    i5 = i7;
                }
            }
            for (int i8 = 0; i8 < 10; i8++) {
                if (c() >= 0) {
                    return true;
                }
            }
            throw new InvalidProtocolBufferException("CodedInputStream encountered a malformed varint.");
        } else if (i3 == 1) {
            f(8);
            return true;
        } else if (i3 == 2) {
            f(d());
            return true;
        } else if (i3 == 3) {
            do {
                g2 = g();
                if (g2 == 0) {
                    break;
                }
            } while (e(g2));
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } else if (i3 == 4) {
            return false;
        } else {
            if (i3 == 5) {
                f(4);
                return true;
            }
            throw new InvalidProtocolBufferException("Protocol message tag had invalid wire type.");
        }
    }

    public String f() {
        byte[] bArr;
        int d2 = d();
        int i2 = this.f2587e;
        boolean z = false;
        if (d2 <= this.c - i2 && d2 > 0) {
            bArr = this.a;
            this.f2587e = i2 + d2;
        } else if (d2 == 0) {
            return "";
        } else {
            if (d2 <= this.c) {
                d(d2);
                bArr = this.a;
                this.f2587e = d2 + 0;
            } else {
                bArr = c(d2);
            }
            i2 = 0;
        }
        if (Utf8.a.a(0, bArr, i2, i2 + d2) == 0) {
            z = true;
        }
        if (z) {
            return new String(bArr, i2, d2, Internal.a);
        }
        throw new InvalidProtocolBufferException("Protocol message had invalid UTF-8.");
    }

    public int g() {
        boolean z = true;
        if (this.f2587e != this.c || g(1)) {
            z = false;
        }
        if (z) {
            this.g = 0;
            return 0;
        }
        int d2 = d();
        this.g = d2;
        if ((d2 >>> 3) != 0) {
            return d2;
        }
        throw new InvalidProtocolBufferException("Protocol message contained an invalid tag (zero).");
    }

    public final void h() {
        int i2 = this.c + this.d;
        this.c = i2;
        int i3 = this.f2589i + i2;
        int i4 = this.f2590j;
        if (i3 > i4) {
            int i5 = i3 - i4;
            this.d = i5;
            this.c = i2 - i5;
            return;
        }
        this.d = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.Parser.a(j.c.e.g, j.c.e.i):MessageType
     arg types: [j.c.e.CodedInputStream, j.c.e.i]
     candidates:
      j.c.e.Parser.a(j.c.e.f, j.c.e.i):MessageType
      j.c.e.Parser.a(j.c.e.g, j.c.e.i):MessageType */
    public <T extends o> T a(Parser<T> parser, i iVar) {
        int d2 = d();
        if (this.f2591k < this.f2592l) {
            int b2 = b(d2);
            this.f2591k++;
            T t2 = (MessageLite) parser.a((g) this, iVar);
            a(0);
            this.f2591k--;
            this.f2590j = b2;
            h();
            return t2;
        }
        throw new InvalidProtocolBufferException("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    public final byte[] c(int i2) {
        if (i2 > 0) {
            int i3 = this.f2589i;
            int i4 = this.f2587e;
            int i5 = i3 + i4 + i2;
            if (i5 <= this.f2593m) {
                int i6 = this.f2590j;
                if (i5 <= i6) {
                    InputStream inputStream = this.f2588f;
                    if (inputStream != null) {
                        int i7 = this.c;
                        int i8 = i7 - i4;
                        this.f2589i = i3 + i7;
                        this.f2587e = 0;
                        this.c = 0;
                        int i9 = i2 - i8;
                        if (i9 < 4096 || i9 <= inputStream.available()) {
                            byte[] bArr = new byte[i2];
                            System.arraycopy(this.a, i4, bArr, 0, i8);
                            while (i8 < i2) {
                                int read = this.f2588f.read(bArr, i8, i2 - i8);
                                if (read != -1) {
                                    this.f2589i += read;
                                    i8 += read;
                                } else {
                                    throw InvalidProtocolBufferException.b();
                                }
                            }
                            return bArr;
                        }
                        ArrayList arrayList = new ArrayList();
                        while (i9 > 0) {
                            int min = Math.min(i9, (int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                            byte[] bArr2 = new byte[min];
                            int i10 = 0;
                            while (i10 < min) {
                                int read2 = this.f2588f.read(bArr2, i10, min - i10);
                                if (read2 != -1) {
                                    this.f2589i += read2;
                                    i10 += read2;
                                } else {
                                    throw InvalidProtocolBufferException.b();
                                }
                            }
                            i9 -= min;
                            arrayList.add(bArr2);
                        }
                        byte[] bArr3 = new byte[i2];
                        System.arraycopy(this.a, i4, bArr3, 0, i8);
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            byte[] bArr4 = (byte[]) it.next();
                            System.arraycopy(bArr4, 0, bArr3, i8, bArr4.length);
                            i8 += bArr4.length;
                        }
                        return bArr3;
                    }
                    throw InvalidProtocolBufferException.b();
                }
                f((i6 - i3) - i4);
                throw InvalidProtocolBufferException.b();
            }
            throw new InvalidProtocolBufferException("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
        } else if (i2 == 0) {
            return Internal.b;
        } else {
            throw InvalidProtocolBufferException.a();
        }
    }

    public final boolean g(int i2) {
        int i3 = this.f2587e;
        if (i3 + i2 <= this.c) {
            throw new IllegalStateException(outline.b("refillBuffer() called when ", i2, " bytes were already available in buffer"));
        } else if (this.f2589i + i3 + i2 > this.f2590j) {
            return false;
        } else {
            a aVar = this.f2594n;
            if (aVar != null) {
                aVar.a();
            }
            if (this.f2588f != null) {
                int i4 = this.f2587e;
                if (i4 > 0) {
                    int i5 = this.c;
                    if (i5 > i4) {
                        byte[] bArr = this.a;
                        System.arraycopy(bArr, i4, bArr, 0, i5 - i4);
                    }
                    this.f2589i += i4;
                    this.c -= i4;
                    this.f2587e = 0;
                }
                InputStream inputStream = this.f2588f;
                byte[] bArr2 = this.a;
                int i6 = this.c;
                int read = inputStream.read(bArr2, i6, bArr2.length - i6);
                if (read == 0 || read < -1 || read > this.a.length) {
                    throw new IllegalStateException(outline.b("InputStream#read(byte[]) returned invalid result: ", read, "\nThe InputStream implementation is buggy."));
                } else if (read > 0) {
                    this.c += read;
                    if ((this.f2589i + i2) - this.f2593m <= 0) {
                        h();
                        if (this.c >= i2) {
                            return true;
                        }
                        return g(i2);
                    }
                    throw new InvalidProtocolBufferException("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
                }
            }
            return false;
        }
    }

    public int b(int i2) {
        if (i2 >= 0) {
            int i3 = this.f2589i + this.f2587e + i2;
            int i4 = this.f2590j;
            if (i3 <= i4) {
                this.f2590j = i3;
                h();
                return i4;
            }
            throw InvalidProtocolBufferException.b();
        }
        throw InvalidProtocolBufferException.a();
    }

    public final void d(int i2) {
        if (!g(i2)) {
            throw InvalidProtocolBufferException.b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c6 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c8 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a() {
        /*
            r11 = this;
            int r0 = r11.f2587e
            int r1 = r11.c
            r2 = 0
            if (r1 != r0) goto L_0x000a
            goto L_0x00b8
        L_0x000a:
            byte[] r4 = r11.a
            int r5 = r0 + 1
            byte r0 = r4[r0]
            if (r0 < 0) goto L_0x0017
            r11.f2587e = r5
            long r0 = (long) r0
            goto L_0x00c2
        L_0x0017:
            int r1 = r1 - r5
            r6 = 9
            if (r1 >= r6) goto L_0x001e
            goto L_0x00b8
        L_0x001e:
            int r1 = r5 + 1
            byte r5 = r4[r5]
            int r5 = r5 << 7
            r0 = r0 ^ r5
            if (r0 >= 0) goto L_0x002a
            r0 = r0 ^ -128(0xffffffffffffff80, float:NaN)
            goto L_0x0045
        L_0x002a:
            int r5 = r1 + 1
            byte r1 = r4[r1]
            int r1 = r1 << 14
            r0 = r0 ^ r1
            if (r0 < 0) goto L_0x0038
            r0 = r0 ^ 16256(0x3f80, float:2.278E-41)
            long r0 = (long) r0
            goto L_0x00c0
        L_0x0038:
            int r1 = r5 + 1
            byte r5 = r4[r5]
            int r5 = r5 << 21
            r0 = r0 ^ r5
            if (r0 >= 0) goto L_0x004b
            r4 = -2080896(0xffffffffffe03f80, float:NaN)
            r0 = r0 ^ r4
        L_0x0045:
            long r4 = (long) r0
        L_0x0046:
            r9 = r4
            r5 = r1
        L_0x0048:
            r0 = r9
            goto L_0x00c0
        L_0x004b:
            long r5 = (long) r0
            int r0 = r1 + 1
            byte r1 = r4[r1]
            long r7 = (long) r1
            r1 = 28
            long r7 = r7 << r1
            long r5 = r5 ^ r7
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 < 0) goto L_0x005d
            r7 = 266354560(0xfe03f80, double:1.315966377E-315)
            goto L_0x0082
        L_0x005d:
            int r1 = r0 + 1
            byte r0 = r4[r0]
            long r7 = (long) r0
            r0 = 35
            long r7 = r7 << r0
            long r5 = r5 ^ r7
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x0070
            r7 = -34093383808(0xfffffff80fe03f80, double:NaN)
            goto L_0x0099
        L_0x0070:
            int r0 = r1 + 1
            byte r1 = r4[r1]
            long r7 = (long) r1
            r1 = 42
            long r7 = r7 << r1
            long r5 = r5 ^ r7
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 < 0) goto L_0x0087
            r7 = 4363953127296(0x3f80fe03f80, double:2.1560793202584E-311)
        L_0x0082:
            long r4 = r5 ^ r7
            r9 = r4
        L_0x0085:
            r5 = r0
            goto L_0x0048
        L_0x0087:
            int r1 = r0 + 1
            byte r0 = r4[r0]
            long r7 = (long) r0
            r0 = 49
            long r7 = r7 << r0
            long r5 = r5 ^ r7
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x009c
            r7 = -558586000294016(0xfffe03f80fe03f80, double:NaN)
        L_0x0099:
            long r4 = r5 ^ r7
            goto L_0x0046
        L_0x009c:
            int r0 = r1 + 1
            byte r1 = r4[r1]
            long r7 = (long) r1
            r1 = 56
            long r7 = r7 << r1
            long r5 = r5 ^ r7
            r7 = 71499008037633920(0xfe03f80fe03f80, double:6.838959413692434E-304)
            long r5 = r5 ^ r7
            int r1 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x00be
            int r1 = r0 + 1
            byte r0 = r4[r0]
            long r7 = (long) r0
            int r0 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bd
        L_0x00b8:
            long r0 = r11.e()
            goto L_0x00c2
        L_0x00bd:
            r0 = r1
        L_0x00be:
            r9 = r5
            goto L_0x0085
        L_0x00c0:
            r11.f2587e = r5
        L_0x00c2:
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x00c8
            r0 = 1
            goto L_0x00c9
        L_0x00c8:
            r0 = 0
        L_0x00c9:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.e.CodedInputStream.a():boolean");
    }

    public long e() {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte c2 = c();
            j2 |= ((long) (c2 & Byte.MAX_VALUE)) << i2;
            if ((c2 & 128) == 0) {
                return j2;
            }
        }
        throw new InvalidProtocolBufferException("CodedInputStream encountered a malformed varint.");
    }

    public void f(int i2) {
        int i3 = this.c;
        int i4 = this.f2587e;
        if (i2 <= i3 - i4 && i2 >= 0) {
            this.f2587e = i4 + i2;
        } else if (i2 >= 0) {
            int i5 = this.f2589i;
            int i6 = this.f2587e;
            int i7 = i5 + i6 + i2;
            int i8 = this.f2590j;
            if (i7 <= i8) {
                int i9 = this.c;
                int i10 = i9 - i6;
                this.f2587e = i9;
                d(1);
                while (true) {
                    int i11 = i2 - i10;
                    int i12 = this.c;
                    if (i11 > i12) {
                        i10 += i12;
                        this.f2587e = i12;
                        d(1);
                    } else {
                        this.f2587e = i11;
                        return;
                    }
                }
            } else {
                f((i8 - i5) - i6);
                throw InvalidProtocolBufferException.b();
            }
        } else {
            throw InvalidProtocolBufferException.a();
        }
    }
}
