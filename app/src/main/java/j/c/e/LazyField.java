package j.c.e;

import java.util.Iterator;
import java.util.Map;

public class LazyField extends LazyFieldLite {

    public static class b<K> implements Map.Entry<K, Object> {
        public Map.Entry<K, m> b;

        public /* synthetic */ b(Map.Entry entry, a aVar) {
            this.b = entry;
        }

        public K getKey() {
            return this.b.getKey();
        }

        public Object getValue() {
            LazyField value = this.b.getValue();
            if (value == null) {
                return null;
            }
            return value.a();
        }

        public Object setValue(Object obj) {
            if (obj instanceof MessageLite) {
                LazyField value = this.b.getValue();
                MessageLite messageLite = value.b;
                value.a = null;
                value.b = (MessageLite) obj;
                return messageLite;
            }
            throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
        }
    }

    public static class c<K> implements Iterator<Map.Entry<K, Object>> {
        public Iterator<Map.Entry<K, Object>> b;

        public c(Iterator<Map.Entry<K, Object>> it) {
            this.b = it;
        }

        public boolean hasNext() {
            return this.b.hasNext();
        }

        public Object next() {
            Map.Entry next = this.b.next();
            return next.getValue() instanceof LazyField ? new b(next, null) : next;
        }

        public void remove() {
            this.b.remove();
        }
    }

    public MessageLite a() {
        a(null);
        return super.b;
    }

    public boolean equals(Object obj) {
        return a().equals(obj);
    }

    public int hashCode() {
        return a().hashCode();
    }

    public String toString() {
        return a().toString();
    }
}
