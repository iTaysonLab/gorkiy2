package j.c.e;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.UninitializedMessageException;
import j.c.e.AbstractMessageLite;
import j.c.e.FieldSet;
import j.c.e.GeneratedMessageLite;
import j.c.e.GeneratedMessageLite.b;
import j.c.e.Internal;
import j.c.e.MessageLite;
import j.c.e.k;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;

public abstract class GeneratedMessageLite<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends AbstractMessageLite<MessageType, BuilderType> {
    public UnknownFieldSetLite c = UnknownFieldSetLite.d;
    public int d = -1;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0010 */
        static {
            /*
                j.c.e.WireFormat1[] r0 = j.c.e.WireFormat1.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.e.GeneratedMessageLite.a.a = r0
                j.c.e.WireFormat1 r1 = j.c.e.WireFormat1.MESSAGE     // Catch:{ NoSuchFieldError -> 0x0010 }
                r1 = 8
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0010 }
            L_0x0010:
                int[] r0 = j.c.e.GeneratedMessageLite.a.a     // Catch:{ NoSuchFieldError -> 0x0018 }
                j.c.e.WireFormat1 r1 = j.c.e.WireFormat1.ENUM     // Catch:{ NoSuchFieldError -> 0x0018 }
                r1 = 7
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0018 }
            L_0x0018:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.e.GeneratedMessageLite.a.<clinit>():void");
        }
    }

    public static abstract class b<MessageType extends GeneratedMessageLite<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends AbstractMessageLite.a<MessageType, BuilderType> {
        public final MessageType b;
        public MessageType c;
        public boolean d = false;

        public b(MessageType messagetype) {
            this.b = messagetype;
            this.c = (GeneratedMessageLite) messagetype.a(j.NEW_MUTABLE_INSTANCE);
        }

        public BuilderType a(GeneratedMessageLite generatedMessageLite) {
            m();
            this.c.a(i.a, generatedMessageLite);
            return this;
        }

        public Object clone() {
            MessageType messagetype = this.b;
            if (messagetype != null) {
                b bVar = (b) messagetype.a(j.NEW_BUILDER);
                bVar.a(l());
                return bVar;
            }
            throw null;
        }

        public MessageLite d() {
            return this.b;
        }

        public final boolean j() {
            if (this.c.a(j.IS_INITIALIZED, false, null) != null) {
                return true;
            }
            return false;
        }

        public final MessageType k() {
            MessageType l2 = l();
            if (l2.j()) {
                return l2;
            }
            throw new UninitializedMessageException();
        }

        public MessageType l() {
            if (this.d) {
                return this.c;
            }
            this.c.f();
            this.d = true;
            return this.c;
        }

        public void m() {
            if (this.d) {
                MessageType messagetype = (GeneratedMessageLite) this.c.a(j.NEW_MUTABLE_INSTANCE);
                messagetype.a(i.a, this.c);
                this.c = messagetype;
                this.d = false;
            }
        }
    }

    public static class c<T extends GeneratedMessageLite<T, ?>> extends AbstractParser<T> {
        public T a;

        public c(T t2) {
            this.a = t2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T
         arg types: [T, j.c.e.CodedInputStream, j.c.e.ExtensionRegistryLite]
         candidates:
          j.c.e.GeneratedMessageLite.a(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):java.lang.Object
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object, java.lang.Object):java.lang.Object
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T */
        public Object a(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
            return GeneratedMessageLite.a((GeneratedMessageLite) this.a, (g) codedInputStream, (i) extensionRegistryLite);
        }
    }

    public static class d implements k {
        public static final d a = new d();
        public static final a b = new a();

        public static final class a extends RuntimeException {
        }

        public boolean a(boolean z, boolean z2, boolean z3, boolean z4) {
            if (z == z3 && z2 == z4) {
                return z2;
            }
            throw b;
        }

        public int a(boolean z, int i2, boolean z2, int i3) {
            if (z == z2 && i2 == i3) {
                return i2;
            }
            throw b;
        }

        public String a(boolean z, String str, boolean z2, String str2) {
            if (z == z2 && str.equals(str2)) {
                return str;
            }
            throw b;
        }

        public ByteString a(boolean z, ByteString byteString, boolean z2, ByteString byteString2) {
            if (z == z2 && byteString.equals(byteString2)) {
                return byteString;
            }
            throw b;
        }

        public <T extends o> T a(T t2, T t3) {
            if (t2 == null && t3 == null) {
                return null;
            }
            if (t2 == null || t3 == null) {
                throw b;
            }
            T t4 = (GeneratedMessageLite) t2;
            if (t4 != t3 && t4.d().getClass().isInstance(t3)) {
                t4.a(this, (GeneratedMessageLite) t3);
            }
            return t2;
        }

        public <T> Internal.c<T> a(Internal.c<T> cVar, Internal.c<T> cVar2) {
            if (cVar.equals(cVar2)) {
                return cVar;
            }
            throw b;
        }

        public FieldSet<k.g> a(FieldSet<k.g> fieldSet, FieldSet<k.g> fieldSet2) {
            if (fieldSet.equals(fieldSet2)) {
                return fieldSet;
            }
            throw b;
        }

        public UnknownFieldSetLite a(UnknownFieldSetLite unknownFieldSetLite, UnknownFieldSetLite unknownFieldSetLite2) {
            if (unknownFieldSetLite.equals(unknownFieldSetLite2)) {
                return unknownFieldSetLite;
            }
            throw b;
        }
    }

    public static abstract class e<MessageType extends e<MessageType, BuilderType>, BuilderType extends Object<MessageType, BuilderType>> extends GeneratedMessageLite<MessageType, BuilderType> implements f<MessageType, BuilderType> {

        /* renamed from: e  reason: collision with root package name */
        public FieldSet<k.g> f2595e = new FieldSet<>();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.e.GeneratedMessageLite.e.a(j.c.e.k$k, j.c.e.GeneratedMessageLite$e):void
         arg types: [j.c.e.GeneratedMessageLite$k, j.c.e.GeneratedMessageLite$e]
         candidates:
          j.c.e.GeneratedMessageLite.e.a(j.c.e.GeneratedMessageLite$k, j.c.e.GeneratedMessageLite):void
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
          j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
          j.c.e.GeneratedMessageLite.e.a(j.c.e.k$k, j.c.e.GeneratedMessageLite$e):void */
        public /* bridge */ /* synthetic */ void a(k kVar, GeneratedMessageLite generatedMessageLite) {
            a((k.k) kVar, (e) generatedMessageLite);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
        public /* bridge */ /* synthetic */ MessageLite d() {
            return GeneratedMessageLite.super.d();
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [j.c.e.MessageLite$a, j.c.e.GeneratedMessageLite$b] */
        public /* bridge */ /* synthetic */ MessageLite.a e() {
            return GeneratedMessageLite.super.e();
        }

        public final void f() {
            GeneratedMessageLite.super.f();
            FieldSet<k.g> fieldSet = this.f2595e;
            if (!fieldSet.b) {
                fieldSet.a.e();
                fieldSet.b = true;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
         arg types: [j.c.e.k$k, MessageType]
         candidates:
          j.c.e.GeneratedMessageLite.e.a(j.c.e.k$k, j.c.e.GeneratedMessageLite$e):void
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
          j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void */
        public final void a(k.k kVar, MessageType messagetype) {
            GeneratedMessageLite.super.a(kVar, (GeneratedMessageLite) super);
            this.f2595e = kVar.a(this.f2595e, messagetype.f2595e);
        }
    }

    public interface f<MessageType extends e<MessageType, BuilderType>, BuilderType extends Object<MessageType, BuilderType>> extends p {
    }

    public static final class g implements FieldSet.a<k.g> {
        public final int b;

        /* JADX WARN: Type inference failed for: r1v2, types: [j.c.e.MessageLite$a, j.c.e.GeneratedMessageLite$b] */
        public MessageLite.a a(MessageLite.a aVar, MessageLite messageLite) {
            return ((b) aVar).a((GeneratedMessageLite) messageLite);
        }

        public int compareTo(Object obj) {
            return 0 - ((g) obj).b;
        }

        public boolean k() {
            return false;
        }

        public WireFormat0 m() {
            return null;
        }

        public WireFormat1 n() {
            throw null;
        }
    }

    public static class h implements k {
        public int a = 0;

        public /* synthetic */ h(a aVar) {
        }

        public boolean a(boolean z, boolean z2, boolean z3, boolean z4) {
            this.a = Internal.a(z2) + (this.a * 53);
            return z2;
        }

        public int a(boolean z, int i2, boolean z2, int i3) {
            this.a = (this.a * 53) + i2;
            return i2;
        }

        public String a(boolean z, String str, boolean z2, String str2) {
            this.a = str.hashCode() + (this.a * 53);
            return str;
        }

        public ByteString a(boolean z, ByteString byteString, boolean z2, ByteString byteString2) {
            this.a = byteString.hashCode() + (this.a * 53);
            return byteString;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
         arg types: [j.c.e.GeneratedMessageLite$h, j.c.e.GeneratedMessageLite]
         candidates:
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
          j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
          j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void */
        public <T extends o> T a(T t2, T t3) {
            int i2;
            if (t2 == null) {
                i2 = 37;
            } else if (t2 instanceof GeneratedMessageLite) {
                GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite) t2;
                if (generatedMessageLite.b == 0) {
                    int i3 = this.a;
                    this.a = 0;
                    generatedMessageLite.a((k.k) this, generatedMessageLite);
                    generatedMessageLite.b = this.a;
                    this.a = i3;
                }
                i2 = generatedMessageLite.b;
            } else {
                i2 = t2.hashCode();
            }
            this.a = (this.a * 53) + i2;
            return t2;
        }

        public <T> Internal.c<T> a(Internal.c<T> cVar, Internal.c<T> cVar2) {
            this.a = cVar.hashCode() + (this.a * 53);
            return cVar;
        }

        public FieldSet<k.g> a(FieldSet<k.g> fieldSet, FieldSet<k.g> fieldSet2) {
            this.a = fieldSet.hashCode() + (this.a * 53);
            return fieldSet;
        }

        public UnknownFieldSetLite a(UnknownFieldSetLite unknownFieldSetLite, UnknownFieldSetLite unknownFieldSetLite2) {
            this.a = unknownFieldSetLite.hashCode() + (this.a * 53);
            return unknownFieldSetLite;
        }
    }

    public enum j {
        IS_INITIALIZED,
        VISIT,
        MERGE_FROM_STREAM,
        MAKE_IMMUTABLE,
        NEW_MUTABLE_INSTANCE,
        NEW_BUILDER,
        GET_DEFAULT_INSTANCE,
        GET_PARSER
    }

    public interface k {
        int a(boolean z, int i2, boolean z2, int i3);

        ByteString a(boolean z, ByteString byteString, boolean z2, ByteString byteString2);

        FieldSet<k.g> a(FieldSet<k.g> fieldSet, FieldSet<k.g> fieldSet2);

        <T> Internal.c<T> a(Internal.c cVar, Internal.c cVar2);

        <T extends o> T a(MessageLite messageLite, MessageLite messageLite2);

        UnknownFieldSetLite a(UnknownFieldSetLite unknownFieldSetLite, UnknownFieldSetLite unknownFieldSetLite2);

        String a(boolean z, String str, boolean z2, String str2);

        boolean a(boolean z, boolean z2, boolean z3, boolean z4);
    }

    public Object a(j jVar, Object obj) {
        return a(jVar, obj, (Object) null);
    }

    public abstract Object a(j jVar, Object obj, Object obj2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
     arg types: [j.c.e.GeneratedMessageLite$d, j.c.e.GeneratedMessageLite]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!d().getClass().isInstance(obj)) {
            return false;
        }
        try {
            a((k.k) d.a, (GeneratedMessageLite) obj);
            return true;
        } catch (d.a unused) {
            return false;
        }
    }

    public void f() {
        a(j.MAKE_IMMUTABLE);
        if (this.c == null) {
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
     arg types: [j.c.e.GeneratedMessageLite$h, j.c.e.GeneratedMessageLite]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void */
    public int hashCode() {
        if (super.b == 0) {
            h hVar = new h(null);
            a((k.k) hVar, this);
            super.b = hVar.a;
        }
        return super.b;
    }

    public final Parser<MessageType> i() {
        return (Parser) a(j.GET_PARSER);
    }

    public final boolean j() {
        return a(j.IS_INITIALIZED, Boolean.TRUE) != null;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite, java.lang.Object] */
    public String toString() {
        String obj = super.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(obj);
        j.c.a.a.c.n.c.a((MessageLite) this, sb, 0);
        return sb.toString();
    }

    public Object a(j jVar) {
        return a(jVar, (Object) null, (Object) null);
    }

    public final MessageType d() {
        return (GeneratedMessageLite) a(j.GET_DEFAULT_INSTANCE);
    }

    public final BuilderType e() {
        BuilderType buildertype = (b) a(j.NEW_BUILDER);
        buildertype.a(this);
        return buildertype;
    }

    public void a(k.k kVar, GeneratedMessageLite generatedMessageLite) {
        a(j.VISIT, kVar, generatedMessageLite);
        this.c = kVar.a(this.c, generatedMessageLite.c);
    }

    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public static class i implements k {
        public static final i a = new i();

        public int a(boolean z, int i2, boolean z2, int i3) {
            return z2 ? i3 : i2;
        }

        public ByteString a(boolean z, ByteString byteString, boolean z2, ByteString byteString2) {
            return z2 ? byteString2 : byteString;
        }

        public UnknownFieldSetLite a(UnknownFieldSetLite unknownFieldSetLite, UnknownFieldSetLite unknownFieldSetLite2) {
            if (unknownFieldSetLite2 == UnknownFieldSetLite.d) {
                return unknownFieldSetLite;
            }
            int i2 = unknownFieldSetLite.a + unknownFieldSetLite2.a;
            int[] copyOf = Arrays.copyOf(unknownFieldSetLite.b, i2);
            System.arraycopy(unknownFieldSetLite2.b, 0, copyOf, unknownFieldSetLite.a, unknownFieldSetLite2.a);
            Object[] copyOf2 = Arrays.copyOf(unknownFieldSetLite.c, i2);
            System.arraycopy(unknownFieldSetLite2.c, 0, copyOf2, unknownFieldSetLite.a, unknownFieldSetLite2.a);
            return new UnknownFieldSetLite(i2, copyOf, copyOf2, true);
        }

        public String a(boolean z, String str, boolean z2, String str2) {
            return z2 ? str2 : str;
        }

        public boolean a(boolean z, boolean z2, boolean z3, boolean z4) {
            return z3 ? z4 : z2;
        }

        public FieldSet<k.g> a(FieldSet<k.g> fieldSet, FieldSet<k.g> fieldSet2) {
            if (fieldSet.b) {
                fieldSet = fieldSet.clone();
            }
            for (int i2 = 0; i2 < fieldSet2.a.b(); i2++) {
                fieldSet.a((Map.Entry) fieldSet2.a.a(i2));
            }
            for (Map.Entry<FieldDescriptorType, Object> entry : fieldSet2.a.c()) {
                fieldSet.a((Map.Entry) entry);
            }
            return fieldSet;
        }

        public <T extends o> T a(T t2, T t3) {
            if (t2 == null || t3 == null) {
                return t2 != null ? t2 : t3;
            }
            AbstractMessageLite.a aVar = (AbstractMessageLite.a) t2.e();
            if (aVar != null) {
                b bVar = (b) aVar;
                if (bVar.b.getClass().isInstance(t3)) {
                    bVar.m();
                    bVar.c.a(a, (GeneratedMessageLite) ((AbstractMessageLite) t3));
                    return bVar.k();
                }
                throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
            }
            throw null;
        }

        public <T> Internal.c<T> a(Internal.c<T> cVar, Internal.c<T> cVar2) {
            int size = cVar.size();
            int size2 = cVar2.size();
            if (size > 0 && size2 > 0) {
                if (!((AbstractProtobufList) cVar).b) {
                    cVar = ((ProtobufArrayList) cVar).c(size2 + size);
                }
                cVar.addAll(cVar2);
            }
            return size > 0 ? cVar : cVar2;
        }
    }

    public static <E> Internal.c<E> a(Internal.c cVar) {
        int size = cVar.size();
        return ((ProtobufArrayList) cVar).c(size == 0 ? 10 : size * 2);
    }

    public static <T extends GeneratedMessageLite<T, ?>> T a(GeneratedMessageLite generatedMessageLite, g gVar, i iVar) {
        T t2 = (GeneratedMessageLite) generatedMessageLite.a(j.NEW_MUTABLE_INSTANCE);
        try {
            t2.a(j.MERGE_FROM_STREAM, gVar, iVar);
            t2.f();
            return t2;
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof InvalidProtocolBufferException) {
                throw ((InvalidProtocolBufferException) e2.getCause());
            }
            throw e2;
        }
    }

    public static <T extends GeneratedMessageLite<T, ?>> T a(GeneratedMessageLite generatedMessageLite) {
        if (generatedMessageLite == null || generatedMessageLite.j()) {
            return generatedMessageLite;
        }
        throw new InvalidProtocolBufferException(new UninitializedMessageException().getMessage());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T
     arg types: [j.c.e.GeneratedMessageLite, j.c.e.CodedInputStream, j.c.e.ExtensionRegistryLite]
     candidates:
      j.c.e.GeneratedMessageLite.a(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T */
    public static <T extends GeneratedMessageLite<T, ?>> T a(GeneratedMessageLite generatedMessageLite, f fVar) {
        ExtensionRegistryLite a2 = ExtensionRegistryLite.a();
        try {
            CodedInputStream c2 = fVar.c();
            T a3 = a(generatedMessageLite, (g) c2, (i) a2);
            c2.a(0);
            a((GeneratedMessageLite) a3);
            a((GeneratedMessageLite) a3);
            return a3;
        } catch (InvalidProtocolBufferException e2) {
            throw e2;
        } catch (InvalidProtocolBufferException e3) {
            throw e3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T
     arg types: [j.c.e.GeneratedMessageLite, j.c.e.CodedInputStream, j.c.e.ExtensionRegistryLite]
     candidates:
      j.c.e.GeneratedMessageLite.a(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.g, j.c.e.i):T */
    public static <T extends GeneratedMessageLite<T, ?>> T a(GeneratedMessageLite generatedMessageLite, byte[] bArr) {
        ExtensionRegistryLite a2 = ExtensionRegistryLite.a();
        try {
            int length = bArr.length;
            CodedInputStream codedInputStream = new CodedInputStream(bArr, 0, length, false);
            codedInputStream.b(length);
            T a3 = a(generatedMessageLite, (g) codedInputStream, (i) a2);
            codedInputStream.a(0);
            a((GeneratedMessageLite) a3);
            return a3;
        } catch (InvalidProtocolBufferException e2) {
            throw new IllegalArgumentException(e2);
        } catch (InvalidProtocolBufferException e3) {
            throw e3;
        } catch (InvalidProtocolBufferException e4) {
            throw e4;
        }
    }
}
