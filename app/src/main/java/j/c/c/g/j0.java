package j.c.c.g;

import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;

public class j0 implements Parcelable {
    public static final Parcelable.Creator<j0> CREATOR = new i0();
    public Messenger b;
    public s0 c;

    public static final class a extends ClassLoader {
        public final Class<?> loadClass(String str, boolean z) {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (!FirebaseInstanceId.h()) {
                return j0.class;
            }
            Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            return j0.class;
        }
    }

    public j0(IBinder iBinder) {
        this.b = new Messenger(iBinder);
    }

    public final void a(Message message) {
        Messenger messenger = this.b;
        if (messenger != null) {
            messenger.send(message);
            return;
        }
        s0 s0Var = this.c;
        if (s0Var != null) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
            obtain.writeInt(1);
            message.writeToParcel(obtain, 0);
            try {
                s0Var.a.transact(1, obtain, null, 1);
            } finally {
                obtain.recycle();
            }
        } else {
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a().equals(((j0) obj).a());
        } catch (ClassCastException unused) {
            return false;
        }
    }

    public int hashCode() {
        return a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Messenger messenger = this.b;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.c.asBinder());
        }
    }

    public final IBinder a() {
        Messenger messenger = this.b;
        return messenger != null ? messenger.getBinder() : this.c.asBinder();
    }
}
