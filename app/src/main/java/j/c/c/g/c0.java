package j.c.c.g;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class c0 {
    public final Intent a;
    public final BroadcastReceiver.PendingResult b;
    public boolean c = false;
    public final ScheduledFuture<?> d;

    public c0(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.a = intent;
        this.b = pendingResult;
        this.d = scheduledExecutorService.schedule(new b0(this, intent), 9000, TimeUnit.MILLISECONDS);
    }

    public final synchronized void a() {
        if (!this.c) {
            this.b.finish();
            this.d.cancel(false);
            this.c = true;
        }
    }
}
