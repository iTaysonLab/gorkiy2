package j.c.c.g;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import i.e.ArrayMap;
import i.h.e.ContextCompat;
import j.a.a.a.outline;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public final class v {
    public final SharedPreferences a;
    public final Context b;
    public final v0 c;
    public final Map<String, u0> d = new ArrayMap();

    public v(Context context) {
        v0 v0Var = new v0();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.c = v0Var;
        File file = new File(ContextCompat.b(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    c();
                    FirebaseInstanceId.f().e();
                }
            } catch (IOException e2) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e2.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    public final synchronized void a(String str) {
        this.a.edit().putString("topic_operation_queue", str).apply();
    }

    public final synchronized String b() {
        return this.a.getString("topic_operation_queue", "");
    }

    public final synchronized void c() {
        this.d.clear();
        for (File file : v0.a(this.b).listFiles()) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
        this.a.edit().clear().commit();
    }

    public static String b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(outline.a(str3, outline.a(str2, outline.a(str, 4))));
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    public final synchronized boolean a() {
        return this.a.getAll().isEmpty();
    }

    public final synchronized u0 b(String str) {
        u0 u0Var;
        u0 u0Var2 = this.d.get(str);
        if (u0Var2 != null) {
            return u0Var2;
        }
        try {
            u0Var = this.c.a(this.b, str);
        } catch (d unused) {
            Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
            FirebaseInstanceId.f().e();
            u0Var = this.c.b(this.b, str);
        }
        this.d.put(str, u0Var);
        return u0Var;
    }

    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(outline.a(str2, outline.a(str, 3)));
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    public final synchronized y a(String str, String str2, String str3) {
        return y.a(this.a.getString(b(str, str2, str3), null));
    }

    public final synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = y.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            SharedPreferences.Editor edit = this.a.edit();
            edit.putString(b(str, str2, str3), a2);
            edit.commit();
        }
    }

    public final synchronized void c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        SharedPreferences.Editor edit = this.a.edit();
        for (String next : this.a.getAll().keySet()) {
            if (next.startsWith(concat)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }
}
