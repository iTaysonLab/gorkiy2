package j.c.c.e;

import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final class n {
    public final Class<?> a;
    public final int b;
    public final int c;

    public n(Class<?> cls, int i2, int i3) {
        ResourcesFlusher.b(cls, "Null dependency anInterface.");
        this.a = cls;
        this.b = i2;
        this.c = i3;
    }

    public static n a(Class<?> cls) {
        return new n(cls, 1, 0);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        if (this.a == nVar.a && this.b == nVar.b && this.c == nVar.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        int i2 = this.b;
        boolean z = true;
        sb.append(i2 == 1 ? "required" : i2 == 0 ? "optional" : "set");
        sb.append(", direct=");
        if (this.c != 0) {
            z = false;
        }
        return outline.a(sb, z, "}");
    }

    public boolean a() {
        return this.b == 2;
    }
}
