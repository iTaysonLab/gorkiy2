package j.c.d.b0.a0;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.b0.a0.ReflectiveTypeAdapterFactory;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

public final class TypeAdapterRuntimeTypeWrapper<T> extends TypeAdapter<T> {
    public final Gson a;
    public final TypeAdapter<T> b;
    public final Type c;

    public TypeAdapterRuntimeTypeWrapper(k kVar, TypeAdapter<T> typeAdapter, Type type) {
        this.a = kVar;
        this.b = super;
        this.c = type;
    }

    public T a(JsonReader jsonReader) {
        return this.b.a(jsonReader);
    }

    public void a(JsonWriter jsonWriter, T t2) {
        TypeAdapter<T> typeAdapter = this.b;
        Type type = this.c;
        if (t2 != null && (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class))) {
            type = t2.getClass();
        }
        if (type != this.c) {
            typeAdapter = this.a.a(new TypeToken(type));
            if (typeAdapter instanceof ReflectiveTypeAdapterFactory.a) {
                TypeAdapter<T> typeAdapter2 = this.b;
                if (!(typeAdapter2 instanceof ReflectiveTypeAdapterFactory.a)) {
                    typeAdapter = typeAdapter2;
                }
            }
        }
        super.a(jsonWriter, t2);
    }
}
