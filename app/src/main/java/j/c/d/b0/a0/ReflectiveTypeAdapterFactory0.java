package j.c.d.b0.a0;

import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.b0.a0.ReflectiveTypeAdapterFactory;
import j.c.d.c0.TypeToken;
import java.lang.reflect.Field;

/* compiled from: ReflectiveTypeAdapterFactory */
public class ReflectiveTypeAdapterFactory0 extends ReflectiveTypeAdapterFactory.b {
    public final /* synthetic */ Field d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ boolean f2564e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ TypeAdapter f2565f;
    public final /* synthetic */ Gson g;
    public final /* synthetic */ TypeToken h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ boolean f2566i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReflectiveTypeAdapterFactory0(ReflectiveTypeAdapterFactory reflectiveTypeAdapterFactory, String str, boolean z, boolean z2, Field field, boolean z3, TypeAdapter typeAdapter, Gson gson, TypeToken typeToken, boolean z4) {
        super(str, z, z2);
        this.d = field;
        this.f2564e = z3;
        this.f2565f = typeAdapter;
        this.g = gson;
        this.h = typeToken;
        this.f2566i = z4;
    }
}
