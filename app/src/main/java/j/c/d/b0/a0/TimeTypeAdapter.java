package j.c.d.b0.a0;

import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class TimeTypeAdapter extends TypeAdapter<Time> {
    public static final TypeAdapterFactory b = new a();
    public final DateFormat a = new SimpleDateFormat("hh:mm:ss a");

    public static class a implements TypeAdapterFactory {
        public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
            if (typeToken.a == Time.class) {
                return new TimeTypeAdapter();
            }
            return null;
        }
    }

    public synchronized Time a(JsonReader jsonReader) {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        try {
            return new Time(this.a.parse(jsonReader.nextString()).getTime());
        } catch (ParseException e2) {
            throw new JsonSyntaxException(e2);
        }
    }

    public synchronized void a(JsonWriter jsonWriter, Time time) {
        jsonWriter.value(time == null ? null : this.a.format(time));
    }
}
