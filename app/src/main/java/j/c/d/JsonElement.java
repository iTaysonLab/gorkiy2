package j.c.d;

import com.google.gson.stream.JsonWriter;
import j.c.d.b0.a0.TypeAdapters;
import java.io.IOException;
import java.io.StringWriter;

public abstract class JsonElement {
    public JsonPrimitive c() {
        if (this instanceof JsonPrimitive) {
            return (JsonPrimitive) this;
        }
        throw new IllegalStateException("Not a JSON Primitive: " + this);
    }

    public String d() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);
            jsonWriter.setLenient(true);
            TypeAdapters.X.a(jsonWriter, this);
            return stringWriter.toString();
        } catch (IOException e2) {
            throw new AssertionError(e2);
        }
    }
}
