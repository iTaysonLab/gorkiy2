package j.c.d.c0;

import j.c.d.b0.a;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class TypeToken<T> {
    public final Class<? super T> a;
    public final Type b;
    public final int c;

    public TypeToken() {
        Type genericSuperclass = TypeToken.class.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            Type a2 = a.a(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
            this.b = a2;
            this.a = a.c(a2);
            this.c = this.b.hashCode();
            return;
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public final boolean equals(Object obj) {
        return (obj instanceof TypeToken) && a.a(this.b, ((TypeToken) obj).b);
    }

    public final int hashCode() {
        return this.c;
    }

    public final String toString() {
        return a.d(this.b);
    }

    public TypeToken(Type type) {
        if (type != null) {
            Type a2 = a.a(type);
            this.b = a2;
            this.a = a.c(a2);
            this.c = this.b.hashCode();
            return;
        }
        throw null;
    }
}
