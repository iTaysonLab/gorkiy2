package j.c.b.a;

import j.c.b.a.z.KeyData;
import j.c.b.a.z.KeyStatusType;
import j.c.b.a.z.Keyset;
import j.c.b.a.z.KeysetInfo;
import j.c.b.a.z.OutputPrefixType;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

public class Util {
    static {
        Charset.forName("UTF-8");
    }

    public static KeysetInfo a(Keyset keyset) {
        KeysetInfo.b bVar = (KeysetInfo.b) KeysetInfo.h.e();
        int i2 = keyset.f2490f;
        bVar.m();
        ((KeysetInfo) bVar.c).f2497f = i2;
        for (Keyset.c next : keyset.g) {
            KeysetInfo.c.a aVar = (KeysetInfo.c.a) KeysetInfo.c.f2498i.e();
            String str = next.g().f2478e;
            aVar.m();
            KeysetInfo.c.a((KeysetInfo.c) aVar.c, str);
            KeyStatusType k2 = next.k();
            aVar.m();
            KeysetInfo.c.a((KeysetInfo.c) aVar.c, k2);
            OutputPrefixType h = next.h();
            aVar.m();
            KeysetInfo.c.a((KeysetInfo.c) aVar.c, h);
            int i3 = next.g;
            aVar.m();
            ((KeysetInfo.c) aVar.c).g = i3;
            bVar.m();
            KeysetInfo.a((KeysetInfo) bVar.c, (KeysetInfo.c) aVar.k());
        }
        return (KeysetInfo) bVar.k();
    }

    public static void b(Keyset keyset) {
        if (keyset.g.size() != 0) {
            int i2 = keyset.f2490f;
            boolean z = false;
            boolean z2 = true;
            for (Keyset.c next : keyset.g) {
                if (!(next.f2493e != null)) {
                    throw new GeneralSecurityException(String.format("key %d has no key data", Integer.valueOf(next.g)));
                } else if (next.h() == OutputPrefixType.UNKNOWN_PREFIX) {
                    throw new GeneralSecurityException(String.format("key %d has unknown prefix", Integer.valueOf(next.g)));
                } else if (next.k() != KeyStatusType.UNKNOWN_STATUS) {
                    if (next.k() == KeyStatusType.ENABLED && next.g == i2) {
                        if (!z) {
                            z = true;
                        } else {
                            throw new GeneralSecurityException("keyset contains multiple primary keys");
                        }
                    }
                    KeyData.c a = KeyData.c.a(next.g().g);
                    if (a == null) {
                        a = KeyData.c.UNRECOGNIZED;
                    }
                    if (a != KeyData.c.ASYMMETRIC_PUBLIC) {
                        z2 = false;
                    }
                } else {
                    throw new GeneralSecurityException(String.format("key %d has unknown status", Integer.valueOf(next.g)));
                }
            }
            if (!z && !z2) {
                throw new GeneralSecurityException("keyset doesn't contain a valid primary key");
            }
            return;
        }
        throw new GeneralSecurityException("empty keyset");
    }
}
