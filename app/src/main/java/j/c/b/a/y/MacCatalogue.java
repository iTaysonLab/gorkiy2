package j.c.b.a.y;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.m;
import java.security.GeneralSecurityException;

public class MacCatalogue implements Catalogue<m> {
    public KeyManager<m> a(String str, String str2, int i2) {
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 107855 && lowerCase.equals("mac")) ? (char) 0 : 65535) == 0) {
            if (str.hashCode() == 836622442 && str.equals("type.googleapis.com/google.crypto.tink.HmacKey")) {
                c = 0;
            }
            if (c == 0) {
                HmacKeyManager hmacKeyManager = new HmacKeyManager();
                if (hmacKeyManager.b() >= i2) {
                    return hmacKeyManager;
                }
                throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
            }
            throw new GeneralSecurityException(String.format("No support for primitive 'Mac' with key type '%s'.", str));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
