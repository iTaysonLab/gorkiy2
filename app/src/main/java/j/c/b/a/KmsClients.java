package j.c.b.a;

import j.a.a.a.outline;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public final class KmsClients {
    public static final CopyOnWriteArrayList<k> a = new CopyOnWriteArrayList<>();

    public static KmsClient a(String str) {
        Iterator<k> it = a.iterator();
        while (it.hasNext()) {
            KmsClient next = it.next();
            if (next.a(str)) {
                return next;
            }
        }
        throw new GeneralSecurityException(outline.a("No KMS client does support: ", str));
    }
}
