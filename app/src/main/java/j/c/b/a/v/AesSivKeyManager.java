package j.c.b.a.v;

import com.google.protobuf.InvalidProtocolBufferException;
import j.a.a.a.outline;
import j.c.b.a.DeterministicAead;
import j.c.b.a.KeyManager;
import j.c.b.a.c0.AesSiv;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.d;
import j.c.b.a.z.AesSivKey;
import j.c.b.a.z.AesSivKeyFormat;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

public class AesSivKeyManager implements KeyManager<d> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesSivKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r3v10, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesSivKeyFormat) {
            AesSivKeyFormat aesSivKeyFormat = (AesSivKeyFormat) messageLite;
            if (aesSivKeyFormat.f2431e == 64) {
                AesSivKey.b bVar = (AesSivKey.b) AesSivKey.g.e();
                ByteString a = ByteString.a(Random.a(aesSivKeyFormat.f2431e));
                bVar.m();
                AesSivKey.a(bVar.c, a);
                bVar.m();
                bVar.c.f2428e = 0;
                return bVar.k();
            }
            StringBuilder a2 = outline.a("invalid key size: ");
            a2.append(aesSivKeyFormat.f2431e);
            a2.append(". Valid keys must have 64 bytes.");
            throw new InvalidAlgorithmParameterException(a2.toString());
        }
        throw new GeneralSecurityException("expected AesSivKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [j.c.b.a.z.AesSivKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesSivKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesSivKey) GeneratedMessageLite.a((GeneratedMessageLite) AesSivKey.g, (f) byteString)));
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("expected AesSivKey proto");
        }
    }

    public DeterministicAead a(MessageLite messageLite) {
        if (messageLite instanceof AesSivKey) {
            AesSivKey aesSivKey = (AesSivKey) messageLite;
            Validators.a(aesSivKey.f2428e, 0);
            if (aesSivKey.f2429f.size() == 64) {
                return new AesSiv(aesSivKey.f2429f.d());
            }
            StringBuilder a = outline.a("invalid key size: ");
            a.append(aesSivKey.f2429f.size());
            a.append(". Valid keys must have 64 bytes.");
            throw new InvalidKeyException(a.toString());
        }
        throw new GeneralSecurityException("expected AesSivKey proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.AesSivKeyFormat, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesSivKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesSivKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesSivKeyFormat.f2430f, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesSivKeyFormat proto", e2);
        }
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesSivKey");
        ByteString a = ((AesSivKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesSivKey");
    }
}
