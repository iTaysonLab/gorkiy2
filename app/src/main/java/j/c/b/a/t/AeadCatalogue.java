package j.c.b.a.t;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.a;
import java.security.GeneralSecurityException;

public class AeadCatalogue implements Catalogue<a> {
    public KeyManager<a> a(String str, String str2, int i2) {
        KeyManager<a> keyManager;
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 2989895 && lowerCase.equals("aead")) ? (char) 0 : 65535) == 0) {
            switch (str.hashCode()) {
                case 360753376:
                    if (str.equals("type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1215885937:
                    if (str.equals("type.googleapis.com/google.crypto.tink.AesCtrHmacAeadKey")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1469984853:
                    if (str.equals("type.googleapis.com/google.crypto.tink.KmsAeadKey")) {
                        c = 4;
                        break;
                    }
                    break;
                case 1797113348:
                    if (str.equals("type.googleapis.com/google.crypto.tink.AesEaxKey")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1855890991:
                    if (str.equals("type.googleapis.com/google.crypto.tink.AesGcmKey")) {
                        c = 2;
                        break;
                    }
                    break;
                case 2079211877:
                    if (str.equals("type.googleapis.com/google.crypto.tink.KmsEnvelopeAeadKey")) {
                        c = 5;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                keyManager = new AesCtrHmacAeadKeyManager();
            } else if (c == 1) {
                keyManager = new AesEaxKeyManager();
            } else if (c == 2) {
                keyManager = new AesGcmKeyManager();
            } else if (c == 3) {
                keyManager = new ChaCha20Poly1305KeyManager();
            } else if (c == 4) {
                keyManager = new KmsAeadKeyManager();
            } else if (c == 5) {
                keyManager = new KmsEnvelopeAeadKeyManager();
            } else {
                throw new GeneralSecurityException(String.format("No support for primitive 'Aead' with key type '%s'.", str));
            }
            if (keyManager.b() >= i2) {
                return keyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
