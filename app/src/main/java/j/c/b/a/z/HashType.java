package j.c.b.a.z;

import j.c.e.Internal;
import j.c.e.l;

public enum HashType implements l.a {
    UNKNOWN_HASH(0),
    SHA1(1),
    SHA256(3),
    SHA512(4),
    UNRECOGNIZED(-1);
    
    public static final int SHA1_VALUE = 1;
    public static final int SHA256_VALUE = 3;
    public static final int SHA512_VALUE = 4;
    public static final int UNKNOWN_HASH_VALUE = 0;
    public static final Internal.b<p1> internalValueMap = new a();
    public final int value;

    public class a implements Internal.b<p1> {
    }

    /* access modifiers changed from: public */
    HashType(int i2) {
        this.value = i2;
    }

    public static HashType a(int i2) {
        if (i2 == 0) {
            return UNKNOWN_HASH;
        }
        if (i2 == 1) {
            return SHA1;
        }
        if (i2 == 3) {
            return SHA256;
        }
        if (i2 != 4) {
            return null;
        }
        return SHA512;
    }
}
