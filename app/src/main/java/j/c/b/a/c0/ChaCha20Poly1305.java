package j.c.b.a.c0;

import j.c.a.a.c.n.c;
import j.c.b.a.Aead;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.GeneralSecurityException;
import javax.crypto.AEADBadTagException;

public final class ChaCha20Poly1305 implements Aead {
    public final Snuffle a;
    public final Snuffle b;

    public ChaCha20Poly1305(byte[] bArr) {
        byte[] bArr2 = (byte[]) bArr.clone();
        this.a = new ChaCha20(bArr, 1);
        this.b = new ChaCha20(bArr, 0);
    }

    public byte[] a(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        ChaCha20 chaCha20 = (ChaCha20) this.a;
        if (chaCha20 == null) {
            throw null;
        } else if (length <= 2147483619) {
            int length2 = bArr.length;
            if (chaCha20 != null) {
                ByteBuffer allocate = ByteBuffer.allocate(length2 + 12 + 16);
                a(allocate, bArr, bArr2);
                return allocate.array();
            }
            throw null;
        } else {
            throw new GeneralSecurityException("plaintext too long");
        }
    }

    public byte[] b(byte[] bArr, byte[] bArr2) {
        return a(ByteBuffer.wrap(bArr), bArr2);
    }

    public final void a(ByteBuffer byteBuffer, byte[] bArr, byte[] bArr2) {
        int remaining = byteBuffer.remaining();
        int length = bArr.length;
        if (((ChaCha20) this.a) == null) {
            throw null;
        } else if (remaining >= length + 12 + 16) {
            int position = byteBuffer.position();
            this.a.a(byteBuffer, bArr);
            byteBuffer.position(position);
            if (((ChaCha20) this.a) != null) {
                byte[] bArr3 = new byte[12];
                byteBuffer.get(bArr3);
                byteBuffer.limit(byteBuffer.limit() - 16);
                if (bArr2 == null) {
                    bArr2 = new byte[0];
                }
                byte[] a2 = c.a(a(bArr3), a(bArr2, byteBuffer));
                byteBuffer.limit(byteBuffer.limit() + 16);
                byteBuffer.put(a2);
                return;
            }
            throw null;
        } else {
            throw new IllegalArgumentException("Given ByteBuffer output is too small");
        }
    }

    public final byte[] a(ByteBuffer byteBuffer, byte[] bArr) {
        int remaining = byteBuffer.remaining();
        if (((ChaCha20) this.a) == null) {
            throw null;
        } else if (remaining >= 28) {
            int position = byteBuffer.position();
            byte[] bArr2 = new byte[16];
            byteBuffer.position(byteBuffer.limit() - 16);
            byteBuffer.get(bArr2);
            byteBuffer.position(position);
            byteBuffer.limit(byteBuffer.limit() - 16);
            if (((ChaCha20) this.a) != null) {
                byte[] bArr3 = new byte[12];
                byteBuffer.get(bArr3);
                if (bArr == null) {
                    bArr = new byte[0];
                }
                try {
                    if (c.b(c.a(a(bArr3), a(bArr, byteBuffer)), bArr2)) {
                        byteBuffer.position(position);
                        return this.a.a(byteBuffer);
                    }
                    throw new GeneralSecurityException("invalid MAC");
                } catch (GeneralSecurityException e2) {
                    throw new AEADBadTagException(e2.toString());
                }
            } else {
                throw null;
            }
        } else {
            throw new GeneralSecurityException("ciphertext too short");
        }
    }

    public static byte[] a(byte[] bArr, ByteBuffer byteBuffer) {
        int length = bArr.length % 16 == 0 ? bArr.length : (bArr.length + 16) - (bArr.length % 16);
        int remaining = byteBuffer.remaining();
        int i2 = remaining % 16;
        int i3 = (i2 == 0 ? remaining : (remaining + 16) - i2) + length;
        ByteBuffer order = ByteBuffer.allocate(i3 + 16).order(ByteOrder.LITTLE_ENDIAN);
        order.put(bArr);
        order.position(length);
        order.put(byteBuffer);
        order.position(i3);
        order.putLong((long) bArr.length);
        order.putLong((long) remaining);
        return order.array();
    }

    public final byte[] a(byte[] bArr) {
        byte[] bArr2 = new byte[32];
        this.b.a(bArr, 0).get(bArr2);
        return bArr2;
    }
}
