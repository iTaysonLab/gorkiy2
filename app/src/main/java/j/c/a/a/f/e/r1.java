package j.c.a.a.f.e;

import android.util.Log;
import j.a.a.a.outline;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class r1 extends p1<Long> {
    public r1(v1 v1Var, String str, Long l2) {
        super(v1Var, str, l2, null);
    }

    public final /* synthetic */ Object a(Object obj) {
        if (obj instanceof Long) {
            return (Long) obj;
        }
        if (obj instanceof String) {
            try {
                return Long.valueOf(Long.parseLong((String) obj));
            } catch (NumberFormatException unused) {
            }
        }
        String a = super.a();
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(a, 25));
        sb.append("Invalid long value for ");
        sb.append(a);
        sb.append(": ");
        sb.append(valueOf);
        Log.e("PhenotypeFlag", sb.toString());
        return null;
    }
}
