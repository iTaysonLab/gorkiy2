package j.c.a.a.g.a;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.nb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i4 {
    public final j4 a;

    public i4(j4 j4Var) {
        ResourcesFlusher.b(j4Var);
        this.a = j4Var;
    }

    public static boolean a(Context context) {
        ActivityInfo receiverInfo;
        ResourcesFlusher.b(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    public final void a(Context context, Intent intent) {
        r4 a2 = r4.a(context, (nb) null);
        n3 a3 = a2.a();
        if (intent == null) {
            a3.f2047i.a("Receiver called with null intent");
            return;
        }
        String action = intent.getAction();
        a3.f2052n.a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            a3.f2052n.a("Starting wakeful intent.");
            this.a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                l4 i2 = a2.i();
                h4 h4Var = new h4(a2, a3);
                i2.o();
                ResourcesFlusher.b(h4Var);
                i2.a((p4<?>) new p4(i2, h4Var, "Task exception on worker thread"));
            } catch (Exception e2) {
                a3.f2047i.a("Install Referrer Reporter encountered a problem", e2);
            }
            BroadcastReceiver.PendingResult a4 = this.a.a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                a3.f2052n.a("Install referrer extras are null");
                if (a4 != null) {
                    a4.finish();
                    return;
                }
                return;
            }
            a3.f2050l.a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                stringExtra = stringExtra.length() != 0 ? "?".concat(stringExtra) : new String("?");
            }
            Bundle a5 = a2.n().a(Uri.parse(stringExtra));
            if (a5 == null) {
                a3.f2052n.a("No campaign defined in install referrer broadcast");
                if (a4 != null) {
                    a4.finish();
                    return;
                }
                return;
            }
            long longExtra = intent.getLongExtra("referrer_timestamp_seconds", 0) * 1000;
            if (longExtra == 0) {
                a3.f2047i.a("Install referrer is missing timestamp");
            }
            l4 i3 = a2.i();
            k4 k4Var = new k4(a2, longExtra, a5, context, a3, a4);
            i3.o();
            ResourcesFlusher.b(k4Var);
            i3.a((p4<?>) new p4(i3, k4Var, "Task exception on worker thread"));
        }
    }
}
