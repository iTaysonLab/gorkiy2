package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class s9 implements p9 {
    public static final p1<Boolean> a;
    public static final p1<Double> b;
    public static final p1<Long> c;
    public static final p1<Long> d;

    /* renamed from: e  reason: collision with root package name */
    public static final p1<String> f1906e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1
     arg types: [j.c.a.a.f.e.v1, java.lang.String, int]
     candidates:
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, long):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, java.lang.String):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1 */
    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.test.boolean_flag", false);
        b = p1.a(v1Var, "measurement.test.double_flag");
        c = p1.a(v1Var, "measurement.test.int_flag", -2);
        d = p1.a(v1Var, "measurement.test.long_flag", -1);
        f1906e = p1.a(v1Var, "measurement.test.string_flag", "---");
    }

    public final boolean a() {
        return a.b().booleanValue();
    }

    public final double b() {
        return b.b().doubleValue();
    }

    public final long c() {
        return c.b().longValue();
    }

    public final long d() {
        return d.b().longValue();
    }

    public final String e() {
        return f1906e.b();
    }
}
