package j.c.a.a.c.l;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.h;
import j.c.a.a.c.m.a;
import j.c.a.a.f.c.d;
import java.util.HashMap;
import javax.annotation.concurrent.GuardedBy;

public final class x extends h implements Handler.Callback {
    @GuardedBy("mConnectionStatus")
    public final HashMap<h.a, y> c = new HashMap<>();
    public final Context d;

    /* renamed from: e  reason: collision with root package name */
    public final Handler f1824e;

    /* renamed from: f  reason: collision with root package name */
    public final a f1825f;
    public final long g;
    public final long h;

    public x(Context context) {
        this.d = context.getApplicationContext();
        this.f1824e = new d(context.getMainLooper(), this);
        this.f1825f = a.a();
        this.g = 5000;
        this.h = 300000;
    }

    public final boolean a(h.a aVar, ServiceConnection serviceConnection, String str) {
        boolean z;
        ResourcesFlusher.b(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.c) {
            y yVar = this.c.get(aVar);
            if (yVar == null) {
                yVar = new y(this, aVar);
                a aVar2 = yVar.g.f1825f;
                yVar.f1826e.a();
                yVar.a.add(serviceConnection);
                yVar.a(str);
                this.c.put(aVar, yVar);
            } else {
                this.f1824e.removeMessages(0, aVar);
                if (!yVar.a.contains(serviceConnection)) {
                    a aVar3 = yVar.g.f1825f;
                    yVar.f1826e.a();
                    yVar.a.add(serviceConnection);
                    int i2 = yVar.b;
                    if (i2 == 1) {
                        serviceConnection.onServiceConnected(yVar.f1827f, yVar.d);
                    } else if (i2 == 2) {
                        yVar.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            z = yVar.c;
        }
        return z;
    }

    public final void b(h.a aVar, ServiceConnection serviceConnection, String str) {
        ResourcesFlusher.b(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.c) {
            y yVar = this.c.get(aVar);
            if (yVar == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(valueOf.length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (yVar.a.contains(serviceConnection)) {
                a aVar2 = yVar.g.f1825f;
                yVar.a.remove(serviceConnection);
                if (yVar.a.isEmpty()) {
                    this.f1824e.sendMessageDelayed(this.f1824e.obtainMessage(0, aVar), this.g);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(valueOf2.length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 0) {
            synchronized (this.c) {
                h.a aVar = (h.a) message.obj;
                y yVar = this.c.get(aVar);
                if (yVar != null && yVar.a.isEmpty()) {
                    if (yVar.c) {
                        yVar.g.f1824e.removeMessages(1, yVar.f1826e);
                        x xVar = yVar.g;
                        a aVar2 = xVar.f1825f;
                        Context context = xVar.d;
                        if (aVar2 != null) {
                            context.unbindService(yVar);
                            yVar.c = false;
                            yVar.b = 2;
                        } else {
                            throw null;
                        }
                    }
                    this.c.remove(aVar);
                }
            }
            return true;
        } else if (i2 != 1) {
            return false;
        } else {
            synchronized (this.c) {
                h.a aVar3 = (h.a) message.obj;
                y yVar2 = this.c.get(aVar3);
                if (yVar2 != null && yVar2.b == 3) {
                    String valueOf = String.valueOf(aVar3);
                    StringBuilder sb = new StringBuilder(valueOf.length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName componentName = yVar2.f1827f;
                    if (componentName == null) {
                        componentName = aVar3.c;
                    }
                    if (componentName == null) {
                        componentName = new ComponentName(aVar3.b, "unknown");
                    }
                    yVar2.onServiceDisconnected(componentName);
                }
            }
            return true;
        }
    }
}
