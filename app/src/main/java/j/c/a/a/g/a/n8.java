package j.c.a.a.g.a;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobScheduler;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class n8 extends o8 {
    public final AlarmManager d = ((AlarmManager) this.a.a.getSystemService("alarm"));

    /* renamed from: e  reason: collision with root package name */
    public final b f2055e;

    /* renamed from: f  reason: collision with root package name */
    public Integer f2056f;

    public n8(q8 q8Var) {
        super(q8Var);
        this.f2055e = new m8(this, q8Var.f2073i, q8Var);
    }

    public final boolean q() {
        this.d.cancel(x());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        v();
        return false;
    }

    public final void u() {
        o();
        this.d.cancel(x());
        this.f2055e.b();
        if (Build.VERSION.SDK_INT >= 24) {
            v();
        }
    }

    @TargetApi(24)
    public final void v() {
        int w = w();
        a().f2052n.a("Cancelling job. JobID", Integer.valueOf(w));
        ((JobScheduler) this.a.a.getSystemService("jobscheduler")).cancel(w);
    }

    public final int w() {
        if (this.f2056f == null) {
            String valueOf = String.valueOf(this.a.a.getPackageName());
            this.f2056f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f2056f.intValue();
    }

    public final PendingIntent x() {
        Context context = this.a.a;
        return PendingIntent.getBroadcast(context, 0, new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }
}
