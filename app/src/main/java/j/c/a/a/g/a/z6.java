package j.c.a.a.g.a;

import android.content.ComponentName;
import android.content.Context;
import android.os.SystemClock;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.m.a;
import j.c.a.a.c.n.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class z6 extends e5 {
    public final r7 c;
    public f3 d;

    /* renamed from: e  reason: collision with root package name */
    public volatile Boolean f2140e;

    /* renamed from: f  reason: collision with root package name */
    public final b f2141f;
    public final k8 g;
    public final List<Runnable> h = new ArrayList();

    /* renamed from: i  reason: collision with root package name */
    public final b f2142i;

    public z6(r4 r4Var) {
        super(r4Var);
        this.g = new k8(r4Var.f2092n);
        this.c = new r7(this);
        this.f2141f = new d7(this, r4Var);
        this.f2142i = new j7(this, r4Var);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0103  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A() {
        /*
            r7 = this;
            r7.d()
            r7.w()
            boolean r0 = r7.z()
            if (r0 == 0) goto L_0x000d
            return
        L_0x000d:
            java.lang.Boolean r0 = r7.f2140e
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0132
            r7.d()
            r7.w()
            j.c.a.a.g.a.w3 r0 = r7.l()
            java.lang.Boolean r0 = r0.t()
            if (r0 == 0) goto L_0x002c
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x002c
            r0 = 1
            goto L_0x0129
        L_0x002c:
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.h9 r0 = r0.f2086f
            j.c.a.a.g.a.g3 r0 = r7.q()
            r0.w()
            int r0 = r0.f1995k
            if (r0 != r2) goto L_0x003d
            goto L_0x00e7
        L_0x003d:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            java.lang.String r3 = "Checking service availability"
            r0.a(r3)
            j.c.a.a.g.a.y8 r0 = r7.k()
            if (r0 == 0) goto L_0x0130
            j.c.a.a.c.f r3 = j.c.a.a.c.f.b
            j.c.a.a.g.a.r4 r0 = r0.a
            android.content.Context r0 = r0.a
            r4 = 12451000(0xbdfcb8, float:1.7447567E-38)
            int r0 = r3.a(r0, r4)
            if (r0 == 0) goto L_0x00dc
            if (r0 == r2) goto L_0x00cf
            r3 = 2
            if (r0 == r3) goto L_0x00a1
            r3 = 3
            if (r0 == r3) goto L_0x0095
            r3 = 9
            if (r0 == r3) goto L_0x0089
            r3 = 18
            if (r0 == r3) goto L_0x007d
            j.c.a.a.g.a.n3 r3 = r7.a()
            j.c.a.a.g.a.p3 r3 = r3.f2047i
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "Unexpected service status"
            r3.a(r4, r0)
            goto L_0x00ca
        L_0x007d:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r3 = "Service updating"
            r0.a(r3)
            goto L_0x00e7
        L_0x0089:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r3 = "Service invalid"
            r0.a(r3)
            goto L_0x00ca
        L_0x0095:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r3 = "Service disabled"
            r0.a(r3)
            goto L_0x00ca
        L_0x00a1:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2051m
            java.lang.String r3 = "Service container out of date"
            r0.a(r3)
            j.c.a.a.g.a.y8 r0 = r7.k()
            int r0 = r0.v()
            r3 = 17443(0x4423, float:2.4443E-41)
            if (r0 >= r3) goto L_0x00b9
            goto L_0x00da
        L_0x00b9:
            j.c.a.a.g.a.w3 r0 = r7.l()
            java.lang.Boolean r0 = r0.t()
            if (r0 == 0) goto L_0x00cc
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00ca
            goto L_0x00cc
        L_0x00ca:
            r0 = 0
            goto L_0x00cd
        L_0x00cc:
            r0 = 1
        L_0x00cd:
            r3 = 0
            goto L_0x00e9
        L_0x00cf:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            java.lang.String r3 = "Service missing"
            r0.a(r3)
        L_0x00da:
            r0 = 0
            goto L_0x00e8
        L_0x00dc:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2052n
            java.lang.String r3 = "Service available"
            r0.a(r3)
        L_0x00e7:
            r0 = 1
        L_0x00e8:
            r3 = 1
        L_0x00e9:
            if (r0 != 0) goto L_0x0101
            j.c.a.a.g.a.r4 r4 = r7.a
            j.c.a.a.g.a.i9 r4 = r4.g
            boolean r4 = r4.t()
            if (r4 == 0) goto L_0x0101
            j.c.a.a.g.a.n3 r3 = r7.a()
            j.c.a.a.g.a.p3 r3 = r3.f2046f
            java.lang.String r4 = "No way to upload. Consider using the full version of Analytics"
            r3.a(r4)
            r3 = 0
        L_0x0101:
            if (r3 == 0) goto L_0x0129
            j.c.a.a.g.a.w3 r3 = r7.l()
            r3.d()
            j.c.a.a.g.a.n3 r4 = r3.a()
            j.c.a.a.g.a.p3 r4 = r4.f2052n
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            java.lang.String r6 = "Setting useService"
            r4.a(r6, r5)
            android.content.SharedPreferences r3 = r3.v()
            android.content.SharedPreferences$Editor r3 = r3.edit()
            java.lang.String r4 = "use_service"
            r3.putBoolean(r4, r0)
            r3.apply()
        L_0x0129:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.f2140e = r0
            goto L_0x0132
        L_0x0130:
            r0 = 0
            throw r0
        L_0x0132:
            java.lang.Boolean r0 = r7.f2140e
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0140
            j.c.a.a.g.a.r7 r0 = r7.c
            r0.a()
            return
        L_0x0140:
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            boolean r0 = r0.t()
            if (r0 != 0) goto L_0x0198
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.h9 r3 = r0.f2086f
            android.content.Context r0 = r0.a
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            j.c.a.a.g.a.r4 r4 = r7.a
            android.content.Context r4 = r4.a
            java.lang.String r5 = "com.google.android.gms.measurement.AppMeasurementService"
            android.content.Intent r3 = r3.setClassName(r4, r5)
            r4 = 65536(0x10000, float:9.18355E-41)
            java.util.List r0 = r0.queryIntentServices(r3, r4)
            if (r0 == 0) goto L_0x0172
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0172
            r1 = 1
        L_0x0172:
            if (r1 == 0) goto L_0x018d
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.google.android.gms.measurement.START"
            r0.<init>(r1)
            android.content.ComponentName r1 = new android.content.ComponentName
            j.c.a.a.g.a.r4 r2 = r7.a
            android.content.Context r2 = r2.a
            r1.<init>(r2, r5)
            r0.setComponent(r1)
            j.c.a.a.g.a.r7 r1 = r7.c
            r1.a(r0)
            return
        L_0x018d:
            j.c.a.a.g.a.n3 r0 = r7.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r1 = "Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest"
            r0.a(r1)
        L_0x0198:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.z6.A():void");
    }

    public final void B() {
        d();
        w();
        r7 r7Var = this.c;
        if (r7Var.b != null && (r7Var.b.c() || r7Var.b.a())) {
            r7Var.b.e();
        }
        r7Var.b = null;
        try {
            a a = a.a();
            Context context = this.a.a;
            r7 r7Var2 = this.c;
            if (a != null) {
                context.unbindService(r7Var2);
                this.d = null;
                return;
            }
            throw null;
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
    }

    public final boolean C() {
        h9 h9Var = this.a.f2086f;
        return true;
    }

    public final void D() {
        d();
        k8 k8Var = this.g;
        if (((b) k8Var.a) != null) {
            k8Var.b = SystemClock.elapsedRealtime();
            this.f2141f.a(k.L.a(null).longValue());
            return;
        }
        throw null;
    }

    public final void E() {
        d();
        a().f2052n.a("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable run : this.h) {
            try {
                run.run();
            } catch (Exception e2) {
                a().f2046f.a("Task exception while flushing queue", e2);
            }
        }
        this.h.clear();
        this.f2142i.b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01bb  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j.c.a.a.g.a.d9 a(boolean r35) {
        /*
            r34 = this;
            r0 = r34
            j.c.a.a.g.a.r4 r1 = r0.a
            j.c.a.a.g.a.h9 r1 = r1.f2086f
            j.c.a.a.g.a.g3 r1 = r34.q()
            if (r35 == 0) goto L_0x0016
            j.c.a.a.g.a.n3 r3 = r34.a()
            java.lang.String r3 = r3.z()
            r15 = r3
            goto L_0x0017
        L_0x0016:
            r15 = 0
        L_0x0017:
            r1.d()
            r1.b()
            j.c.a.a.g.a.d9 r3 = new j.c.a.a.g.a.d9
            r1.w()
            java.lang.String r5 = r1.c
            r1.w()
            java.lang.String r6 = r1.f1996l
            r1.w()
            java.lang.String r7 = r1.d
            r1.w()
            int r4 = r1.f1991e
            long r8 = (long) r4
            r1.w()
            java.lang.String r10 = r1.f1992f
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            r4.o()
            r1.w()
            r1.d()
            long r11 = r1.g
            r13 = 0
            int r4 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r4 != 0) goto L_0x0062
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.y8 r4 = r4.n()
            j.c.a.a.g.a.r4 r11 = r1.a
            android.content.Context r11 = r11.a
            java.lang.String r12 = r11.getPackageName()
            long r11 = r4.a(r11, r12)
            r1.g = r11
        L_0x0062:
            long r11 = r1.g
            j.c.a.a.g.a.r4 r4 = r1.a
            boolean r16 = r4.c()
            j.c.a.a.g.a.w3 r4 = r1.l()
            boolean r4 = r4.x
            r13 = 1
            r19 = r4 ^ 1
            r1.d()
            r1.b()
            j.c.a.a.g.a.r4 r4 = r1.a
            boolean r4 = r4.c()
            if (r4 != 0) goto L_0x0082
            goto L_0x009e
        L_0x0082:
            j.c.a.a.f.e.qa r4 = j.c.a.a.f.e.qa.c
            r4.a()
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.K0
            boolean r2 = r4.a(r2)
            if (r2 == 0) goto L_0x00a0
            j.c.a.a.g.a.n3 r2 = r1.a()
            j.c.a.a.g.a.p3 r2 = r2.f2052n
            java.lang.String r4 = "Disabled IID for tests."
            r2.a(r4)
        L_0x009e:
            r0 = 0
            goto L_0x00f9
        L_0x00a0:
            j.c.a.a.g.a.r4 r2 = r1.a     // Catch:{ ClassNotFoundException -> 0x009e }
            android.content.Context r2 = r2.a     // Catch:{ ClassNotFoundException -> 0x009e }
            java.lang.ClassLoader r2 = r2.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x009e }
            java.lang.String r4 = "com.google.firebase.analytics.FirebaseAnalytics"
            java.lang.Class r2 = r2.loadClass(r4)     // Catch:{ ClassNotFoundException -> 0x009e }
            if (r2 != 0) goto L_0x00b1
            goto L_0x009e
        L_0x00b1:
            java.lang.String r4 = "getInstance"
            java.lang.Class[] r14 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x00ed }
            java.lang.Class<android.content.Context> r21 = android.content.Context.class
            r13 = 0
            r14[r13] = r21     // Catch:{ Exception -> 0x00ed }
            java.lang.reflect.Method r4 = r2.getDeclaredMethod(r4, r14)     // Catch:{ Exception -> 0x00ed }
            r14 = 1
            java.lang.Object[] r13 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x00ed }
            j.c.a.a.g.a.r4 r14 = r1.a     // Catch:{ Exception -> 0x00ed }
            android.content.Context r14 = r14.a     // Catch:{ Exception -> 0x00ed }
            r0 = 0
            r13[r0] = r14     // Catch:{ Exception -> 0x00ed }
            r14 = 0
            java.lang.Object r4 = r4.invoke(r14, r13)     // Catch:{ Exception -> 0x00ed }
            if (r4 != 0) goto L_0x00d0
            goto L_0x009e
        L_0x00d0:
            java.lang.String r13 = "getFirebaseInstanceId"
            java.lang.Class[] r14 = new java.lang.Class[r0]     // Catch:{ Exception -> 0x00e1 }
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r13, r14)     // Catch:{ Exception -> 0x00e1 }
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x00e1 }
            java.lang.Object r0 = r2.invoke(r4, r13)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00e1 }
            goto L_0x00f9
        L_0x00e1:
            j.c.a.a.g.a.n3 r0 = r1.a()
            j.c.a.a.g.a.p3 r0 = r0.f2049k
            java.lang.String r2 = "Failed to retrieve Firebase Instance Id"
            r0.a(r2)
            goto L_0x009e
        L_0x00ed:
            j.c.a.a.g.a.n3 r0 = r1.a()
            j.c.a.a.g.a.p3 r0 = r0.f2048j
            java.lang.String r2 = "Failed to obtain Firebase Analytics instance"
            r0.a(r2)
            goto L_0x009e
        L_0x00f9:
            r1.w()
            long r13 = r1.h
            j.c.a.a.g.a.r4 r2 = r1.a
            j.c.a.a.g.a.w3 r4 = r2.l()
            j.c.a.a.g.a.b4 r4 = r4.f2114j
            long r23 = r4.a()
            java.lang.Long r4 = java.lang.Long.valueOf(r23)
            long r23 = r4.longValue()
            r17 = 0
            int r21 = (r23 > r17 ? 1 : (r23 == r17 ? 0 : -1))
            if (r21 != 0) goto L_0x0121
            r17 = r11
            long r11 = r2.F
            r25 = r11
            r23 = r13
            goto L_0x0131
        L_0x0121:
            r17 = r11
            long r11 = r2.F
            r23 = r13
            long r13 = r4.longValue()
            long r11 = java.lang.Math.min(r11, r13)
            r25 = r11
        L_0x0131:
            r1.w()
            int r2 = r1.f1995k
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            java.lang.Boolean r4 = r4.s()
            boolean r27 = r4.booleanValue()
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            r4.b()
            java.lang.String r11 = "google_analytics_ssaid_collection_enabled"
            java.lang.Boolean r4 = r4.b(r11)
            if (r4 == 0) goto L_0x015a
            boolean r4 = r4.booleanValue()
            if (r4 == 0) goto L_0x0158
            goto L_0x015a
        L_0x0158:
            r13 = 0
            goto L_0x015b
        L_0x015a:
            r13 = 1
        L_0x015b:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r13)
            boolean r28 = r4.booleanValue()
            j.c.a.a.g.a.w3 r4 = r1.l()
            r4.d()
            android.content.SharedPreferences r4 = r4.v()
            java.lang.String r11 = "deferred_analytics_collection"
            r12 = 0
            boolean r29 = r4.getBoolean(r11, r12)
            r1.w()
            java.lang.String r13 = r1.f1997m
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            r1.w()
            java.lang.String r11 = r1.c
            j.c.a.a.g.a.b3<java.lang.Boolean> r12 = j.c.a.a.g.a.k.i0
            boolean r4 = r4.d(r11, r12)
            if (r4 == 0) goto L_0x01a4
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            java.lang.String r11 = "google_analytics_default_allow_ad_personalization_signals"
            java.lang.Boolean r4 = r4.b(r11)
            if (r4 == 0) goto L_0x01a4
            boolean r4 = r4.booleanValue()
            r11 = 1
            r4 = r4 ^ r11
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)
            r30 = r4
            goto L_0x01a6
        L_0x01a4:
            r30 = 0
        L_0x01a6:
            long r11 = r1.f1993i
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            r1.w()
            java.lang.String r14 = r1.c
            r21 = r11
            j.c.a.a.g.a.b3<java.lang.Boolean> r11 = j.c.a.a.g.a.k.v0
            boolean r4 = r4.d(r14, r11)
            if (r4 == 0) goto L_0x01c0
            java.util.List<java.lang.String> r1 = r1.f1994j
            r31 = r1
            goto L_0x01c2
        L_0x01c0:
            r31 = 0
        L_0x01c2:
            r11 = 18079(0x469f, double:8.932E-320)
            r32 = r21
            r4 = r3
            r1 = r13
            r20 = r23
            r13 = r17
            r17 = r19
            r18 = r0
            r19 = r20
            r21 = r25
            r23 = r2
            r24 = r27
            r25 = r28
            r26 = r29
            r27 = r1
            r28 = r30
            r29 = r32
            r4.<init>(r5, r6, r7, r8, r10, r11, r13, r15, r16, r17, r18, r19, r21, r23, r24, r25, r26, r27, r28, r29, r31)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.z6.a(boolean):j.c.a.a.g.a.d9");
    }

    public final boolean y() {
        return false;
    }

    public final boolean z() {
        d();
        w();
        return this.d != null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:102|103|104|105) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:63|64|65|66) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:86|87|88|89) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0193, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        r8.a().f2046f.a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r5.recycle();
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01a8, code lost:
        r5.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01ab, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0205, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x020c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x020e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0210, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0211, code lost:
        r17 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0214, code lost:
        r17 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0218, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0219, code lost:
        r17 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x021c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x021d, code lost:
        r25 = r10;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0222, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0225, code lost:
        r17 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0229, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x022c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x022d, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x022e, code lost:
        r25 = r10;
        r10 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0233, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0236, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0238, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x023a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0251, code lost:
        if (r10.inTransaction() != false) goto L_0x0253;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0253, code lost:
        r10.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0264, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0269, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x027a, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x027f, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x02a8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x02a9, code lost:
        r25 = r4;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x02b6, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x02bb, code lost:
        r25.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        r17 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c1, code lost:
        r17 = r6;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c6, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c7, code lost:
        r17 = r6;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0120, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r8.a().f2046f.a("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        r5.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0134, code lost:
        r5.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0137, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0157, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r8.a().f2046f.a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r5.recycle();
        r0 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:102:0x0195 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x0122 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:86:0x015f */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0205 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:50:0x00ed] */
    /* JADX WARNING: Removed duplicated region for block: B:142:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:17:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x022c A[ExcHandler: all (th java.lang.Throwable), Splitter:B:22:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0233 A[ExcHandler: SQLiteException (e android.database.sqlite.SQLiteException), Splitter:B:17:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x023a A[ExcHandler: SQLiteFullException (e android.database.sqlite.SQLiteFullException), Splitter:B:17:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x024d A[SYNTHETIC, Splitter:B:157:0x024d] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0264  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0297  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x02b6  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x02bb  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x02d0  */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x02d9  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x029f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x029f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x029f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(j.c.a.a.g.a.f3 r30, j.c.a.a.c.l.p.a r31, j.c.a.a.g.a.d9 r32) {
        /*
            r29 = this;
            r1 = r30
            r2 = r31
            r3 = r32
            r29.d()
            r29.b()
            r29.w()
            r29.C()
            r4 = 100
            r0 = 100
            r6 = 0
        L_0x0017:
            r7 = 1001(0x3e9, float:1.403E-42)
            if (r6 >= r7) goto L_0x034b
            if (r0 != r4) goto L_0x034b
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            j.c.a.a.g.a.j3 r8 = r29.t()
            java.lang.String r9 = "Error reading entries from local database"
            r8.d()
            r8.b()
            boolean r0 = r8.d
            if (r0 == 0) goto L_0x0038
        L_0x0032:
            r17 = r6
            r10 = 0
        L_0x0035:
            r15 = 0
            goto L_0x02ce
        L_0x0038:
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            j.c.a.a.g.a.r4 r0 = r8.a
            android.content.Context r0 = r0.a
            java.lang.String r12 = "google_app_measurement_local.db"
            java.io.File r0 = r0.getDatabasePath(r12)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0051
            r17 = r6
            r10 = r11
            goto L_0x0035
        L_0x0051:
            r12 = 5
            r13 = 0
            r14 = 5
        L_0x0054:
            if (r13 >= r12) goto L_0x02bf
            r15 = 1
            android.database.sqlite.SQLiteDatabase r10 = r8.B()     // Catch:{ SQLiteFullException -> 0x0283, SQLiteDatabaseLockedException -> 0x026d, SQLiteException -> 0x0245, all -> 0x023f }
            if (r10 != 0) goto L_0x0067
            r8.d = r15     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            if (r10 == 0) goto L_0x0032
            r10.close()
            goto L_0x0032
        L_0x0065:
            r0 = move-exception
            goto L_0x00cc
        L_0x0067:
            r10.beginTransaction()     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x022c }
            j.c.a.a.g.a.r4 r0 = r8.a     // Catch:{ SQLiteFullException -> 0x0229, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0222, all -> 0x022c }
            j.c.a.a.g.a.i9 r0 = r0.g     // Catch:{ SQLiteFullException -> 0x0229, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0222, all -> 0x022c }
            j.c.a.a.g.a.b3<java.lang.Boolean> r12 = j.c.a.a.g.a.k.D0     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x022c }
            boolean r0 = r0.a(r12)     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x022c }
            java.lang.String r12 = "entry"
            java.lang.String r4 = "type"
            java.lang.String r5 = "rowid"
            r26 = -1
            if (r0 == 0) goto L_0x00d0
            long r16 = j.c.a.a.g.a.j3.a(r10)     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            int r0 = (r16 > r26 ? 1 : (r16 == r26 ? 0 : -1))
            if (r0 == 0) goto L_0x00a3
            java.lang.String r0 = "rowid<?"
            r18 = r0
            java.lang.String[] r0 = new java.lang.String[r15]     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            java.lang.String r16 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            r17 = 0
            r0[r17] = r16     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            r20 = r0
            r19 = r18
            goto L_0x00a7
        L_0x0099:
            r17 = r6
            r15 = 0
            goto L_0x0236
        L_0x009e:
            r17 = r6
            r15 = 0
            goto L_0x023d
        L_0x00a3:
            r19 = 0
            r20 = 0
        L_0x00a7:
            java.lang.String r17 = "messages"
            java.lang.String[] r18 = new java.lang.String[]{r5, r4, r12}     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            r21 = 0
            r22 = 0
            java.lang.String r23 = "rowid asc"
            r4 = 100
            java.lang.String r24 = java.lang.Integer.toString(r4)     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0233, all -> 0x0065 }
            r16 = r10
            android.database.Cursor r0 = r16.query(r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ SQLiteFullException -> 0x00c6, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x00c0, all -> 0x021c }
            goto L_0x00ec
        L_0x00c0:
            r0 = move-exception
            r17 = r6
            r4 = 0
            goto L_0x01cb
        L_0x00c6:
            r0 = move-exception
            r17 = r6
            r4 = 0
            goto L_0x01ce
        L_0x00cc:
            r25 = 0
            goto L_0x02ae
        L_0x00d0:
            java.lang.String r17 = "messages"
            java.lang.String[] r18 = new java.lang.String[]{r5, r4, r12}     // Catch:{ SQLiteFullException -> 0x0229, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0222, all -> 0x021c }
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            java.lang.String r23 = "rowid asc"
            r4 = 100
            java.lang.String r24 = java.lang.Integer.toString(r4)     // Catch:{ SQLiteFullException -> 0x0229, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0222, all -> 0x021c }
            r16 = r10
            android.database.Cursor r0 = r16.query(r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ SQLiteFullException -> 0x0229, SQLiteDatabaseLockedException -> 0x0225, SQLiteException -> 0x0222, all -> 0x021c }
        L_0x00ec:
            r4 = r0
        L_0x00ed:
            boolean r0 = r4.moveToNext()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            if (r0 == 0) goto L_0x01d1
            r5 = 0
            long r26 = r4.getLong(r5)     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            int r0 = r4.getInt(r15)     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            r5 = 2
            byte[] r12 = r4.getBlob(r5)     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            if (r0 != 0) goto L_0x0138
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            int r0 = r12.length     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            r15 = 0
            r5.unmarshall(r12, r15, r0)     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            r5.setDataPosition(r15)     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            android.os.Parcelable$Creator<j.c.a.a.g.a.i> r0 = j.c.a.a.g.a.i.CREATOR     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            java.lang.Object r0 = r0.createFromParcel(r5)     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            j.c.a.a.g.a.i r0 = (j.c.a.a.g.a.i) r0     // Catch:{ SafeParcelReader$ParseException -> 0x0122 }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            if (r0 == 0) goto L_0x0130
            r11.add(r0)     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            goto L_0x0130
        L_0x0120:
            r0 = move-exception
            goto L_0x0134
        L_0x0122:
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ all -> 0x0120 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x0120 }
            java.lang.String r12 = "Failed to load event from local database"
            r0.a(r12)     // Catch:{ all -> 0x0120 }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
        L_0x0130:
            r17 = r6
            goto L_0x01c6
        L_0x0134:
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            throw r0     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
        L_0x0138:
            java.lang.String r15 = "Failed to load user property from local database"
            r5 = 1
            if (r0 != r5) goto L_0x0176
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x0218, SQLiteDatabaseLockedException -> 0x0214, SQLiteException -> 0x0210, all -> 0x0205 }
            int r0 = r12.length     // Catch:{ SafeParcelReader$ParseException -> 0x015d, all -> 0x0159 }
            r17 = r6
            r6 = 0
            r5.unmarshall(r12, r6, r0)     // Catch:{ SafeParcelReader$ParseException -> 0x015f }
            r5.setDataPosition(r6)     // Catch:{ SafeParcelReader$ParseException -> 0x015f }
            android.os.Parcelable$Creator<j.c.a.a.g.a.x8> r0 = j.c.a.a.g.a.x8.CREATOR     // Catch:{ SafeParcelReader$ParseException -> 0x015f }
            java.lang.Object r0 = r0.createFromParcel(r5)     // Catch:{ SafeParcelReader$ParseException -> 0x015f }
            j.c.a.a.g.a.x8 r0 = (j.c.a.a.g.a.x8) r0     // Catch:{ SafeParcelReader$ParseException -> 0x015f }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            goto L_0x016c
        L_0x0157:
            r0 = move-exception
            goto L_0x0172
        L_0x0159:
            r0 = move-exception
            r17 = r6
            goto L_0x0172
        L_0x015d:
            r17 = r6
        L_0x015f:
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ all -> 0x0157 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x0157 }
            r0.a(r15)     // Catch:{ all -> 0x0157 }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            r0 = 0
        L_0x016c:
            if (r0 == 0) goto L_0x01c6
            r11.add(r0)     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            goto L_0x01c6
        L_0x0172:
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            throw r0     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
        L_0x0176:
            r17 = r6
            r5 = 2
            if (r0 != r5) goto L_0x01ac
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            int r0 = r12.length     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            r6 = 0
            r5.unmarshall(r12, r6, r0)     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            r5.setDataPosition(r6)     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            android.os.Parcelable$Creator<j.c.a.a.g.a.g9> r0 = j.c.a.a.g.a.g9.CREATOR     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            java.lang.Object r0 = r0.createFromParcel(r5)     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            j.c.a.a.g.a.g9 r0 = (j.c.a.a.g.a.g9) r0     // Catch:{ SafeParcelReader$ParseException -> 0x0195 }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            goto L_0x01a2
        L_0x0193:
            r0 = move-exception
            goto L_0x01a8
        L_0x0195:
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ all -> 0x0193 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ all -> 0x0193 }
            r0.a(r15)     // Catch:{ all -> 0x0193 }
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            r0 = 0
        L_0x01a2:
            if (r0 == 0) goto L_0x01c6
            r11.add(r0)     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            goto L_0x01c6
        L_0x01a8:
            r5.recycle()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            throw r0     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
        L_0x01ac:
            r5 = 3
            if (r0 != r5) goto L_0x01bb
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            j.c.a.a.g.a.p3 r0 = r0.f2047i     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            java.lang.String r5 = "Skipping app launch break"
            r0.a(r5)     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            goto L_0x01c6
        L_0x01bb:
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            java.lang.String r5 = "Unknown record type in local database"
            r0.a(r5)     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
        L_0x01c6:
            r6 = r17
            r15 = 1
            goto L_0x00ed
        L_0x01cb:
            r15 = 0
            goto L_0x024b
        L_0x01ce:
            r15 = 0
            goto L_0x0289
        L_0x01d1:
            r17 = r6
            java.lang.String r0 = "messages"
            java.lang.String r5 = "rowid <= ?"
            r6 = 1
            java.lang.String[] r12 = new java.lang.String[r6]     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            java.lang.String r6 = java.lang.Long.toString(r26)     // Catch:{ SQLiteFullException -> 0x020e, SQLiteDatabaseLockedException -> 0x0216, SQLiteException -> 0x020c, all -> 0x0205 }
            r15 = 0
            r12[r15] = r6     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            int r0 = r10.delete(r0, r5, r12)     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            int r5 = r11.size()     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            if (r0 >= r5) goto L_0x01f6
            j.c.a.a.g.a.n3 r0 = r8.a()     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            j.c.a.a.g.a.p3 r0 = r0.f2046f     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            java.lang.String r5 = "Fewer entries removed from local database than expected"
            r0.a(r5)     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
        L_0x01f6:
            r10.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            r10.endTransaction()     // Catch:{ SQLiteFullException -> 0x0209, SQLiteDatabaseLockedException -> 0x0272, SQLiteException -> 0x0207, all -> 0x0205 }
            r4.close()
            r10.close()
            r10 = r11
            goto L_0x02ce
        L_0x0205:
            r0 = move-exception
            goto L_0x022e
        L_0x0207:
            r0 = move-exception
            goto L_0x024b
        L_0x0209:
            r0 = move-exception
            goto L_0x0289
        L_0x020c:
            r0 = move-exception
            goto L_0x01cb
        L_0x020e:
            r0 = move-exception
            goto L_0x01ce
        L_0x0210:
            r0 = move-exception
            r17 = r6
            goto L_0x01cb
        L_0x0214:
            r17 = r6
        L_0x0216:
            r15 = 0
            goto L_0x0272
        L_0x0218:
            r0 = move-exception
            r17 = r6
            goto L_0x01ce
        L_0x021c:
            r0 = move-exception
            r25 = r10
            r10 = 0
            goto L_0x02b4
        L_0x0222:
            r0 = move-exception
            goto L_0x0099
        L_0x0225:
            r17 = r6
            r15 = 0
            goto L_0x0238
        L_0x0229:
            r0 = move-exception
            goto L_0x009e
        L_0x022c:
            r0 = move-exception
            r4 = 0
        L_0x022e:
            r25 = r10
            r10 = r4
            goto L_0x02b4
        L_0x0233:
            r0 = move-exception
            goto L_0x0099
        L_0x0236:
            r4 = 0
            goto L_0x024b
        L_0x0238:
            r4 = 0
            goto L_0x0272
        L_0x023a:
            r0 = move-exception
            goto L_0x009e
        L_0x023d:
            r4 = 0
            goto L_0x0289
        L_0x023f:
            r0 = move-exception
            r10 = 0
            r25 = 0
            goto L_0x02b4
        L_0x0245:
            r0 = move-exception
            r17 = r6
            r15 = 0
            r4 = 0
            r10 = 0
        L_0x024b:
            if (r10 == 0) goto L_0x0256
            boolean r5 = r10.inTransaction()     // Catch:{ all -> 0x02a8 }
            if (r5 == 0) goto L_0x0256
            r10.endTransaction()     // Catch:{ all -> 0x02a8 }
        L_0x0256:
            j.c.a.a.g.a.n3 r5 = r8.a()     // Catch:{ all -> 0x02a8 }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ all -> 0x02a8 }
            r5.a(r9, r0)     // Catch:{ all -> 0x02a8 }
            r5 = 1
            r8.d = r5     // Catch:{ all -> 0x02a8 }
            if (r4 == 0) goto L_0x0267
            r4.close()
        L_0x0267:
            if (r10 == 0) goto L_0x029f
            r10.close()
            goto L_0x029f
        L_0x026d:
            r17 = r6
            r15 = 0
            r4 = 0
            r10 = 0
        L_0x0272:
            long r5 = (long) r14
            android.os.SystemClock.sleep(r5)     // Catch:{ all -> 0x02a8 }
            int r14 = r14 + 20
            if (r4 == 0) goto L_0x027d
            r4.close()
        L_0x027d:
            if (r10 == 0) goto L_0x029f
            r10.close()
            goto L_0x029f
        L_0x0283:
            r0 = move-exception
            r17 = r6
            r15 = 0
            r4 = 0
            r10 = 0
        L_0x0289:
            j.c.a.a.g.a.n3 r5 = r8.a()     // Catch:{ all -> 0x02a8 }
            j.c.a.a.g.a.p3 r5 = r5.f2046f     // Catch:{ all -> 0x02a8 }
            r5.a(r9, r0)     // Catch:{ all -> 0x02a8 }
            r5 = 1
            r8.d = r5     // Catch:{ all -> 0x02a8 }
            if (r4 == 0) goto L_0x029a
            r4.close()
        L_0x029a:
            if (r10 == 0) goto L_0x029f
            r10.close()
        L_0x029f:
            int r13 = r13 + 1
            r6 = r17
            r4 = 100
            r12 = 5
            goto L_0x0054
        L_0x02a8:
            r0 = move-exception
            r1 = r10
            r10 = r4
            r25 = r10
            r10 = r1
        L_0x02ae:
            r28 = r25
            r25 = r10
            r10 = r28
        L_0x02b4:
            if (r10 == 0) goto L_0x02b9
            r10.close()
        L_0x02b9:
            if (r25 == 0) goto L_0x02be
            r25.close()
        L_0x02be:
            throw r0
        L_0x02bf:
            r17 = r6
            r15 = 0
            j.c.a.a.g.a.n3 r0 = r8.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r4 = "Failed to read events from database in reasonable time"
            r0.a(r4)
            r10 = 0
        L_0x02ce:
            if (r10 == 0) goto L_0x02d9
            r7.addAll(r10)
            int r0 = r10.size()
            r4 = r0
            goto L_0x02da
        L_0x02d9:
            r4 = 0
        L_0x02da:
            r5 = 100
            if (r2 == 0) goto L_0x02e3
            if (r4 >= r5) goto L_0x02e3
            r7.add(r2)
        L_0x02e3:
            int r6 = r7.size()
            r0 = 0
        L_0x02e8:
            if (r0 >= r6) goto L_0x0344
            java.lang.Object r8 = r7.get(r0)
            int r9 = r0 + 1
            j.c.a.a.c.l.p.a r8 = (j.c.a.a.c.l.p.a) r8
            boolean r0 = r8 instanceof j.c.a.a.g.a.i
            if (r0 == 0) goto L_0x0309
            j.c.a.a.g.a.i r8 = (j.c.a.a.g.a.i) r8     // Catch:{ RemoteException -> 0x02fc }
            r1.a(r8, r3)     // Catch:{ RemoteException -> 0x02fc }
            goto L_0x0342
        L_0x02fc:
            r0 = move-exception
            j.c.a.a.g.a.n3 r8 = r29.a()
            j.c.a.a.g.a.p3 r8 = r8.f2046f
            java.lang.String r10 = "Failed to send event to the service"
            r8.a(r10, r0)
            goto L_0x0342
        L_0x0309:
            boolean r0 = r8 instanceof j.c.a.a.g.a.x8
            if (r0 == 0) goto L_0x0320
            j.c.a.a.g.a.x8 r8 = (j.c.a.a.g.a.x8) r8     // Catch:{ RemoteException -> 0x0313 }
            r1.a(r8, r3)     // Catch:{ RemoteException -> 0x0313 }
            goto L_0x0342
        L_0x0313:
            r0 = move-exception
            j.c.a.a.g.a.n3 r8 = r29.a()
            j.c.a.a.g.a.p3 r8 = r8.f2046f
            java.lang.String r10 = "Failed to send attribute to the service"
            r8.a(r10, r0)
            goto L_0x0342
        L_0x0320:
            boolean r0 = r8 instanceof j.c.a.a.g.a.g9
            if (r0 == 0) goto L_0x0337
            j.c.a.a.g.a.g9 r8 = (j.c.a.a.g.a.g9) r8     // Catch:{ RemoteException -> 0x032a }
            r1.a(r8, r3)     // Catch:{ RemoteException -> 0x032a }
            goto L_0x0342
        L_0x032a:
            r0 = move-exception
            j.c.a.a.g.a.n3 r8 = r29.a()
            j.c.a.a.g.a.p3 r8 = r8.f2046f
            java.lang.String r10 = "Failed to send conditional property to the service"
            r8.a(r10, r0)
            goto L_0x0342
        L_0x0337:
            j.c.a.a.g.a.n3 r0 = r29.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r8 = "Discarding data. Unrecognized parcel type."
            r0.a(r8)
        L_0x0342:
            r0 = r9
            goto L_0x02e8
        L_0x0344:
            int r6 = r17 + 1
            r0 = r4
            r4 = 100
            goto L_0x0017
        L_0x034b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.z6.a(j.c.a.a.g.a.f3, j.c.a.a.c.l.p.a, j.c.a.a.g.a.d9):void");
    }

    public final void a(g9 g9Var) {
        boolean z;
        ResourcesFlusher.b(g9Var);
        d();
        w();
        h9 h9Var = this.a.f2086f;
        j3 t2 = t();
        t2.k();
        byte[] a = y8.a(g9Var);
        if (a.length > 131072) {
            t2.a().f2047i.a("Conditional user property too long for local database. Sending directly to service");
            z = false;
        } else {
            z = t2.a(2, a);
        }
        a(new o7(this, true, z, new g9(g9Var), a(true), g9Var));
    }

    public static /* synthetic */ void a(z6 z6Var, ComponentName componentName) {
        z6Var.d();
        if (z6Var.d != null) {
            z6Var.d = null;
            z6Var.a().f2052n.a("Disconnected from device MeasurementService", componentName);
            z6Var.d();
            z6Var.A();
        }
    }

    public final void a(Runnable runnable) {
        d();
        if (z()) {
            runnable.run();
        } else if (((long) this.h.size()) >= 1000) {
            a().f2046f.a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.f2142i.a(60000);
            A();
        }
    }
}
