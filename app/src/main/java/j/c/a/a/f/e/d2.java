package j.c.a.a.f.e;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d2<T> extends a2<T> {
    public final T b;

    public d2(T t2) {
        this.b = t2;
    }

    public final boolean a() {
        return true;
    }

    public final T b() {
        return this.b;
    }

    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof d2) {
            return this.b.equals(((d2) obj).b);
        }
        return false;
    }

    public final int hashCode() {
        return this.b.hashCode() + 1502476572;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(valueOf.length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
