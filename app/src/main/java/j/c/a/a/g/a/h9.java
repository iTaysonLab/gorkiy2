package j.c.a.a.g.a;

import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h9 {
    public static boolean a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
