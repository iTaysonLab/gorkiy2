package j.c.a.a.g.a;

import android.os.Handler;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.q5;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class b {
    public static volatile Handler d;
    public final p5 a;
    public final Runnable b;
    public volatile long c;

    public b(p5 p5Var) {
        ResourcesFlusher.b(p5Var);
        this.a = p5Var;
        this.b = new d(this, p5Var);
    }

    public abstract void a();

    public final void a(long j2) {
        b();
        if (j2 < 0) {
            return;
        }
        if (((j.c.a.a.c.n.b) this.a.j()) != null) {
            this.c = System.currentTimeMillis();
            if (!c().postDelayed(this.b, j2)) {
                this.a.a().f2046f.a("Failed to schedule delayed post. time", Long.valueOf(j2));
                return;
            }
            return;
        }
        throw null;
    }

    public final void b() {
        this.c = 0;
        c().removeCallbacks(this.b);
    }

    public final Handler c() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (b.class) {
            if (d == null) {
                d = new q5(this.a.f().getMainLooper());
            }
            handler = d;
        }
        return handler;
    }
}
