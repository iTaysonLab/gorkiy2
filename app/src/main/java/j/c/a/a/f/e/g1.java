package j.c.a.a.f.e;

import android.database.ContentObserver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g1 extends ContentObserver {
    public final /* synthetic */ e1 a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g1(e1 e1Var) {
        super(null);
        this.a = e1Var;
    }

    public final void onChange(boolean z) {
        this.a.b();
    }
}
