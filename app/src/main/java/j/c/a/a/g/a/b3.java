package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class b3<V> {
    public static final Object h = new Object();
    public final String a;
    public final c3<V> b;
    public final V c;
    public final V d;

    /* renamed from: e  reason: collision with root package name */
    public final Object f1938e = new Object();

    /* renamed from: f  reason: collision with root package name */
    public volatile V f1939f = null;
    public volatile V g = null;

    public /* synthetic */ b3(String str, Object obj, Object obj2, c3 c3Var, z2 z2Var) {
        this.a = str;
        this.c = obj;
        this.d = obj2;
        this.b = c3Var;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 180 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0021, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r1.g = null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x0055 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V a(V r6) {
        /*
            r5 = this;
            java.lang.Object r0 = r5.f1938e
            monitor-enter(r0)
            monitor-exit(r0)     // Catch:{ all -> 0x0092 }
            if (r6 == 0) goto L_0x0007
            return r6
        L_0x0007:
            j.c.a.a.g.a.h9 r6 = j.c.a.a.g.a.k.a
            if (r6 != 0) goto L_0x000e
            V r6 = r5.c
            return r6
        L_0x000e:
            java.lang.Object r6 = j.c.a.a.g.a.b3.h
            monitor-enter(r6)
            boolean r0 = j.c.a.a.g.a.h9.a()     // Catch:{ all -> 0x008d }
            if (r0 == 0) goto L_0x0022
            V r0 = r5.g     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x001e
            V r0 = r5.c     // Catch:{ all -> 0x008d }
            goto L_0x0020
        L_0x001e:
            V r0 = r5.g     // Catch:{ all -> 0x008d }
        L_0x0020:
            monitor-exit(r6)     // Catch:{ all -> 0x008d }
            return r0
        L_0x0022:
            boolean r0 = j.c.a.a.g.a.h9.a()     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0085
            j.c.a.a.g.a.h9 r0 = j.c.a.a.g.a.k.a     // Catch:{ all -> 0x008d }
            java.util.List<j.c.a.a.g.a.b3<?>> r0 = j.c.a.a.g.a.k.b     // Catch:{ SecurityException -> 0x0064 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ SecurityException -> 0x0064 }
        L_0x0030:
            boolean r1 = r0.hasNext()     // Catch:{ SecurityException -> 0x0064 }
            if (r1 == 0) goto L_0x0068
            java.lang.Object r1 = r0.next()     // Catch:{ SecurityException -> 0x0064 }
            j.c.a.a.g.a.b3 r1 = (j.c.a.a.g.a.b3) r1     // Catch:{ SecurityException -> 0x0064 }
            java.lang.Object r2 = j.c.a.a.g.a.b3.h     // Catch:{ SecurityException -> 0x0064 }
            monitor-enter(r2)     // Catch:{ SecurityException -> 0x0064 }
            boolean r3 = j.c.a.a.g.a.h9.a()     // Catch:{ all -> 0x0061 }
            if (r3 != 0) goto L_0x0059
            r3 = 0
            j.c.a.a.g.a.c3<V> r4 = r1.b     // Catch:{ IllegalStateException -> 0x0055 }
            if (r4 == 0) goto L_0x0051
            j.c.a.a.g.a.c3<V> r4 = r1.b     // Catch:{ IllegalStateException -> 0x0055 }
            java.lang.Object r4 = r4.a()     // Catch:{ IllegalStateException -> 0x0055 }
            goto L_0x0052
        L_0x0051:
            r4 = r3
        L_0x0052:
            r1.g = r4     // Catch:{ IllegalStateException -> 0x0055 }
            goto L_0x0057
        L_0x0055:
            r1.g = r3     // Catch:{ all -> 0x0061 }
        L_0x0057:
            monitor-exit(r2)     // Catch:{ all -> 0x0061 }
            goto L_0x0030
        L_0x0059:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0061 }
            java.lang.String r1 = "Refreshing flag cache must be done on a worker thread."
            r0.<init>(r1)     // Catch:{ all -> 0x0061 }
            throw r0     // Catch:{ all -> 0x0061 }
        L_0x0061:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0061 }
            throw r0     // Catch:{ SecurityException -> 0x0064 }
        L_0x0064:
            r0 = move-exception
            j.c.a.a.g.a.k.a(r0)     // Catch:{ all -> 0x008d }
        L_0x0068:
            monitor-exit(r6)     // Catch:{ all -> 0x008d }
            j.c.a.a.g.a.c3<V> r6 = r5.b
            if (r6 != 0) goto L_0x0072
            j.c.a.a.g.a.h9 r6 = j.c.a.a.g.a.k.a
            V r6 = r5.c
            return r6
        L_0x0072:
            java.lang.Object r6 = r6.a()     // Catch:{ SecurityException -> 0x007c, IllegalStateException -> 0x0077 }
            return r6
        L_0x0077:
            j.c.a.a.g.a.h9 r6 = j.c.a.a.g.a.k.a
            V r6 = r5.c
            return r6
        L_0x007c:
            r6 = move-exception
            j.c.a.a.g.a.k.a(r6)
            j.c.a.a.g.a.h9 r6 = j.c.a.a.g.a.k.a
            V r6 = r5.c
            return r6
        L_0x0085:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "Tried to refresh flag cache on main thread or on package side."
            r0.<init>(r1)     // Catch:{ all -> 0x008d }
            throw r0     // Catch:{ all -> 0x008d }
        L_0x008d:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x008d }
            throw r0
        L_0x0090:
            monitor-exit(r0)     // Catch:{ all -> 0x0092 }
            throw r6
        L_0x0092:
            r6 = move-exception
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.b3.a(java.lang.Object):java.lang.Object");
    }
}
