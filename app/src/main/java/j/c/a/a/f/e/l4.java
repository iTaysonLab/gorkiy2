package j.c.a.a.f.e;

import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l4<K> implements Iterator<Map.Entry<K, Object>> {
    public Iterator<Map.Entry<K, Object>> b;

    public l4(Iterator<Map.Entry<K, Object>> it) {
        this.b = it;
    }

    public final boolean hasNext() {
        return this.b.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.b.next();
        return next.getValue() instanceof g4 ? new i4(next, null) : next;
    }

    public final void remove() {
        this.b.remove();
    }
}
