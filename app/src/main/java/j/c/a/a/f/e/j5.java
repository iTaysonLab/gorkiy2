package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import com.google.android.gms.internal.measurement.zzfn;
import j.c.a.a.c.n.c;
import j.c.a.a.f.e.j3;
import j.c.a.a.f.e.x3;
import java.util.Iterator;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class j5<T> implements t5<T> {
    public final f5 a;
    public final g6<?, ?> b;
    public final boolean c;
    public final l3<?> d;

    public j5(g6<?, ?> g6Var, l3<?> l3Var, f5 f5Var) {
        this.b = g6Var;
        if (((k3) l3Var) != null) {
            this.c = f5Var instanceof x3.b;
            this.d = l3Var;
            this.a = f5Var;
            return;
        }
        throw null;
    }

    public final T a() {
        return ((x3.a) this.a.b()).j();
    }

    public final void b(T t2, T t3) {
        u5.a(this.b, t2, t3);
        if (this.c) {
            u5.a(this.d, t2, t3);
        }
    }

    public final int c(T t2) {
        if (((h6) this.b) != null) {
            i6 i6Var = ((x3) t2).zzb;
            int i2 = i6Var.d;
            if (i2 == -1) {
                i2 = 0;
                for (int i3 = 0; i3 < i6Var.a; i3++) {
                    int c2 = zzek.c(2, i6Var.b[i3] >>> 3);
                    i2 += zzek.a(3, (w2) i6Var.c[i3]) + c2 + (zzek.f(1) << 1);
                }
                i6Var.d = i2;
            }
            int i4 = i2 + 0;
            if (!this.c) {
                return i4;
            }
            if (((k3) this.d) != null) {
                m3<Object> m3Var = ((x3.b) t2).zzc;
                int i5 = 0;
                for (int i6 = 0; i6 < m3Var.a.b(); i6++) {
                    i5 += m3.c(m3Var.a.a(i6));
                }
                for (Map.Entry<FieldDescriptorType, Object> c3 : m3Var.a.c()) {
                    i5 += m3.c(c3);
                }
                return i4 + i5;
            }
            throw null;
        }
        throw null;
    }

    public final boolean d(T t2) {
        if (((k3) this.d) != null) {
            return ((x3.b) t2).zzc.b();
        }
        throw null;
    }

    public final boolean a(T t2, T t3) {
        g6<?, ?> g6Var = this.b;
        if (((h6) g6Var) != null) {
            i6 i6Var = ((x3) t2).zzb;
            if (((h6) g6Var) == null) {
                throw null;
            } else if (!i6Var.equals(((x3) t3).zzb)) {
                return false;
            } else {
                if (!this.c) {
                    return true;
                }
                l3<?> l3Var = this.d;
                if (((k3) l3Var) != null) {
                    m3<Object> m3Var = ((x3.b) t2).zzc;
                    if (((k3) l3Var) != null) {
                        return m3Var.equals(((x3.b) t3).zzc);
                    }
                    throw null;
                }
                throw null;
            }
        } else {
            throw null;
        }
    }

    public final void b(T t2) {
        if (((h6) this.b) != null) {
            ((x3) t2).zzb.f1867e = false;
            if (((k3) this.d) != null) {
                m3<Object> m3Var = ((x3.b) t2).zzc;
                if (!m3Var.b) {
                    m3Var.a.a();
                    m3Var.b = true;
                    return;
                }
                return;
            }
            throw null;
        }
        throw null;
    }

    public final int a(T t2) {
        if (((h6) this.b) != null) {
            int hashCode = ((x3) t2).zzb.hashCode();
            if (!this.c) {
                return hashCode;
            }
            if (((k3) this.d) != null) {
                return (hashCode * 53) + ((x3.b) t2).zzc.hashCode();
            }
            throw null;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.g3.a(int, java.lang.Object):void
     arg types: [int, j.c.a.a.f.e.w2]
     candidates:
      j.c.a.a.f.e.g3.a(int, double):void
      j.c.a.a.f.e.g3.a(int, float):void
      j.c.a.a.f.e.g3.a(int, int):void
      j.c.a.a.f.e.g3.a(int, long):void
      j.c.a.a.f.e.g3.a(int, j.c.a.a.f.e.w2):void
      j.c.a.a.f.e.g3.a(int, boolean):void
      j.c.a.a.f.e.g3.a(int, java.lang.Object):void */
    public final void a(T t2, c7 c7Var) {
        if (((k3) this.d) != null) {
            Iterator<Map.Entry<Object, Object>> a2 = ((x3.b) t2).zzc.a();
            while (a2.hasNext()) {
                Map.Entry next = a2.next();
                o3 o3Var = (o3) next.getKey();
                if (o3Var.c() != z6.MESSAGE || o3Var.d() || o3Var.e()) {
                    throw new IllegalStateException("Found invalid MessageSet item.");
                } else if (next instanceof i4) {
                    ((g3) c7Var).a(o3Var.a(), (Object) ((i4) next).b.getValue().b());
                } else {
                    ((g3) c7Var).a(o3Var.a(), next.getValue());
                }
            }
            if (((h6) this.b) != null) {
                i6 i6Var = ((x3) t2).zzb;
                if (i6Var != null) {
                    g3 g3Var = (g3) c7Var;
                    if (g3Var != null) {
                        for (int i2 = 0; i2 < i6Var.a; i2++) {
                            g3Var.a(i6Var.b[i2] >>> 3, i6Var.c[i2]);
                        }
                        return;
                    }
                    throw null;
                }
                throw null;
            }
            throw null;
        }
        throw null;
    }

    public final void a(T t2, byte[] bArr, int i2, int i3, s2 s2Var) {
        x3 x3Var = (x3) t2;
        i6 i6Var = x3Var.zzb;
        if (i6Var == i6.f1866f) {
            i6Var = i6.b();
            x3Var.zzb = i6Var;
        }
        ((x3.b) t2).a();
        x3.e eVar = null;
        while (i2 < i3) {
            int a2 = c.a(bArr, i2, s2Var);
            int i4 = s2Var.a;
            if (i4 == 11) {
                int i5 = 0;
                w2 w2Var = null;
                while (a2 < i3) {
                    a2 = c.a(bArr, a2, s2Var);
                    int i6 = s2Var.a;
                    int i7 = i6 >>> 3;
                    int i8 = i6 & 7;
                    if (i7 != 2) {
                        if (i7 == 3) {
                            if (eVar != null) {
                                o5 o5Var = o5.c;
                                throw new NoSuchMethodError();
                            } else if (i8 == 2) {
                                a2 = c.e(bArr, a2, s2Var);
                                w2Var = (w2) s2Var.c;
                            }
                        }
                    } else if (i8 == 0) {
                        a2 = c.a(bArr, a2, s2Var);
                        i5 = s2Var.a;
                        l3<?> l3Var = this.d;
                        j3 j3Var = s2Var.d;
                        f5 f5Var = this.a;
                        if (((k3) l3Var) != null) {
                            eVar = j3Var.a.get(new j3.a(f5Var, i5));
                        } else {
                            throw null;
                        }
                    }
                    if (i6 == 12) {
                        break;
                    }
                    a2 = c.a(i6, bArr, a2, i3, s2Var);
                }
                if (w2Var != null) {
                    i6Var.a((i5 << 3) | 2, w2Var);
                }
                i2 = a2;
            } else if ((i4 & 7) == 2) {
                l3<?> l3Var2 = this.d;
                j3 j3Var2 = s2Var.d;
                f5 f5Var2 = this.a;
                int i9 = i4 >>> 3;
                if (((k3) l3Var2) != null) {
                    eVar = j3Var2.a.get(new j3.a(f5Var2, i9));
                    if (eVar == null) {
                        i2 = c.a(i4, bArr, a2, i3, i6Var, s2Var);
                    } else {
                        o5 o5Var2 = o5.c;
                        throw new NoSuchMethodError();
                    }
                } else {
                    throw null;
                }
            } else {
                i2 = c.a(i4, bArr, a2, i3, s2Var);
            }
        }
        if (i2 != i3) {
            throw zzfn.e();
        }
    }
}
