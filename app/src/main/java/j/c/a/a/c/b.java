package j.c.a.a.c;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.n;
import j.c.a.a.c.l.p.a;
import java.util.Arrays;

public final class b extends a {
    public static final Parcelable.Creator<b> CREATOR = new p();

    /* renamed from: f  reason: collision with root package name */
    public static final b f1774f = new b(0);
    public final int b;
    public final int c;
    public final PendingIntent d;

    /* renamed from: e  reason: collision with root package name */
    public final String f1775e;

    public b(int i2) {
        this.b = 1;
        this.c = i2;
        this.d = null;
        this.f1775e = null;
    }

    public static String a(int i2) {
        if (i2 == 99) {
            return "UNFINISHED";
        }
        if (i2 == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (i2) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            default:
                switch (i2) {
                    case 13:
                        return "CANCELED";
                    case 14:
                        return "TIMEOUT";
                    case 15:
                        return "INTERRUPTED";
                    case 16:
                        return "API_UNAVAILABLE";
                    case 17:
                        return "SIGN_IN_FAILED";
                    case 18:
                        return "SERVICE_UPDATING";
                    case 19:
                        return "SERVICE_MISSING_PERMISSION";
                    case 20:
                        return "RESTRICTED_PROFILE";
                    case 21:
                        return "API_VERSION_UPDATE_REQUIRED";
                    default:
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(i2);
                        sb.append(")");
                        return sb.toString();
                }
        }
    }

    public final boolean c() {
        return this.c == 0;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.c == bVar.c && ResourcesFlusher.c(this.d, bVar.d) && ResourcesFlusher.c(this.f1775e, bVar.f1775e);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.c), this.d, this.f1775e});
    }

    public final String toString() {
        n d2 = ResourcesFlusher.d(this);
        d2.a("statusCode", a(this.c));
        d2.a("resolution", this.d);
        d2.a("message", this.f1775e);
        return d2.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, float):float
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int, java.lang.String):android.animation.PropertyValuesHolder
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int, int, i.f.a.h.ChainHead):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 1, this.b);
        ResourcesFlusher.a(parcel, 2, this.c);
        ResourcesFlusher.a(parcel, 3, (Parcelable) this.d, i2, false);
        ResourcesFlusher.a(parcel, 4, this.f1775e, false);
        ResourcesFlusher.k(parcel, a);
    }

    public b(int i2, int i3, PendingIntent pendingIntent, String str) {
        this.b = i2;
        this.c = i3;
        this.d = pendingIntent;
        this.f1775e = str;
    }

    public b(int i2, PendingIntent pendingIntent) {
        this.b = 1;
        this.c = i2;
        this.d = pendingIntent;
        this.f1775e = null;
    }
}
