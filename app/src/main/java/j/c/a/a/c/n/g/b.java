package j.c.a.a.c.n.g;

import android.os.Process;

public final class b implements Runnable {
    public final Runnable b;
    public final int c;

    public b(Runnable runnable, int i2) {
        this.b = runnable;
        this.c = i2;
    }

    public final void run() {
        Process.setThreadPriority(this.c);
        this.b.run();
    }
}
