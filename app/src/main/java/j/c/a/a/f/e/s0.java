package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class s0 extends x3<s0, a> implements g5 {
    public static final s0 zzi;
    public static volatile m5<s0> zzj;
    public int zzc;
    public String zzd = "";
    public String zze = "";
    public long zzf;
    public float zzg;
    public double zzh;

    static {
        s0 s0Var = new s0();
        zzi = s0Var;
        x3.zzd.put(s0.class, s0Var);
    }

    public static /* synthetic */ void b(s0 s0Var, String str) {
        if (str != null) {
            s0Var.zzc |= 2;
            s0Var.zze = str;
            return;
        }
        throw null;
    }

    public static a p() {
        return (a) zzi.g();
    }

    public final String a() {
        return this.zzd;
    }

    public final String i() {
        return this.zze;
    }

    public final boolean j() {
        return (this.zzc & 4) != 0;
    }

    public final long m() {
        return this.zzf;
    }

    public final boolean n() {
        return (this.zzc & 16) != 0;
    }

    public final double o() {
        return this.zzh;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<s0, a> implements g5 {
        public a() {
            super(s0.zzi);
        }

        public final a a(String str) {
            i();
            s0.a((s0) super.c, str);
            return this;
        }

        public final a b(String str) {
            i();
            s0.b((s0) super.c, str);
            return this;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(s0.zzi);
        }

        public final a a(long j2) {
            i();
            s0 s0Var = (s0) super.c;
            s0Var.zzc |= 4;
            s0Var.zzf = j2;
            return this;
        }
    }

    public static /* synthetic */ void a(s0 s0Var, String str) {
        if (str != null) {
            s0Var.zzc |= 1;
            s0Var.zzd = str;
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new s0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0001\u0003\u0005\u0000\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                m5<s0> m5Var = zzj;
                if (m5Var == null) {
                    synchronized (s0.class) {
                        m5Var = zzj;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzi);
                            zzj = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
