package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class x4 implements Runnable {
    public final /* synthetic */ g9 b;
    public final /* synthetic */ d9 c;
    public final /* synthetic */ s4 d;

    public x4(s4 s4Var, g9 g9Var, d9 d9Var) {
        this.d = s4Var;
        this.b = g9Var;
        this.c = d9Var;
    }

    public final void run() {
        this.d.a.o();
        this.d.a.a(this.b, this.c);
    }
}
