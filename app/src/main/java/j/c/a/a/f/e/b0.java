package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class b0 extends x3<b0, a> implements g5 {
    public static final b0 zzh;
    public static volatile m5<b0> zzi;
    public int zzc;
    public e0 zzd;
    public c0 zze;
    public boolean zzf;
    public String zzg = "";

    static {
        b0 b0Var = new b0();
        zzh = b0Var;
        x3.zzd.put(b0.class, b0Var);
    }

    public static /* synthetic */ void a(b0 b0Var, String str) {
        if (str != null) {
            b0Var.zzc |= 8;
            b0Var.zzg = str;
            return;
        }
        throw null;
    }

    public final e0 i() {
        e0 e0Var = this.zzd;
        return e0Var == null ? e0.zzh : e0Var;
    }

    public final boolean j() {
        return (this.zzc & 2) != 0;
    }

    public final c0 m() {
        c0 c0Var = this.zze;
        return c0Var == null ? c0.zzi : c0Var;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<b0, a> implements g5 {
        public a() {
            super(b0.zzh);
        }

        public /* synthetic */ a(f0 f0Var) {
            super(b0.zzh);
        }
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new b0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001\u0003\u0007\u0002\u0004\b\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                m5<b0> m5Var = zzi;
                if (m5Var == null) {
                    synchronized (b0.class) {
                        m5Var = zzi;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzh);
                            zzi = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
