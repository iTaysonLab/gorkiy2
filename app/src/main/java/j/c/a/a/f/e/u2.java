package j.c.a.a.f.e;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u2 extends r2<Boolean> implements e4<Boolean>, p5, RandomAccess {
    public boolean[] c;
    public int d;

    static {
        new u2(new boolean[0], 0).b = false;
    }

    public u2() {
        this.c = new boolean[10];
        this.d = 0;
    }

    public final void a(boolean z) {
        c();
        int i2 = this.d;
        boolean[] zArr = this.c;
        if (i2 == zArr.length) {
            boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i2);
            this.c = zArr2;
        }
        boolean[] zArr3 = this.c;
        int i3 = this.d;
        this.d = i3 + 1;
        zArr3[i3] = z;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        int i3;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(c(i2));
        }
        boolean[] zArr = this.c;
        if (i3 < zArr.length) {
            System.arraycopy(zArr, i2, zArr, i2 + 1, i3 - i2);
        } else {
            boolean[] zArr2 = new boolean[(((i3 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i2);
            System.arraycopy(this.c, i2, zArr2, i2 + 1, this.d - i2);
            this.c = zArr2;
        }
        this.c[i2] = booleanValue;
        this.d++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Boolean> collection) {
        c();
        y3.a(collection);
        if (!(collection instanceof u2)) {
            return super.addAll(collection);
        }
        u2 u2Var = (u2) collection;
        int i2 = u2Var.d;
        if (i2 == 0) {
            return false;
        }
        int i3 = this.d;
        if (Integer.MAX_VALUE - i3 >= i2) {
            int i4 = i3 + i2;
            boolean[] zArr = this.c;
            if (i4 > zArr.length) {
                this.c = Arrays.copyOf(zArr, i4);
            }
            System.arraycopy(u2Var.c, 0, this.c, this.d, u2Var.d);
            this.d = i4;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final void b(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(c(i2));
        }
    }

    public final String c(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u2)) {
            return super.equals(obj);
        }
        u2 u2Var = (u2) obj;
        if (this.d != u2Var.d) {
            return false;
        }
        boolean[] zArr = u2Var.c;
        for (int i2 = 0; i2 < this.d; i2++) {
            if (this.c[i2] != zArr[i2]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i2) {
        b(i2);
        return Boolean.valueOf(this.c[i2]);
    }

    public final int hashCode() {
        int i2 = 1;
        for (int i3 = 0; i3 < this.d; i3++) {
            i2 = (i2 * 31) + y3.a(this.c[i3]);
        }
        return i2;
    }

    public final boolean remove(Object obj) {
        c();
        for (int i2 = 0; i2 < this.d; i2++) {
            if (obj.equals(Boolean.valueOf(this.c[i2]))) {
                boolean[] zArr = this.c;
                System.arraycopy(zArr, i2 + 1, zArr, i2, (this.d - i2) - 1);
                this.d--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    public final void removeRange(int i2, int i3) {
        c();
        if (i3 >= i2) {
            boolean[] zArr = this.c;
            System.arraycopy(zArr, i3, zArr, i2, this.d - i3);
            this.d -= i3 - i2;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        c();
        b(i2);
        boolean[] zArr = this.c;
        boolean z = zArr[i2];
        zArr[i2] = booleanValue;
        return Boolean.valueOf(z);
    }

    public final int size() {
        return this.d;
    }

    public u2(boolean[] zArr, int i2) {
        this.c = zArr;
        this.d = i2;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        b(i2);
        boolean[] zArr = this.c;
        boolean z = zArr[i2];
        int i3 = this.d;
        if (i2 < i3 - 1) {
            System.arraycopy(zArr, i2 + 1, zArr, i2, (i3 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    public final /* synthetic */ e4 a(int i2) {
        if (i2 >= this.d) {
            return new u2(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        a(((Boolean) obj).booleanValue());
        return true;
    }
}
