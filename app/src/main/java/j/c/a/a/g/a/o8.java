package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public abstract class o8 extends o5 implements p5 {
    public final q8 b;
    public boolean c;

    public o8(q8 q8Var) {
        super(q8Var.f2073i);
        ResourcesFlusher.b(q8Var);
        this.b = q8Var;
        q8Var.f2079o++;
    }

    public f9 n() {
        q8 q8Var = this.b;
        q8.a(q8Var.f2072f);
        return q8Var.f2072f;
    }

    public final void o() {
        if (!this.c) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void p() {
        if (!this.c) {
            q();
            this.b.f2080p++;
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    public abstract boolean q();

    public u8 r() {
        return this.b.k();
    }

    public n9 s() {
        return this.b.e();
    }

    public m4 t() {
        return this.b.c();
    }
}
