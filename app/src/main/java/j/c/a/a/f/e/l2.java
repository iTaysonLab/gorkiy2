package j.c.a.a.f.e;

import java.lang.ref.Reference;
import java.util.List;
import java.util.Vector;

/* compiled from: com.google.android.gms:play-services-measurement-base@@17.0.1 */
public final class l2 extends h2 {
    public final k2 a = new k2();

    public final void a(Throwable th, Throwable th2) {
        List putIfAbsent;
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        } else if (th2 != null) {
            k2 k2Var = this.a;
            for (Reference<? extends Throwable> poll = k2Var.b.poll(); poll != null; poll = k2Var.b.poll()) {
                k2Var.a.remove(poll);
            }
            List list = k2Var.a.get(new j2(th, null));
            if (list == null && (putIfAbsent = k2Var.a.putIfAbsent(new j2(th, k2Var.b), (list = new Vector(2)))) != null) {
                list = putIfAbsent;
            }
            list.add(th2);
        } else {
            throw new NullPointerException("The suppressed exception cannot be null.");
        }
    }
}
