package j.c.a.a.g.a;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class t8 implements Callable<String> {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ q8 c;

    public t8(q8 q8Var, d9 d9Var) {
        this.c = q8Var;
        this.b = d9Var;
    }

    public final /* synthetic */ Object call() {
        e4 b2 = this.c.b(this.b);
        if (b2 != null) {
            return b2.h();
        }
        this.c.a().f2047i.a("App info was null when attempting to get app instance id");
        return null;
    }
}
