package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class w7 implements Runnable {
    public final /* synthetic */ f3 b;
    public final /* synthetic */ r7 c;

    public w7(r7 r7Var, f3 f3Var) {
        this.c = r7Var;
        this.b = f3Var;
    }

    public final void run() {
        synchronized (this.c) {
            this.c.a = false;
            if (!this.c.c.z()) {
                this.c.c.a().f2051m.a("Connected to remote service");
                z6 z6Var = this.c.c;
                f3 f3Var = this.b;
                z6Var.d();
                ResourcesFlusher.b(f3Var);
                z6Var.d = f3Var;
                z6Var.D();
                z6Var.E();
            }
        }
    }
}
