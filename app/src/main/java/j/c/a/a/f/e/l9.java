package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l9 implements i9 {
    public static final p1<Boolean> a = p1.a(new v1(q1.a("com.google.android.gms.measurement")), "measurement.collection.null_empty_event_name_fix", true);

    public final boolean a() {
        return a.b().booleanValue();
    }
}
