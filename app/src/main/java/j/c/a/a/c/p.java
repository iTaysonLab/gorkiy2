package j.c.a.a.c;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

public final class p implements Parcelable.Creator<b> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = ResourcesFlusher.b(parcel);
        int i2 = 0;
        PendingIntent pendingIntent = null;
        String str = null;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            int i4 = 65535 & readInt;
            if (i4 == 1) {
                i2 = ResourcesFlusher.f(parcel, readInt);
            } else if (i4 == 2) {
                i3 = ResourcesFlusher.f(parcel, readInt);
            } else if (i4 == 3) {
                pendingIntent = (PendingIntent) ResourcesFlusher.a(parcel, readInt, PendingIntent.CREATOR);
            } else if (i4 != 4) {
                ResourcesFlusher.i(parcel, readInt);
            } else {
                str = ResourcesFlusher.b(parcel, readInt);
            }
        }
        ResourcesFlusher.c(parcel, b);
        return new b(i2, i3, pendingIntent, str);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new b[i2];
    }
}
