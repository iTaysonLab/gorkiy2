package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d implements Runnable {
    public final /* synthetic */ p5 b;
    public final /* synthetic */ b c;

    public d(b bVar, p5 p5Var) {
        this.c = bVar;
        this.b = p5Var;
    }

    public final void run() {
        this.b.h();
        if (h9.a()) {
            l4 i2 = this.b.i();
            i2.o();
            ResourcesFlusher.b(this);
            i2.a((p4<?>) new p4(i2, this, "Task exception on worker thread"));
            return;
        }
        boolean z = this.c.c != 0;
        this.c.c = 0;
        if (z) {
            this.c.a();
        }
    }
}
