package j.c.a.a.f.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class b extends Handler {
    public b(Looper looper) {
        super(looper);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }

    public b(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
