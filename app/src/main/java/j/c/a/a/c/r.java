package j.c.a.a.c;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.d0;
import j.c.a.a.c.l.e0;
import j.c.a.a.d.b;
import javax.annotation.CheckReturnValue;

@CheckReturnValue
public final class r {
    public static volatile d0 a;
    public static final Object b = new Object();
    public static Context c;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void a(android.content.Context r2) {
        /*
            java.lang.Class<j.c.a.a.c.r> r0 = j.c.a.a.c.r.class
            monitor-enter(r0)
            android.content.Context r1 = j.c.a.a.c.r.c     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0011
            if (r2 == 0) goto L_0x0018
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x001a }
            j.c.a.a.c.r.c = r2     // Catch:{ all -> 0x001a }
            monitor-exit(r0)
            return
        L_0x0011:
            java.lang.String r2 = "GoogleCertificates"
            java.lang.String r1 = "GoogleCertificates has been initialized already"
            android.util.Log.w(r2, r1)     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r0)
            return
        L_0x001a:
            r2 = move-exception
            monitor-exit(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.r.a(android.content.Context):void");
    }

    public static b0 b(String str, t tVar, boolean z, boolean z2) {
        try {
            if (a == null) {
                ResourcesFlusher.b(c);
                synchronized (b) {
                    if (a == null) {
                        a = e0.a(DynamiteModule.a(c, DynamiteModule.f380j, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            ResourcesFlusher.b(c);
            try {
                if (a.a(new z(str, tVar, z, z2), new b(c.getPackageManager()))) {
                    return b0.d;
                }
                return b0.a(new s(z, str, tVar));
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
                return new b0(false, "module call", e2);
            }
        } catch (DynamiteModule.LoadingException e3) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e3);
            String valueOf = String.valueOf(e3.getMessage());
            return new b0(false, valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e3);
        }
    }

    public static b0 a(String str, t tVar, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return b(str, tVar, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
