package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class e0 extends x3<e0, a> implements g5 {
    public static final e0 zzh;
    public static volatile m5<e0> zzi;
    public int zzc;
    public int zzd;
    public String zze = "";
    public boolean zzf;
    public e4<String> zzg = s5.f1905e;

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<e0, a> implements g5 {
        public /* synthetic */ a(f0 f0Var) {
            super(e0.zzh);
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public enum b implements b4 {
        UNKNOWN_MATCH_TYPE(0),
        REGEXP(1),
        BEGINS_WITH(2),
        ENDS_WITH(3),
        PARTIAL(4),
        EXACT(5),
        IN_LIST(6);
        
        public static final a4<b> zzh = new i0();
        public final int zzi;

        /* access modifiers changed from: public */
        b(int i2) {
            this.zzi = i2;
        }

        public static d4 b() {
            return m0.a;
        }

        public final int a() {
            return this.zzi;
        }

        public final String toString() {
            return "<" + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzi + " name=" + name() + '>';
        }

        public static b a(int i2) {
            switch (i2) {
                case 0:
                    return UNKNOWN_MATCH_TYPE;
                case 1:
                    return REGEXP;
                case 2:
                    return BEGINS_WITH;
                case 3:
                    return ENDS_WITH;
                case 4:
                    return PARTIAL;
                case 5:
                    return EXACT;
                case 6:
                    return IN_LIST;
                default:
                    return null;
            }
        }
    }

    static {
        e0 e0Var = new e0();
        zzh = e0Var;
        x3.zzd.put(e0.class, e0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (f0.a[i2 - 1]) {
            case 1:
                return new e0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0001\u0000\u0001\f\u0000\u0002\b\u0001\u0003\u0007\u0002\u0004\u001a", new Object[]{"zzc", "zzd", b.b(), "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                m5<e0> m5Var = zzi;
                if (m5Var == null) {
                    synchronized (e0.class) {
                        m5Var = zzi;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzh);
                            zzi = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    public final b i() {
        b a2 = b.a(this.zzd);
        return a2 == null ? b.UNKNOWN_MATCH_TYPE : a2;
    }

    public final int j() {
        return this.zzg.size();
    }
}
