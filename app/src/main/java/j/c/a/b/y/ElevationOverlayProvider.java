package j.c.a.b.y;

import android.content.Context;
import j.c.a.a.c.n.c;
import j.c.a.b.b;

public class ElevationOverlayProvider {
    public final boolean a;
    public final int b;
    public final int c;
    public final float d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      j.c.a.a.c.n.c.a(float, float, float):float
      j.c.a.a.c.n.c.a(int, int, float):int
      j.c.a.a.c.n.c.a(android.content.Context, int, int):int
      j.c.a.a.c.n.c.a(android.content.Context, int, java.lang.String):int
      j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(byte[], int, int):long
      j.c.a.a.c.n.c.a(android.content.Context, android.content.res.TypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.content.Context, i.b.q.TintTypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList, android.graphics.PorterDuff$Mode):android.graphics.PorterDuffColorFilter
      j.c.a.a.c.n.c.a(j.c.a.a.i.e, long, java.util.concurrent.TimeUnit):TResult
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[], byte[]):java.security.interfaces.ECPublicKey
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(byte[], long, int):void
      j.c.a.a.c.n.c.a(l.b.ObservableSource, l.b.Observer, l.b.t.Function):boolean
      j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean */
    public ElevationOverlayProvider(Context context) {
        this.a = c.a(context, b.elevationOverlayEnabled, false);
        this.b = c.a(context, b.elevationOverlayColor, 0);
        this.c = c.a(context, b.colorSurface, 0);
        this.d = context.getResources().getDisplayMetrics().density;
    }
}
