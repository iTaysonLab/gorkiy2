package j.c.a.b.h0;

import android.animation.ValueAnimator;
import com.google.android.material.snackbar.BaseTransientBottomBar;

/* compiled from: BaseTransientBottomBar */
public class BaseTransientBottomBar4 implements ValueAnimator.AnimatorUpdateListener {
    public int a = this.b;
    public final /* synthetic */ int b;
    public final /* synthetic */ BaseTransientBottomBar c;

    public BaseTransientBottomBar4(BaseTransientBottomBar baseTransientBottomBar, int i2) {
        this.c = baseTransientBottomBar;
        this.b = i2;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int intValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        this.c.c.setTranslationY((float) intValue);
        this.a = intValue;
    }
}
