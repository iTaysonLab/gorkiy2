package j.c.a.b.t;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import androidx.cardview.widget.CardView;
import com.google.android.material.card.MaterialCardView;
import i.b.k.ResourcesFlusher;
import i.d.e.CardViewApi21Impl;
import j.c.a.b.d;
import j.c.a.b.e0.RippleUtils;
import j.c.a.b.f;
import j.c.a.b.g0.CornerTreatment;
import j.c.a.b.g0.CutCornerTreatment;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.RoundedCornerTreatment;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.k;
import j.c.a.b.l;

public class MaterialCardViewHelper {

    /* renamed from: t  reason: collision with root package name */
    public static final int[] f2340t = {16842912};
    public static final double u = Math.cos(Math.toRadians(45.0d));
    public final MaterialCardView a;
    public final Rect b = new Rect();
    public final MaterialShapeDrawable c;
    public final MaterialShapeDrawable d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2341e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2342f;
    public int g;
    public Drawable h;

    /* renamed from: i  reason: collision with root package name */
    public Drawable f2343i;

    /* renamed from: j  reason: collision with root package name */
    public ColorStateList f2344j;

    /* renamed from: k  reason: collision with root package name */
    public ColorStateList f2345k;

    /* renamed from: l  reason: collision with root package name */
    public ShapeAppearanceModel f2346l;

    /* renamed from: m  reason: collision with root package name */
    public ColorStateList f2347m;

    /* renamed from: n  reason: collision with root package name */
    public Drawable f2348n;

    /* renamed from: o  reason: collision with root package name */
    public LayerDrawable f2349o;

    /* renamed from: p  reason: collision with root package name */
    public MaterialShapeDrawable f2350p;

    /* renamed from: q  reason: collision with root package name */
    public MaterialShapeDrawable f2351q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f2352r = false;

    /* renamed from: s  reason: collision with root package name */
    public boolean f2353s;

    public class a extends InsetDrawable {
        public a(MaterialCardViewHelper materialCardViewHelper, Drawable drawable, int i2, int i3, int i4, int i5) {
            super(drawable, i2, i3, i4, i5);
        }

        public int getMinimumHeight() {
            return -1;
        }

        public int getMinimumWidth() {
            return -1;
        }

        public boolean getPadding(Rect rect) {
            return false;
        }
    }

    public MaterialCardViewHelper(MaterialCardView materialCardView, AttributeSet attributeSet, int i2, int i3) {
        this.a = materialCardView;
        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(ShapeAppearanceModel.a(materialCardView.getContext(), attributeSet, i2, i3).a());
        this.c = materialShapeDrawable;
        materialShapeDrawable.a(materialCardView.getContext());
        this.c.b(-12303292);
        ShapeAppearanceModel shapeAppearanceModel = this.c.b.a;
        if (shapeAppearanceModel != null) {
            ShapeAppearanceModel.b bVar = new ShapeAppearanceModel.b(shapeAppearanceModel);
            TypedArray obtainStyledAttributes = materialCardView.getContext().obtainStyledAttributes(attributeSet, l.CardView, i2, k.CardView);
            if (obtainStyledAttributes.hasValue(l.CardView_cardCornerRadius)) {
                float dimension = obtainStyledAttributes.getDimension(l.CardView_cardCornerRadius, 0.0f);
                bVar.c(dimension);
                bVar.d(dimension);
                bVar.b(dimension);
                bVar.a(dimension);
            }
            this.d = new MaterialShapeDrawable();
            a(bVar.a());
            Resources resources = materialCardView.getResources();
            this.f2341e = resources.getDimensionPixelSize(d.mtrl_card_checked_icon_margin);
            this.f2342f = resources.getDimensionPixelSize(d.mtrl_card_checked_icon_size);
            obtainStyledAttributes.recycle();
            return;
        }
        throw null;
    }

    public void a(ShapeAppearanceModel shapeAppearanceModel) {
        this.f2346l = shapeAppearanceModel;
        MaterialShapeDrawable materialShapeDrawable = this.c;
        materialShapeDrawable.b.a = shapeAppearanceModel;
        materialShapeDrawable.invalidateSelf();
        MaterialShapeDrawable materialShapeDrawable2 = this.c;
        materialShapeDrawable2.w = !materialShapeDrawable2.h();
        MaterialShapeDrawable materialShapeDrawable3 = this.d;
        if (materialShapeDrawable3 != null) {
            materialShapeDrawable3.b.a = shapeAppearanceModel;
            materialShapeDrawable3.invalidateSelf();
        }
        MaterialShapeDrawable materialShapeDrawable4 = this.f2351q;
        if (materialShapeDrawable4 != null) {
            materialShapeDrawable4.b.a = shapeAppearanceModel;
            materialShapeDrawable4.invalidateSelf();
        }
        MaterialShapeDrawable materialShapeDrawable5 = this.f2350p;
        if (materialShapeDrawable5 != null) {
            materialShapeDrawable5.b.a = shapeAppearanceModel;
            materialShapeDrawable5.invalidateSelf();
        }
    }

    public void b(Drawable drawable) {
        this.f2343i = drawable;
        if (drawable != null) {
            Drawable d2 = ResourcesFlusher.d(drawable.mutate());
            this.f2343i = d2;
            d2.setTintList(this.f2345k);
        }
        if (this.f2349o != null) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            Drawable drawable2 = this.f2343i;
            if (drawable2 != null) {
                stateListDrawable.addState(f2340t, drawable2);
            }
            this.f2349o.setDrawableByLayerId(f.mtrl_card_checked_layer_id, stateListDrawable);
        }
    }

    public final boolean c() {
        return this.a.getPreventCornerOverlap() && !this.c.h();
    }

    public final boolean d() {
        return this.a.getPreventCornerOverlap() && this.c.h() && this.a.getUseCompatPadding();
    }

    public void e() {
        float f2 = 0.0f;
        float a2 = c() || d() ? a() : 0.0f;
        if (this.a.getPreventCornerOverlap() && this.a.getUseCompatPadding()) {
            f2 = (float) ((1.0d - u) * ((double) this.a.getCardViewRadius()));
        }
        int i2 = (int) (a2 - f2);
        MaterialCardView materialCardView = this.a;
        Rect rect = this.b;
        materialCardView.f147f.set(rect.left + i2, rect.top + i2, rect.right + i2, rect.bottom + i2);
        ((CardViewApi21Impl) CardView.f145j).d(materialCardView.h);
    }

    public void f() {
        if (!this.f2352r) {
            this.a.setBackgroundInternal(a(this.c));
        }
        this.a.setForeground(a(this.h));
    }

    public final void g() {
        Drawable drawable;
        if (!RippleUtils.a || (drawable = this.f2348n) == null) {
            MaterialShapeDrawable materialShapeDrawable = this.f2350p;
            if (materialShapeDrawable != null) {
                materialShapeDrawable.a(this.f2344j);
                return;
            }
            return;
        }
        ((RippleDrawable) drawable).setColor(this.f2344j);
    }

    public void h() {
        this.d.a((float) this.g, this.f2347m);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final Drawable b() {
        RippleDrawable rippleDrawable;
        if (this.f2348n == null) {
            if (RippleUtils.a) {
                this.f2351q = new MaterialShapeDrawable(this.f2346l);
                rippleDrawable = new RippleDrawable(this.f2344j, null, this.f2351q);
            } else {
                StateListDrawable stateListDrawable = new StateListDrawable();
                MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(this.f2346l);
                this.f2350p = materialShapeDrawable;
                materialShapeDrawable.a(this.f2344j);
                stateListDrawable.addState(new int[]{16842919}, this.f2350p);
                rippleDrawable = stateListDrawable;
            }
            this.f2348n = rippleDrawable;
        }
        if (this.f2349o == null) {
            StateListDrawable stateListDrawable2 = new StateListDrawable();
            Drawable drawable = this.f2343i;
            if (drawable != null) {
                stateListDrawable2.addState(f2340t, drawable);
            }
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{this.f2348n, this.d, stateListDrawable2});
            this.f2349o = layerDrawable;
            layerDrawable.setId(2, f.mtrl_card_checked_layer_id);
        }
        return this.f2349o;
    }

    public final Drawable a(Drawable drawable) {
        int i2;
        int i3;
        if (0 != 0 || this.a.getUseCompatPadding()) {
            float f2 = 0.0f;
            int ceil = (int) Math.ceil((double) ((this.a.getMaxCardElevation() * 1.5f) + (d() ? a() : 0.0f)));
            float maxCardElevation = this.a.getMaxCardElevation();
            if (d()) {
                f2 = a();
            }
            i2 = ceil;
            i3 = (int) Math.ceil((double) (maxCardElevation + f2));
        } else {
            i3 = 0;
            i2 = 0;
        }
        return new a(this, drawable, i3, i2, i3, i2);
    }

    public final float a() {
        float a2 = a(this.f2346l.a, this.c.f());
        CornerTreatment cornerTreatment = this.f2346l.b;
        MaterialShapeDrawable materialShapeDrawable = this.c;
        float max = Math.max(a2, a(cornerTreatment, materialShapeDrawable.b.a.f2243f.a(materialShapeDrawable.b())));
        CornerTreatment cornerTreatment2 = this.f2346l.c;
        MaterialShapeDrawable materialShapeDrawable2 = this.c;
        float a3 = a(cornerTreatment2, materialShapeDrawable2.b.a.g.a(materialShapeDrawable2.b()));
        CornerTreatment cornerTreatment3 = this.f2346l.d;
        MaterialShapeDrawable materialShapeDrawable3 = this.c;
        return Math.max(max, Math.max(a3, a(cornerTreatment3, materialShapeDrawable3.b.a.h.a(materialShapeDrawable3.b()))));
    }

    public final float a(CornerTreatment cornerTreatment, float f2) {
        if (cornerTreatment instanceof RoundedCornerTreatment) {
            return (float) ((1.0d - u) * ((double) f2));
        }
        if (cornerTreatment instanceof CutCornerTreatment) {
            return f2 / 2.0f;
        }
        return 0.0f;
    }
}
