package j.c.a.b.a0;

import android.view.ViewTreeObserver;

/* compiled from: FloatingActionButtonImpl */
public class FloatingActionButtonImpl3 implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ FloatingActionButtonImpl1 b;

    public FloatingActionButtonImpl3(FloatingActionButtonImpl1 floatingActionButtonImpl1) {
        this.b = floatingActionButtonImpl1;
    }

    public boolean onPreDraw() {
        FloatingActionButtonImpl1 floatingActionButtonImpl1 = this.b;
        float rotation = floatingActionButtonImpl1.f2162t.getRotation();
        if (floatingActionButtonImpl1.f2156n == rotation) {
            return true;
        }
        floatingActionButtonImpl1.f2156n = rotation;
        floatingActionButtonImpl1.l();
        return true;
    }
}
