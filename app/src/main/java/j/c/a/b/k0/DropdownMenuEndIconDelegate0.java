package j.c.a.b.k0;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;

/* compiled from: DropdownMenuEndIconDelegate */
public class DropdownMenuEndIconDelegate0 implements View.OnTouchListener {
    public final /* synthetic */ AutoCompleteTextView b;
    public final /* synthetic */ DropdownMenuEndIconDelegate c;

    public DropdownMenuEndIconDelegate0(DropdownMenuEndIconDelegate dropdownMenuEndIconDelegate, AutoCompleteTextView autoCompleteTextView) {
        this.c = dropdownMenuEndIconDelegate;
        this.b = autoCompleteTextView;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            if (this.c.c()) {
                this.c.g = false;
            }
            DropdownMenuEndIconDelegate.a(this.c, this.b);
            view.performClick();
        }
        return false;
    }
}
