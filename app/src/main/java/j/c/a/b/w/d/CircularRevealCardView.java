package j.c.a.b.w.d;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.google.android.material.card.MaterialCardView;
import j.c.a.b.w.CircularRevealWidget;

public class CircularRevealCardView extends MaterialCardView implements CircularRevealWidget {
    public void a() {
        throw null;
    }

    public void b() {
        throw null;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    public Drawable getCircularRevealOverlayDrawable() {
        throw null;
    }

    public int getCircularRevealScrimColor() {
        throw null;
    }

    public CircularRevealWidget.e getRevealInfo() {
        throw null;
    }

    public boolean isOpaque() {
        return super.isOpaque();
    }

    public void setCircularRevealOverlayDrawable(Drawable drawable) {
        throw null;
    }

    public void setCircularRevealScrimColor(int i2) {
        throw null;
    }

    public void setRevealInfo(CircularRevealWidget.e eVar) {
        throw null;
    }
}
