package j.c.a.b.n0;

import android.animation.ValueAnimator;
import android.view.View;

public class FabTransformationBehavior implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ View a;

    public FabTransformationBehavior(com.google.android.material.transformation.FabTransformationBehavior fabTransformationBehavior, View view) {
        this.a = view;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.a.invalidate();
    }
}
