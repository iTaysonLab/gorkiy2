package j.c.a.b.o;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.b0.TextDrawableHelper;
import j.c.a.b.b0.ThemeEnforcement;
import j.c.a.b.d;
import j.c.a.b.d0.TextAppearance;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.i;
import j.c.a.b.j;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.o.a;
import java.lang.ref.WeakReference;

public class BadgeDrawable extends Drawable implements TextDrawableHelper.b {

    /* renamed from: r  reason: collision with root package name */
    public static final int f2305r = k.Widget_MaterialComponents_Badge;

    /* renamed from: s  reason: collision with root package name */
    public static final int f2306s = b.badgeStyle;
    public final WeakReference<Context> b;
    public final MaterialShapeDrawable c = new MaterialShapeDrawable();
    public final TextDrawableHelper d;

    /* renamed from: e  reason: collision with root package name */
    public final Rect f2307e = new Rect();

    /* renamed from: f  reason: collision with root package name */
    public final float f2308f;
    public final float g;
    public final float h;

    /* renamed from: i  reason: collision with root package name */
    public final a f2309i;

    /* renamed from: j  reason: collision with root package name */
    public float f2310j;

    /* renamed from: k  reason: collision with root package name */
    public float f2311k;

    /* renamed from: l  reason: collision with root package name */
    public int f2312l;

    /* renamed from: m  reason: collision with root package name */
    public float f2313m;

    /* renamed from: n  reason: collision with root package name */
    public float f2314n;

    /* renamed from: o  reason: collision with root package name */
    public float f2315o;

    /* renamed from: p  reason: collision with root package name */
    public WeakReference<View> f2316p;

    /* renamed from: q  reason: collision with root package name */
    public WeakReference<ViewGroup> f2317q;

    public BadgeDrawable(Context context) {
        TextAppearance textAppearance;
        Context context2;
        this.b = new WeakReference<>(context);
        ThemeEnforcement.a(context, ThemeEnforcement.b, "Theme.MaterialComponents");
        Resources resources = context.getResources();
        this.f2308f = (float) resources.getDimensionPixelSize(d.mtrl_badge_radius);
        this.h = (float) resources.getDimensionPixelSize(d.mtrl_badge_long_text_horizontal_padding);
        this.g = (float) resources.getDimensionPixelSize(d.mtrl_badge_with_text_radius);
        TextDrawableHelper textDrawableHelper = new TextDrawableHelper(this);
        this.d = textDrawableHelper;
        textDrawableHelper.a.setTextAlign(Paint.Align.CENTER);
        this.f2309i = new a(context);
        int i2 = k.TextAppearance_MaterialComponents_Badge;
        Context context3 = this.b.get();
        if (context3 != null && this.d.f2193f != (textAppearance = new TextAppearance(context3, i2)) && (context2 = this.b.get()) != null) {
            this.d.a(textAppearance, context2);
            e();
        }
    }

    public void a() {
        invalidateSelf();
    }

    public final String b() {
        if (c() <= this.f2312l) {
            return Integer.toString(c());
        }
        Context context = this.b.get();
        if (context == null) {
            return "";
        }
        return context.getString(j.mtrl_exceed_max_badge_number_suffix, Integer.valueOf(this.f2312l), "+");
    }

    public int c() {
        if (!d()) {
            return 0;
        }
        return this.f2309i.f2318e;
    }

    public boolean d() {
        return this.f2309i.f2318e != -1;
    }

    public void draw(Canvas canvas) {
        if (!getBounds().isEmpty() && this.f2309i.d != 0 && isVisible()) {
            this.c.draw(canvas);
            if (d()) {
                Rect rect = new Rect();
                String b2 = b();
                this.d.a.getTextBounds(b2, 0, b2.length(), rect);
                canvas.drawText(b2, this.f2310j, this.f2311k + ((float) (rect.height() / 2)), this.d.a);
            }
        }
    }

    public final void e() {
        float f2;
        float f3;
        Context context = this.b.get();
        WeakReference<View> weakReference = this.f2316p;
        ViewGroup viewGroup = null;
        View view = weakReference != null ? weakReference.get() : null;
        if (context != null && view != null) {
            Rect rect = new Rect();
            rect.set(this.f2307e);
            Rect rect2 = new Rect();
            view.getDrawingRect(rect2);
            WeakReference<ViewGroup> weakReference2 = this.f2317q;
            if (weakReference2 != null) {
                viewGroup = weakReference2.get();
            }
            if (viewGroup != null) {
                if (viewGroup == null) {
                    viewGroup = (ViewGroup) view.getParent();
                }
                viewGroup.offsetDescendantRectToMyCoords(view, rect2);
            }
            a aVar = this.f2309i;
            int i2 = aVar.f2320i;
            if (i2 == 8388691 || i2 == 8388693) {
                this.f2311k = (float) (rect2.bottom - this.f2309i.f2322k);
            } else {
                this.f2311k = (float) (rect2.top + aVar.f2322k);
            }
            if (c() <= 9) {
                float f4 = !d() ? this.f2308f : this.g;
                this.f2313m = f4;
                this.f2315o = f4;
                this.f2314n = f4;
            } else {
                float f5 = this.g;
                this.f2313m = f5;
                this.f2315o = f5;
                this.f2314n = (this.d.a(b()) / 2.0f) + this.h;
            }
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(d() ? d.mtrl_badge_text_horizontal_edge_offset : d.mtrl_badge_horizontal_edge_offset);
            int i3 = this.f2309i.f2320i;
            if (i3 == 8388659 || i3 == 8388691) {
                if (ViewCompat.k(view) == 0) {
                    f2 = (((float) rect2.left) - this.f2314n) + ((float) dimensionPixelSize) + ((float) this.f2309i.f2321j);
                } else {
                    f2 = ((((float) rect2.right) + this.f2314n) - ((float) dimensionPixelSize)) - ((float) this.f2309i.f2321j);
                }
                this.f2310j = f2;
            } else {
                if (ViewCompat.k(view) == 0) {
                    f3 = ((((float) rect2.right) + this.f2314n) - ((float) dimensionPixelSize)) - ((float) this.f2309i.f2321j);
                } else {
                    f3 = (((float) rect2.left) - this.f2314n) + ((float) dimensionPixelSize) + ((float) this.f2309i.f2321j);
                }
                this.f2310j = f3;
            }
            Rect rect3 = this.f2307e;
            float f6 = this.f2310j;
            float f7 = this.f2311k;
            float f8 = this.f2314n;
            float f9 = this.f2315o;
            rect3.set((int) (f6 - f8), (int) (f7 - f9), (int) (f6 + f8), (int) (f7 + f9));
            MaterialShapeDrawable materialShapeDrawable = this.c;
            materialShapeDrawable.setShapeAppearanceModel(materialShapeDrawable.b.a.a(this.f2313m));
            if (!rect.equals(this.f2307e)) {
                this.c.setBounds(this.f2307e);
            }
        }
    }

    public int getAlpha() {
        return this.f2309i.d;
    }

    public int getIntrinsicHeight() {
        return this.f2307e.height();
    }

    public int getIntrinsicWidth() {
        return this.f2307e.width();
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isStateful() {
        return false;
    }

    public boolean onStateChange(int[] iArr) {
        return super.onStateChange(iArr);
    }

    public void setAlpha(int i2) {
        this.f2309i.d = i2;
        this.d.a.setAlpha(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public static final class a implements Parcelable {
        public static final Parcelable.Creator<a.a> CREATOR = new C0028a();
        public int b;
        public int c;
        public int d = 255;

        /* renamed from: e  reason: collision with root package name */
        public int f2318e = -1;

        /* renamed from: f  reason: collision with root package name */
        public int f2319f;
        public CharSequence g;
        public int h;

        /* renamed from: i  reason: collision with root package name */
        public int f2320i;

        /* renamed from: j  reason: collision with root package name */
        public int f2321j;

        /* renamed from: k  reason: collision with root package name */
        public int f2322k;

        /* renamed from: j.c.a.b.o.BadgeDrawable$a$a  reason: collision with other inner class name */
        public static class C0028a implements Parcelable.Creator<a.a> {
            public Object createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            public Object[] newArray(int i2) {
                return new a[i2];
            }
        }

        public a(Context context) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(k.TextAppearance_MaterialComponents_Badge, l.TextAppearance);
            obtainStyledAttributes.getDimension(l.TextAppearance_android_textSize, 0.0f);
            ColorStateList a = c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColor);
            c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColorHint);
            c.a(context, obtainStyledAttributes, l.TextAppearance_android_textColorLink);
            obtainStyledAttributes.getInt(l.TextAppearance_android_textStyle, 0);
            obtainStyledAttributes.getInt(l.TextAppearance_android_typeface, 1);
            int i2 = l.TextAppearance_fontFamily;
            i2 = !obtainStyledAttributes.hasValue(i2) ? l.TextAppearance_android_fontFamily : i2;
            obtainStyledAttributes.getResourceId(i2, 0);
            obtainStyledAttributes.getString(i2);
            obtainStyledAttributes.getBoolean(l.TextAppearance_textAllCaps, false);
            c.a(context, obtainStyledAttributes, l.TextAppearance_android_shadowColor);
            obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowDx, 0.0f);
            obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowDy, 0.0f);
            obtainStyledAttributes.getFloat(l.TextAppearance_android_shadowRadius, 0.0f);
            obtainStyledAttributes.recycle();
            this.c = a.getDefaultColor();
            this.g = context.getString(j.mtrl_badge_numberless_content_description);
            this.h = i.mtrl_badge_content_description;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            parcel.writeInt(this.f2318e);
            parcel.writeInt(this.f2319f);
            parcel.writeString(this.g.toString());
            parcel.writeInt(this.h);
            parcel.writeInt(this.f2320i);
            parcel.writeInt(this.f2321j);
            parcel.writeInt(this.f2322k);
        }

        public a(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            this.f2318e = parcel.readInt();
            this.f2319f = parcel.readInt();
            this.g = parcel.readString();
            this.h = parcel.readInt();
            this.f2320i = parcel.readInt();
            this.f2321j = parcel.readInt();
            this.f2322k = parcel.readInt();
        }
    }
}
