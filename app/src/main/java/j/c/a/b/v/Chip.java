package j.c.a.b.v;

import android.annotation.TargetApi;
import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;

public class Chip extends ViewOutlineProvider {
    public final /* synthetic */ com.google.android.material.chip.Chip a;

    public Chip(com.google.android.material.chip.Chip chip) {
        this.a = chip;
    }

    @TargetApi(21)
    public void getOutline(View view, Outline outline) {
        ChipDrawable chipDrawable = this.a.f448e;
        if (chipDrawable != null) {
            chipDrawable.getOutline(outline);
        } else {
            outline.setAlpha(0.0f);
        }
    }
}
