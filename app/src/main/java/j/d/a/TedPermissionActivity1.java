package j.d.a;

import android.content.DialogInterface;
import com.gun0912.tedpermission.TedPermissionActivity;
import java.util.List;

/* compiled from: TedPermissionActivity */
public class TedPermissionActivity1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ List b;
    public final /* synthetic */ TedPermissionActivity c;

    public TedPermissionActivity1(TedPermissionActivity tedPermissionActivity, List list) {
        this.c = tedPermissionActivity;
        this.b = list;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        this.c.a(this.b);
    }
}
