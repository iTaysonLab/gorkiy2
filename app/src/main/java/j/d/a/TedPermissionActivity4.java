package j.d.a;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.gun0912.tedpermission.TedPermissionActivity;

/* compiled from: TedPermissionActivity */
public class TedPermissionActivity4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ TedPermissionActivity b;

    public TedPermissionActivity4(TedPermissionActivity tedPermissionActivity) {
        this.b = tedPermissionActivity;
    }

    @TargetApi(23)
    public void onClick(DialogInterface dialogInterface, int i2) {
        this.b.startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", Uri.fromParts("package", this.b.v, null)), 31);
    }
}
