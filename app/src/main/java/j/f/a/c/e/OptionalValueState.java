package j.f.a.c.e;

import com.crashlytics.android.answers.SessionEventTransform;
import j.a.a.a.outline;
import j.f.a.c.Next;
import j.f.a.c.State;
import kotlin.NoWhenBranchMatchedException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: OptionalValueState.kt */
public final class OptionalValueState extends State {
    public final a b;

    /* compiled from: OptionalValueState.kt */
    public static abstract class a {

        /* renamed from: j.f.a.c.e.OptionalValueState$a$a  reason: collision with other inner class name */
        /* compiled from: OptionalValueState.kt */
        public static final class C0031a extends a {
            public C0031a() {
                super(null);
            }
        }

        /* compiled from: OptionalValueState.kt */
        public static final class b extends a {
            public final char a;
            public final String b;
        }

        /* compiled from: OptionalValueState.kt */
        public static final class c extends a {
            public c() {
                super(null);
            }
        }

        /* compiled from: OptionalValueState.kt */
        public static final class d extends a {
            public d() {
                super(null);
            }
        }

        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OptionalValueState(State state, a aVar) {
        super(super);
        if (state == null) {
            Intrinsics.a("child");
            throw null;
        } else if (aVar != null) {
            this.b = aVar;
        } else {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
     arg types: [java.lang.String, char, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
    public Next a(char c) {
        boolean z;
        a aVar = this.b;
        if (aVar instanceof a.d) {
            z = Character.isDigit(c);
        } else if (aVar instanceof a.c) {
            z = Character.isLetter(c);
        } else if (aVar instanceof a.C0031a) {
            z = Character.isLetterOrDigit(c);
        } else if (aVar instanceof a.b) {
            z = Indent.a((CharSequence) ((a.b) aVar).b, c, false, 2);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        if (z) {
            return new Next(b(), Character.valueOf(c), true, Character.valueOf(c));
        }
        return new Next(b(), null, false, null);
    }

    public String toString() {
        a aVar = this.b;
        String str = "null";
        if (aVar instanceof a.c) {
            StringBuilder a2 = outline.a("[a] -> ");
            State state = super.a;
            if (state != null) {
                str = super.toString();
            }
            a2.append(str);
            return a2.toString();
        } else if (aVar instanceof a.d) {
            StringBuilder a3 = outline.a("[9] -> ");
            State state2 = super.a;
            if (state2 != null) {
                str = super.toString();
            }
            a3.append(str);
            return a3.toString();
        } else if (aVar instanceof a.C0031a) {
            StringBuilder a4 = outline.a("[-] -> ");
            State state3 = super.a;
            if (state3 != null) {
                str = super.toString();
            }
            a4.append(str);
            return a4.toString();
        } else if (aVar instanceof a.b) {
            StringBuilder a5 = outline.a("[");
            a5.append(((a.b) this.b).a);
            a5.append("] -> ");
            State state4 = super.a;
            if (state4 != null) {
                str = super.toString();
            }
            a5.append(str);
            return a5.toString();
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }
}
