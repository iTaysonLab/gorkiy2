package j.b.a.n;

import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import androidx.fragment.app.Fragment;
import i.e.ArrayMap;
import i.l.a.BackStackRecord;
import i.l.a.FragmentActivity;
import i.l.a.FragmentManagerImpl;
import i.l.a.j;
import j.b.a.Glide;
import j.b.a.RequestManager;
import j.b.a.s.Util;
import java.util.HashMap;
import java.util.Map;

public class RequestManagerRetriever implements Handler.Callback {

    /* renamed from: f  reason: collision with root package name */
    public static final b f1742f = new a();
    public volatile RequestManager a;
    public final Map<FragmentManager, k> b = new HashMap();
    public final Map<j, o> c = new HashMap();
    public final Handler d;

    /* renamed from: e  reason: collision with root package name */
    public final b f1743e;

    public class a implements b {
    }

    public interface b {
    }

    public RequestManagerRetriever(b bVar) {
        new ArrayMap();
        new ArrayMap();
        new Bundle();
        this.f1743e = bVar == null ? f1742f : bVar;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    public static Activity c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return c(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    public static boolean d(Context context) {
        Activity c2 = c(context);
        return c2 == null || !c2.isFinishing();
    }

    public RequestManager a(Context context) {
        if (context != null) {
            if (Util.c() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    FragmentActivity fragmentActivity = (FragmentActivity) context;
                    if (Util.b()) {
                        return a(fragmentActivity.getApplicationContext());
                    }
                    if (!fragmentActivity.isDestroyed()) {
                        SupportRequestManagerFragment a2 = a(fragmentActivity.j(), (Fragment) null, d(fragmentActivity));
                        RequestManager requestManager = a2.b0;
                        if (requestManager != null) {
                            return requestManager;
                        }
                        Glide a3 = Glide.a(fragmentActivity);
                        b bVar = this.f1743e;
                        ActivityFragmentLifecycle activityFragmentLifecycle = a2.X;
                        RequestManagerTreeNode requestManagerTreeNode = a2.Y;
                        if (((a) bVar) != null) {
                            RequestManager requestManager2 = new RequestManager(a3, activityFragmentLifecycle, requestManagerTreeNode, fragmentActivity);
                            a2.b0 = requestManager2;
                            return requestManager2;
                        }
                        throw null;
                    }
                    throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
                } else if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    if (Util.b()) {
                        return a(activity.getApplicationContext());
                    }
                    if (!activity.isDestroyed()) {
                        RequestManagerFragment a4 = a(activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
                        RequestManager requestManager3 = a4.f1740e;
                        if (requestManager3 != null) {
                            return requestManager3;
                        }
                        Glide a5 = Glide.a(activity);
                        b bVar2 = this.f1743e;
                        ActivityFragmentLifecycle activityFragmentLifecycle2 = a4.b;
                        RequestManagerTreeNode requestManagerTreeNode2 = a4.c;
                        if (((a) bVar2) != null) {
                            RequestManager requestManager4 = new RequestManager(a5, activityFragmentLifecycle2, requestManagerTreeNode2, activity);
                            a4.f1740e = requestManager4;
                            return requestManager4;
                        }
                        throw null;
                    }
                    throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
                } else if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return a(contextWrapper.getBaseContext());
                    }
                }
            }
            return b(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    public final RequestManager b(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    Glide a2 = Glide.a(context.getApplicationContext());
                    b bVar = this.f1743e;
                    ApplicationLifecycle applicationLifecycle = new ApplicationLifecycle();
                    EmptyRequestManagerTreeNode emptyRequestManagerTreeNode = new EmptyRequestManagerTreeNode();
                    Context applicationContext = context.getApplicationContext();
                    if (((a) bVar) != null) {
                        this.a = new RequestManager(a2, applicationLifecycle, emptyRequestManagerTreeNode, applicationContext);
                    } else {
                        throw null;
                    }
                }
            }
        }
        return this.a;
    }

    public boolean handleMessage(Message message) {
        Object obj;
        o oVar;
        Object obj2;
        int i2 = message.what;
        o oVar2 = null;
        boolean z = true;
        if (i2 == 1) {
            obj2 = (FragmentManager) message.obj;
            oVar = this.b.remove(obj2);
        } else if (i2 != 2) {
            z = false;
            obj = null;
            if (z && oVar2 == null && Log.isLoggable("RMRetriever", 5)) {
                Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
            }
            return z;
        } else {
            obj2 = (i.l.a.FragmentManager) message.obj;
            oVar = this.c.remove(obj2);
        }
        Object obj3 = obj2;
        oVar2 = oVar;
        obj = obj3;
        Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
        return z;
    }

    public final RequestManagerFragment a(FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        RequestManagerFragment requestManagerFragment = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (requestManagerFragment == null && (requestManagerFragment = (RequestManagerFragment) this.b.get(fragmentManager)) == null) {
            requestManagerFragment = new RequestManagerFragment();
            requestManagerFragment.g = fragment;
            if (!(fragment == null || fragment.getActivity() == null)) {
                requestManagerFragment.a(fragment.getActivity());
            }
            if (z) {
                requestManagerFragment.b.b();
            }
            this.b.put(fragmentManager, requestManagerFragment);
            fragmentManager.beginTransaction().add(requestManagerFragment, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.d.obtainMessage(1, fragmentManager).sendToTarget();
        }
        return requestManagerFragment;
    }

    public final SupportRequestManagerFragment a(i.l.a.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) fragmentManager.a("com.bumptech.glide.manager");
        if (supportRequestManagerFragment == null && (supportRequestManagerFragment = (SupportRequestManagerFragment) this.c.get(fragmentManager)) == null) {
            supportRequestManagerFragment = new SupportRequestManagerFragment();
            supportRequestManagerFragment.c0 = fragment;
            if (!(fragment == null || fragment.p() == null)) {
                Fragment fragment2 = fragment;
                while (true) {
                    Fragment fragment3 = fragment2.v;
                    if (fragment3 == null) {
                        break;
                    }
                    fragment2 = fragment3;
                }
                FragmentManagerImpl fragmentManagerImpl = fragment2.f232s;
                if (fragmentManagerImpl != null) {
                    supportRequestManagerFragment.a(fragment.p(), fragmentManagerImpl);
                }
            }
            if (z) {
                supportRequestManagerFragment.X.b();
            }
            this.c.put(fragmentManager, supportRequestManagerFragment);
            BackStackRecord backStackRecord = new BackStackRecord((FragmentManagerImpl) fragmentManager);
            backStackRecord.a(0, supportRequestManagerFragment, "com.bumptech.glide.manager", 1);
            backStackRecord.a(true);
            this.d.obtainMessage(2, fragmentManager).sendToTarget();
        }
        return supportRequestManagerFragment;
    }
}
