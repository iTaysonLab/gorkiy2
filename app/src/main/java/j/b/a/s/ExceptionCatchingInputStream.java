package j.b.a.s;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public class ExceptionCatchingInputStream extends InputStream {
    public static final Queue<d> d = Util.a(0);
    public InputStream b;
    public IOException c;

    public static ExceptionCatchingInputStream a(InputStream inputStream) {
        ExceptionCatchingInputStream poll;
        synchronized (d) {
            poll = d.poll();
        }
        if (poll == null) {
            poll = new ExceptionCatchingInputStream();
        }
        poll.b = super;
        return poll;
    }

    public int available() {
        return this.b.available();
    }

    public void close() {
        this.b.close();
    }

    public void mark(int i2) {
        this.b.mark(i2);
    }

    public boolean markSupported() {
        return this.b.markSupported();
    }

    public int read(byte[] bArr) {
        try {
            return this.b.read(bArr);
        } catch (IOException e2) {
            this.c = e2;
            return -1;
        }
    }

    public synchronized void reset() {
        this.b.reset();
    }

    public long skip(long j2) {
        try {
            return this.b.skip(j2);
        } catch (IOException e2) {
            this.c = e2;
            return 0;
        }
    }

    public int read(byte[] bArr, int i2, int i3) {
        try {
            return this.b.read(bArr, i2, i3);
        } catch (IOException e2) {
            this.c = e2;
            return -1;
        }
    }

    public int read() {
        try {
            return this.b.read();
        } catch (IOException e2) {
            this.c = e2;
            return -1;
        }
    }

    public void a() {
        this.c = null;
        this.b = null;
        synchronized (d) {
            d.offer(this);
        }
    }
}
