package j.b.a.q.h;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import j.b.a.q.Request;
import j.b.a.s.Util;

public abstract class CustomTarget<T> implements Target<T> {
    public final int b;
    public final int c;
    public Request d;

    public CustomTarget() {
        if (Util.a((int) RecyclerView.UNDEFINED_DURATION, (int) RecyclerView.UNDEFINED_DURATION)) {
            this.b = RecyclerView.UNDEFINED_DURATION;
            this.c = RecyclerView.UNDEFINED_DURATION;
            return;
        }
        throw new IllegalArgumentException(outline.a("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: ", (int) RecyclerView.UNDEFINED_DURATION, " and height: ", (int) RecyclerView.UNDEFINED_DURATION));
    }

    public void a() {
    }

    public void a(Drawable drawable) {
    }

    public final void a(Request request) {
        this.d = request;
    }

    public final void a(SizeReadyCallback sizeReadyCallback) {
    }

    public void b(Drawable drawable) {
    }

    public final void b(SizeReadyCallback sizeReadyCallback) {
        sizeReadyCallback.a(this.b, this.c);
    }

    public void c() {
    }

    public void d() {
    }

    public final Request b() {
        return this.d;
    }
}
