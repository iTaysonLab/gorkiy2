package j.b.a.m.o.c;

import android.content.Context;
import android.graphics.Bitmap;
import j.b.a.Glide;
import j.b.a.m.Transformation;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.s.Util;

public abstract class BitmapTransformation implements Transformation<Bitmap> {
    public abstract Bitmap a(BitmapPool bitmapPool, Bitmap bitmap, int i2, int i3);

    public final Resource<Bitmap> a(Context context, Resource<Bitmap> resource, int i2, int i3) {
        if (Util.a(i2, i3)) {
            BitmapPool bitmapPool = Glide.a(context).b;
            Bitmap bitmap = resource.get();
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getWidth();
            }
            if (i3 == Integer.MIN_VALUE) {
                i3 = bitmap.getHeight();
            }
            Bitmap a = a(bitmapPool, bitmap, i2, i3);
            return bitmap.equals(a) ? resource : BitmapResource.a(a, bitmapPool);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i2 + " or height: " + i3 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
