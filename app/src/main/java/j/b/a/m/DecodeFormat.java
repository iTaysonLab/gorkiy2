package j.b.a.m;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class DecodeFormat extends Enum<b> {
    public static final /* synthetic */ DecodeFormat[] $VALUES;
    public static final DecodeFormat DEFAULT;
    public static final DecodeFormat PREFER_ARGB_8888 = new DecodeFormat("PREFER_ARGB_8888", 0);
    public static final DecodeFormat PREFER_RGB_565;

    static {
        DecodeFormat decodeFormat = new DecodeFormat("PREFER_RGB_565", 1);
        PREFER_RGB_565 = decodeFormat;
        DecodeFormat decodeFormat2 = PREFER_ARGB_8888;
        $VALUES = new DecodeFormat[]{decodeFormat2, decodeFormat};
        DEFAULT = decodeFormat2;
    }

    public DecodeFormat(String str, int i2) {
    }

    public static DecodeFormat valueOf(String str) {
        return (DecodeFormat) Enum.valueOf(DecodeFormat.class, str);
    }

    public static DecodeFormat[] values() {
        return (DecodeFormat[]) $VALUES.clone();
    }
}
