package j.b.a.m.o.g;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import i.b.k.ResourcesFlusher;
import i.x.a.a.Animatable2Compat;
import j.b.a.Glide;
import j.b.a.m.Transformation;
import j.b.a.m.o.g.GifFrameLoader;

public class GifDrawable extends Drawable implements GifFrameLoader.b, Animatable, Animatable2Compat {
    public final a b;
    public boolean c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1720e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1721f = true;
    public int g;
    public int h = -1;

    /* renamed from: i  reason: collision with root package name */
    public boolean f1722i;

    /* renamed from: j  reason: collision with root package name */
    public Paint f1723j;

    /* renamed from: k  reason: collision with root package name */
    public Rect f1724k;

    public static final class a extends Drawable.ConstantState {
        public final GifFrameLoader a;

        public a(GifFrameLoader gifFrameLoader) {
            this.a = gifFrameLoader;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable() {
            return new GifDrawable(this);
        }

        public Drawable newDrawable(Resources resources) {
            return new GifDrawable(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.o.g.GifDrawable$a, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public GifDrawable(Context context, j.b.a.l.a aVar, Transformation<Bitmap> transformation, int i2, int i3, Bitmap bitmap) {
        a aVar2 = new a(new GifFrameLoader(Glide.a(context), aVar, i2, i3, transformation, bitmap));
        ResourcesFlusher.a((Object) aVar2, "Argument must not be null");
        this.b = aVar2;
    }

    public void a() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        if (callback == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        GifFrameLoader.a aVar = this.b.a.f1728j;
        if ((aVar != null ? aVar.f1737f : -1) == this.b.a.a.e() - 1) {
            this.g++;
        }
        int i2 = this.h;
        if (i2 != -1 && this.g >= i2) {
            stop();
        }
    }

    public Bitmap b() {
        return this.b.a.f1731m;
    }

    public final Paint c() {
        if (this.f1723j == null) {
            this.f1723j = new Paint(2);
        }
        return this.f1723j;
    }

    public final void d() {
        ResourcesFlusher.a(!this.f1720e, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.b.a.a.e() == 1) {
            invalidateSelf();
        } else if (!this.c) {
            this.c = true;
            GifFrameLoader gifFrameLoader = this.b.a;
            if (gifFrameLoader.f1729k) {
                throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
            } else if (!gifFrameLoader.c.contains(this)) {
                boolean isEmpty = gifFrameLoader.c.isEmpty();
                gifFrameLoader.c.add(this);
                if (isEmpty && !gifFrameLoader.f1726f) {
                    gifFrameLoader.f1726f = true;
                    gifFrameLoader.f1729k = false;
                    gifFrameLoader.a();
                }
                invalidateSelf();
            } else {
                throw new IllegalStateException("Cannot subscribe twice in a row");
            }
        }
    }

    public void draw(Canvas canvas) {
        Bitmap bitmap;
        if (!this.f1720e) {
            if (this.f1722i) {
                int intrinsicWidth = getIntrinsicWidth();
                int intrinsicHeight = getIntrinsicHeight();
                Rect bounds = getBounds();
                if (this.f1724k == null) {
                    this.f1724k = new Rect();
                }
                Gravity.apply(119, intrinsicWidth, intrinsicHeight, bounds, this.f1724k);
                this.f1722i = false;
            }
            GifFrameLoader gifFrameLoader = this.b.a;
            GifFrameLoader.a aVar = gifFrameLoader.f1728j;
            if (aVar != null) {
                bitmap = aVar.h;
            } else {
                bitmap = gifFrameLoader.f1731m;
            }
            if (this.f1724k == null) {
                this.f1724k = new Rect();
            }
            canvas.drawBitmap(bitmap, (Rect) null, this.f1724k, c());
        }
    }

    public final void e() {
        this.c = false;
        GifFrameLoader gifFrameLoader = this.b.a;
        gifFrameLoader.c.remove(this);
        if (gifFrameLoader.c.isEmpty()) {
            gifFrameLoader.f1726f = false;
        }
    }

    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    public int getIntrinsicHeight() {
        return this.b.a.f1735q;
    }

    public int getIntrinsicWidth() {
        return this.b.a.f1734p;
    }

    public int getOpacity() {
        return -2;
    }

    public boolean isRunning() {
        return this.c;
    }

    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f1722i = true;
    }

    public void setAlpha(int i2) {
        c().setAlpha(i2);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        c().setColorFilter(colorFilter);
    }

    public boolean setVisible(boolean z, boolean z2) {
        ResourcesFlusher.a(!this.f1720e, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.f1721f = z;
        if (!z) {
            e();
        } else if (this.d) {
            d();
        }
        return super.setVisible(z, z2);
    }

    public void start() {
        this.d = true;
        this.g = 0;
        if (this.f1721f) {
            d();
        }
    }

    public void stop() {
        this.d = false;
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.o.g.GifDrawable$a, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public GifDrawable(a aVar) {
        ResourcesFlusher.a((Object) aVar, "Argument must not be null");
        this.b = aVar;
    }
}
