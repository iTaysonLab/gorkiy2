package j.b.a.m.m;

import j.a.a.a.outline;
import j.b.a.m.Key;
import java.security.MessageDigest;

public final class DataCacheKey implements Key {
    public final Key b;
    public final Key c;

    public DataCacheKey(Key key, Key key2) {
        this.b = key;
        this.c = key2;
    }

    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DataCacheKey)) {
            return false;
        }
        DataCacheKey dataCacheKey = (DataCacheKey) obj;
        if (!this.b.equals(dataCacheKey.b) || !this.c.equals(dataCacheKey.c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.c.hashCode() + (this.b.hashCode() * 31);
    }

    public String toString() {
        StringBuilder a = outline.a("DataCacheKey{sourceKey=");
        a.append(this.b);
        a.append(", signature=");
        a.append(this.c);
        a.append('}');
        return a.toString();
    }
}
