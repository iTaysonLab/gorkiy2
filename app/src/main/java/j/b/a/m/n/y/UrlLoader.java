package j.b.a.m.n.y;

import j.b.a.m.Options;
import j.b.a.m.n.GlideUrl;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderFactory;
import j.b.a.m.n.g;
import j.b.a.m.n.r;
import java.io.InputStream;
import java.net.URL;

public class UrlLoader implements ModelLoader<URL, InputStream> {
    public final ModelLoader<g, InputStream> a;

    public static class a implements ModelLoaderFactory<URL, InputStream> {
        public ModelLoader<URL, InputStream> a(r rVar) {
            return new UrlLoader(rVar.a(GlideUrl.class, InputStream.class));
        }
    }

    public UrlLoader(ModelLoader<g, InputStream> modelLoader) {
        this.a = modelLoader;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        return this.a.a(new GlideUrl((URL) obj), i2, i3, options);
    }

    public boolean a(Object obj) {
        URL url = (URL) obj;
        return true;
    }
}
