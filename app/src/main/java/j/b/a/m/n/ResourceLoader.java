package j.b.a.m.n;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import j.b.a.m.Options;
import j.b.a.m.n.ModelLoader;
import java.io.InputStream;

public class ResourceLoader<Data> implements ModelLoader<Integer, Data> {
    public final ModelLoader<Uri, Data> a;
    public final Resources b;

    public static final class a implements ModelLoaderFactory<Integer, AssetFileDescriptor> {
        public final Resources a;

        public a(Resources resources) {
            this.a = resources;
        }

        public ModelLoader<Integer, AssetFileDescriptor> a(r rVar) {
            return new ResourceLoader(this.a, rVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    public static class b implements ModelLoaderFactory<Integer, ParcelFileDescriptor> {
        public final Resources a;

        public b(Resources resources) {
            this.a = resources;
        }

        public ModelLoader<Integer, ParcelFileDescriptor> a(r rVar) {
            return new ResourceLoader(this.a, rVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    public static class c implements ModelLoaderFactory<Integer, InputStream> {
        public final Resources a;

        public c(Resources resources) {
            this.a = resources;
        }

        public ModelLoader<Integer, InputStream> a(r rVar) {
            return new ResourceLoader(this.a, rVar.a(Uri.class, InputStream.class));
        }
    }

    public static class d implements ModelLoaderFactory<Integer, Uri> {
        public final Resources a;

        public d(Resources resources) {
            this.a = resources;
        }

        public ModelLoader<Integer, Uri> a(r rVar) {
            return new ResourceLoader(this.a, UnitModelLoader.a);
        }
    }

    public ResourceLoader(Resources resources, ModelLoader<Uri, Data> modelLoader) {
        this.b = resources;
        this.a = modelLoader;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        Uri uri;
        Integer num = (Integer) obj;
        try {
            uri = Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e2) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e2);
            }
            uri = null;
        }
        if (uri == null) {
            return null;
        }
        return this.a.a(uri, i2, i3, options);
    }

    public boolean a(Object obj) {
        Integer num = (Integer) obj;
        return true;
    }
}
