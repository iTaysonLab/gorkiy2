package j.b.a.m.m;

public interface Resource<Z> {
    int b();

    Class<Z> c();

    void d();

    Z get();
}
