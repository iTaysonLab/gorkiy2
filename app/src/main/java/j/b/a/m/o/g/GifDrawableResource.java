package j.b.a.m.o.g;

import android.graphics.Bitmap;
import j.b.a.m.m.r;
import j.b.a.m.o.e.DrawableResource;
import j.b.a.m.o.g.GifFrameLoader;

public class GifDrawableResource extends DrawableResource<c> implements r {
    public GifDrawableResource(GifDrawable gifDrawable) {
        super(gifDrawable);
    }

    public void a() {
        ((GifDrawable) super.b).b().prepareToDraw();
    }

    public int b() {
        GifFrameLoader gifFrameLoader = ((GifDrawable) super.b).b.a;
        return gifFrameLoader.a.b() + gifFrameLoader.f1733o;
    }

    public Class<c> c() {
        return GifDrawable.class;
    }

    public void d() {
        ((GifDrawable) super.b).stop();
        GifDrawable gifDrawable = (GifDrawable) super.b;
        gifDrawable.f1720e = true;
        GifFrameLoader gifFrameLoader = gifDrawable.b.a;
        gifFrameLoader.c.clear();
        Bitmap bitmap = gifFrameLoader.f1731m;
        if (bitmap != null) {
            gifFrameLoader.f1725e.a(bitmap);
            gifFrameLoader.f1731m = null;
        }
        gifFrameLoader.f1726f = false;
        GifFrameLoader.a aVar = gifFrameLoader.f1728j;
        if (aVar != null) {
            gifFrameLoader.d.a(aVar);
            gifFrameLoader.f1728j = null;
        }
        GifFrameLoader.a aVar2 = gifFrameLoader.f1730l;
        if (aVar2 != null) {
            gifFrameLoader.d.a(aVar2);
            gifFrameLoader.f1730l = null;
        }
        GifFrameLoader.a aVar3 = gifFrameLoader.f1732n;
        if (aVar3 != null) {
            gifFrameLoader.d.a(aVar3);
            gifFrameLoader.f1732n = null;
        }
        gifFrameLoader.a.clear();
        gifFrameLoader.f1729k = true;
    }
}
