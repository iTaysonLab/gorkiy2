package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.EncodeStrategy;
import j.b.a.m.Option;
import j.b.a.m.Options;
import j.b.a.m.ResourceEncoder;
import j.b.a.m.m.b0.ArrayPool;

public class BitmapEncoder implements ResourceEncoder<Bitmap> {
    public static final Option<Integer> b = Option.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    public static final Option<Bitmap.CompressFormat> c = new Option<>("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat", null, Option.f1597e);
    public final ArrayPool a;

    public BitmapEncoder(ArrayPool arrayPool) {
        this.a = arrayPool;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:24|41|(2:43|44)|45|46) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0067, code lost:
        if (r6 == null) goto L_0x006c;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00c0 */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0062 A[Catch:{ all -> 0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bd A[SYNTHETIC, Splitter:B:43:0x00bd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.Object r9, java.io.File r10, j.b.a.m.Options r11) {
        /*
            r8 = this;
            j.b.a.m.m.Resource r9 = (j.b.a.m.m.Resource) r9
            java.lang.String r0 = "BitmapEncoder"
            java.lang.Object r9 = r9.get()
            android.graphics.Bitmap r9 = (android.graphics.Bitmap) r9
            j.b.a.m.Option<android.graphics.Bitmap$CompressFormat> r1 = j.b.a.m.o.c.BitmapEncoder.c
            java.lang.Object r1 = r11.a(r1)
            android.graphics.Bitmap$CompressFormat r1 = (android.graphics.Bitmap.CompressFormat) r1
            if (r1 == 0) goto L_0x0015
            goto L_0x0020
        L_0x0015:
            boolean r1 = r9.hasAlpha()
            if (r1 == 0) goto L_0x001e
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.PNG
            goto L_0x0020
        L_0x001e:
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.JPEG
        L_0x0020:
            r9.getWidth()
            r9.getHeight()
            long r2 = j.b.a.s.LogTime.a()     // Catch:{ all -> 0x00c1 }
            j.b.a.m.Option<java.lang.Integer> r4 = j.b.a.m.o.c.BitmapEncoder.b     // Catch:{ all -> 0x00c1 }
            java.lang.Object r4 = r11.a(r4)     // Catch:{ all -> 0x00c1 }
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ all -> 0x00c1 }
            int r4 = r4.intValue()     // Catch:{ all -> 0x00c1 }
            r5 = 0
            r6 = 0
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005a }
            r7.<init>(r10)     // Catch:{ IOException -> 0x005a }
            j.b.a.m.m.b0.ArrayPool r10 = r8.a     // Catch:{ IOException -> 0x0055, all -> 0x0053 }
            if (r10 == 0) goto L_0x004a
            j.b.a.m.l.BufferedOutputStream r10 = new j.b.a.m.l.BufferedOutputStream     // Catch:{ IOException -> 0x0055, all -> 0x0053 }
            j.b.a.m.m.b0.ArrayPool r6 = r8.a     // Catch:{ IOException -> 0x0055, all -> 0x0053 }
            r10.<init>(r7, r6)     // Catch:{ IOException -> 0x0055, all -> 0x0053 }
            r6 = r10
            goto L_0x004b
        L_0x004a:
            r6 = r7
        L_0x004b:
            r9.compress(r1, r4, r6)     // Catch:{ IOException -> 0x005a }
            r6.close()     // Catch:{ IOException -> 0x005a }
            r5 = 1
            goto L_0x0069
        L_0x0053:
            r9 = move-exception
            goto L_0x00bb
        L_0x0055:
            r10 = move-exception
            r6 = r7
            goto L_0x005b
        L_0x0058:
            r9 = move-exception
            goto L_0x00ba
        L_0x005a:
            r10 = move-exception
        L_0x005b:
            r4 = 3
            boolean r4 = android.util.Log.isLoggable(r0, r4)     // Catch:{ all -> 0x0058 }
            if (r4 == 0) goto L_0x0067
            java.lang.String r4 = "Failed to encode Bitmap"
            android.util.Log.d(r0, r4, r10)     // Catch:{ all -> 0x0058 }
        L_0x0067:
            if (r6 == 0) goto L_0x006c
        L_0x0069:
            r6.close()     // Catch:{ IOException -> 0x006c }
        L_0x006c:
            r10 = 2
            boolean r10 = android.util.Log.isLoggable(r0, r10)     // Catch:{ all -> 0x00c1 }
            if (r10 == 0) goto L_0x00b9
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c1 }
            r10.<init>()     // Catch:{ all -> 0x00c1 }
            java.lang.String r4 = "Compressed with type: "
            r10.append(r4)     // Catch:{ all -> 0x00c1 }
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            java.lang.String r1 = " of size "
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            int r1 = j.b.a.s.Util.a(r9)     // Catch:{ all -> 0x00c1 }
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            java.lang.String r1 = " in "
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            double r1 = j.b.a.s.LogTime.a(r2)     // Catch:{ all -> 0x00c1 }
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            java.lang.String r1 = ", options format: "
            r10.append(r1)     // Catch:{ all -> 0x00c1 }
            j.b.a.m.Option<android.graphics.Bitmap$CompressFormat> r1 = j.b.a.m.o.c.BitmapEncoder.c     // Catch:{ all -> 0x00c1 }
            java.lang.Object r11 = r11.a(r1)     // Catch:{ all -> 0x00c1 }
            r10.append(r11)     // Catch:{ all -> 0x00c1 }
            java.lang.String r11 = ", hasAlpha: "
            r10.append(r11)     // Catch:{ all -> 0x00c1 }
            boolean r9 = r9.hasAlpha()     // Catch:{ all -> 0x00c1 }
            r10.append(r9)     // Catch:{ all -> 0x00c1 }
            java.lang.String r9 = r10.toString()     // Catch:{ all -> 0x00c1 }
            android.util.Log.v(r0, r9)     // Catch:{ all -> 0x00c1 }
        L_0x00b9:
            return r5
        L_0x00ba:
            r7 = r6
        L_0x00bb:
            if (r7 == 0) goto L_0x00c0
            r7.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00c0:
            throw r9     // Catch:{ all -> 0x00c1 }
        L_0x00c1:
            r9 = move-exception
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.o.c.BitmapEncoder.a(java.lang.Object, java.io.File, j.b.a.m.Options):boolean");
    }

    public EncodeStrategy a(Options options) {
        return EncodeStrategy.TRANSFORMED;
    }
}
