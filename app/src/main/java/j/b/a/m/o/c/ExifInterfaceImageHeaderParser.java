package j.b.a.m.o.c;

import com.bumptech.glide.load.ImageHeaderParser;
import i.k.a.ExifInterface;
import j.b.a.m.m.b0.ArrayPool;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class ExifInterfaceImageHeaderParser implements ImageHeaderParser {
    public ImageHeaderParser.ImageType a(InputStream inputStream) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    public ImageHeaderParser.ImageType a(ByteBuffer byteBuffer) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    public int a(InputStream inputStream, ArrayPool arrayPool) {
        ExifInterface exifInterface = new ExifInterface(inputStream);
        ExifInterface.b b = exifInterface.b("Orientation");
        int i2 = 1;
        if (b != null) {
            try {
                i2 = b.b(exifInterface.f1268f);
            } catch (NumberFormatException unused) {
            }
        }
        if (i2 == 0) {
            return -1;
        }
        return i2;
    }
}
