package j.b.a.m.o.g;

import j.b.a.l.GifDecoder;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.BitmapPool;

public final class GifBitmapProvider implements GifDecoder.a {
    public final BitmapPool a;
    public final ArrayPool b;

    public GifBitmapProvider(BitmapPool bitmapPool, ArrayPool arrayPool) {
        this.a = bitmapPool;
        this.b = arrayPool;
    }

    public byte[] a(int i2) {
        ArrayPool arrayPool = this.b;
        if (arrayPool == null) {
            return new byte[i2];
        }
        return (byte[]) arrayPool.b(i2, byte[].class);
    }
}
