package q.b.a.v;

import java.util.Map;
import n.i.Collections;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.DayOfWeek;
import q.b.a.Duration;
import q.b.a.LocalDate;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.t.i;

public final class IsoFields {
    public static final TemporalField a = b.QUARTER_OF_YEAR;
    public static final TemporalField b = b.WEEK_OF_WEEK_BASED_YEAR;
    public static final TemporalField c = b.WEEK_BASED_YEAR;
    public static final TemporalUnit d = c.WEEK_BASED_YEARS;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000f */
        static {
            /*
                q.b.a.v.IsoFields$c[] r0 = q.b.a.v.IsoFields.c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                q.b.a.v.IsoFields.a.a = r0
                r1 = 1
                q.b.a.v.IsoFields$c r2 = q.b.a.v.IsoFields.c.WEEK_BASED_YEARS     // Catch:{ NoSuchFieldError -> 0x000f }
                r2 = 0
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                int[] r0 = q.b.a.v.IsoFields.a.a     // Catch:{ NoSuchFieldError -> 0x0016 }
                q.b.a.v.IsoFields$c r2 = q.b.a.v.IsoFields.c.QUARTER_YEARS     // Catch:{ NoSuchFieldError -> 0x0016 }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0016 }
            L_0x0016:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.v.IsoFields.a.<clinit>():void");
        }
    }

    public enum c implements m {
        WEEK_BASED_YEARS("WeekBasedYears", Duration.b(31556952)),
        QUARTER_YEARS("QuarterYears", Duration.b(7889238));
        
        public final Duration duration;
        public final String name;

        /* access modifiers changed from: public */
        c(String str, q.b.a.b bVar) {
            this.name = str;
            this.duration = bVar;
        }

        /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
        public <R extends d> R a(R r2, long j2) {
            int ordinal = ordinal();
            if (ordinal == 0) {
                return r2.a(IsoFields.c, Collections.d((long) r2.b(IsoFields.c), j2));
            } else if (ordinal == 1) {
                return r2.b(j2 / 256, ChronoUnit.YEARS).b((j2 % 256) * 3, ChronoUnit.MONTHS);
            } else {
                throw new IllegalStateException("Unreachable");
            }
        }

        public boolean f() {
            return true;
        }

        public String toString() {
            return this.name;
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
    /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalUnit, q.b.a.v.IsoFields$c] */
    static {
        b bVar = b.DAY_OF_QUARTER;
        c cVar = c.QUARTER_YEARS;
    }

    public enum b implements j {
        DAY_OF_QUARTER {
            /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public boolean a(TemporalAccessor temporalAccessor) {
                return temporalAccessor.c(ChronoField.DAY_OF_YEAR) && temporalAccessor.c(ChronoField.MONTH_OF_YEAR) && temporalAccessor.c(ChronoField.YEAR) && Chronology.c(temporalAccessor).equals(IsoChronology.d);
            }

            /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public long b(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    return (long) (temporalAccessor.b(ChronoField.DAY_OF_YEAR) - b.QUARTER_DAYS[((temporalAccessor.b(ChronoField.MONTH_OF_YEAR) - 1) / 3) + (IsoChronology.d.a(temporalAccessor.d(ChronoField.YEAR)) ? 4 : 0)]);
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: DayOfQuarter");
            }

            /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r0v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public ValueRange c(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    long d = temporalAccessor.d(b.QUARTER_OF_YEAR);
                    if (d == 1) {
                        return IsoChronology.d.a(temporalAccessor.d(ChronoField.YEAR)) ? ValueRange.a(1, 91) : ValueRange.a(1, 90);
                    } else if (d == 2) {
                        return ValueRange.a(1, 91);
                    } else {
                        if (d == 3 || d == 4) {
                            return ValueRange.a(1, 92);
                        }
                        return g();
                    }
                } else {
                    throw new UnsupportedTemporalTypeException("Unsupported field: DayOfQuarter");
                }
            }

            public ValueRange g() {
                return ValueRange.a(1, 90, 92);
            }

            public String toString() {
                return "DayOfQuarter";
            }

            /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public <R extends d> R a(R r2, long j2) {
                long b = b((TemporalAccessor) r2);
                g().b(j2, this);
                ? r22 = ChronoField.DAY_OF_YEAR;
                return r2.a(r22, (j2 - b) + r2.d(r22));
            }

            /* JADX WARN: Type inference failed for: r3v1, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x0079, code lost:
                if (r0 == 2) goto L_0x007b;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public q.b.a.v.e a(java.util.Map<q.b.a.v.j, java.lang.Long> r11, q.b.a.v.e r12, q.b.a.t.i r13) {
                /*
                    r10 = this;
                    q.b.a.v.ChronoField r12 = q.b.a.v.ChronoField.YEAR
                    java.lang.Object r12 = r11.get(r12)
                    java.lang.Long r12 = (java.lang.Long) r12
                    q.b.a.v.IsoFields$b r0 = q.b.a.v.IsoFields.b.QUARTER_OF_YEAR
                    java.lang.Object r0 = r11.get(r0)
                    java.lang.Long r0 = (java.lang.Long) r0
                    if (r12 == 0) goto L_0x00a8
                    if (r0 != 0) goto L_0x0016
                    goto L_0x00a8
                L_0x0016:
                    q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.YEAR
                    long r2 = r12.longValue()
                    int r12 = r1.a(r2)
                    q.b.a.v.IsoFields$b r1 = q.b.a.v.IsoFields.b.DAY_OF_QUARTER
                    java.lang.Object r1 = r11.get(r1)
                    java.lang.Long r1 = (java.lang.Long) r1
                    long r1 = r1.longValue()
                    q.b.a.t.ResolverStyle r3 = q.b.a.t.ResolverStyle.LENIENT
                    r4 = 3
                    r5 = 1
                    r7 = 1
                    if (r13 != r3) goto L_0x0051
                    long r8 = r0.longValue()
                    q.b.a.LocalDate r12 = q.b.a.LocalDate.a(r12, r7, r7)
                    long r7 = n.i.Collections.f(r8, r5)
                    long r3 = n.i.Collections.b(r7, r4)
                    q.b.a.LocalDate r12 = r12.b(r3)
                    long r0 = n.i.Collections.f(r1, r5)
                    q.b.a.LocalDate r12 = r12.a(r0)
                    goto L_0x009a
                L_0x0051:
                    q.b.a.v.IsoFields$b r3 = q.b.a.v.IsoFields.b.QUARTER_OF_YEAR
                    q.b.a.v.ValueRange r3 = r3.g()
                    long r8 = r0.longValue()
                    q.b.a.v.IsoFields$b r0 = q.b.a.v.IsoFields.b.QUARTER_OF_YEAR
                    int r0 = r3.a(r8, r0)
                    q.b.a.t.ResolverStyle r3 = q.b.a.t.ResolverStyle.STRICT
                    if (r13 != r3) goto L_0x0086
                    r13 = 92
                    r3 = 91
                    if (r0 != r7) goto L_0x0078
                    q.b.a.s.IsoChronology r13 = q.b.a.s.IsoChronology.d
                    long r8 = (long) r12
                    boolean r13 = r13.a(r8)
                    if (r13 == 0) goto L_0x0075
                    goto L_0x007b
                L_0x0075:
                    r13 = 90
                    goto L_0x007d
                L_0x0078:
                    r8 = 2
                    if (r0 != r8) goto L_0x007d
                L_0x007b:
                    r13 = 91
                L_0x007d:
                    long r8 = (long) r13
                    q.b.a.v.ValueRange r13 = q.b.a.v.ValueRange.a(r5, r8)
                    r13.b(r1, r10)
                    goto L_0x008d
                L_0x0086:
                    q.b.a.v.ValueRange r13 = r10.g()
                    r13.b(r1, r10)
                L_0x008d:
                    int r0 = r0 - r7
                    int r0 = r0 * 3
                    int r0 = r0 + r7
                    q.b.a.LocalDate r12 = q.b.a.LocalDate.a(r12, r0, r7)
                    long r1 = r1 - r5
                    q.b.a.LocalDate r12 = r12.a(r1)
                L_0x009a:
                    r11.remove(r10)
                    q.b.a.v.ChronoField r13 = q.b.a.v.ChronoField.YEAR
                    r11.remove(r13)
                    q.b.a.v.IsoFields$b r13 = q.b.a.v.IsoFields.b.QUARTER_OF_YEAR
                    r11.remove(r13)
                    return r12
                L_0x00a8:
                    r11 = 0
                    return r11
                */
                throw new UnsupportedOperationException("Method not decompiled: q.b.a.v.IsoFields.b.a.a(java.util.Map, q.b.a.v.TemporalAccessor, q.b.a.t.ResolverStyle):q.b.a.v.TemporalAccessor");
            }
        },
        QUARTER_OF_YEAR {
            /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public boolean a(TemporalAccessor temporalAccessor) {
                return temporalAccessor.c(ChronoField.MONTH_OF_YEAR) && Chronology.c(temporalAccessor).equals(IsoChronology.d);
            }

            /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public long b(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    return (temporalAccessor.d(ChronoField.MONTH_OF_YEAR) + 2) / 3;
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: QuarterOfYear");
            }

            public ValueRange c(TemporalAccessor temporalAccessor) {
                return g();
            }

            public ValueRange g() {
                return ValueRange.a(1, 4);
            }

            public String toString() {
                return "QuarterOfYear";
            }

            /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public <R extends d> R a(R r2, long j2) {
                long b = b((TemporalAccessor) r2);
                g().b(j2, this);
                ? r22 = ChronoField.MONTH_OF_YEAR;
                return r2.a(r22, ((j2 - b) * 3) + r2.d(r22));
            }
        },
        WEEK_OF_WEEK_BASED_YEAR {
            /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public boolean a(TemporalAccessor temporalAccessor) {
                return temporalAccessor.c(ChronoField.EPOCH_DAY) && Chronology.c(temporalAccessor).equals(IsoChronology.d);
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.IsoFields$b$c, q.b.a.v.TemporalField] */
            public long b(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    return (long) b.a(LocalDate.a(temporalAccessor));
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: WeekOfWeekBasedYear");
            }

            /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.IsoFields$b$c, q.b.a.v.TemporalField] */
            public ValueRange c(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    return ValueRange.a(1, (long) b.a(b.b(LocalDate.a(temporalAccessor))));
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: WeekOfWeekBasedYear");
            }

            public ValueRange g() {
                return ValueRange.a(1, 52, 53);
            }

            public String toString() {
                return "WeekOfWeekBasedYear";
            }

            /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
            public <R extends d> R a(R r2, long j2) {
                g().b(j2, this);
                return r2.b(Collections.f(j2, b((TemporalAccessor) r2)), ChronoUnit.WEEKS);
            }

            /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r5v0, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r3v3, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r3v7, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r3v8, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate
             arg types: [?, long]
             candidates:
              q.b.a.LocalDate.a(int, int):q.b.a.LocalDate
              q.b.a.LocalDate.a(java.lang.CharSequence, q.b.a.t.DateTimeFormatter):q.b.a.LocalDate
              q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.LocalDate
              q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
              q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
              q.b.a.LocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
              q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
              q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.s.ChronoLocalDate
              q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.s.ChronoLocalDate
              q.b.a.s.ChronoLocalDate.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
              q.b.a.s.ChronoLocalDate.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
              q.b.a.v.Temporal.a(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
              q.b.a.v.Temporal.a(q.b.a.v.TemporalField, long):q.b.a.v.Temporal
              q.b.a.LocalDate.a(q.b.a.v.TemporalField, long):q.b.a.LocalDate */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public q.b.a.v.e a(java.util.Map<q.b.a.v.j, java.lang.Long> r19, q.b.a.v.e r20, q.b.a.t.i r21) {
                /*
                    r18 = this;
                    r0 = r18
                    r1 = r19
                    r2 = r21
                    q.b.a.v.IsoFields$b r3 = q.b.a.v.IsoFields.b.WEEK_BASED_YEAR
                    java.lang.Object r3 = r1.get(r3)
                    java.lang.Long r3 = (java.lang.Long) r3
                    q.b.a.v.ChronoField r4 = q.b.a.v.ChronoField.DAY_OF_WEEK
                    java.lang.Object r4 = r1.get(r4)
                    java.lang.Long r4 = (java.lang.Long) r4
                    if (r3 == 0) goto L_0x00ba
                    if (r4 != 0) goto L_0x001c
                    goto L_0x00ba
                L_0x001c:
                    q.b.a.v.IsoFields$b r5 = q.b.a.v.IsoFields.b.WEEK_BASED_YEAR
                    q.b.a.v.ValueRange r5 = r5.g()
                    long r6 = r3.longValue()
                    q.b.a.v.IsoFields$b r3 = q.b.a.v.IsoFields.b.WEEK_BASED_YEAR
                    int r3 = r5.a(r6, r3)
                    q.b.a.v.IsoFields$b r5 = q.b.a.v.IsoFields.b.WEEK_OF_WEEK_BASED_YEAR
                    java.lang.Object r5 = r1.get(r5)
                    java.lang.Long r5 = (java.lang.Long) r5
                    long r5 = r5.longValue()
                    q.b.a.t.ResolverStyle r7 = q.b.a.t.ResolverStyle.LENIENT
                    r8 = 4
                    r9 = 1
                    r10 = 1
                    if (r2 != r7) goto L_0x0072
                    long r12 = r4.longValue()
                    r14 = 0
                    r16 = 7
                    int r2 = (r12 > r16 ? 1 : (r12 == r16 ? 0 : -1))
                    if (r2 <= 0) goto L_0x0053
                    long r12 = r12 - r10
                    long r14 = r12 / r16
                    long r12 = r12 % r16
                    long r12 = r12 + r10
                    goto L_0x005e
                L_0x0053:
                    int r2 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))
                    if (r2 >= 0) goto L_0x005e
                    long r14 = r12 / r16
                    long r14 = r14 - r10
                    long r12 = r12 % r16
                    long r12 = r12 + r16
                L_0x005e:
                    q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r3, r9, r8)
                    long r5 = r5 - r10
                    q.b.a.LocalDate r2 = r2.c(r5)
                    q.b.a.LocalDate r2 = r2.c(r14)
                    q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.DAY_OF_WEEK
                    q.b.a.LocalDate r2 = r2.a(r3, r12)
                    goto L_0x00ac
                L_0x0072:
                    q.b.a.v.ChronoField r7 = q.b.a.v.ChronoField.DAY_OF_WEEK
                    long r12 = r4.longValue()
                    int r4 = r7.a(r12)
                    q.b.a.t.ResolverStyle r7 = q.b.a.t.ResolverStyle.STRICT
                    if (r2 != r7) goto L_0x0095
                    q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r3, r9, r8)
                    int r2 = q.b.a.v.IsoFields.b.b(r2)
                    int r2 = q.b.a.v.IsoFields.b.a(r2)
                    long r12 = (long) r2
                    q.b.a.v.ValueRange r2 = q.b.a.v.ValueRange.a(r10, r12)
                    r2.b(r5, r0)
                    goto L_0x009c
                L_0x0095:
                    q.b.a.v.ValueRange r2 = r18.g()
                    r2.b(r5, r0)
                L_0x009c:
                    q.b.a.LocalDate r2 = q.b.a.LocalDate.a(r3, r9, r8)
                    long r5 = r5 - r10
                    q.b.a.LocalDate r2 = r2.c(r5)
                    q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.DAY_OF_WEEK
                    long r4 = (long) r4
                    q.b.a.LocalDate r2 = r2.a(r3, r4)
                L_0x00ac:
                    r1.remove(r0)
                    q.b.a.v.IsoFields$b r3 = q.b.a.v.IsoFields.b.WEEK_BASED_YEAR
                    r1.remove(r3)
                    q.b.a.v.ChronoField r3 = q.b.a.v.ChronoField.DAY_OF_WEEK
                    r1.remove(r3)
                    return r2
                L_0x00ba:
                    r1 = 0
                    return r1
                */
                throw new UnsupportedOperationException("Method not decompiled: q.b.a.v.IsoFields.b.c.a(java.util.Map, q.b.a.v.TemporalAccessor, q.b.a.t.ResolverStyle):q.b.a.v.TemporalAccessor");
            }
        },
        WEEK_BASED_YEAR {
            /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public boolean a(TemporalAccessor temporalAccessor) {
                return temporalAccessor.c(ChronoField.EPOCH_DAY) && Chronology.c(temporalAccessor).equals(IsoChronology.d);
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.IsoFields$b$d, q.b.a.v.TemporalField] */
            public long b(TemporalAccessor temporalAccessor) {
                if (temporalAccessor.c(this)) {
                    return (long) b.b(LocalDate.a(temporalAccessor));
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: WeekBasedYear");
            }

            public ValueRange c(TemporalAccessor temporalAccessor) {
                return ChronoField.YEAR.range;
            }

            public ValueRange g() {
                return ChronoField.YEAR.range;
            }

            public String toString() {
                return "WeekBasedYear";
            }

            /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.IsoFields$b, q.b.a.v.TemporalField] */
            /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
            public <R extends d> R a(R r2, long j2) {
                if (a((TemporalAccessor) r2)) {
                    int a = ChronoField.YEAR.range.a(j2, (TemporalField) b.WEEK_BASED_YEAR);
                    LocalDate a2 = LocalDate.a((TemporalAccessor) r2);
                    int b = a2.b((TemporalField) ChronoField.DAY_OF_WEEK);
                    int a3 = b.a(a2);
                    if (a3 == 53 && b.a(a) == 52) {
                        a3 = 52;
                    }
                    LocalDate a4 = LocalDate.a(a, 1, 4);
                    return r2.a(a4.a((long) (((a3 - 1) * 7) + (b - a4.b((TemporalField) ChronoField.DAY_OF_WEEK)))));
                }
                throw new UnsupportedTemporalTypeException("Unsupported field: WeekBasedYear");
            }
        };
        
        public static final int[] QUARTER_DAYS = {0, 90, 181, 273, 0, 91, 182, 274};

        /* access modifiers changed from: public */
        /* synthetic */ b(a aVar) {
        }

        public static int a(int i2) {
            LocalDate a2 = LocalDate.a(i2, 1, 1);
            if (a2.o() != DayOfWeek.THURSDAY) {
                return (a2.o() != DayOfWeek.WEDNESDAY || !a2.q()) ? 52 : 53;
            }
            return 53;
        }

        public static int b(LocalDate localDate) {
            int i2 = localDate.b;
            int p2 = localDate.p();
            if (p2 <= 3) {
                if (p2 - localDate.o().ordinal() < -2) {
                    return i2 - 1;
                }
                return i2;
            } else if (p2 < 363) {
                return i2;
            } else {
                return ((p2 - 363) - (localDate.q() ? 1 : 0)) - localDate.o().ordinal() >= 0 ? i2 + 1 : i2;
            }
        }

        public e a(Map<j, Long> map, e eVar, i iVar) {
            return null;
        }

        public boolean f() {
            return true;
        }

        public boolean h() {
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x005d, code lost:
            if ((r0 == -3 || (r0 == -2 && r5.q())) == false) goto L_0x0061;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static /* synthetic */ int a(q.b.a.LocalDate r5) {
            /*
                q.b.a.DayOfWeek r0 = r5.o()
                int r0 = r0.ordinal()
                int r1 = r5.p()
                r2 = 1
                int r1 = r1 - r2
                int r0 = 3 - r0
                int r0 = r0 + r1
                int r3 = r0 / 7
                int r3 = r3 * 7
                int r0 = r0 - r3
                r3 = -3
                int r0 = r0 + r3
                if (r0 >= r3) goto L_0x001c
                int r0 = r0 + 7
            L_0x001c:
                if (r1 >= r0) goto L_0x0046
                r0 = 180(0xb4, float:2.52E-43)
                int r1 = r5.p()
                if (r1 != r0) goto L_0x0027
                goto L_0x002d
            L_0x0027:
                int r5 = r5.b
                q.b.a.LocalDate r5 = q.b.a.LocalDate.a(r5, r0)
            L_0x002d:
                r0 = -1
                q.b.a.LocalDate r5 = r5.d(r0)
                int r5 = b(r5)
                int r5 = a(r5)
                long r0 = (long) r5
                r2 = 1
                q.b.a.v.ValueRange r5 = q.b.a.v.ValueRange.a(r2, r0)
                long r0 = r5.f3152e
                int r5 = (int) r0
                goto L_0x0062
            L_0x0046:
                int r1 = r1 - r0
                int r1 = r1 / 7
                int r1 = r1 + r2
                r4 = 53
                if (r1 != r4) goto L_0x0060
                if (r0 == r3) goto L_0x005c
                r3 = -2
                if (r0 != r3) goto L_0x005a
                boolean r5 = r5.q()
                if (r5 == 0) goto L_0x005a
                goto L_0x005c
            L_0x005a:
                r5 = 0
                goto L_0x005d
            L_0x005c:
                r5 = 1
            L_0x005d:
                if (r5 != 0) goto L_0x0060
                goto L_0x0061
            L_0x0060:
                r2 = r1
            L_0x0061:
                r5 = r2
            L_0x0062:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: q.b.a.v.IsoFields.b.a(q.b.a.LocalDate):int");
        }
    }
}
