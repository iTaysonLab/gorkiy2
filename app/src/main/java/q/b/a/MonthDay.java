package q.b.a;

import com.crashlytics.android.core.SessionProtobufHelper;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.t.DateTimeFormatterBuilder;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.e;
import q.b.a.v.f;

public final class MonthDay extends c implements e, f, Comparable<h>, Serializable {
    public final int b;
    public final int c;

    /* JADX WARN: Type inference failed for: r1v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    static {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.a("--");
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.MONTH_OF_YEAR, 2);
        dateTimeFormatterBuilder.a('-');
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.DAY_OF_MONTH, 2);
        dateTimeFormatterBuilder.c();
    }

    public MonthDay(int i2, int i3) {
        this.b = i2;
        this.c = i3;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 64, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.MonthDay, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return IsoChronology.d;
        }
        return MonthDay.super.a(temporalQuery);
    }

    public int b(TemporalField temporalField) {
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.MonthDay, q.b.a.v.TemporalAccessor] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.MONTH_OF_YEAR || temporalField == ChronoField.DAY_OF_MONTH) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public int compareTo(Object obj) {
        MonthDay monthDay = (MonthDay) obj;
        int i2 = this.b - monthDay.b;
        return i2 == 0 ? this.c - monthDay.c : i2;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.MonthDay, q.b.a.v.TemporalAccessor] */
    public long d(TemporalField temporalField) {
        int i2;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 18) {
            i2 = this.c;
        } else if (ordinal == 23) {
            i2 = this.b;
        } else {
            throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
        return (long) i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MonthDay)) {
            return false;
        }
        MonthDay monthDay = (MonthDay) obj;
        if (this.b == monthDay.b && this.c == monthDay.c) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.b << 6) + this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(10);
        sb.append("--");
        sb.append(this.b < 10 ? SessionProtobufHelper.SIGNAL_DEFAULT : "");
        sb.append(this.b);
        sb.append(this.c < 10 ? "-0" : "-");
        sb.append(this.c);
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.Month, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static MonthDay a(int i2, int i3) {
        Month a = Month.a(i2);
        Collections.a((Object) a, "month");
        ? r0 = ChronoField.DAY_OF_MONTH;
        r0.range.b((long) i3, r0);
        if (i3 <= a.f()) {
            return new MonthDay(a.getValue(), i3);
        }
        StringBuilder a2 = outline.a("Illegal value for DayOfMonth field, value ", i3, " is not valid for month ");
        a2.append(a.name());
        throw new DateTimeException(a2.toString());
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [q.b.a.MonthDay, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.MONTH_OF_YEAR) {
            return temporalField.g();
        }
        if (temporalField != ChronoField.DAY_OF_MONTH) {
            return MonthDay.super.a(temporalField);
        }
        int ordinal = Month.a(this.b).ordinal();
        return ValueRange.a(1, (long) (ordinal != 1 ? (ordinal == 3 || ordinal == 5 || ordinal == 8 || ordinal == 10) ? 30 : 31 : 28), (long) Month.a(this.b).f());
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        if (Chronology.c(temporal).equals(IsoChronology.d)) {
            Temporal a = temporal.a((TemporalField) ChronoField.MONTH_OF_YEAR, (long) this.b);
            ? r0 = ChronoField.DAY_OF_MONTH;
            return a.a((TemporalField) r0, Math.min(a.a((TemporalField) r0).f3152e, (long) this.c));
        }
        throw new DateTimeException("Adjustment only supported on ISO date-time");
    }

    public static MonthDay a(DataInput dataInput) {
        return a(dataInput.readByte(), dataInput.readByte());
    }
}
