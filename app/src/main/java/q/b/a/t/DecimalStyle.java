package q.b.a.t;

import j.a.a.a.outline;
import java.util.concurrent.ConcurrentHashMap;

public final class DecimalStyle {

    /* renamed from: e  reason: collision with root package name */
    public static final DecimalStyle f3149e = new DecimalStyle('0', '+', '-', '.');
    public final char a;
    public final char b;
    public final char c;
    public final char d;

    static {
        new ConcurrentHashMap(16, 0.75f, 2);
    }

    public DecimalStyle(char c2, char c3, char c4, char c5) {
        this.a = c2;
        this.b = c3;
        this.c = c4;
        this.d = c5;
    }

    public String a(String str) {
        char c2 = this.a;
        if (c2 == '0') {
            return str;
        }
        int i2 = c2 - '0';
        char[] charArray = str.toCharArray();
        for (int i3 = 0; i3 < charArray.length; i3++) {
            charArray[i3] = (char) (charArray[i3] + i2);
        }
        return new String(charArray);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DecimalStyle)) {
            return false;
        }
        DecimalStyle decimalStyle = (DecimalStyle) obj;
        if (this.a == decimalStyle.a && this.b == decimalStyle.b && this.c == decimalStyle.c && this.d == decimalStyle.d) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.a + this.b + this.c + this.d;
    }

    public String toString() {
        StringBuilder a2 = outline.a("DecimalStyle[");
        a2.append(this.a);
        a2.append(this.b);
        a2.append(this.c);
        a2.append(this.d);
        a2.append("]");
        return a2.toString();
    }
}
