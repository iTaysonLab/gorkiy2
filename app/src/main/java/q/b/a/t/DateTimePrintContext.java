package q.b.a.t;

import j.a.a.a.outline;
import java.util.Locale;
import org.threeten.bp.DateTimeException;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQuery;

public final class DateTimePrintContext {
    public TemporalAccessor a;
    public Locale b;
    public DecimalStyle c;
    public int d;

    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x002e */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v5, types: [q.b.a.v.ChronoField[]] */
    /* JADX WARN: Type inference failed for: r8v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r7v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r7v9, types: [q.b.a.ZoneOffset] */
    /* JADX WARN: Type inference failed for: r11v8, types: [q.b.a.s.ChronoZonedDateTime] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DateTimePrintContext(q.b.a.v.TemporalAccessor r11, q.b.a.t.DateTimeFormatter r12) {
        /*
            r10 = this;
            r10.<init>()
            q.b.a.s.Chronology r0 = r12.f3132f
            q.b.a.ZoneId r1 = r12.g
            if (r0 != 0) goto L_0x000d
            if (r1 != 0) goto L_0x000d
            goto L_0x00eb
        L_0x000d:
            q.b.a.v.TemporalQuery<q.b.a.s.h> r2 = q.b.a.v.TemporalQueries.b
            java.lang.Object r2 = r11.a(r2)
            q.b.a.s.Chronology r2 = (q.b.a.s.Chronology) r2
            q.b.a.v.TemporalQuery<q.b.a.o> r3 = q.b.a.v.TemporalQueries.a
            java.lang.Object r3 = r11.a(r3)
            q.b.a.ZoneId r3 = (q.b.a.ZoneId) r3
            boolean r4 = n.i.Collections.a(r2, r0)
            r5 = 0
            if (r4 == 0) goto L_0x0025
            r0 = r5
        L_0x0025:
            boolean r4 = n.i.Collections.a(r3, r1)
            if (r4 == 0) goto L_0x002c
            r1 = r5
        L_0x002c:
            if (r0 != 0) goto L_0x0032
            if (r1 != 0) goto L_0x0032
            goto L_0x00eb
        L_0x0032:
            if (r0 == 0) goto L_0x0036
            r4 = r0
            goto L_0x0037
        L_0x0036:
            r4 = r2
        L_0x0037:
            if (r1 == 0) goto L_0x003a
            r3 = r1
        L_0x003a:
            java.lang.String r6 = " "
            if (r1 == 0) goto L_0x0099
            q.b.a.v.ChronoField r7 = q.b.a.v.ChronoField.INSTANT_SECONDS
            boolean r7 = r11.c(r7)
            if (r7 == 0) goto L_0x0055
            if (r4 == 0) goto L_0x0049
            goto L_0x004b
        L_0x0049:
            q.b.a.s.IsoChronology r4 = q.b.a.s.IsoChronology.d
        L_0x004b:
            q.b.a.Instant r11 = q.b.a.Instant.a(r11)
            q.b.a.s.ChronoZonedDateTime r11 = r4.a(r11, r1)
            goto L_0x00eb
        L_0x0055:
            q.b.a.w.ZoneRules r7 = r1.g()     // Catch:{ ZoneRulesException -> 0x0066 }
            boolean r8 = r7.a()     // Catch:{ ZoneRulesException -> 0x0066 }
            if (r8 == 0) goto L_0x0066
            q.b.a.Instant r8 = q.b.a.Instant.d     // Catch:{ ZoneRulesException -> 0x0066 }
            q.b.a.ZoneOffset r7 = r7.a(r8)     // Catch:{ ZoneRulesException -> 0x0066 }
            goto L_0x0067
        L_0x0066:
            r7 = r1
        L_0x0067:
            q.b.a.v.TemporalQuery<q.b.a.p> r8 = q.b.a.v.TemporalQueries.f3150e
            java.lang.Object r8 = r11.a(r8)
            q.b.a.ZoneOffset r8 = (q.b.a.ZoneOffset) r8
            boolean r9 = r7 instanceof q.b.a.ZoneOffset
            if (r9 == 0) goto L_0x0099
            if (r8 == 0) goto L_0x0099
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x007c
            goto L_0x0099
        L_0x007c:
            org.threeten.bp.DateTimeException r12 = new org.threeten.bp.DateTimeException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Invalid override zone for temporal: "
            r0.append(r2)
            r0.append(r1)
            r0.append(r6)
            r0.append(r11)
            java.lang.String r11 = r0.toString()
            r12.<init>(r11)
            throw r12
        L_0x0099:
            if (r0 == 0) goto L_0x00e5
            q.b.a.v.ChronoField r1 = q.b.a.v.ChronoField.EPOCH_DAY
            boolean r1 = r11.c(r1)
            if (r1 == 0) goto L_0x00a8
            q.b.a.s.ChronoLocalDate r5 = r4.a(r11)
            goto L_0x00e5
        L_0x00a8:
            q.b.a.s.IsoChronology r1 = q.b.a.s.IsoChronology.d
            if (r0 != r1) goto L_0x00ae
            if (r2 == 0) goto L_0x00e5
        L_0x00ae:
            q.b.a.v.ChronoField[] r1 = q.b.a.v.ChronoField.values()
            int r2 = r1.length
            r7 = 0
        L_0x00b4:
            if (r7 >= r2) goto L_0x00e5
            r8 = r1[r7]
            boolean r9 = r8.f()
            if (r9 == 0) goto L_0x00e2
            boolean r8 = r11.c(r8)
            if (r8 != 0) goto L_0x00c5
            goto L_0x00e2
        L_0x00c5:
            org.threeten.bp.DateTimeException r12 = new org.threeten.bp.DateTimeException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid override chronology for temporal: "
            r1.append(r2)
            r1.append(r0)
            r1.append(r6)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            r12.<init>(r11)
            throw r12
        L_0x00e2:
            int r7 = r7 + 1
            goto L_0x00b4
        L_0x00e5:
            q.b.a.t.DateTimePrintContext0 r0 = new q.b.a.t.DateTimePrintContext0
            r0.<init>(r5, r11, r4, r3)
            r11 = r0
        L_0x00eb:
            r10.a = r11
            java.util.Locale r11 = r12.b
            r10.b = r11
            q.b.a.t.DecimalStyle r11 = r12.c
            r10.c = r11
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: q.b.a.t.DateTimePrintContext.<init>(q.b.a.v.TemporalAccessor, q.b.a.t.DateTimeFormatter):void");
    }

    public void a() {
        this.d--;
    }

    public String toString() {
        return this.a.toString();
    }

    public <R> R a(TemporalQuery temporalQuery) {
        R a2 = this.a.a(temporalQuery);
        if (a2 != null || this.d != 0) {
            return a2;
        }
        StringBuilder a3 = outline.a("Unable to extract value: ");
        a3.append(this.a.getClass());
        throw new DateTimeException(a3.toString());
    }

    public Long a(TemporalField temporalField) {
        try {
            return Long.valueOf(this.a.d(temporalField));
        } catch (DateTimeException e2) {
            if (this.d > 0) {
                return null;
            }
            throw e2;
        }
    }
}
