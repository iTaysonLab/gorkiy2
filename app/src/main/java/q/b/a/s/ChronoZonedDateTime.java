package q.b.a.s;

import j.a.a.a.outline;
import n.i.Collections;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.LocalDate;
import q.b.a.LocalTime;
import q.b.a.ZoneId;
import q.b.a.ZoneOffset;
import q.b.a.o;
import q.b.a.s.b;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;
import q.b.a.v.j;
import q.b.a.v.m;

public abstract class ChronoZonedDateTime<D extends b> extends q.b.a.u.b implements d, Comparable<ChronoZonedDateTime<?>> {
    public abstract ChronoZonedDateTime<D> a(o oVar);

    public abstract ChronoZonedDateTime<D> a(j jVar, long j2);

    public abstract ChronoZonedDateTime<D> b(long j2, m mVar);

    /* JADX WARN: Type inference failed for: r0v7, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDateTime] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            return l();
        }
        if (ordinal != 29) {
            return p().d(temporalField);
        }
        return (long) i().c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChronoZonedDateTime) || compareTo((ChronoZonedDateTime) obj) != 0) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (p().hashCode() ^ i().c) ^ Integer.rotateLeft(j().hashCode(), 3);
    }

    public abstract ZoneOffset i();

    public abstract ZoneId j();

    public long l() {
        return ((o().l() * 86400) + ((long) q().j())) - ((long) i().c);
    }

    public D o() {
        return p().j();
    }

    public abstract ChronoLocalDateTime<D> p();

    public LocalTime q() {
        return p().l();
    }

    public String toString() {
        String str = p().toString() + i().d;
        if (i() == j()) {
            return str;
        }
        return str + '[' + j().toString() + ']';
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.ChronoLocalDateTime] */
    public int b(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return ChronoZonedDateTime.super.b(temporalField);
        }
        int ordinal = ((ChronoField) temporalField).ordinal();
        if (ordinal == 28) {
            throw new UnsupportedTemporalTypeException(outline.a("Field too large for an int: ", temporalField));
        } else if (ordinal != 29) {
            return p().b(temporalField);
        } else {
            return i().c;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.s.ChronoZonedDateTime, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.d) {
            return j();
        }
        if (temporalQuery == TemporalQueries.b) {
            return o().i();
        }
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.f3150e) {
            return i();
        }
        if (temporalQuery == TemporalQueries.f3151f) {
            return LocalDate.e(o().l());
        }
        if (temporalQuery == TemporalQueries.g) {
            return q();
        }
        return ChronoZonedDateTime.super.a(temporalQuery);
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.s.ChronoLocalDateTime] */
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.OFFSET_SECONDS) {
            return temporalField.g();
        }
        return p().a(temporalField);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoZonedDateTime, q.b.a.v.Temporal] */
    public ChronoZonedDateTime<D> a(f fVar) {
        return o().i().c((d) fVar.a(this));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.s.ChronoZonedDateTime, q.b.a.u.DefaultInterfaceTemporal] */
    public ChronoZonedDateTime<D> a(long j2, m mVar) {
        return o().i().c((d) ChronoZonedDateTime.super.a(j2, mVar));
    }

    /* JADX WARN: Type inference failed for: r5v1, types: [q.b.a.s.ChronoLocalDate] */
    /* renamed from: a */
    public int compareTo(ChronoZonedDateTime<?> chronoZonedDateTime) {
        int a = Collections.a(l(), chronoZonedDateTime.l());
        if (a != 0) {
            return a;
        }
        int i2 = q().f3094e - chronoZonedDateTime.q().f3094e;
        if (i2 != 0) {
            return i2;
        }
        int a2 = p().compareTo(chronoZonedDateTime.p());
        if (a2 != 0) {
            return a2;
        }
        int compareTo = j().f().compareTo(chronoZonedDateTime.j().f());
        return compareTo == 0 ? o().i().compareTo(chronoZonedDateTime.o().i()) : compareTo;
    }
}
