package q.b.a;

import j.a.a.a.outline;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.regex.Pattern;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.zone.ZoneRulesException;
import q.b.a.w.ZoneRules;
import q.b.a.w.ZoneRulesProvider;

public final class ZoneRegion extends ZoneId implements Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final Pattern f3099e = Pattern.compile("[A-Za-z][A-Za-z0-9~/._+-]+");
    public final String c;
    public final transient ZoneRules d;

    public ZoneRegion(String str, ZoneRules zoneRules) {
        this.c = str;
        this.d = zoneRules;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static ZoneRegion a(String str, boolean z) {
        Collections.a((Object) str, "zoneId");
        if (str.length() < 2 || !f3099e.matcher(str).matches()) {
            throw new DateTimeException(outline.a("Invalid ID for region-based ZoneId, invalid format: ", str));
        }
        ZoneRules zoneRules = null;
        try {
            zoneRules = ZoneRulesProvider.a(str, true);
        } catch (ZoneRulesException e2) {
            if (str.equals("GMT0")) {
                zoneRules = ZoneOffset.g.g();
            } else if (z) {
                throw e2;
            }
        }
        return new ZoneRegion(str, zoneRules);
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 7, this);
    }

    public String f() {
        return this.c;
    }

    public ZoneRules g() {
        ZoneRules zoneRules = this.d;
        return zoneRules != null ? zoneRules : ZoneRulesProvider.a(this.c, false);
    }

    public void a(DataOutput dataOutput) {
        dataOutput.writeByte(7);
        dataOutput.writeUTF(this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.ZoneRegion.a(java.lang.String, boolean):q.b.a.ZoneRegion
     arg types: [java.lang.String, int]
     candidates:
      q.b.a.ZoneId.a(java.lang.String, q.b.a.ZoneOffset):q.b.a.ZoneId
      q.b.a.ZoneRegion.a(java.lang.String, boolean):q.b.a.ZoneRegion */
    public static ZoneId a(DataInput dataInput) {
        ZoneRegion superR;
        ZoneRegion superR2;
        String readUTF = dataInput.readUTF();
        if (readUTF.equals("Z") || readUTF.startsWith("+") || readUTF.startsWith("-")) {
            throw new DateTimeException(outline.a("Invalid ID for region-based ZoneId, invalid format: ", readUTF));
        } else if (readUTF.equals("UTC") || readUTF.equals("GMT") || readUTF.equals("UT")) {
            return new ZoneRegion(readUTF, ZoneOffset.g.g());
        } else {
            if (readUTF.startsWith("UTC+") || readUTF.startsWith("GMT+") || readUTF.startsWith("UTC-") || readUTF.startsWith("GMT-")) {
                ZoneOffset a = ZoneOffset.a(readUTF.substring(3));
                if (a.c == 0) {
                    superR = new ZoneRegion(readUTF.substring(0, 3), a.g());
                } else {
                    superR = new ZoneRegion(readUTF.substring(0, 3) + a.d, a.g());
                }
                return superR;
            } else if (!readUTF.startsWith("UT+") && !readUTF.startsWith("UT-")) {
                return a(readUTF, false);
            } else {
                ZoneOffset a2 = ZoneOffset.a(readUTF.substring(2));
                if (a2.c == 0) {
                    superR2 = new ZoneRegion("UT", a2.g());
                } else {
                    StringBuilder a3 = outline.a("UT");
                    a3.append(a2.d);
                    superR2 = new ZoneRegion(a3.toString(), a2.g());
                }
                return superR2;
            }
        }
    }
}
