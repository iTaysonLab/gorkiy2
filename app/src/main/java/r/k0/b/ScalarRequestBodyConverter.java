package r.k0.b;

import o.MediaType;
import o.RequestBody;
import o.g0;
import r.Converter;

public final class ScalarRequestBodyConverter<T> implements Converter<T, g0> {
    public static final ScalarRequestBodyConverter<Object> a = new ScalarRequestBodyConverter<>();
    public static final MediaType b = MediaType.a("text/plain; charset=UTF-8");

    public Object a(Object obj) {
        MediaType mediaType = b;
        return RequestBody.a.a(String.valueOf(obj), mediaType);
    }
}
