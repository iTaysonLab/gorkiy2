package r.k0.a;

import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.k;
import java.io.Reader;
import java.nio.charset.Charset;
import n.r.Charsets;
import o.MediaType;
import o.ResponseBody;
import o.i0;
import p.BufferedSource;
import r.Converter;

public final class GsonResponseBodyConverter<T> implements Converter<i0, T> {
    public final Gson a;
    public final TypeAdapter<T> b;

    public GsonResponseBodyConverter(k kVar, TypeAdapter<T> typeAdapter) {
        this.a = kVar;
        this.b = typeAdapter;
    }

    public Object a(Object obj) {
        Charset charset;
        ResponseBody responseBody = (ResponseBody) obj;
        Gson gson = this.a;
        Reader reader = responseBody.b;
        if (reader == null) {
            BufferedSource g = responseBody.g();
            MediaType f2 = responseBody.f();
            if (f2 == null || (charset = f2.a(Charsets.a)) == null) {
                charset = Charsets.a;
            }
            reader = new ResponseBody.a(g, charset);
            responseBody.b = reader;
        }
        if (gson != null) {
            JsonReader jsonReader = new JsonReader(reader);
            jsonReader.setLenient(gson.f2537i);
            try {
                T a2 = this.b.a(jsonReader);
                if (jsonReader.peek() == JsonToken.END_DOCUMENT) {
                    return a2;
                }
                throw new JsonIOException("JSON document was not fully consumed.");
            } finally {
                responseBody.close();
            }
        } else {
            throw null;
        }
    }
}
