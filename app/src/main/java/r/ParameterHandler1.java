package r;

import java.lang.reflect.Array;
import javax.annotation.Nullable;

/* compiled from: ParameterHandler */
public class ParameterHandler1 extends ParameterHandler<Object> {
    public final /* synthetic */ ParameterHandler a;

    public ParameterHandler1(ParameterHandler parameterHandler) {
        this.a = super;
    }

    public void a(RequestBuilder requestBuilder, @Nullable Object obj) {
        if (obj != null) {
            int length = Array.getLength(obj);
            for (int i2 = 0; i2 < length; i2++) {
                this.a.a(requestBuilder, Array.get(obj, i2));
            }
        }
    }
}
