package r.j0.a;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import r.Response;
import retrofit2.adapter.rxjava2.HttpException;

public final class BodyObservable<T> extends Observable<T> {
    public final Observable<Response<T>> b;

    public static class a<R> implements Observer<Response<R>> {
        public final Observer<? super R> b;
        public boolean c;

        public a(Observer<? super R> observer) {
            this.b = observer;
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        public void b(Object obj) {
            Response response = (Response) obj;
            if (response.a()) {
                this.b.b(response.b);
                return;
            }
            this.c = true;
            HttpException httpException = new HttpException(response);
            try {
                this.b.a(httpException);
            } catch (Throwable th) {
                c.c(th);
                c.b((Throwable) new CompositeException(httpException, th));
            }
        }

        public void a() {
            if (!this.c) {
                this.b.a();
            }
        }

        public void a(Throwable th) {
            if (!this.c) {
                this.b.a(th);
                return;
            }
            AssertionError assertionError = new AssertionError("This should never happen! Report as a bug with the full stacktrace.");
            assertionError.initCause(th);
            c.b((Throwable) assertionError);
        }
    }

    public BodyObservable(Observable<Response<T>> observable) {
        this.b = super;
    }

    public void b(Observer<? super T> observer) {
        this.b.a(new a(observer));
    }
}
