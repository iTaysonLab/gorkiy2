package r;

import java.util.concurrent.CompletableFuture;
import r.CompletableFutureCallAdapterFactory2;

/* compiled from: CompletableFutureCallAdapterFactory */
public class CompletableFutureCallAdapterFactory3 implements Callback<R> {
    public final /* synthetic */ CompletableFuture b;

    public CompletableFutureCallAdapterFactory3(CompletableFutureCallAdapterFactory2.b bVar, CompletableFuture completableFuture) {
        this.b = completableFuture;
    }

    public void a(Call<R> call, Response<R> response) {
        this.b.complete(response);
    }

    public void a(Call<R> call, Throwable th) {
        this.b.completeExceptionally(th);
    }
}
