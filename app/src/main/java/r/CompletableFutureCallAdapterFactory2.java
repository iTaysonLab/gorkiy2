package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import javax.annotation.Nullable;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import r.CallAdapter;

@IgnoreJRERequirement
/* compiled from: CompletableFutureCallAdapterFactory */
public final class CompletableFutureCallAdapterFactory2 extends CallAdapter.a {
    public static final CallAdapter.a a = new CompletableFutureCallAdapterFactory2();

    @IgnoreJRERequirement
    /* compiled from: CompletableFutureCallAdapterFactory */
    public static final class a<R> implements CallAdapter<R, CompletableFuture<R>> {
        public final Type a;

        public a(Type type) {
            this.a = type;
        }

        public Type a() {
            return this.a;
        }

        public Object a(Call call) {
            CompletableFutureCallAdapterFactory completableFutureCallAdapterFactory = new CompletableFutureCallAdapterFactory(this, call);
            call.a(new CompletableFutureCallAdapterFactory1(this, completableFutureCallAdapterFactory));
            return completableFutureCallAdapterFactory;
        }
    }

    @IgnoreJRERequirement
    /* compiled from: CompletableFutureCallAdapterFactory */
    public static final class b<R> implements CallAdapter<R, CompletableFuture<Response<R>>> {
        public final Type a;

        public b(Type type) {
            this.a = type;
        }

        public Type a() {
            return this.a;
        }

        public Object a(Call call) {
            CompletableFutureCallAdapterFactory0 completableFutureCallAdapterFactory0 = new CompletableFutureCallAdapterFactory0(this, call);
            call.a(new CompletableFutureCallAdapterFactory3(this, completableFutureCallAdapterFactory0));
            return completableFutureCallAdapterFactory0;
        }
    }

    @Nullable
    public CallAdapter<?, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        if (Utils.b(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = Utils.a(0, (ParameterizedType) type);
            if (Utils.b(a2) != Response.class) {
                return new a(a2);
            }
            if (a2 instanceof ParameterizedType) {
                return new b(Utils.a(0, (ParameterizedType) a2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
