package ru.covid19.core.presentation.common.processing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import e.a.a.a.c.b.ProcessingFragmentViewState;
import e.a.a.a.c.b.ProcessingFragmentViewState0;
import e.a.a.a.c.b.ProcessingFragmentVm;
import e.a.a.a.c.b.c;
import e.a.a.a.c.b.d;
import e.a.a.a.e.p.ProcessingNavigationDto;
import e.a.a.e;
import e.a.a.f;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.c.c.b;
import java.util.HashMap;
import n.i.Collections;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.base.BaseDpFragment;

/* compiled from: ProcessingFragment.kt */
public final class ProcessingFragment extends BaseDpFragment<c, d> {
    public HashMap d0;

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<d> P() {
        return ProcessingFragmentVm.class;
    }

    public void Q() {
        ProcessingNavigationDto processingNavigationDto = (ProcessingNavigationDto) Collections.a(this.g);
        if (processingNavigationDto != null) {
            ((ProcessingFragmentVm) h()).g.a((b) new ProcessingFragmentViewState0.a(processingNavigationDto));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        ProcessingFragmentViewState processingFragmentViewState = (ProcessingFragmentViewState) obj;
        View view = null;
        if (processingFragmentViewState != null) {
            int i2 = e.a.a.d.frag_processing_tv_description;
            if (this.d0 == null) {
                this.d0 = new HashMap();
            }
            View view2 = (View) this.d0.get(Integer.valueOf(i2));
            if (view2 == null) {
                View view3 = this.H;
                if (view3 != null) {
                    view = view3.findViewById(i2);
                    this.d0.put(Integer.valueOf(i2), view);
                }
            } else {
                view = view2;
            }
            TextView textView = (TextView) view;
            Intrinsics.a((Object) textView, "frag_processing_tv_description");
            String str = processingFragmentViewState.a;
            if (str == null) {
                str = J().getString(f.frag_processing_description);
            }
            textView.setText(str);
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        CoreComponent coreComponent = CoreComponentsHolder.b;
        if (coreComponent != null) {
            coreComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.b("coreComponent");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate(e.frag_processing, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
