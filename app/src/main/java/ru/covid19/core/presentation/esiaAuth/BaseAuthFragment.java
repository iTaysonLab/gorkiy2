package ru.covid19.core.presentation.esiaAuth;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.Group;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import e.a.a.a.a.BaseAuthFragmentVm8;
import e.a.a.a.a.LoginPagerAdapter1;
import e.a.a.a.a.a;
import e.a.a.d;
import e.a.a.e;
import e.a.a.f;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.c.d.a.ViewAction;
import e.c.d.a.ViewState;
import e.c.d.b.c.BaseMvvmFragment;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.e.a.b.AnyToUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.TypeCastException;
import l.b.Observable;
import l.b.l;
import l.b.r.b.AndroidSchedulers;
import l.b.s.CompositeDisposable;
import l.b.t.Consumer;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.i.Collections;
import n.i.Iterators;
import n.n.c.Intrinsics;
import ru.covid19.droid.presentation.main.auth.AuthFragment;

/* compiled from: BaseAuthFragment.kt */
public abstract class BaseAuthFragment<T extends e.a.a.a.a.a> extends BaseMvvmFragment<T> implements e.c.d.b.c.b {
    public LoginPagerAdapter1 c0;
    public HashMap d0;

    /* compiled from: java-style lambda group */
    public static final class a<T> implements Consumer<Boolean> {
        public final /* synthetic */ int b;
        public final /* synthetic */ Object c;

        public a(int i2, Object obj) {
            this.b = i2;
            this.c = obj;
        }

        /* JADX WARN: Type inference failed for: r1v21, types: [n.o.Ranges0, java.lang.Iterable] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [androidx.constraintlayout.widget.Group, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Boolean, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ProgressBar, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [com.google.android.material.tabs.TabLayout, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.Button, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final void a(Object obj) {
            ViewState<Boolean> viewState;
            ViewState<Boolean> viewState2;
            int i2 = this.b;
            String str = null;
            if (i2 == 0) {
                Boolean bool = (Boolean) obj;
                TextInputLayout textInputLayout = (TextInputLayout) ((BaseAuthFragment) this.c).c(d.frag_auth_til_pass);
                Intrinsics.a((Object) textInputLayout, "frag_auth_til_pass");
                Intrinsics.a((Object) bool, "valid");
                if (!bool.booleanValue()) {
                    str = ((BaseAuthFragment) this.c).a(f.frag_auth_request_failed);
                }
                textInputLayout.setError(str);
            } else if (i2 == 1) {
                Boolean bool2 = (Boolean) obj;
                Button button = (Button) ((BaseAuthFragment) this.c).c(d.frag_auth_btn_login);
                Intrinsics.a((Object) button, "frag_auth_btn_login");
                Intrinsics.a((Object) bool2, "it");
                button.setEnabled(bool2.booleanValue());
            } else if (i2 == 2) {
                Boolean bool3 = (Boolean) obj;
                ProgressBar progressBar = (ProgressBar) ((BaseAuthFragment) this.c).c(d.frag_auth_pb_loading);
                Intrinsics.a((Object) progressBar, "frag_auth_pb_loading");
                Intrinsics.a((Object) bool3, "isLoading");
                Collections.a(progressBar, bool3.booleanValue());
                LoginPagerAdapter1 loginPagerAdapter1 = ((BaseAuthFragment) this.c).c0;
                if (!(loginPagerAdapter1 == null || (viewState2 = loginPagerAdapter1.d) == null)) {
                    viewState2.a(Boolean.valueOf(!bool3.booleanValue()));
                }
                LoginPagerAdapter1 loginPagerAdapter12 = ((BaseAuthFragment) this.c).c0;
                if (!(loginPagerAdapter12 == null || (viewState = loginPagerAdapter12.f580e) == null)) {
                    viewState.a(Boolean.valueOf(!bool3.booleanValue()));
                }
                TextInputEditText textInputEditText = (TextInputEditText) ((BaseAuthFragment) this.c).c(d.frag_auth_et_pass);
                Intrinsics.a((Object) textInputEditText, "frag_auth_et_pass");
                textInputEditText.setEnabled(!bool3.booleanValue());
                BaseAuthFragment baseAuthFragment = (BaseAuthFragment) this.c;
                boolean z = !bool3.booleanValue();
                TabLayout tabLayout = (TabLayout) baseAuthFragment.c(d.frag_auth_pager_tabLayout);
                Intrinsics.a((Object) tabLayout, "frag_auth_pager_tabLayout");
                if (tabLayout.getChildCount() > 0) {
                    View childAt = ((TabLayout) baseAuthFragment.c(d.frag_auth_pager_tabLayout)).getChildAt(0);
                    if (childAt != null) {
                        ViewGroup viewGroup = (ViewGroup) childAt;
                        Iterator it = n.o.d.b(0, viewGroup.getChildCount()).iterator();
                        while (it.hasNext()) {
                            View childAt2 = viewGroup.getChildAt(((Iterators) it).b());
                            Intrinsics.a((Object) childAt2, "getChildAt(it)");
                            childAt2.setEnabled(z);
                        }
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup");
                }
            } else if (i2 == 3) {
                Boolean bool4 = (Boolean) obj;
                Group group = (Group) ((BaseAuthFragment) this.c).c(d.frag_auth_group_content);
                Intrinsics.a((Object) group, "frag_auth_group_content");
                Intrinsics.a((Object) bool4, "isContentVisible");
                if (bool4.booleanValue()) {
                    group.setVisibility(0);
                    return;
                }
                group.setVisibility(8);
                group.setVisibility(4);
            } else {
                throw null;
            }
        }
    }

    /* compiled from: BaseAuthFragment.kt */
    public static final class b extends ViewPager.m {
        public final /* synthetic */ BaseAuthFragment a;

        public b(BaseAuthFragment baseAuthFragment) {
            this.a = baseAuthFragment;
        }

        public void b(int i2) {
            ((BaseAuthFragmentVm8) this.a.h()).h.a((Boolean) Integer.valueOf(i2));
        }
    }

    public void C() {
        this.c0 = null;
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
     arg types: [l.b.Observable<R>, e.c.d.a.ViewAction<n.g>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
     arg types: [j.e.a.InitialValueObservable, e.c.d.a.ViewAction<java.lang.CharSequence>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b */
    public void O() {
        CompositeDisposable compositeDisposable = super.Z;
        Button button = (Button) c(d.frag_auth_btn_login);
        Intrinsics.a((Object) button, "frag_auth_btn_login");
        Observable<R> c = c.a((View) button).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c, "RxView.clicks(this).map(AnyToUnit)");
        TextView textView = (TextView) c(d.frag_auth_tv_forgot_password);
        Intrinsics.a((Object) textView, "frag_auth_tv_forgot_password");
        Observable<R> c2 = c.a((View) textView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        Button button2 = (Button) c(d.frag_auth_btn_reg);
        Intrinsics.a((Object) button2, "frag_auth_btn_reg");
        Observable<R> c3 = c.a((View) button2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText = (TextInputEditText) c(d.frag_auth_et_pass);
        compositeDisposable.a(Collections.a((Observable) c, (ViewAction) ((BaseAuthFragmentVm8) h()).f568i), Collections.a((Observable) c2, (ViewAction) ((BaseAuthFragmentVm8) h()).f572m), Collections.a((Observable) c3, (ViewAction) ((BaseAuthFragmentVm8) h()).f573n), Collections.a((Observable) outline.a(textInputEditText, "frag_auth_et_pass", textInputEditText, "RxTextView.textChanges(this)"), (ViewAction) ((BaseAuthFragmentVm8) h()).f569j), ((BaseAuthFragmentVm8) h()).f577r.b.a((l) AndroidSchedulers.a()).a(new a(0, this)), ((BaseAuthFragmentVm8) h()).f578s.b.a((l) AndroidSchedulers.a()).a(new a(1, this)), ((BaseAuthFragmentVm8) h()).f579t.b.a((l) AndroidSchedulers.a()).a(new a(2, this)), ((BaseAuthFragmentVm8) h()).u.b.a((l) AndroidSchedulers.a()).a(new a(3, this)));
        LoginPagerAdapter1 loginPagerAdapter1 = this.c0;
        if (loginPagerAdapter1 != null) {
            super.Z.a(loginPagerAdapter1.b.a(((BaseAuthFragmentVm8) h()).f571l.a), loginPagerAdapter1.c.a(((BaseAuthFragmentVm8) h()).f570k.a), ((BaseAuthFragmentVm8) h()).f575p.a(loginPagerAdapter1.f581f.a), ((BaseAuthFragmentVm8) h()).f576q.a(loginPagerAdapter1.g.a));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate(e.frag_auth, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    public void b(Bundle bundle) {
        AuthFragment authFragment = (AuthFragment) this;
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                Intrinsics.b("coreComponent");
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(super.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public boolean e() {
        ViewAction<g> viewAction = ((BaseAuthFragmentVm8) h()).f574o;
        viewAction.a().a(Unit.a);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.viewpager.widget.ViewPager, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(View view, Bundle bundle) {
        if (view != null) {
            Context J = J();
            Intrinsics.a((Object) J, "requireContext()");
            this.c0 = new LoginPagerAdapter1(J);
            ViewPager viewPager = (ViewPager) c(d.frag_auth_view_pager);
            Intrinsics.a((Object) viewPager, "frag_auth_view_pager");
            viewPager.setAdapter(this.c0);
            ((TabLayout) c(d.frag_auth_pager_tabLayout)).setupWithViewPager((ViewPager) c(d.frag_auth_view_pager));
            ViewPager viewPager2 = (ViewPager) c(d.frag_auth_view_pager);
            b bVar = new b(this);
            if (viewPager2.S == null) {
                viewPager2.S = new ArrayList();
            }
            viewPager2.S.add(bVar);
            return;
        }
        Intrinsics.a("view");
        throw null;
    }
}
