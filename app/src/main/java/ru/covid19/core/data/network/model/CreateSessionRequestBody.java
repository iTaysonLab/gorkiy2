package ru.covid19.core.data.network.model;

import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import com.crashlytics.android.core.MetaDataStore;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: CreateSessionRequestBody.kt */
public final class CreateSessionRequestBody {
    public final String accessToken;
    public final String id;
    public final String name;
    public final String type;
    public final String userId;

    public CreateSessionRequestBody(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            Intrinsics.a(MetaDataStore.KEY_USER_ID);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("accessToken");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("id");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str5 != null) {
            this.userId = str;
            this.accessToken = str2;
            this.id = str3;
            this.name = str4;
            this.type = str5;
        } else {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        }
    }

    public static /* synthetic */ CreateSessionRequestBody copy$default(CreateSessionRequestBody createSessionRequestBody, String str, String str2, String str3, String str4, String str5, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = createSessionRequestBody.userId;
        }
        if ((i2 & 2) != 0) {
            str2 = createSessionRequestBody.accessToken;
        }
        String str6 = str2;
        if ((i2 & 4) != 0) {
            str3 = createSessionRequestBody.id;
        }
        String str7 = str3;
        if ((i2 & 8) != 0) {
            str4 = createSessionRequestBody.name;
        }
        String str8 = str4;
        if ((i2 & 16) != 0) {
            str5 = createSessionRequestBody.type;
        }
        return createSessionRequestBody.copy(str, str6, str7, str8, str5);
    }

    public final String component1() {
        return this.userId;
    }

    public final String component2() {
        return this.accessToken;
    }

    public final String component3() {
        return this.id;
    }

    public final String component4() {
        return this.name;
    }

    public final String component5() {
        return this.type;
    }

    public final CreateSessionRequestBody copy(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            Intrinsics.a(MetaDataStore.KEY_USER_ID);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("accessToken");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("id");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str5 != null) {
            return new CreateSessionRequestBody(str, str2, str3, str4, str5);
        } else {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CreateSessionRequestBody)) {
            return false;
        }
        CreateSessionRequestBody createSessionRequestBody = (CreateSessionRequestBody) obj;
        return Intrinsics.a(this.userId, createSessionRequestBody.userId) && Intrinsics.a(this.accessToken, createSessionRequestBody.accessToken) && Intrinsics.a(this.id, createSessionRequestBody.id) && Intrinsics.a(this.name, createSessionRequestBody.name) && Intrinsics.a(this.type, createSessionRequestBody.type);
    }

    public final String getAccessToken() {
        return this.accessToken;
    }

    public final String getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getType() {
        return this.type;
    }

    public final String getUserId() {
        return this.userId;
    }

    public int hashCode() {
        String str = this.userId;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.accessToken;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.id;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.name;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.type;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("CreateSessionRequestBody(userId=");
        a.append(this.userId);
        a.append(", accessToken=");
        a.append(this.accessToken);
        a.append(", id=");
        a.append(this.id);
        a.append(", name=");
        a.append(this.name);
        a.append(", type=");
        return outline.a(a, this.type, ")");
    }
}
