package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: GetCountriesRequestBody.kt */
public final class GetCountriesRequestBody {
    public final int pageNum;
    public final String pageSize;
    public final String parentRefItemValue;
    public final List<String> selectAttributes;
    public final String treeFiltering;
    public final String tx;

    public GetCountriesRequestBody() {
        this(null, 0, null, null, null, null, 63, null);
    }

    public GetCountriesRequestBody(String str, int i2, String str2, String str3, List<String> list, String str4) {
        if (str == null) {
            Intrinsics.a("treeFiltering");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("pageSize");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("parentRefItemValue");
            throw null;
        } else if (list == null) {
            Intrinsics.a("selectAttributes");
            throw null;
        } else if (str4 != null) {
            this.treeFiltering = str;
            this.pageNum = i2;
            this.pageSize = str2;
            this.parentRefItemValue = str3;
            this.selectAttributes = list;
            this.tx = str4;
        } else {
            Intrinsics.a("tx");
            throw null;
        }
    }

    public static /* synthetic */ GetCountriesRequestBody copy$default(GetCountriesRequestBody getCountriesRequestBody, String str, int i2, String str2, String str3, List list, String str4, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = getCountriesRequestBody.treeFiltering;
        }
        if ((i3 & 2) != 0) {
            i2 = getCountriesRequestBody.pageNum;
        }
        int i4 = i2;
        if ((i3 & 4) != 0) {
            str2 = getCountriesRequestBody.pageSize;
        }
        String str5 = str2;
        if ((i3 & 8) != 0) {
            str3 = getCountriesRequestBody.parentRefItemValue;
        }
        String str6 = str3;
        if ((i3 & 16) != 0) {
            list = getCountriesRequestBody.selectAttributes;
        }
        List list2 = list;
        if ((i3 & 32) != 0) {
            str4 = getCountriesRequestBody.tx;
        }
        return getCountriesRequestBody.copy(str, i4, str5, str6, list2, str4);
    }

    public final String component1() {
        return this.treeFiltering;
    }

    public final int component2() {
        return this.pageNum;
    }

    public final String component3() {
        return this.pageSize;
    }

    public final String component4() {
        return this.parentRefItemValue;
    }

    public final List<String> component5() {
        return this.selectAttributes;
    }

    public final String component6() {
        return this.tx;
    }

    public final GetCountriesRequestBody copy(String str, int i2, String str2, String str3, List<String> list, String str4) {
        if (str == null) {
            Intrinsics.a("treeFiltering");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("pageSize");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("parentRefItemValue");
            throw null;
        } else if (list == null) {
            Intrinsics.a("selectAttributes");
            throw null;
        } else if (str4 != null) {
            return new GetCountriesRequestBody(str, i2, str2, str3, list, str4);
        } else {
            Intrinsics.a("tx");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GetCountriesRequestBody)) {
            return false;
        }
        GetCountriesRequestBody getCountriesRequestBody = (GetCountriesRequestBody) obj;
        return Intrinsics.a(this.treeFiltering, getCountriesRequestBody.treeFiltering) && this.pageNum == getCountriesRequestBody.pageNum && Intrinsics.a(this.pageSize, getCountriesRequestBody.pageSize) && Intrinsics.a(this.parentRefItemValue, getCountriesRequestBody.parentRefItemValue) && Intrinsics.a(this.selectAttributes, getCountriesRequestBody.selectAttributes) && Intrinsics.a(this.tx, getCountriesRequestBody.tx);
    }

    public final int getPageNum() {
        return this.pageNum;
    }

    public final String getPageSize() {
        return this.pageSize;
    }

    public final String getParentRefItemValue() {
        return this.parentRefItemValue;
    }

    public final List<String> getSelectAttributes() {
        return this.selectAttributes;
    }

    public final String getTreeFiltering() {
        return this.treeFiltering;
    }

    public final String getTx() {
        return this.tx;
    }

    public int hashCode() {
        String str = this.treeFiltering;
        int i2 = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.pageNum) * 31;
        String str2 = this.pageSize;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.parentRefItemValue;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        List<String> list = this.selectAttributes;
        int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
        String str4 = this.tx;
        if (str4 != null) {
            i2 = str4.hashCode();
        }
        return hashCode4 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("GetCountriesRequestBody(treeFiltering=");
        a.append(this.treeFiltering);
        a.append(", pageNum=");
        a.append(this.pageNum);
        a.append(", pageSize=");
        a.append(this.pageSize);
        a.append(", parentRefItemValue=");
        a.append(this.parentRefItemValue);
        a.append(", selectAttributes=");
        a.append(this.selectAttributes);
        a.append(", tx=");
        return outline.a(a, this.tx, ")");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GetCountriesRequestBody(String str, int i2, String str2, String str3, List list, String str4, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this((i3 & 1) != 0 ? "ONELEVEL" : str, (i3 & 2) != 0 ? 1 : i2, (i3 & 4) != 0 ? "258" : str2, (i3 & 8) != 0 ? "" : str3, (i3 & 16) != 0 ? c.c((Object) "*") : list, (i3 & 32) != 0 ? "" : str4);
    }
}
