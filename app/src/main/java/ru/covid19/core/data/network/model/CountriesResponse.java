package ru.covid19.core.data.network.model;

import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: CountriesResponse.kt */
public final class CountriesResponse extends BaseResponse {
    @SerializedName("items")
    public final List<Country> countries;
    public final int total;

    public CountriesResponse(int i2, List<Country> list) {
        if (list != null) {
            this.total = i2;
            this.countries = list;
            return;
        }
        Intrinsics.a("countries");
        throw null;
    }

    public static /* synthetic */ CountriesResponse copy$default(CountriesResponse countriesResponse, int i2, List list, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i2 = countriesResponse.total;
        }
        if ((i3 & 2) != 0) {
            list = countriesResponse.countries;
        }
        return countriesResponse.copy(i2, list);
    }

    public final int component1() {
        return this.total;
    }

    public final List<Country> component2() {
        return this.countries;
    }

    public final CountriesResponse copy(int i2, List<Country> list) {
        if (list != null) {
            return new CountriesResponse(i2, list);
        }
        Intrinsics.a("countries");
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CountriesResponse)) {
            return false;
        }
        CountriesResponse countriesResponse = (CountriesResponse) obj;
        return this.total == countriesResponse.total && Intrinsics.a(this.countries, countriesResponse.countries);
    }

    public final List<Country> getCountries() {
        return this.countries;
    }

    public final int getTotal() {
        return this.total;
    }

    public int hashCode() {
        int i2 = this.total * 31;
        List<Country> list = this.countries;
        return i2 + (list != null ? list.hashCode() : 0);
    }

    public String toString() {
        StringBuilder a = outline.a("CountriesResponse(total=");
        a.append(this.total);
        a.append(", countries=");
        a.append(this.countries);
        a.append(")");
        return a.toString();
    }
}
