package ru.covid19.droid.data.model.profileData;

import com.crashlytics.android.core.CodedOutputStream;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: Passenger.kt */
public final class Passenger {
    public Integer birthDate;
    public String citizenship;
    public String citizenshipName;
    public String firstName;
    public String fullName;
    public ProfileData0 gender;
    public String lastName;
    public String patronymic;
    public String placementAddress;
    public String quarantineAddress;
    public Boolean quarantineAddressMatchesPlacement;
    public String registrationAddress;
    public Boolean registrationAddressMatchesPlacement;

    public Passenger() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, 8191, null);
    }

    public Passenger(Integer num, String str, String str2, ProfileData0 profileData0, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Boolean bool, Boolean bool2) {
        if (str == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("placementAddress");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a("registrationAddress");
            throw null;
        } else if (str5 != null) {
            this.birthDate = num;
            this.citizenship = str;
            this.fullName = str2;
            this.gender = profileData0;
            this.placementAddress = str3;
            this.registrationAddress = str4;
            this.quarantineAddress = str5;
            this.citizenshipName = str6;
            this.firstName = str7;
            this.lastName = str8;
            this.patronymic = str9;
            this.registrationAddressMatchesPlacement = bool;
            this.quarantineAddressMatchesPlacement = bool2;
        } else {
            Intrinsics.a("quarantineAddress");
            throw null;
        }
    }

    public static /* synthetic */ Passenger copy$default(Passenger passenger, Integer num, String str, String str2, ProfileData0 profileData0, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Boolean bool, Boolean bool2, int i2, Object obj) {
        Passenger passenger2 = passenger;
        int i3 = i2;
        return passenger.copy((i3 & 1) != 0 ? passenger2.birthDate : num, (i3 & 2) != 0 ? passenger2.citizenship : str, (i3 & 4) != 0 ? passenger2.fullName : str2, (i3 & 8) != 0 ? passenger2.gender : profileData0, (i3 & 16) != 0 ? passenger2.placementAddress : str3, (i3 & 32) != 0 ? passenger2.registrationAddress : str4, (i3 & 64) != 0 ? passenger2.quarantineAddress : str5, (i3 & 128) != 0 ? passenger2.citizenshipName : str6, (i3 & 256) != 0 ? passenger2.firstName : str7, (i3 & 512) != 0 ? passenger2.lastName : str8, (i3 & 1024) != 0 ? passenger2.patronymic : str9, (i3 & 2048) != 0 ? passenger2.registrationAddressMatchesPlacement : bool, (i3 & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0 ? passenger2.quarantineAddressMatchesPlacement : bool2);
    }

    public final Integer component1() {
        return this.birthDate;
    }

    public final String component10() {
        return this.lastName;
    }

    public final String component11() {
        return this.patronymic;
    }

    public final Boolean component12() {
        return this.registrationAddressMatchesPlacement;
    }

    public final Boolean component13() {
        return this.quarantineAddressMatchesPlacement;
    }

    public final String component2() {
        return this.citizenship;
    }

    public final String component3() {
        return this.fullName;
    }

    public final ProfileData0 component4() {
        return this.gender;
    }

    public final String component5() {
        return this.placementAddress;
    }

    public final String component6() {
        return this.registrationAddress;
    }

    public final String component7() {
        return this.quarantineAddress;
    }

    public final String component8() {
        return this.citizenshipName;
    }

    public final String component9() {
        return this.firstName;
    }

    public final Passenger copy(Integer num, String str, String str2, ProfileData0 profileData0, String str3, String str4, String str5, String str6, String str7, String str8, String str9, Boolean bool, Boolean bool2) {
        if (str == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("placementAddress");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a("registrationAddress");
            throw null;
        } else if (str5 != null) {
            return new Passenger(num, str, str2, profileData0, str3, str4, str5, str6, str7, str8, str9, bool, bool2);
        } else {
            Intrinsics.a("quarantineAddress");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Passenger)) {
            return false;
        }
        Passenger passenger = (Passenger) obj;
        return Intrinsics.a(this.birthDate, passenger.birthDate) && Intrinsics.a(this.citizenship, passenger.citizenship) && Intrinsics.a(this.fullName, passenger.fullName) && Intrinsics.a(this.gender, passenger.gender) && Intrinsics.a(this.placementAddress, passenger.placementAddress) && Intrinsics.a(this.registrationAddress, passenger.registrationAddress) && Intrinsics.a(this.quarantineAddress, passenger.quarantineAddress) && Intrinsics.a(this.citizenshipName, passenger.citizenshipName) && Intrinsics.a(this.firstName, passenger.firstName) && Intrinsics.a(this.lastName, passenger.lastName) && Intrinsics.a(this.patronymic, passenger.patronymic) && Intrinsics.a(this.registrationAddressMatchesPlacement, passenger.registrationAddressMatchesPlacement) && Intrinsics.a(this.quarantineAddressMatchesPlacement, passenger.quarantineAddressMatchesPlacement);
    }

    public final Integer getBirthDate() {
        return this.birthDate;
    }

    public final String getCitizenship() {
        return this.citizenship;
    }

    public final String getCitizenshipName() {
        return this.citizenshipName;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final String getFullName() {
        return this.fullName;
    }

    public final ProfileData0 getGender() {
        return this.gender;
    }

    public final String getLastName() {
        return this.lastName;
    }

    public final String getPatronymic() {
        return this.patronymic;
    }

    public final String getPlacementAddress() {
        return this.placementAddress;
    }

    public final String getQuarantineAddress() {
        return this.quarantineAddress;
    }

    public final Boolean getQuarantineAddressMatchesPlacement() {
        return this.quarantineAddressMatchesPlacement;
    }

    public final String getRegistrationAddress() {
        return this.registrationAddress;
    }

    public final Boolean getRegistrationAddressMatchesPlacement() {
        return this.registrationAddressMatchesPlacement;
    }

    public int hashCode() {
        Integer num = this.birthDate;
        int i2 = 0;
        int hashCode = (num != null ? num.hashCode() : 0) * 31;
        String str = this.citizenship;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.fullName;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ProfileData0 profileData0 = this.gender;
        int hashCode4 = (hashCode3 + (profileData0 != null ? profileData0.hashCode() : 0)) * 31;
        String str3 = this.placementAddress;
        int hashCode5 = (hashCode4 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.registrationAddress;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.quarantineAddress;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.citizenshipName;
        int hashCode8 = (hashCode7 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.firstName;
        int hashCode9 = (hashCode8 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.lastName;
        int hashCode10 = (hashCode9 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.patronymic;
        int hashCode11 = (hashCode10 + (str9 != null ? str9.hashCode() : 0)) * 31;
        Boolean bool = this.registrationAddressMatchesPlacement;
        int hashCode12 = (hashCode11 + (bool != null ? bool.hashCode() : 0)) * 31;
        Boolean bool2 = this.quarantineAddressMatchesPlacement;
        if (bool2 != null) {
            i2 = bool2.hashCode();
        }
        return hashCode12 + i2;
    }

    public final void setBirthDate(Integer num) {
        this.birthDate = num;
    }

    public final void setCitizenship(String str) {
        if (str != null) {
            this.citizenship = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setCitizenshipName(String str) {
        this.citizenshipName = str;
    }

    public final void setFirstName(String str) {
        this.firstName = str;
    }

    public final void setFullName(String str) {
        if (str != null) {
            this.fullName = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setGender(ProfileData0 profileData0) {
        this.gender = profileData0;
    }

    public final void setLastName(String str) {
        this.lastName = str;
    }

    public final void setPatronymic(String str) {
        this.patronymic = str;
    }

    public final void setPlacementAddress(String str) {
        if (str != null) {
            this.placementAddress = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setQuarantineAddress(String str) {
        if (str != null) {
            this.quarantineAddress = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setQuarantineAddressMatchesPlacement(Boolean bool) {
        this.quarantineAddressMatchesPlacement = bool;
    }

    public final void setRegistrationAddress(String str) {
        if (str != null) {
            this.registrationAddress = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setRegistrationAddressMatchesPlacement(Boolean bool) {
        this.registrationAddressMatchesPlacement = bool;
    }

    public String toString() {
        StringBuilder a = outline.a("Passenger(birthDate=");
        a.append(this.birthDate);
        a.append(", citizenship=");
        a.append(this.citizenship);
        a.append(", fullName=");
        a.append(this.fullName);
        a.append(", gender=");
        a.append(this.gender);
        a.append(", placementAddress=");
        a.append(this.placementAddress);
        a.append(", registrationAddress=");
        a.append(this.registrationAddress);
        a.append(", quarantineAddress=");
        a.append(this.quarantineAddress);
        a.append(", citizenshipName=");
        a.append(this.citizenshipName);
        a.append(", firstName=");
        a.append(this.firstName);
        a.append(", lastName=");
        a.append(this.lastName);
        a.append(", patronymic=");
        a.append(this.patronymic);
        a.append(", registrationAddressMatchesPlacement=");
        a.append(this.registrationAddressMatchesPlacement);
        a.append(", quarantineAddressMatchesPlacement=");
        a.append(this.quarantineAddressMatchesPlacement);
        a.append(")");
        return a.toString();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Passenger(java.lang.Integer r15, java.lang.String r16, java.lang.String r17, ru.covid19.droid.data.model.profileData.ProfileData0 r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.Boolean r26, java.lang.Boolean r27, int r28, n.n.c.DefaultConstructorMarker r29) {
        /*
            r14 = this;
            r0 = r28
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = r2
            goto L_0x000a
        L_0x0009:
            r1 = r15
        L_0x000a:
            r3 = r0 & 2
            java.lang.String r4 = ""
            if (r3 == 0) goto L_0x0012
            r3 = r4
            goto L_0x0014
        L_0x0012:
            r3 = r16
        L_0x0014:
            r5 = r0 & 4
            if (r5 == 0) goto L_0x001a
            r5 = r4
            goto L_0x001c
        L_0x001a:
            r5 = r17
        L_0x001c:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x0021
            goto L_0x0023
        L_0x0021:
            r2 = r18
        L_0x0023:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0029
            r6 = r4
            goto L_0x002b
        L_0x0029:
            r6 = r19
        L_0x002b:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x0031
            r7 = r4
            goto L_0x0033
        L_0x0031:
            r7 = r20
        L_0x0033:
            r8 = r0 & 64
            if (r8 == 0) goto L_0x0039
            r8 = r4
            goto L_0x003b
        L_0x0039:
            r8 = r21
        L_0x003b:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0041
            r9 = r4
            goto L_0x0043
        L_0x0041:
            r9 = r22
        L_0x0043:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x0049
            r10 = r4
            goto L_0x004b
        L_0x0049:
            r10 = r23
        L_0x004b:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0051
            r11 = r4
            goto L_0x0053
        L_0x0051:
            r11 = r24
        L_0x0053:
            r12 = r0 & 1024(0x400, float:1.435E-42)
            if (r12 == 0) goto L_0x0058
            goto L_0x005a
        L_0x0058:
            r4 = r25
        L_0x005a:
            r12 = r0 & 2048(0x800, float:2.87E-42)
            r13 = 0
            if (r12 == 0) goto L_0x0064
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r13)
            goto L_0x0066
        L_0x0064:
            r12 = r26
        L_0x0066:
            r0 = r0 & 4096(0x1000, float:5.74E-42)
            if (r0 == 0) goto L_0x006f
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r13)
            goto L_0x0071
        L_0x006f:
            r0 = r27
        L_0x0071:
            r15 = r14
            r16 = r1
            r17 = r3
            r18 = r5
            r19 = r2
            r20 = r6
            r21 = r7
            r22 = r8
            r23 = r9
            r24 = r10
            r25 = r11
            r26 = r4
            r27 = r12
            r28 = r0
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.data.model.profileData.Passenger.<init>(java.lang.Integer, java.lang.String, java.lang.String, ru.covid19.droid.data.model.profileData.ProfileData0, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, int, n.n.c.DefaultConstructorMarker):void");
    }
}
