package ru.covid19.droid.data.model.profileData;

import j.a.a.a.outline;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: ProfileData.kt */
public final class ProfileData {
    public List<Children> children;
    public boolean dataCorrectnessAccept;
    public Document document;
    public Passenger passenger;
    public boolean quarantineRequirementsAccept;
    public boolean quarantineViolationLiability;
    public Boolean sympthoms;
    public Transport transport;
    public Travel travel;

    public ProfileData() {
        this(false, false, false, null, null, null, null, null, null, 511, null);
    }

    public ProfileData(boolean z, boolean z2, boolean z3, Boolean bool, Passenger passenger2, Document document2, Travel travel2, Transport transport2, List<Children> list) {
        if (passenger2 == null) {
            Intrinsics.a("passenger");
            throw null;
        } else if (document2 == null) {
            Intrinsics.a("document");
            throw null;
        } else if (travel2 == null) {
            Intrinsics.a("travel");
            throw null;
        } else if (transport2 == null) {
            Intrinsics.a("transport");
            throw null;
        } else if (list != null) {
            this.quarantineRequirementsAccept = z;
            this.quarantineViolationLiability = z2;
            this.dataCorrectnessAccept = z3;
            this.sympthoms = bool;
            this.passenger = passenger2;
            this.document = document2;
            this.travel = travel2;
            this.transport = transport2;
            this.children = list;
        } else {
            Intrinsics.a("children");
            throw null;
        }
    }

    public static /* synthetic */ ProfileData copy$default(ProfileData profileData, boolean z, boolean z2, boolean z3, Boolean bool, Passenger passenger2, Document document2, Travel travel2, Transport transport2, List list, int i2, Object obj) {
        ProfileData profileData2 = profileData;
        int i3 = i2;
        return profileData.copy((i3 & 1) != 0 ? profileData2.quarantineRequirementsAccept : z, (i3 & 2) != 0 ? profileData2.quarantineViolationLiability : z2, (i3 & 4) != 0 ? profileData2.dataCorrectnessAccept : z3, (i3 & 8) != 0 ? profileData2.sympthoms : bool, (i3 & 16) != 0 ? profileData2.passenger : passenger2, (i3 & 32) != 0 ? profileData2.document : document2, (i3 & 64) != 0 ? profileData2.travel : travel2, (i3 & 128) != 0 ? profileData2.transport : transport2, (i3 & 256) != 0 ? profileData2.children : list);
    }

    public final boolean component1() {
        return this.quarantineRequirementsAccept;
    }

    public final boolean component2() {
        return this.quarantineViolationLiability;
    }

    public final boolean component3() {
        return this.dataCorrectnessAccept;
    }

    public final Boolean component4() {
        return this.sympthoms;
    }

    public final Passenger component5() {
        return this.passenger;
    }

    public final Document component6() {
        return this.document;
    }

    public final Travel component7() {
        return this.travel;
    }

    public final Transport component8() {
        return this.transport;
    }

    public final List<Children> component9() {
        return this.children;
    }

    public final ProfileData copy(boolean z, boolean z2, boolean z3, Boolean bool, Passenger passenger2, Document document2, Travel travel2, Transport transport2, List<Children> list) {
        if (passenger2 == null) {
            Intrinsics.a("passenger");
            throw null;
        } else if (document2 == null) {
            Intrinsics.a("document");
            throw null;
        } else if (travel2 == null) {
            Intrinsics.a("travel");
            throw null;
        } else if (transport2 == null) {
            Intrinsics.a("transport");
            throw null;
        } else if (list != null) {
            return new ProfileData(z, z2, z3, bool, passenger2, document2, travel2, transport2, list);
        } else {
            Intrinsics.a("children");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileData)) {
            return false;
        }
        ProfileData profileData = (ProfileData) obj;
        return this.quarantineRequirementsAccept == profileData.quarantineRequirementsAccept && this.quarantineViolationLiability == profileData.quarantineViolationLiability && this.dataCorrectnessAccept == profileData.dataCorrectnessAccept && Intrinsics.a(this.sympthoms, profileData.sympthoms) && Intrinsics.a(this.passenger, profileData.passenger) && Intrinsics.a(this.document, profileData.document) && Intrinsics.a(this.travel, profileData.travel) && Intrinsics.a(this.transport, profileData.transport) && Intrinsics.a(this.children, profileData.children);
    }

    public final List<Children> getChildren() {
        return this.children;
    }

    public final boolean getDataCorrectnessAccept() {
        return this.dataCorrectnessAccept;
    }

    public final Document getDocument() {
        return this.document;
    }

    public final Passenger getPassenger() {
        return this.passenger;
    }

    public final boolean getQuarantineRequirementsAccept() {
        return this.quarantineRequirementsAccept;
    }

    public final boolean getQuarantineViolationLiability() {
        return this.quarantineViolationLiability;
    }

    public final Boolean getSympthoms() {
        return this.sympthoms;
    }

    public final Transport getTransport() {
        return this.transport;
    }

    public final Travel getTravel() {
        return this.travel;
    }

    public int hashCode() {
        boolean z = this.quarantineRequirementsAccept;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        boolean z3 = this.quarantineViolationLiability;
        if (z3) {
            z3 = true;
        }
        int i3 = (i2 + (z3 ? 1 : 0)) * 31;
        boolean z4 = this.dataCorrectnessAccept;
        if (!z4) {
            z2 = z4;
        }
        int i4 = (i3 + (z2 ? 1 : 0)) * 31;
        Boolean bool = this.sympthoms;
        int i5 = 0;
        int hashCode = (i4 + (bool != null ? bool.hashCode() : 0)) * 31;
        Passenger passenger2 = this.passenger;
        int hashCode2 = (hashCode + (passenger2 != null ? passenger2.hashCode() : 0)) * 31;
        Document document2 = this.document;
        int hashCode3 = (hashCode2 + (document2 != null ? document2.hashCode() : 0)) * 31;
        Travel travel2 = this.travel;
        int hashCode4 = (hashCode3 + (travel2 != null ? travel2.hashCode() : 0)) * 31;
        Transport transport2 = this.transport;
        int hashCode5 = (hashCode4 + (transport2 != null ? transport2.hashCode() : 0)) * 31;
        List<Children> list = this.children;
        if (list != null) {
            i5 = list.hashCode();
        }
        return hashCode5 + i5;
    }

    public final void setChildren(List<Children> list) {
        if (list != null) {
            this.children = list;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setDataCorrectnessAccept(boolean z) {
        this.dataCorrectnessAccept = z;
    }

    public final void setDocument(Document document2) {
        if (document2 != null) {
            this.document = document2;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setPassenger(Passenger passenger2) {
        if (passenger2 != null) {
            this.passenger = passenger2;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setQuarantineRequirementsAccept(boolean z) {
        this.quarantineRequirementsAccept = z;
    }

    public final void setQuarantineViolationLiability(boolean z) {
        this.quarantineViolationLiability = z;
    }

    public final void setSympthoms(Boolean bool) {
        this.sympthoms = bool;
    }

    public final void setTransport(Transport transport2) {
        if (transport2 != null) {
            this.transport = transport2;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setTravel(Travel travel2) {
        if (travel2 != null) {
            this.travel = travel2;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public String toString() {
        StringBuilder a = outline.a("ProfileData(quarantineRequirementsAccept=");
        a.append(this.quarantineRequirementsAccept);
        a.append(", quarantineViolationLiability=");
        a.append(this.quarantineViolationLiability);
        a.append(", dataCorrectnessAccept=");
        a.append(this.dataCorrectnessAccept);
        a.append(", sympthoms=");
        a.append(this.sympthoms);
        a.append(", passenger=");
        a.append(this.passenger);
        a.append(", document=");
        a.append(this.document);
        a.append(", travel=");
        a.append(this.travel);
        a.append(", transport=");
        a.append(this.transport);
        a.append(", children=");
        a.append(this.children);
        a.append(")");
        return a.toString();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ProfileData(boolean r24, boolean r25, boolean r26, java.lang.Boolean r27, ru.covid19.droid.data.model.profileData.Passenger r28, ru.covid19.droid.data.model.profileData.Document r29, ru.covid19.droid.data.model.profileData.Travel r30, ru.covid19.droid.data.model.profileData.Transport r31, java.util.List r32, int r33, n.n.c.DefaultConstructorMarker r34) {
        /*
            r23 = this;
            r0 = r33
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = 0
            goto L_0x000b
        L_0x0009:
            r1 = r24
        L_0x000b:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0011
            r3 = 0
            goto L_0x0013
        L_0x0011:
            r3 = r25
        L_0x0013:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x0018
            goto L_0x001a
        L_0x0018:
            r2 = r26
        L_0x001a:
            r4 = r0 & 8
            r5 = 0
            if (r4 == 0) goto L_0x0021
            r4 = r5
            goto L_0x0023
        L_0x0021:
            r4 = r27
        L_0x0023:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0044
            ru.covid19.droid.data.model.profileData.Passenger r6 = new ru.covid19.droid.data.model.profileData.Passenger
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 8191(0x1fff, float:1.1478E-41)
            r22 = 0
            r7 = r6
            r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            goto L_0x0046
        L_0x0044:
            r6 = r28
        L_0x0046:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x0061
            ru.covid19.droid.data.model.profileData.Document r7 = new ru.covid19.droid.data.model.profileData.Document
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 7
            r12 = 0
            r24 = r7
            r25 = r8
            r26 = r9
            r27 = r10
            r28 = r11
            r29 = r12
            r24.<init>(r25, r26, r27, r28, r29)
            goto L_0x0063
        L_0x0061:
            r7 = r29
        L_0x0063:
            r8 = r0 & 64
            if (r8 == 0) goto L_0x0077
            ru.covid19.droid.data.model.profileData.Travel r8 = new ru.covid19.droid.data.model.profileData.Travel
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 31
            r16 = 0
            r9 = r8
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            goto L_0x0079
        L_0x0077:
            r8 = r30
        L_0x0079:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0084
            ru.covid19.droid.data.model.profileData.Transport r9 = new ru.covid19.droid.data.model.profileData.Transport
            r10 = 3
            r9.<init>(r5, r5, r10, r5)
            goto L_0x0086
        L_0x0084:
            r9 = r31
        L_0x0086:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L_0x008d
            n.i.Collections2 r0 = n.i.Collections2.b
            goto L_0x008f
        L_0x008d:
            r0 = r32
        L_0x008f:
            r24 = r23
            r25 = r1
            r26 = r3
            r27 = r2
            r28 = r4
            r29 = r6
            r30 = r7
            r31 = r8
            r32 = r9
            r33 = r0
            r24.<init>(r25, r26, r27, r28, r29, r30, r31, r32, r33)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.data.model.profileData.ProfileData.<init>(boolean, boolean, boolean, java.lang.Boolean, ru.covid19.droid.data.model.profileData.Passenger, ru.covid19.droid.data.model.profileData.Document, ru.covid19.droid.data.model.profileData.Travel, ru.covid19.droid.data.model.profileData.Transport, java.util.List, int, n.n.c.DefaultConstructorMarker):void");
    }
}
