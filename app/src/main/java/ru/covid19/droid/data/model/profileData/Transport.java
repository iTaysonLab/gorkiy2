package ru.covid19.droid.data.model.profileData;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import e.a.b.h.b.h.o.TransportHealthStepViewState2;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: Transport.kt */
public final class Transport {
    public String name;
    public TransportHealthStepViewState2 type;

    public Transport() {
        this(null, null, 3, null);
    }

    public Transport(TransportHealthStepViewState2 transportHealthStepViewState2, String str) {
        if (str != null) {
            this.type = transportHealthStepViewState2;
            this.name = str;
            return;
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public static /* synthetic */ Transport copy$default(Transport transport, TransportHealthStepViewState2 transportHealthStepViewState2, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            transportHealthStepViewState2 = transport.type;
        }
        if ((i2 & 2) != 0) {
            str = transport.name;
        }
        return transport.copy(transportHealthStepViewState2, str);
    }

    public final TransportHealthStepViewState2 component1() {
        return this.type;
    }

    public final String component2() {
        return this.name;
    }

    public final Transport copy(TransportHealthStepViewState2 transportHealthStepViewState2, String str) {
        if (str != null) {
            return new Transport(transportHealthStepViewState2, str);
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Transport)) {
            return false;
        }
        Transport transport = (Transport) obj;
        return Intrinsics.a(this.type, transport.type) && Intrinsics.a(this.name, transport.name);
    }

    public final String getName() {
        return this.name;
    }

    public final TransportHealthStepViewState2 getType() {
        return this.type;
    }

    public int hashCode() {
        TransportHealthStepViewState2 transportHealthStepViewState2 = this.type;
        int i2 = 0;
        int hashCode = (transportHealthStepViewState2 != null ? transportHealthStepViewState2.hashCode() : 0) * 31;
        String str = this.name;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode + i2;
    }

    public final void setName(String str) {
        if (str != null) {
            this.name = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setType(TransportHealthStepViewState2 transportHealthStepViewState2) {
        this.type = transportHealthStepViewState2;
    }

    public String toString() {
        StringBuilder a = outline.a("Transport(type=");
        a.append(this.type);
        a.append(", name=");
        return outline.a(a, this.name, ")");
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Transport(TransportHealthStepViewState2 transportHealthStepViewState2, String str, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? null : transportHealthStepViewState2, (i2 & 2) != 0 ? "" : str);
    }
}
