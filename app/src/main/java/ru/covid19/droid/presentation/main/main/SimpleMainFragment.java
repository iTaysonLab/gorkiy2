package ru.covid19.droid.presentation.main.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.button.MaterialButton;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.f.SimpleMainFragmentViewState0;
import e.a.b.h.b.f.SimpleMainFragmentVm;
import e.a.b.h.b.f.g;
import e.a.b.h.b.f.h;
import e.c.c.b;
import j.c.a.a.c.n.c;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i._Arrays;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.base.BaseDpFragment;

/* compiled from: SimpleMainFragment.kt */
public final class SimpleMainFragment extends BaseDpFragment<g, h> {
    public HashMap d0;

    /* compiled from: SimpleMainFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return SimpleMainFragmentViewState0.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<h> P() {
        return SimpleMainFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_simple_main, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.button.MaterialButton, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List<R>]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends b>> g() {
        View view;
        List<Observable<? extends b>> g = super.g();
        int i2 = e.a.b.b.frag_simple_main_btn_open_questionnaire;
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view2 = (View) this.d0.get(Integer.valueOf(i2));
        if (view2 == null) {
            View view3 = this.H;
            if (view3 == null) {
                view = null;
                MaterialButton materialButton = (MaterialButton) view;
                Intrinsics.a((Object) materialButton, "frag_simple_main_btn_open_questionnaire");
                Observable<R> c = c.a((View) materialButton).c((Function) AnyToUnit.a);
                Intrinsics.a((Object) c, "RxView.clicks(this).map(AnyToUnit)");
                return _Arrays.a((Collection) g, (Iterable) c.c(c.c((Function) a.a)));
            }
            view2 = view3.findViewById(i2);
            this.d0.put(Integer.valueOf(i2), view2);
        }
        view = view2;
        MaterialButton materialButton2 = (MaterialButton) view;
        Intrinsics.a((Object) materialButton2, "frag_simple_main_btn_open_questionnaire");
        Observable<R> c2 = c.a((View) materialButton2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g, (Iterable) c.c(c2.c((Function) a.a)));
    }
}
