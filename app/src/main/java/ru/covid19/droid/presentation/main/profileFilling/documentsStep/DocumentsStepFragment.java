package ru.covid19.droid.presentation.main.profileFilling.documentsStep;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState0;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import e.a.b.h.b.h.m.DocumentsStepFragmentVm;
import j.a.a.a.outline;
import j.e.a.InitialValueObservable;
import j.e.a.b.AnyToUnit;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import ru.covid19.droid.data.model.profileData.Document;
import ru.covid19.droid.data.model.profileData.Document0;
import ru.covid19.droid.presentation.main.profileFilling.BaseStepValidatedFragment;

/* compiled from: DocumentsStepFragment.kt */
public final class DocumentsStepFragment extends BaseStepValidatedFragment<e.a.b.h.b.h.m.c, e.a.b.h.b.h.m.d> {
    public HashMap d0;

    /* compiled from: java-style lambda group */
    public static final class a<T, R> implements Function<T, R> {
        public static final a b = new a(0);
        public static final a c = new a(1);
        public final /* synthetic */ int a;

        public a(int i2) {
            this.a = i2;
        }

        public final Object a(Object obj) {
            int i2 = this.a;
            if (i2 != 0) {
                if (i2 != 1) {
                    throw null;
                } else if (((Unit) obj) != null) {
                    return DocumentsStepFragmentViewState1.h.a;
                } else {
                    Intrinsics.a("it");
                    throw null;
                }
            } else if (((Unit) obj) != null) {
                return DocumentsStepFragmentViewState1.h.a;
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public final /* synthetic */ DocumentsStepFragment a;

        public b(DocumentsStepFragment documentsStepFragment) {
            this.a = documentsStepFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            Document document;
            Document document2;
            Document document3;
            Document document4;
            Document0 document0 = null;
            if (((Unit) obj) != null) {
                DocumentsStepFragmentViewState[] documentsStepFragmentViewStateArr = new DocumentsStepFragmentViewState[4];
                Document0 document02 = Document0.russianPassport;
                String a2 = this.a.a((int) R.string.frag_filling_profile_documents_movement_passport);
                Intrinsics.a((Object) a2, "getString(R.string.frag_…uments_movement_passport)");
                DocumentsStepFragmentViewState0 documentsStepFragmentViewState0 = (DocumentsStepFragmentViewState0) ((DocumentsStepFragmentVm) this.a.h()).d.c();
                boolean z = true;
                documentsStepFragmentViewStateArr[0] = new DocumentsStepFragmentViewState(document02, a2, ((documentsStepFragmentViewState0 == null || (document4 = documentsStepFragmentViewState0.a) == null) ? null : document4.getType()) == Document0.russianPassport);
                Document0 document03 = Document0.birthCertificate;
                String a3 = this.a.a((int) R.string.frag_filling_profile_documents_movement_birth_certificate);
                Intrinsics.a((Object) a3, "getString(R.string.frag_…vement_birth_certificate)");
                DocumentsStepFragmentViewState0 documentsStepFragmentViewState02 = (DocumentsStepFragmentViewState0) ((DocumentsStepFragmentVm) this.a.h()).d.c();
                documentsStepFragmentViewStateArr[1] = new DocumentsStepFragmentViewState(document03, a3, ((documentsStepFragmentViewState02 == null || (document3 = documentsStepFragmentViewState02.a) == null) ? null : document3.getType()) == Document0.birthCertificate);
                Document0 document04 = Document0.internationalPassport;
                String a4 = this.a.a((int) R.string.frag_filling_profile_documents_movement_frgn_passport);
                Intrinsics.a((Object) a4, "getString(R.string.frag_…s_movement_frgn_passport)");
                DocumentsStepFragmentViewState0 documentsStepFragmentViewState03 = (DocumentsStepFragmentViewState0) ((DocumentsStepFragmentVm) this.a.h()).d.c();
                documentsStepFragmentViewStateArr[2] = new DocumentsStepFragmentViewState(document04, a4, ((documentsStepFragmentViewState03 == null || (document2 = documentsStepFragmentViewState03.a) == null) ? null : document2.getType()) == Document0.internationalPassport);
                Document0 document05 = Document0.foreignCitizenDoc;
                String a5 = this.a.a((int) R.string.frag_filling_profile_documents_movement_fid);
                Intrinsics.a((Object) a5, "getString(R.string.frag_…e_documents_movement_fid)");
                DocumentsStepFragmentViewState0 documentsStepFragmentViewState04 = (DocumentsStepFragmentViewState0) ((DocumentsStepFragmentVm) this.a.h()).d.c();
                if (!(documentsStepFragmentViewState04 == null || (document = documentsStepFragmentViewState04.a) == null)) {
                    document0 = document.getType();
                }
                if (document0 != Document0.foreignCitizenDoc) {
                    z = false;
                }
                documentsStepFragmentViewStateArr[3] = new DocumentsStepFragmentViewState(document05, a5, z);
                List a6 = Collections.a((Object[]) documentsStepFragmentViewStateArr);
                String a7 = this.a.a((int) R.string.frag_filling_profile_document_type);
                Intrinsics.a((Object) a7, "getString(R.string.frag_…ng_profile_document_type)");
                return new DocumentsStepFragmentViewState1.k(a6, a7);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public final /* synthetic */ DocumentsStepFragment a;

        public c(DocumentsStepFragment documentsStepFragment) {
            this.a = documentsStepFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                String a2 = this.a.a((int) R.string.frag_filling_profile_documents_movement_country);
                Intrinsics.a((Object) a2, "getString(R.string.frag_…cuments_movement_country)");
                return new DocumentsStepFragmentViewState1.j(a2);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new DocumentsStepFragmentViewState1.g(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new DocumentsStepFragmentViewState1.e(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new DocumentsStepFragmentViewState1.f(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class g<T, R> implements Function<T, R> {
        public static final g a = new g();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new DocumentsStepFragmentViewState1.c(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DocumentsStepFragment.kt */
    public static final class h extends j implements Functions0<Date, n.g> {
        public final /* synthetic */ DocumentsStepFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(DocumentsStepFragment documentsStepFragment) {
            super(1);
            this.c = documentsStepFragment;
        }

        public Object a(Object obj) {
            Date date = (Date) obj;
            if (date != null) {
                this.c.c0.a((e.c.c.b) new DocumentsStepFragmentViewState1.d(date));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.h.m.d> P() {
        return DocumentsStepFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x018f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r12) {
        /*
            r11 = this;
            e.a.b.h.b.h.m.DocumentsStepFragmentViewState0 r12 = (e.a.b.h.b.h.m.DocumentsStepFragmentViewState0) r12
            r0 = 0
            if (r12 == 0) goto L_0x01ac
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Document r2 = r12.a
            ru.covid19.droid.data.model.profileData.Document0 r2 = r2.getType()
            r3 = 2131624056(0x7f0e0078, float:1.887528E38)
            r4 = 2131624055(0x7f0e0077, float:1.8875279E38)
            r5 = 2131624052(0x7f0e0074, float:1.8875273E38)
            r6 = 2131624057(0x7f0e0079, float:1.8875283E38)
            java.lang.String r7 = ""
            r8 = 3
            r9 = 2
            r10 = 1
            if (r2 != 0) goto L_0x0027
            goto L_0x0033
        L_0x0027:
            int r2 = r2.ordinal()
            if (r2 == 0) goto L_0x0044
            if (r2 == r10) goto L_0x003f
            if (r2 == r9) goto L_0x003a
            if (r2 == r8) goto L_0x0035
        L_0x0033:
            r2 = r7
            goto L_0x0048
        L_0x0035:
            java.lang.String r2 = r11.a(r5)
            goto L_0x0048
        L_0x003a:
            java.lang.String r2 = r11.a(r4)
            goto L_0x0048
        L_0x003f:
            java.lang.String r2 = r11.a(r3)
            goto L_0x0048
        L_0x0044:
            java.lang.String r2 = r11.a(r6)
        L_0x0048:
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Document r2 = r12.a
            java.lang.String r2 = r2.getSeries()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_number
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Document r2 = r12.a
            java.lang.String r2 = r2.getNumber()
            r1.setText(r2)
            ru.covid19.droid.data.model.profileData.Document r1 = r12.a
            ru.covid19.droid.data.model.profileData.Document0 r1 = r1.getType()
            java.lang.String r2 = "frag_filling_profile_documents_til_document_series"
            if (r1 != 0) goto L_0x0078
            goto L_0x0084
        L_0x0078:
            int r1 = r1.ordinal()
            if (r1 == 0) goto L_0x00ed
            if (r1 == r10) goto L_0x00d3
            if (r1 == r9) goto L_0x00b8
            if (r1 == r8) goto L_0x009e
        L_0x0084:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            r1.setText(r7)
            int r1 = e.a.b.b.frag_filling_profile_documents_til_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            n.n.c.Intrinsics.a(r1, r2)
            r1.setEnabled(r10)
            goto L_0x0106
        L_0x009e:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            r1.setText(r5)
            int r1 = e.a.b.b.frag_filling_profile_documents_til_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            n.n.c.Intrinsics.a(r1, r2)
            r1.setEnabled(r10)
            goto L_0x0106
        L_0x00b8:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            r1.setText(r4)
            int r1 = e.a.b.b.frag_filling_profile_documents_til_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            n.n.c.Intrinsics.a(r1, r2)
            r2 = 0
            r1.setEnabled(r2)
            goto L_0x0106
        L_0x00d3:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            r1.setText(r3)
            int r1 = e.a.b.b.frag_filling_profile_documents_til_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            n.n.c.Intrinsics.a(r1, r2)
            r1.setEnabled(r10)
            goto L_0x0106
        L_0x00ed:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_document_type
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            r1.setText(r6)
            int r1 = e.a.b.b.frag_filling_profile_documents_til_document_series
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputLayout r1 = (com.google.android.material.textfield.TextInputLayout) r1
            n.n.c.Intrinsics.a(r1, r2)
            r1.setEnabled(r10)
        L_0x0106:
            int r1 = e.a.b.b.frag_filling_profile_documents_et_arrival_date
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Travel r2 = r12.b
            int r2 = r2.getArrivalDate()
            java.lang.String r2 = n.i.Collections.a(r2, r0, r10)
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_documents_et_country
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Travel r2 = r12.b
            java.lang.String r2 = r2.getCountryName()
            r1.setText(r2)
            int r1 = e.a.b.b.frag_filling_profile_documents_et_place_of_entry
            android.view.View r1 = r11.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Travel r2 = r12.b
            java.lang.String r2 = r2.getArrivalPlace()
            r1.setText(r2)
            android.net.Uri r1 = r12.c
            java.lang.String r2 = "frag_filling_profile_documents_add_photo"
            java.lang.String r3 = "frag_filling_profile_documents_page_iv_photo"
            if (r1 == 0) goto L_0x018f
            android.content.Context r1 = r11.J()
            j.b.a.RequestManager r1 = j.b.a.Glide.b(r1)
            android.net.Uri r12 = r12.c
            if (r1 == 0) goto L_0x018e
            java.lang.Class<android.graphics.drawable.Drawable> r0 = android.graphics.drawable.Drawable.class
            j.b.a.RequestBuilder r4 = new j.b.a.RequestBuilder
            j.b.a.Glide r5 = r1.b
            android.content.Context r6 = r1.c
            r4.<init>(r5, r1, r0, r6)
            r4.G = r12
            r4.J = r10
            j.b.a.q.BaseRequestOptions r12 = r4.b()
            j.b.a.RequestBuilder r12 = (j.b.a.RequestBuilder) r12
            int r0 = e.a.b.b.frag_filling_profile_documents_page_iv_photo
            android.view.View r0 = r11.c(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            r12.a(r0)
            int r12 = e.a.b.b.frag_filling_profile_documents_page_iv_photo
            android.view.View r12 = r11.c(r12)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            n.n.c.Intrinsics.a(r12, r3)
            n.i.Collections.b(r12)
            int r12 = e.a.b.b.frag_filling_profile_documents_add_photo
            android.view.View r12 = r11.c(r12)
            androidx.constraintlayout.widget.ConstraintLayout r12 = (androidx.constraintlayout.widget.ConstraintLayout) r12
            n.n.c.Intrinsics.a(r12, r2)
            n.i.Collections.a(r12)
            goto L_0x01ab
        L_0x018e:
            throw r0
        L_0x018f:
            int r12 = e.a.b.b.frag_filling_profile_documents_page_iv_photo
            android.view.View r12 = r11.c(r12)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            n.n.c.Intrinsics.a(r12, r3)
            n.i.Collections.a(r12)
            int r12 = e.a.b.b.frag_filling_profile_documents_add_photo
            android.view.View r12 = r11.c(r12)
            androidx.constraintlayout.widget.ConstraintLayout r12 = (androidx.constraintlayout.widget.ConstraintLayout) r12
            n.n.c.Intrinsics.a(r12, r2)
            n.i.Collections.b(r12)
        L_0x01ab:
            return
        L_0x01ac:
            java.lang.String r12 = "vs"
            n.n.c.Intrinsics.a(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.presentation.main.profileFilling.documentsStep.DocumentsStepFragment.a(java.lang.Object):void");
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [androidx.constraintlayout.widget.ConstraintLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g2 = super.g();
        TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_document_type);
        Intrinsics.a((Object) textInputEditText, "frag_filling_profile_documents_et_document_type");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) textInputEditText).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText2 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_country);
        Intrinsics.a((Object) textInputEditText2, "frag_filling_profile_documents_et_country");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) textInputEditText2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText3 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_document_series);
        TextInputEditText textInputEditText4 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_document_number);
        TextInputEditText textInputEditText5 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_place_of_entry);
        ConstraintLayout constraintLayout = (ConstraintLayout) c(e.a.b.b.frag_filling_profile_documents_add_photo);
        Intrinsics.a((Object) constraintLayout, "frag_filling_profile_documents_add_photo");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) constraintLayout).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        ImageView imageView = (ImageView) c(e.a.b.b.frag_filling_profile_documents_page_iv_photo);
        Intrinsics.a((Object) imageView, "frag_filling_profile_documents_page_iv_photo");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) imageView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText6 = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_city);
        return _Arrays.a((Collection) g2, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) new b(this)), c3.c((Function) new c(this)), outline.a(textInputEditText3, "frag_filling_profile_documents_et_document_series", textInputEditText3, "RxTextView.textChanges(this)").c((Function) d.a), outline.a(textInputEditText4, "frag_filling_profile_documents_et_document_number", textInputEditText4, "RxTextView.textChanges(this)").c((Function) e.a), outline.a(textInputEditText5, "frag_filling_profile_documents_et_place_of_entry", textInputEditText5, "RxTextView.textChanges(this)").c((Function) f.a), c4.c((Function) a.b), c5.c((Function) a.c), new InitialValueObservable.a().c((Function) g.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_filling_profile_documents, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(View view, Bundle bundle) {
        if (view != null) {
            TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_filling_profile_documents_et_arrival_date);
            Intrinsics.a((Object) textInputEditText, "frag_filling_profile_documents_et_arrival_date");
            Calendar instance = Calendar.getInstance();
            instance.add(5, 1);
            Intrinsics.a((Object) instance, "Calendar.getInstance().a…F_MONTH, 1)\n            }");
            Collections.a(textInputEditText, Long.valueOf(instance.getTimeInMillis()), null, null, new h(this), 6);
            return;
        }
        Intrinsics.a("view");
        throw null;
    }
}
