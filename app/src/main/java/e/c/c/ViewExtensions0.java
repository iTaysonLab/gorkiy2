package e.c.c;

import l.b.t.Function;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: ViewExtensions.kt */
public final class ViewExtensions0<T, R> implements Function<T, R> {
    public final /* synthetic */ Functions0 a;

    public ViewExtensions0(Functions0 functions0) {
        this.a = functions0;
    }

    public Object a(Object obj) {
        String str = (String) obj;
        if (str != null) {
            return (BaseViewState0) this.a.a(str);
        }
        Intrinsics.a("it");
        throw null;
    }
}
