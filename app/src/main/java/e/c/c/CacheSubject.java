package e.c.c;

import j.a.a.a.outline;
import j.e.b.PublishRelay;
import java.util.LinkedList;
import l.b.Observable;
import l.b.Observer;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableDoOnEach;
import l.b.u.e.c.ObservableEmpty;
import l.b.u.e.c.ObservableFromIterable;
import l.b.u.e.c.ObservableRange;
import n.n.c.Intrinsics;
import n.o.d;

/* compiled from: CacheSubject.kt */
public final class CacheSubject<T> extends Observable<T> {
    public final PublishRelay<T> b;
    public final LinkedList<T> c = new LinkedList<>();

    /* compiled from: CacheSubject.kt */
    public static final class a<T, R> implements Function<T, R> {
        public final /* synthetic */ CacheSubject a;

        public a(CacheSubject cacheSubject) {
            this.a = cacheSubject;
        }

        public Object a(Object obj) {
            if (((Integer) obj) != null) {
                return this.a.c.poll();
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: CacheSubject.kt */
    public static final class b<T> implements Consumer<T> {
        public final /* synthetic */ Observer b;

        public b(Observer observer) {
            this.b = observer;
        }

        public final void a(T t2) {
            this.b.b(t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public CacheSubject() {
        PublishRelay<T> publishRelay = new PublishRelay<>();
        Intrinsics.a((Object) publishRelay, "PublishRelay.create<T>()");
        this.b = publishRelay;
    }

    public final void a(l lVar) {
        if (((PublishRelay.a[]) this.b.b.get()).length != 0) {
            this.b.a((b) lVar);
        } else {
            this.c.add(lVar);
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [n.o.Ranges0, java.lang.Object, n.o.Progressions, java.lang.Iterable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.c.c.CacheSubject$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<java.lang.Object>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Action, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b
     arg types: [l.b.t.Consumer<java.lang.Object>, l.b.t.Consumer<java.lang.Throwable>, l.b.t.Action, l.b.t.Consumer<java.lang.Object>]
     candidates:
      l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R>
      l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b */
    public void b(Observer<? super l> observer) {
        Observable observable;
        if (observer != null) {
            ? b2 = d.b(0, this.c.size());
            if (b2 != 0) {
                if (b2.d == 1) {
                    int i2 = b2.c;
                    int i3 = b2.b;
                    long j2 = (long) i3;
                    if (((long) i2) - j2 < ((long) Integer.MAX_VALUE)) {
                        int max = Math.max(0, (i2 - i3) + 1);
                        if (max >= 0) {
                            if (max == 0) {
                                observable = ObservableEmpty.b;
                            } else if (max == 1) {
                                observable = Observable.d(Integer.valueOf(i3));
                            } else if (j2 + ((long) (max - 1)) <= 2147483647L) {
                                observable = new ObservableRange(i3, max);
                            } else {
                                throw new IllegalArgumentException("Integer overflow");
                            }
                            Intrinsics.a((Object) observable, "Observable.range(first, …max(0, last - first + 1))");
                            Observable c2 = super.c((Function) new a(this));
                            b bVar = new b(observer);
                            Consumer<Object> consumer = Functions.d;
                            Action action = Functions.c;
                            ObjectHelper.a((Object) bVar, "onNext is null");
                            ObjectHelper.a((Object) consumer, "onError is null");
                            ObjectHelper.a((Object) action, "onComplete is null");
                            ObjectHelper.a((Object) action, "onAfterTerminate is null");
                            new ObservableDoOnEach(c2, bVar, consumer, action, action).a((Consumer) Functions.d, (Consumer<? super Throwable>) Functions.f2676e, (l.b.t.a) Functions.c, (Consumer<? super l.b.s.b>) Functions.d);
                            this.b.a((Observer) observer);
                            return;
                        }
                        throw new IllegalArgumentException(outline.b("count >= 0 required but it was ", max));
                    }
                }
                ObjectHelper.a((Object) b2, "source is null");
                observable = new ObservableFromIterable(b2);
                Intrinsics.a((Object) observable, "Observable.fromIterable(this)");
                Observable c22 = super.c((Function) new a(this));
                b bVar2 = new b(observer);
                Consumer<Object> consumer2 = Functions.d;
                Action action2 = Functions.c;
                ObjectHelper.a((Object) bVar2, "onNext is null");
                ObjectHelper.a((Object) consumer2, "onError is null");
                ObjectHelper.a((Object) action2, "onComplete is null");
                ObjectHelper.a((Object) action2, "onAfterTerminate is null");
                new ObservableDoOnEach(c22, bVar2, consumer2, action2, action2).a((Consumer) Functions.d, (Consumer<? super Throwable>) Functions.f2676e, (l.b.t.a) Functions.c, (Consumer<? super l.b.s.b>) Functions.d);
                this.b.a((Observer) observer);
                return;
            }
            Intrinsics.a("$this$toObservable");
            throw null;
        }
        Intrinsics.a("observer");
        throw null;
    }
}
