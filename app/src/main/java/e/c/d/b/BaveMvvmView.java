package e.c.d.b;

import e.c.d.c.a;
import l.b.s.CompositeDisposable;

/* compiled from: BaveMvvmView.kt */
public interface BaveMvvmView<VM extends a> {
    VM h();

    CompositeDisposable i();
}
