package e.c.d.b.b;

import android.app.Application;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import e.c.d.b.BaveMvvmView;
import e.c.d.b.c.OnBackPressedListener;
import e.c.d.c.BaseVm;
import e.c.d.c.a;
import i.b.k.h;
import i.b.q.VectorEnabledTintResources;
import i.l.a.FragmentManager;
import i.o.ViewModel;
import i.o.ViewModelProvider;
import i.o.ViewModelStore;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import l.b.s.CompositeDisposable;
import n.Lazy;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.j;
import n.p.KProperty;

/* compiled from: BaseMvvmActivity.kt */
public abstract class BaseMvvmActivity<VM extends e.c.d.c.a> extends h implements BaveMvvmView<VM> {

    /* renamed from: t  reason: collision with root package name */
    public static final /* synthetic */ KProperty[] f723t;

    /* renamed from: q  reason: collision with root package name */
    public final Lazy f724q = c.a((Functions) new a(this));

    /* renamed from: r  reason: collision with root package name */
    public boolean f725r = true;

    /* renamed from: s  reason: collision with root package name */
    public final CompositeDisposable f726s = new CompositeDisposable();

    /* compiled from: BaseMvvmActivity.kt */
    public static final class a extends j implements Functions<VM> {
        public final /* synthetic */ BaseMvvmActivity c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseMvvmActivity baseMvvmActivity) {
            super(0);
            this.c = baseMvvmActivity;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [e.c.d.b.b.BaseMvvmActivity, androidx.activity.ComponentActivity, android.app.Activity] */
        public Object b() {
            ViewModel viewModel;
            ? r0 = this.c;
            Object q2 = r0.q();
            Application application = r0.getApplication();
            if (application != null) {
                if (q2 == null) {
                    if (ViewModelProvider.a.b == null) {
                        ViewModelProvider.a.b = new ViewModelProvider.a(application);
                    }
                    q2 = ViewModelProvider.a.b;
                }
                ViewModelStore f2 = r0.f();
                Class p2 = this.c.p();
                String canonicalName = p2.getCanonicalName();
                if (canonicalName != null) {
                    String a = outline.a("androidx.lifecycle.ViewModelProvider.DefaultKey:", canonicalName);
                    ViewModel viewModel2 = f2.a.get(a);
                    if (!p2.isInstance(viewModel2)) {
                        if (q2 instanceof ViewModelProvider.c) {
                            viewModel = ((ViewModelProvider.c) q2).a(a, p2);
                        } else {
                            viewModel = q2.a(p2);
                        }
                        viewModel2 = viewModel;
                        ViewModel put = f2.a.put(a, viewModel2);
                        if (put != null) {
                            put.b();
                        }
                    }
                    return (BaseVm) viewModel2;
                }
                throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
            }
            throw new IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
        }
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    static {
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(BaseMvvmActivity.class), "vm", "getVm()Lru/waveaccess/wamvvmrx/vm/BaseVm;");
        Reflection.a(propertyReference1Impl);
        f723t = new KProperty[]{propertyReference1Impl};
    }

    public VM h() {
        Lazy lazy = this.f724q;
        KProperty kProperty = f723t[0];
        return (BaseVm) lazy.getValue();
    }

    public CompositeDisposable i() {
        return this.f726s;
    }

    public void o() {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [e.c.d.b.b.BaseMvvmActivity, androidx.activity.ComponentActivity, i.l.a.FragmentActivity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [i.l.a.FragmentManager, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void onBackPressed() {
        FragmentManager j2 = j();
        Intrinsics.a((Object) j2, "supportFragmentManager");
        for (Fragment next : j2.a()) {
            if ((next instanceof OnBackPressedListener) && ((OnBackPressedListener) next).e()) {
                return;
            }
        }
        this.f2f.a();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.c.d.b.b.BaseMvvmActivity, i.b.k.AppCompatActivity] */
    public void onCreate(Bundle bundle) {
        h().a(bundle);
        BaseMvvmActivity.super.onCreate(bundle);
        VectorEnabledTintResources.a = true;
        if (bundle != null) {
            this.f725r = false;
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.c.d.b.b.BaseMvvmActivity, i.l.a.FragmentActivity] */
    public void onPause() {
        BaseMvvmActivity.super.onPause();
        this.f726s.a();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.c.d.b.b.BaseMvvmActivity, i.l.a.FragmentActivity] */
    public void onResume() {
        BaseMvvmActivity.super.onResume();
        o();
        if (this.f725r) {
            this.f725r = false;
        }
    }

    public abstract Class<VM> p();

    public abstract ViewModelProvider.b q();
}
