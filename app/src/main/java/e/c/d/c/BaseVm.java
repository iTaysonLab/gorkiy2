package e.c.d.c;

import android.os.Bundle;
import i.o.ViewModel;
import l.b.s.CompositeDisposable;

/* compiled from: BaseVm.kt */
public abstract class BaseVm extends ViewModel {
    public final CompositeDisposable b = new CompositeDisposable();
    public boolean c;

    public void a(Bundle bundle) {
        if (!this.c && bundle != null && !bundle.isEmpty()) {
            c();
        } else if (!this.c && (bundle == null || bundle.isEmpty())) {
            c();
        }
        this.c = true;
    }

    public void b() {
        this.b.a();
    }

    public abstract void c();
}
