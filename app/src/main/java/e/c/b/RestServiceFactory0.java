package e.c.b;

import e.c.b.j.b;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.Map;
import l.b.t.Consumer;
import n.n.c.Intrinsics;
import r.Platform;
import r.Retrofit;
import r.Retrofit0;
import r.e0;

/* compiled from: RestServiceFactory.kt */
public final class RestServiceFactory0<T> implements Consumer<e0> {
    public final /* synthetic */ RestServiceFactory b;

    public RestServiceFactory0(RestServiceFactory restServiceFactory) {
        this.b = restServiceFactory;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [T, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Retrofit0 retrofit0 = (Retrofit0) obj;
        RestServiceFactory restServiceFactory = this.b;
        Class<T> cls = restServiceFactory.f713e;
        if (retrofit0 == null) {
            throw null;
        } else if (!cls.isInterface()) {
            throw new IllegalArgumentException("API declarations must be interfaces.");
        } else if (cls.getInterfaces().length <= 0) {
            if (retrofit0.f3203f) {
                Platform platform = Platform.a;
                for (Method method : cls.getDeclaredMethods()) {
                    if (!platform.a(method) && !Modifier.isStatic(method.getModifiers())) {
                        retrofit0.a(method);
                    }
                }
            }
            T newProxyInstance = Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new Retrofit(retrofit0, cls));
            Intrinsics.a((Object) newProxyInstance, "it.create(clazz)");
            restServiceFactory.a = newProxyInstance;
            this.b.b.b((Map<b, String>) true);
        } else {
            throw new IllegalArgumentException("API interfaces must not extend other interfaces.");
        }
    }
}
