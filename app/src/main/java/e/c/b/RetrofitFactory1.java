package e.c.b;

import e.c.b.j.b;
import java.util.Map;
import l.b.t.Consumer;
import n.Unit;
import n.n.c.Intrinsics;

/* compiled from: RetrofitFactory.kt */
public final class RetrofitFactory1<T> implements Consumer<Boolean> {
    public final /* synthetic */ RetrofitFactory b;

    public RetrofitFactory1(RetrofitFactory retrofitFactory) {
        this.b = retrofitFactory;
    }

    public void a(Object obj) {
        Boolean bool = (Boolean) obj;
        this.b.d.b((Map<b, String>) false);
        RetrofitFactory retrofitFactory = this.b;
        retrofitFactory.b.b((Map<b, String>) retrofitFactory.a());
        Map<b, String> a = retrofitFactory.f714e.a();
        if (a != null) {
            retrofitFactory.a(a);
            this.b.d.b((Map<b, String>) true);
            this.b.c.b((Map<b, String>) Unit.a);
            return;
        }
        Intrinsics.a();
        throw null;
    }
}
