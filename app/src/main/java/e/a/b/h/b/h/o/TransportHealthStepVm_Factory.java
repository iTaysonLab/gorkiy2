package e.a.b.h.b.h.o;

import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class TransportHealthStepVm_Factory implements Factory<g> {
    public final Provider<g> a;
    public final Provider<a> b;

    public TransportHealthStepVm_Factory(Provider<g> provider, Provider<a> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    public Object get() {
        return new TransportHealthStepVm(this.a.get(), this.b.get());
    }
}
