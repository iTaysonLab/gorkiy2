package e.a.b.h.b.f;

import e.a.b.h.b.f.MainFragmentViewState1;
import e.c.c.BaseMviVm0;
import l.b.t.Function;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.Statistic;

/* compiled from: MainFragmentVm.kt */
public final class MainFragmentVm0<T, R> implements Function<T, R> {
    public static final MainFragmentVm0 a = new MainFragmentVm0();

    public Object a(Object obj) {
        Statistic statistic = (Statistic) obj;
        if (statistic != null) {
            return new BaseMviVm0.a(new MainFragmentViewState1.a(statistic));
        }
        Intrinsics.a("statistic");
        throw null;
    }
}
