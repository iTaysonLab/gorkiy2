package e.a.b.h.b.h.m;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import l.b.t.Function;
import n.Tuples;
import n.Unit;
import n.i.Collections;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CountriesResponse;
import ru.covid19.core.data.network.model.CountriesResponse0;
import ru.covid19.core.data.network.model.CountriesResponse1;
import ru.covid19.core.data.network.model.Country;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm2<T, R> implements Function<T, R> {
    public final /* synthetic */ DocumentsStepFragmentVm a;

    public DocumentsStepFragmentVm2(DocumentsStepFragmentVm documentsStepFragmentVm) {
        this.a = documentsStepFragmentVm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public Object a(Object obj) {
        Tuples tuples = (Tuples) obj;
        if (tuples != null) {
            IMainCoordinator iMainCoordinator = this.a.f692n;
            String str = ((DocumentsStepFragmentViewState1.j) tuples.b).a;
            List<Country> countries = ((CountriesResponse) tuples.c).getCountries();
            ArrayList arrayList = new ArrayList(Collections.a(countries, 10));
            Iterator<T> it = countries.iterator();
            while (it.hasNext()) {
                CountriesResponse0 countriesResponse0 = (CountriesResponse0) it.next();
                arrayList.add(new CountriesResponse1(countriesResponse0, countriesResponse0.getTitle(), Intrinsics.a((Object) countriesResponse0.getCode(), (Object) DocumentsStepFragmentVm.b(this.a).b.getCountry())));
            }
            iMainCoordinator.a(new BottomSheetSelectorNavigationDto0(str, arrayList));
            return Unit.a;
        }
        Intrinsics.a("pair");
        throw null;
    }
}
