package e.a.b.h.b.h.n;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingResultFragmentViewState.kt */
public final class ProfileFillingResultFragmentViewState2 extends ProfileFillingResultFragmentViewState {
    public final String a;
    public final String b;
    public final int c;

    public ProfileFillingResultFragmentViewState2(String str, String str2, int i2) {
        if (str == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str2 != null) {
            this.a = str;
            this.b = str2;
            this.c = i2;
        } else {
            Intrinsics.a("birthDate");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileFillingResultFragmentViewState2)) {
            return false;
        }
        ProfileFillingResultFragmentViewState2 profileFillingResultFragmentViewState2 = (ProfileFillingResultFragmentViewState2) obj;
        return Intrinsics.a(this.a, profileFillingResultFragmentViewState2.a) && Intrinsics.a(this.b, profileFillingResultFragmentViewState2.b) && this.c == profileFillingResultFragmentViewState2.c;
    }

    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return ((hashCode + i2) * 31) + this.c;
    }

    public String toString() {
        StringBuilder a2 = outline.a("FillingProfileResultPersonInfoItem(fullName=");
        a2.append(this.a);
        a2.append(", birthDate=");
        a2.append(this.b);
        a2.append(", personNumber=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
