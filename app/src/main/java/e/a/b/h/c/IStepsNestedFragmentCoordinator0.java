package e.a.b.h.c;

import n.n.c.Intrinsics;

/* compiled from: IStepsNestedFragmentCoordinator.kt */
public final class IStepsNestedFragmentCoordinator0 {
    public final String a;
    public final int b;
    public final boolean c;
    public final boolean d;

    public IStepsNestedFragmentCoordinator0(String str, int i2, boolean z, boolean z2) {
        if (str != null) {
            this.a = str;
            this.b = i2;
            this.c = z;
            this.d = z2;
            return;
        }
        Intrinsics.a("titleResId");
        throw null;
    }
}
