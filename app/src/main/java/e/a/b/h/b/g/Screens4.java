package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.questionnaire.pages.informedConfirmation.InformedConfirmationFragment;

/* compiled from: Screens.kt */
public final class Screens4 extends BaseScreens2 {
    public static final Screens4 d = new Screens4();

    public Screens4() {
        super(null, 1);
    }

    public Fragment a() {
        return new InformedConfirmationFragment();
    }
}
