package e.a.b.h.b.f;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.b.h.b.f.SimpleMainFragmentViewState0;
import e.a.b.h.b.f.f;
import e.a.b.h.b.g.IMainCoordinator;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.b;
import e.c.c.k;
import l.b.Observable;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: SimpleMainFragmentVm.kt */
public final class SimpleMainFragmentVm extends BaseDpFragmentVm1<g> {

    /* renamed from: j  reason: collision with root package name */
    public final IMainCoordinator f674j;

    /* compiled from: SimpleMainFragmentVm.kt */
    public static final class a extends j implements Functions0<f.a, g> {
        public final /* synthetic */ SimpleMainFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(SimpleMainFragmentVm simpleMainFragmentVm) {
            super(1);
            this.c = simpleMainFragmentVm;
        }

        public Object a(Object obj) {
            if (((SimpleMainFragmentViewState0.a) obj) != null) {
                this.c.f674j.e();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SimpleMainFragmentVm(IMainCoordinator iMainCoordinator) {
        super(iMainCoordinator);
        if (iMainCoordinator != null) {
            this.f674j = iMainCoordinator;
            return;
        }
        Intrinsics.a("mainNavigator");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(SimpleMainFragmentViewState0.a.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a3 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)));
            Intrinsics.a((Object) a3, "Observable.mergeArray(\n …)\n            }\n        )");
            return a3;
        }
        Intrinsics.a("o");
        throw null;
    }

    public Object d() {
        return SimpleMainFragmentViewState.a;
    }
}
