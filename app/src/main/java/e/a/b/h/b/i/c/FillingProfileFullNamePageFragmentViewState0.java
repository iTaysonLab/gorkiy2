package e.a.b.h.b.i.c;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.ProfileData0;

/* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
public final class FillingProfileFullNamePageFragmentViewState0 implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new a();
    public final ProfileData0 b;
    public final String c;
    public final boolean d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new FillingProfileFullNamePageFragmentViewState0((ProfileData0) Enum.valueOf(ProfileData0.class, parcel.readString()), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new FillingProfileFullNamePageFragmentViewState0[i2];
        }
    }

    public FillingProfileFullNamePageFragmentViewState0(ProfileData0 profileData0, String str, boolean z) {
        if (profileData0 == null) {
            Intrinsics.a("gender");
            throw null;
        } else if (str != null) {
            this.b = profileData0;
            this.c = str;
            this.d = z;
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getLabel() {
        return this.c;
    }

    public boolean isSelected() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
            parcel.writeString(this.c);
            parcel.writeInt(this.d ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
