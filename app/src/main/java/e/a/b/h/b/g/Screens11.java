package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.profileFilling.transportHealthStep.TransportHealthStepFragment;

/* compiled from: Screens.kt */
public final class Screens11 extends BaseScreens2 {
    public static final Screens11 d = new Screens11();

    public Screens11() {
        super(null, 1);
    }

    public Fragment a() {
        return new TransportHealthStepFragment();
    }
}
