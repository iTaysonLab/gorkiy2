package e.a.b.h.b.h.n;

import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: ProfileFillingResultFragmentViewState.kt */
public final class ProfileFillingResultFragmentViewState0 {
    public final ProfileData a;

    public ProfileFillingResultFragmentViewState0() {
        this(null, 1);
    }

    public ProfileFillingResultFragmentViewState0(ProfileData profileData) {
        if (profileData != null) {
            this.a = profileData;
        } else {
            Intrinsics.a("profile");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof ProfileFillingResultFragmentViewState0) && Intrinsics.a(this.a, ((ProfileFillingResultFragmentViewState0) obj).a);
        }
        return true;
    }

    public int hashCode() {
        ProfileData profileData = this.a;
        if (profileData != null) {
            return profileData.hashCode();
        }
        return 0;
    }

    public String toString() {
        StringBuilder a2 = outline.a("ProfileFillingResultFragmentViewState(profile=");
        a2.append(this.a);
        a2.append(")");
        return a2.toString();
    }

    public /* synthetic */ ProfileFillingResultFragmentViewState0(ProfileData profileData, int i2) {
        profileData = (i2 & 1) != 0 ? new ProfileData(false, false, false, null, null, null, null, null, null, 511, null) : profileData;
        if (profileData != null) {
            this.a = profileData;
        } else {
            Intrinsics.a("profile");
            throw null;
        }
    }
}
