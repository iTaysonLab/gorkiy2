package e.a.b.h.b.i;

import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class QuestionnaireContainerFragmentVm_Factory implements Factory<a> {
    public final Provider<g> a;

    public QuestionnaireContainerFragmentVm_Factory(Provider<g> provider) {
        this.a = provider;
    }

    public Object get() {
        return new QuestionnaireContainerFragmentVm(this.a.get());
    }
}
