package e.a.b.h.b.i.c.m;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;

/* compiled from: InformedConfirmationFragmentViewState.kt */
public abstract class InformedConfirmationFragmentViewState1 extends BaseViewState1 {

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class a extends InformedConfirmationFragmentViewState1 {
        public final boolean a;

        public a(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && this.a == ((a) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("AllInfoIsCorrectChangedResult(isCorrect="), this.a, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class b extends InformedConfirmationFragmentViewState1 {
        public final boolean a;
        public final boolean b;
        public final boolean c;

        public b(boolean z, boolean z2, boolean z3) {
            super(null);
            this.a = z;
            this.b = z2;
            this.c = z3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && this.b == bVar.b && this.c == bVar.c;
        }

        public int hashCode() {
            boolean z = this.a;
            boolean z2 = true;
            if (z) {
                z = true;
            }
            int i2 = (z ? 1 : 0) * true;
            boolean z3 = this.b;
            if (z3) {
                z3 = true;
            }
            int i3 = (i2 + (z3 ? 1 : 0)) * 31;
            boolean z4 = this.c;
            if (!z4) {
                z2 = z4;
            }
            return i3 + (z2 ? 1 : 0);
        }

        public String toString() {
            StringBuilder a2 = outline.a("InformedConfirmationInitResult(isSelfIsolationInformed=");
            a2.append(this.a);
            a2.append(", isQuarantineTerminationResponsibilityInformed=");
            a2.append(this.b);
            a2.append(", isAllInfoIsCorrectConfirmed=");
            return outline.a(a2, this.c, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class c extends InformedConfirmationFragmentViewState1 {
        public final boolean a;

        public c(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && this.a == ((c) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("QuarantineTerminationResponsibilityInformedChangedResult(isInformed="), this.a, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class d extends InformedConfirmationFragmentViewState1 {
        public final boolean a;

        public d(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof d) && this.a == ((d) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("SelfIsolationInformedChangedResult(isInformed="), this.a, ")");
        }
    }

    public /* synthetic */ InformedConfirmationFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
