package e.a.b.h.b.i.c;

import android.net.Uri;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import e.c.c.b;
import j.e.b.PublishRelay;
import l.b.t.Consumer;
import n.n.c.Intrinsics;

/* compiled from: FillingProfileFullNamePageFragmentVm.kt */
public final class FillingProfileFullNamePageFragmentVm4<T> implements Consumer<Uri> {
    public final /* synthetic */ FillingProfileFullNamePageFragmentVm b;

    public FillingProfileFullNamePageFragmentVm4(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
        this.b = fillingProfileFullNamePageFragmentVm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.net.Uri, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Uri uri = (Uri) obj;
        PublishRelay a = this.b.g;
        Intrinsics.a((Object) uri, "photoUri");
        a.a((b) new FillingProfileFullNamePageFragmentViewState1.l(uri));
    }
}
