package e.a.b.h.b.i.c;

import android.net.Uri;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import java.util.Date;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CitizenshipResponse;
import ru.covid19.droid.data.model.profileData.ProfileData0;

/* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
public abstract class FillingProfileFullNamePageFragmentViewState1 extends BaseViewState0 {

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class a extends FillingProfileFullNamePageFragmentViewState1 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class b extends FillingProfileFullNamePageFragmentViewState1 {
        public final CitizenshipResponse a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(CitizenshipResponse citizenshipResponse) {
            super(null);
            if (citizenshipResponse != null) {
                this.a = citizenshipResponse;
                return;
            }
            Intrinsics.a("citizenship");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            CitizenshipResponse citizenshipResponse = this.a;
            if (citizenshipResponse != null) {
                return citizenshipResponse.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("CitizenshipSelectedEvent(citizenship=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class c extends FillingProfileFullNamePageFragmentViewState1 {
        public final ProfileData0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ProfileData0 profileData0) {
            super(null);
            if (profileData0 != null) {
                this.a = profileData0;
                return;
            }
            Intrinsics.a("gender");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && Intrinsics.a(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ProfileData0 profileData0 = this.a;
            if (profileData0 != null) {
                return profileData0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("GenderSelectedEvent(gender=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class d extends FillingProfileFullNamePageFragmentViewState1 {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class e extends FillingProfileFullNamePageFragmentViewState1 {
        public final boolean a;

        public e(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof e) && this.a == ((e) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("OnAddressMatchesChanged(matches="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class f extends FillingProfileFullNamePageFragmentViewState1 {
        public final Date a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(Date date) {
            super(null);
            if (date != null) {
                this.a = date;
                return;
            }
            Intrinsics.a("date");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof f) && Intrinsics.a(this.a, ((f) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Date date = this.a;
            if (date != null) {
                return date.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("OnBirthDateChangedEvent(date=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class g extends FillingProfileFullNamePageFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("address");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof g) && Intrinsics.a(this.a, ((g) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnLivingPlaceChangedEvent(address="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class h extends FillingProfileFullNamePageFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof h) && Intrinsics.a(this.a, ((h) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnNameChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class i extends FillingProfileFullNamePageFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof i) && Intrinsics.a(this.a, ((i) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnPatronymicChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class j extends FillingProfileFullNamePageFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("address");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof j) && Intrinsics.a(this.a, ((j) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnRegistrationAddressChangedEvent(address="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class k extends FillingProfileFullNamePageFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof k) && Intrinsics.a(this.a, ((k) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnSurnameChangedEvent(text="), this.a, ")");
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class l extends FillingProfileFullNamePageFragmentViewState1 {
        public final Uri a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(Uri uri) {
            super(null);
            if (uri != null) {
                this.a = uri;
                return;
            }
            Intrinsics.a("uri");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof l) && Intrinsics.a(this.a, ((l) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Uri uri = this.a;
            if (uri != null) {
                return uri.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("PhotoAddedEvent(uri=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class m extends FillingProfileFullNamePageFragmentViewState1 {
        public final BottomSheetSelectorNavigationDto0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0) {
            super(null);
            if (bottomSheetSelectorNavigationDto0 != null) {
                this.a = bottomSheetSelectorNavigationDto0;
                return;
            }
            Intrinsics.a("dto");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof m) && Intrinsics.a(this.a, ((m) obj).a);
            }
            return true;
        }

        public int hashCode() {
            BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0 = this.a;
            if (bottomSheetSelectorNavigationDto0 != null) {
                return bottomSheetSelectorNavigationDto0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SelectCitizenshipEvent(dto=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class n extends FillingProfileFullNamePageFragmentViewState1 {
        public static final n a = new n();

        public n() {
            super(null);
        }
    }

    public /* synthetic */ FillingProfileFullNamePageFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
