package e.a.b.h.b.h.l;

import e.a.a.a.e.q.b;
import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class ProfileFillingAddChildFragmentVm_Factory implements Factory<c> {
    public final Provider<b> a;
    public final Provider<g> b;
    public final Provider<a> c;

    public ProfileFillingAddChildFragmentVm_Factory(Provider<b> provider, Provider<g> provider2, Provider<a> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    public Object get() {
        return new ProfileFillingAddChildFragmentVm(this.a.get(), this.b.get(), this.c.get());
    }
}
