package e.a.b.g;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import n.n.c.Intrinsics;

/* compiled from: EditTextExtensions.kt */
public final class EditTextExtensions implements View.OnClickListener {
    public final /* synthetic */ EditText b;
    public final /* synthetic */ EditTextExtensions0 c;
    public final /* synthetic */ Long d;

    public EditTextExtensions(EditText editText, EditTextExtensions0 editTextExtensions0, Long l2) {
        this.b = editText;
        this.c = editTextExtensions0;
        this.d = l2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.DatePicker, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void onClick(View view) {
        Context context = this.b.getContext();
        EditTextExtensions0 editTextExtensions0 = this.c;
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, editTextExtensions0, editTextExtensions0.a.get(1), this.c.a.get(2), this.c.a.get(5));
        Long l2 = this.d;
        if (l2 != null) {
            long longValue = l2.longValue();
            DatePicker datePicker = datePickerDialog.getDatePicker();
            Intrinsics.a((Object) datePicker, "dialog.datePicker");
            datePicker.setMaxDate(longValue);
        }
        datePickerDialog.show();
    }
}
