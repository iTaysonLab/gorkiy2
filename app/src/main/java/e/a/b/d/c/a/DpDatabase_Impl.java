package e.a.b.d.c.a;

import i.s.RoomOpenHelper;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.e.FrameworkSQLiteDatabase0;

public class DpDatabase_Impl extends RoomOpenHelper.a {
    public final /* synthetic */ ru.covid19.droid.data.storage.db.DpDatabase_Impl b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DpDatabase_Impl(ru.covid19.droid.data.storage.db.DpDatabase_Impl dpDatabase_Impl, int i2) {
        super(i2);
        this.b = dpDatabase_Impl;
    }

    public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
        ((FrameworkSQLiteDatabase0) supportSQLiteDatabase).b.execSQL("CREATE TABLE IF NOT EXISTS `cacheMap` (`response` TEXT NOT NULL, `timeS` INTEGER NOT NULL, `name` TEXT NOT NULL, `owner` TEXT NOT NULL, PRIMARY KEY(`name`, `owner`))");
        FrameworkSQLiteDatabase0 frameworkSQLiteDatabase0 = (FrameworkSQLiteDatabase0) supportSQLiteDatabase;
        frameworkSQLiteDatabase0.b.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        frameworkSQLiteDatabase0.b.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'd0ad62d7a3cc5fec9606eeaa4ea045da')");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public i.s.RoomOpenHelper.b b(i.v.a.SupportSQLiteDatabase r28) {
        /*
            r27 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r1 = 4
            r0.<init>(r1)
            i.s.m.TableInfo$a r1 = new i.s.m.TableInfo$a
            java.lang.String r3 = "response"
            java.lang.String r4 = "TEXT"
            r5 = 1
            r6 = 0
            r7 = 0
            r8 = 1
            r2 = r1
            r2.<init>(r3, r4, r5, r6, r7, r8)
            java.lang.String r2 = "response"
            r0.put(r2, r1)
            i.s.m.TableInfo$a r1 = new i.s.m.TableInfo$a
            java.lang.String r4 = "timeS"
            java.lang.String r5 = "INTEGER"
            r7 = 0
            r6 = 1
            r8 = 0
            r9 = 1
            r3 = r1
            r3.<init>(r4, r5, r6, r7, r8, r9)
            java.lang.String r2 = "timeS"
            r0.put(r2, r1)
            i.s.m.TableInfo$a r1 = new i.s.m.TableInfo$a
            java.lang.String r4 = "name"
            java.lang.String r5 = "TEXT"
            r7 = 1
            r3 = r1
            r3.<init>(r4, r5, r6, r7, r8, r9)
            java.lang.String r2 = "name"
            r0.put(r2, r1)
            i.s.m.TableInfo$a r1 = new i.s.m.TableInfo$a
            java.lang.String r4 = "owner"
            java.lang.String r5 = "TEXT"
            r7 = 2
            r3 = r1
            r3.<init>(r4, r5, r6, r7, r8, r9)
            java.lang.String r3 = "owner"
            r0.put(r3, r1)
            java.util.HashSet r1 = new java.util.HashSet
            r3 = 0
            r1.<init>(r3)
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>(r3)
            i.s.m.TableInfo r5 = new i.s.m.TableInfo
            java.lang.String r6 = "cacheMap"
            r5.<init>(r6, r0, r1, r4)
            r0 = r28
            i.v.a.e.FrameworkSQLiteDatabase0 r0 = (i.v.a.e.FrameworkSQLiteDatabase0) r0
            java.lang.String r1 = "PRAGMA table_info(`cacheMap`)"
            android.database.Cursor r1 = r0.b(r1)
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            int r7 = r1.getColumnCount()     // Catch:{ all -> 0x0207 }
            if (r7 <= 0) goto L_0x00bf
            int r7 = r1.getColumnIndex(r2)     // Catch:{ all -> 0x0207 }
            java.lang.String r9 = "type"
            int r9 = r1.getColumnIndex(r9)     // Catch:{ all -> 0x0207 }
            java.lang.String r10 = "notnull"
            int r10 = r1.getColumnIndex(r10)     // Catch:{ all -> 0x0207 }
            java.lang.String r11 = "pk"
            int r11 = r1.getColumnIndex(r11)     // Catch:{ all -> 0x0207 }
            java.lang.String r12 = "dflt_value"
            int r12 = r1.getColumnIndex(r12)     // Catch:{ all -> 0x0207 }
        L_0x008f:
            boolean r13 = r1.moveToNext()     // Catch:{ all -> 0x0207 }
            if (r13 == 0) goto L_0x00bf
            java.lang.String r13 = r1.getString(r7)     // Catch:{ all -> 0x0207 }
            java.lang.String r16 = r1.getString(r9)     // Catch:{ all -> 0x0207 }
            int r14 = r1.getInt(r10)     // Catch:{ all -> 0x0207 }
            if (r14 == 0) goto L_0x00a6
            r17 = 1
            goto L_0x00a8
        L_0x00a6:
            r17 = 0
        L_0x00a8:
            int r18 = r1.getInt(r11)     // Catch:{ all -> 0x0207 }
            java.lang.String r19 = r1.getString(r12)     // Catch:{ all -> 0x0207 }
            i.s.m.TableInfo$a r15 = new i.s.m.TableInfo$a     // Catch:{ all -> 0x0207 }
            r20 = 2
            r14 = r15
            r3 = r15
            r15 = r13
            r14.<init>(r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x0207 }
            r4.put(r13, r3)     // Catch:{ all -> 0x0207 }
            r3 = 0
            goto L_0x008f
        L_0x00bf:
            r1.close()
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>()
            java.lang.String r3 = "PRAGMA foreign_key_list(`cacheMap`)"
            android.database.Cursor r3 = r0.b(r3)
            java.lang.String r7 = "id"
            int r7 = r3.getColumnIndex(r7)     // Catch:{ all -> 0x0202 }
            java.lang.String r9 = "seq"
            int r9 = r3.getColumnIndex(r9)     // Catch:{ all -> 0x0202 }
            java.lang.String r10 = "table"
            int r10 = r3.getColumnIndex(r10)     // Catch:{ all -> 0x0202 }
            java.lang.String r11 = "on_delete"
            int r11 = r3.getColumnIndex(r11)     // Catch:{ all -> 0x0202 }
            java.lang.String r12 = "on_update"
            int r12 = r3.getColumnIndex(r12)     // Catch:{ all -> 0x0202 }
            java.util.List r13 = i.s.m.TableInfo.a(r3)     // Catch:{ all -> 0x0202 }
            int r14 = r3.getCount()     // Catch:{ all -> 0x0202 }
            r15 = 0
        L_0x00f4:
            if (r15 >= r14) goto L_0x0170
            r3.moveToPosition(r15)     // Catch:{ all -> 0x0202 }
            int r16 = r3.getInt(r9)     // Catch:{ all -> 0x0202 }
            if (r16 == 0) goto L_0x0108
            r16 = r7
            r17 = r9
            r20 = r13
            r19 = r14
            goto L_0x0165
        L_0x0108:
            int r8 = r3.getInt(r7)     // Catch:{ all -> 0x0202 }
            r16 = r7
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0202 }
            r7.<init>()     // Catch:{ all -> 0x0202 }
            r17 = r9
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x0202 }
            r9.<init>()     // Catch:{ all -> 0x0202 }
            r18 = r13
            java.util.ArrayList r18 = (java.util.ArrayList) r18
            java.util.Iterator r18 = r18.iterator()     // Catch:{ all -> 0x0202 }
        L_0x0122:
            boolean r19 = r18.hasNext()     // Catch:{ all -> 0x0202 }
            if (r19 == 0) goto L_0x0147
            java.lang.Object r19 = r18.next()     // Catch:{ all -> 0x0202 }
            r20 = r13
            r13 = r19
            i.s.m.TableInfo$c r13 = (i.s.m.TableInfo.c) r13     // Catch:{ all -> 0x0202 }
            r19 = r14
            int r14 = r13.b     // Catch:{ all -> 0x0202 }
            if (r14 != r8) goto L_0x0142
            java.lang.String r14 = r13.d     // Catch:{ all -> 0x0202 }
            r7.add(r14)     // Catch:{ all -> 0x0202 }
            java.lang.String r13 = r13.f1444e     // Catch:{ all -> 0x0202 }
            r9.add(r13)     // Catch:{ all -> 0x0202 }
        L_0x0142:
            r14 = r19
            r13 = r20
            goto L_0x0122
        L_0x0147:
            r20 = r13
            r19 = r14
            i.s.m.TableInfo$b r8 = new i.s.m.TableInfo$b     // Catch:{ all -> 0x0202 }
            java.lang.String r22 = r3.getString(r10)     // Catch:{ all -> 0x0202 }
            java.lang.String r23 = r3.getString(r11)     // Catch:{ all -> 0x0202 }
            java.lang.String r24 = r3.getString(r12)     // Catch:{ all -> 0x0202 }
            r21 = r8
            r25 = r7
            r26 = r9
            r21.<init>(r22, r23, r24, r25, r26)     // Catch:{ all -> 0x0202 }
            r1.add(r8)     // Catch:{ all -> 0x0202 }
        L_0x0165:
            int r15 = r15 + 1
            r7 = r16
            r9 = r17
            r14 = r19
            r13 = r20
            goto L_0x00f4
        L_0x0170:
            r3.close()
            java.lang.String r3 = "PRAGMA index_list(`cacheMap`)"
            android.database.Cursor r3 = r0.b(r3)
            int r2 = r3.getColumnIndex(r2)     // Catch:{ all -> 0x01fd }
            java.lang.String r7 = "origin"
            int r7 = r3.getColumnIndex(r7)     // Catch:{ all -> 0x01fd }
            java.lang.String r8 = "unique"
            int r8 = r3.getColumnIndex(r8)     // Catch:{ all -> 0x01fd }
            r9 = -1
            r10 = 0
            if (r2 == r9) goto L_0x01c7
            if (r7 == r9) goto L_0x01c7
            if (r8 != r9) goto L_0x0192
            goto L_0x01c7
        L_0x0192:
            java.util.HashSet r9 = new java.util.HashSet     // Catch:{ all -> 0x01fd }
            r9.<init>()     // Catch:{ all -> 0x01fd }
        L_0x0197:
            boolean r11 = r3.moveToNext()     // Catch:{ all -> 0x01fd }
            if (r11 == 0) goto L_0x01c3
            java.lang.String r11 = r3.getString(r7)     // Catch:{ all -> 0x01fd }
            java.lang.String r12 = "c"
            boolean r11 = r12.equals(r11)     // Catch:{ all -> 0x01fd }
            if (r11 != 0) goto L_0x01aa
            goto L_0x0197
        L_0x01aa:
            java.lang.String r11 = r3.getString(r2)     // Catch:{ all -> 0x01fd }
            int r12 = r3.getInt(r8)     // Catch:{ all -> 0x01fd }
            r13 = 1
            if (r12 != r13) goto L_0x01b7
            r13 = 1
            goto L_0x01b8
        L_0x01b7:
            r13 = 0
        L_0x01b8:
            i.s.m.TableInfo$d r11 = i.s.m.TableInfo.a(r0, r11, r13)     // Catch:{ all -> 0x01fd }
            if (r11 != 0) goto L_0x01bf
            goto L_0x01c7
        L_0x01bf:
            r9.add(r11)     // Catch:{ all -> 0x01fd }
            goto L_0x0197
        L_0x01c3:
            r3.close()
            goto L_0x01cb
        L_0x01c7:
            r3.close()
            r9 = r10
        L_0x01cb:
            i.s.m.TableInfo r0 = new i.s.m.TableInfo
            r0.<init>(r6, r4, r1, r9)
            boolean r1 = r5.equals(r0)
            if (r1 != 0) goto L_0x01f6
            i.s.RoomOpenHelper$b r1 = new i.s.RoomOpenHelper$b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "cacheMap(ru.covid19.core.data.storage.db.cache.CacheItem).\n Expected:\n"
            r2.append(r3)
            r2.append(r5)
            java.lang.String r3 = "\n Found:\n"
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r2 = 0
            r1.<init>(r2, r0)
            return r1
        L_0x01f6:
            i.s.RoomOpenHelper$b r0 = new i.s.RoomOpenHelper$b
            r1 = 1
            r0.<init>(r1, r10)
            return r0
        L_0x01fd:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x0202:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x0207:
            r0 = move-exception
            r1.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.b.d.c.a.DpDatabase_Impl.b(i.v.a.SupportSQLiteDatabase):i.s.RoomOpenHelper$b");
    }
}
