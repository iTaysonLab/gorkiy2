package e.a.a.l;

import android.app.Activity;
import android.os.Bundle;
import n.n.c.Intrinsics;

/* compiled from: IAppLifecycle.kt */
public final class IAppLifecycle0 implements IAppLifecycle {
    public int b;

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (activity != null) {
            this.b++;
        } else {
            Intrinsics.a("p0");
            throw null;
        }
    }

    public void onActivityDestroyed(Activity activity) {
        if (activity != null) {
            this.b--;
        } else {
            Intrinsics.a("p0");
            throw null;
        }
    }

    public void onActivityPaused(Activity activity) {
        if (activity == null) {
            Intrinsics.a("p0");
            throw null;
        }
    }

    public void onActivityResumed(Activity activity) {
        if (activity == null) {
            Intrinsics.a("p0");
            throw null;
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        if (activity == null) {
            Intrinsics.a("p0");
            throw null;
        } else if (bundle == null) {
            Intrinsics.a("p1");
            throw null;
        }
    }

    public void onActivityStarted(Activity activity) {
        if (activity == null) {
            Intrinsics.a("p0");
            throw null;
        }
    }

    public void onActivityStopped(Activity activity) {
        if (activity == null) {
            Intrinsics.a("p0");
            throw null;
        }
    }
}
