package e.a.a.i.c;

import e.a.a.g.a.IAuthenticateRequestChecker;
import e.a.a.g.a.Session;
import e.a.a.g.a.c;
import e.a.a.g.a.h.EsiaAddBearerInterceptor;
import e.a.a.g.a.h.d;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: NetworkModule_ProvideEsiaAddBearerInterceptor$core_prodReleaseFactory */
public final class f0 implements Factory<d> {
    public final NetworkModule a;
    public final Provider<e.a.a.g.a.d> b;
    public final Provider<c> c;

    public f0(y yVar, Provider<e.a.a.g.a.d> provider, Provider<c> provider2) {
        this.a = yVar;
        this.b = provider;
        this.c = provider2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.g.a.h.EsiaAddBearerInterceptor, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        NetworkModule networkModule = this.a;
        Session session = this.b.get();
        IAuthenticateRequestChecker iAuthenticateRequestChecker = this.c.get();
        if (networkModule == null) {
            throw null;
        } else if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iAuthenticateRequestChecker != null) {
            EsiaAddBearerInterceptor esiaAddBearerInterceptor = new EsiaAddBearerInterceptor(session, iAuthenticateRequestChecker);
            j.c.a.a.c.n.c.a((Object) esiaAddBearerInterceptor, "Cannot return null from a non-@Nullable @Provides method");
            return esiaAddBearerInterceptor;
        } else {
            Intrinsics.a("authenticateRequestChecker");
            throw null;
        }
    }
}
