package e.a.a.j.c;

import android.annotation.SuppressLint;
import android.content.Context;
import j.e.b.PublishRelay;
import n.n.c.Intrinsics;

@SuppressLint({"CheckResult"})
/* compiled from: PermissionsRepository.kt */
public final class PermissionsRepository {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.b.PublishRelay, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public PermissionsRepository(Context context) {
        if (context != null) {
            Intrinsics.a((Object) new PublishRelay(), "PublishRelay.create()");
        } else {
            Intrinsics.a("appContext");
            throw null;
        }
    }
}
