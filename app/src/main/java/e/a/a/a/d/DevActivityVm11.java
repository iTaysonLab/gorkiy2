package e.a.a.a.d;

import e.a.a.a.d.DevActivityViewState1;
import e.c.c.BaseMviVm0;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm11<T, R> implements Function<T, R> {
    public static final DevActivityVm11 a = new DevActivityVm11();

    public Object a(Object obj) {
        String str = (String) obj;
        if (str != null) {
            return new BaseMviVm0.a(new DevActivityViewState1.a(str));
        }
        Intrinsics.a("it");
        throw null;
    }
}
