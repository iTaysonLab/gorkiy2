package e.a.a.a.c.a;

import android.os.Parcel;
import android.os.Parcelable;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ErrorFragmentViewState.kt */
public final class ErrorFragmentViewState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f587e;

    /* renamed from: f  reason: collision with root package name */
    public final String f588f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f589i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f590j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f591k;

    /* renamed from: l  reason: collision with root package name */
    public final Parcelable f592l;

    /* renamed from: m  reason: collision with root package name */
    public final Parcelable f593m;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Parcel parcel2 = parcel;
            if (parcel2 != null) {
                return new ErrorFragmentViewState(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel2.readParcelable(ErrorFragmentViewState.class.getClassLoader()), parcel2.readParcelable(ErrorFragmentViewState.class.getClassLoader()));
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new ErrorFragmentViewState[i2];
        }
    }

    public ErrorFragmentViewState() {
        this(null, null, null, null, null, false, false, false, false, false, null, null, 4095);
    }

    public ErrorFragmentViewState(String str, String str2, String str3, String str4, String str5, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, Parcelable parcelable, Parcelable parcelable2) {
        if (str != null) {
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.f587e = str4;
            this.f588f = str5;
            this.g = z;
            this.h = z2;
            this.f589i = z3;
            this.f590j = z4;
            this.f591k = z5;
            this.f592l = parcelable;
            this.f593m = parcelable2;
            return;
        }
        Intrinsics.a("errorKey");
        throw null;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ErrorFragmentViewState)) {
            return false;
        }
        ErrorFragmentViewState errorFragmentViewState = (ErrorFragmentViewState) obj;
        return Intrinsics.a(this.b, errorFragmentViewState.b) && Intrinsics.a(this.c, errorFragmentViewState.c) && Intrinsics.a(this.d, errorFragmentViewState.d) && Intrinsics.a(this.f587e, errorFragmentViewState.f587e) && Intrinsics.a(this.f588f, errorFragmentViewState.f588f) && this.g == errorFragmentViewState.g && this.h == errorFragmentViewState.h && this.f589i == errorFragmentViewState.f589i && this.f590j == errorFragmentViewState.f590j && this.f591k == errorFragmentViewState.f591k && Intrinsics.a(this.f592l, errorFragmentViewState.f592l) && Intrinsics.a(this.f593m, errorFragmentViewState.f593m);
    }

    public int hashCode() {
        String str = this.b;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.f587e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f588f;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        boolean z = this.g;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i3 = (hashCode5 + (z ? 1 : 0)) * 31;
        boolean z3 = this.h;
        if (z3) {
            z3 = true;
        }
        int i4 = (i3 + (z3 ? 1 : 0)) * 31;
        boolean z4 = this.f589i;
        if (z4) {
            z4 = true;
        }
        int i5 = (i4 + (z4 ? 1 : 0)) * 31;
        boolean z5 = this.f590j;
        if (z5) {
            z5 = true;
        }
        int i6 = (i5 + (z5 ? 1 : 0)) * 31;
        boolean z6 = this.f591k;
        if (!z6) {
            z2 = z6;
        }
        int i7 = (i6 + (z2 ? 1 : 0)) * 31;
        Parcelable parcelable = this.f592l;
        int hashCode6 = (i7 + (parcelable != null ? parcelable.hashCode() : 0)) * 31;
        Parcelable parcelable2 = this.f593m;
        if (parcelable2 != null) {
            i2 = parcelable2.hashCode();
        }
        return hashCode6 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("Error(errorKey=");
        a2.append(this.b);
        a2.append(", title=");
        a2.append(this.c);
        a2.append(", message=");
        a2.append(this.d);
        a2.append(", submitButtonMessage=");
        a2.append(this.f587e);
        a2.append(", backButtonMessage=");
        a2.append(this.f588f);
        a2.append(", submitButtonVisible=");
        a2.append(this.g);
        a2.append(", backButtonVisible=");
        a2.append(this.h);
        a2.append(", hideAppOnHardwareBackPressed=");
        a2.append(this.f589i);
        a2.append(", leavePreviousScreenOnBackAction=");
        a2.append(this.f590j);
        a2.append(", leavePreviousScreenOnSubmitAction=");
        a2.append(this.f591k);
        a2.append(", submitEvent=");
        a2.append(this.f592l);
        a2.append(", backEvent=");
        a2.append(this.f593m);
        a2.append(")");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            parcel.writeString(this.f587e);
            parcel.writeString(this.f588f);
            parcel.writeInt(this.g ? 1 : 0);
            parcel.writeInt(this.h ? 1 : 0);
            parcel.writeInt(this.f589i ? 1 : 0);
            parcel.writeInt(this.f590j ? 1 : 0);
            parcel.writeInt(this.f591k ? 1 : 0);
            parcel.writeParcelable(this.f592l, i2);
            parcel.writeParcelable(this.f593m, i2);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ErrorFragmentViewState(java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, boolean r19, boolean r20, boolean r21, boolean r22, boolean r23, android.os.Parcelable r24, android.os.Parcelable r25, int r26) {
        /*
            r13 = this;
            r0 = r26
            r1 = r0 & 1
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x000a
            r1 = r2
            goto L_0x000b
        L_0x000a:
            r1 = r14
        L_0x000b:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0011
            r3 = r2
            goto L_0x0012
        L_0x0011:
            r3 = r15
        L_0x0012:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x0018
            r4 = r2
            goto L_0x001a
        L_0x0018:
            r4 = r16
        L_0x001a:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x0020
            r5 = r2
            goto L_0x0022
        L_0x0020:
            r5 = r17
        L_0x0022:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0027
            goto L_0x0029
        L_0x0027:
            r2 = r18
        L_0x0029:
            r6 = r0 & 32
            r7 = 0
            if (r6 == 0) goto L_0x0030
            r6 = 0
            goto L_0x0032
        L_0x0030:
            r6 = r19
        L_0x0032:
            r8 = r0 & 64
            if (r8 == 0) goto L_0x0038
            r8 = 0
            goto L_0x003a
        L_0x0038:
            r8 = r20
        L_0x003a:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0040
            r9 = 0
            goto L_0x0042
        L_0x0040:
            r9 = r21
        L_0x0042:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x0048
            r10 = 0
            goto L_0x004a
        L_0x0048:
            r10 = r22
        L_0x004a:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x004f
            goto L_0x0051
        L_0x004f:
            r7 = r23
        L_0x0051:
            r11 = r0 & 1024(0x400, float:1.435E-42)
            r12 = 0
            if (r11 == 0) goto L_0x0058
            r11 = r12
            goto L_0x005a
        L_0x0058:
            r11 = r24
        L_0x005a:
            r0 = r0 & 2048(0x800, float:2.87E-42)
            if (r0 == 0) goto L_0x005f
            goto L_0x0061
        L_0x005f:
            r12 = r25
        L_0x0061:
            r14 = r13
            r15 = r1
            r16 = r3
            r17 = r4
            r18 = r5
            r19 = r2
            r20 = r6
            r21 = r8
            r22 = r9
            r23 = r10
            r24 = r7
            r25 = r11
            r26 = r12
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.a.c.a.ErrorFragmentViewState.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, boolean, android.os.Parcelable, android.os.Parcelable, int):void");
    }
}
