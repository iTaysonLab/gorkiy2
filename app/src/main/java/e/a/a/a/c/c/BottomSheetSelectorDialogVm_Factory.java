package e.a.a.a.c.c;

import e.a.a.a.e.q.b;
import k.a.Factory;
import m.a.Provider;

public final class BottomSheetSelectorDialogVm_Factory implements Factory<f> {
    public final Provider<b> a;

    public BottomSheetSelectorDialogVm_Factory(Provider<b> provider) {
        this.a = provider;
    }

    public Object get() {
        return new BottomSheetSelectorDialogVm(this.a.get());
    }
}
