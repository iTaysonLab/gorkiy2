package e.a.a.a.e.s;

import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.Commands6;
import e.b.a.h.Command;
import n.n.c.Intrinsics;

/* compiled from: ActivityRouter.kt */
public class ActivityRouter extends DpRouter0 {
    public final void a(BaseScreens1 baseScreens1) {
        if (baseScreens1 != null) {
            this.a.a(new Command[]{new Commands6(baseScreens1)});
            return;
        }
        Intrinsics.a("screen");
        throw null;
    }
}
