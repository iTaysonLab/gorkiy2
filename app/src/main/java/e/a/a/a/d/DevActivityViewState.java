package e.a.a.a.d;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: DevActivityViewState.kt */
public final class DevActivityViewState {
    public final String a;
    public final boolean b;

    public DevActivityViewState() {
        this(null, false, 3);
    }

    public DevActivityViewState(String str, boolean z) {
        if (str != null) {
            this.a = str;
            this.b = z;
            return;
        }
        Intrinsics.a("currentEndpoint");
        throw null;
    }

    public static /* synthetic */ DevActivityViewState a(DevActivityViewState devActivityViewState, String str, boolean z, int i2) {
        if ((i2 & 1) != 0) {
            str = devActivityViewState.a;
        }
        if ((i2 & 2) != 0) {
            z = devActivityViewState.b;
        }
        if (devActivityViewState == null) {
            throw null;
        } else if (str != null) {
            return new DevActivityViewState(str, z);
        } else {
            Intrinsics.a("currentEndpoint");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DevActivityViewState)) {
            return false;
        }
        DevActivityViewState devActivityViewState = (DevActivityViewState) obj;
        return Intrinsics.a(this.a, devActivityViewState.a) && this.b == devActivityViewState.b;
    }

    public int hashCode() {
        String str = this.a;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("DevActivityViewState(currentEndpoint=");
        a2.append(this.a);
        a2.append(", secureActivity=");
        return outline.a(a2, this.b, ")");
    }

    public /* synthetic */ DevActivityViewState(String str, boolean z, int i2) {
        str = (i2 & 1) != 0 ? "" : str;
        z = (i2 & 2) != 0 ? false : z;
        if (str != null) {
            this.a = str;
            this.b = z;
            return;
        }
        Intrinsics.a("currentEndpoint");
        throw null;
    }
}
