package e.a.a.a.a;

import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.f;
import l.b.t.Consumer;
import n.n.c.Intrinsics;
import ru.waveaccess.waandroidnetwork.Exceptions0;

/* compiled from: BaseAuthFragmentVm.kt */
public final class BaseAuthFragmentVm1<T> implements Consumer<Throwable> {
    public final /* synthetic */ BaseAuthFragmentVm8 b;

    public BaseAuthFragmentVm1(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
        this.b = baseAuthFragmentVm8;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Throwable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Throwable th = (Throwable) obj;
        this.b.f579t.a((Boolean) false);
        this.b.f578s.a((Boolean) true);
        Intrinsics.a((Object) th, "it");
        if (th instanceof Exceptions0) {
            IFragmentCoordinator iFragmentCoordinator = this.b.w;
            ErrorNavigationDto errorNavigationDto = r2;
            ErrorNavigationDto errorNavigationDto2 = new ErrorNavigationDto("", null, null, null, null, null, f.frag_error_no_internet_connection_title, f.frag_error_no_internet_connection_description, f.frag_error_no_internet_connection_btn, 0, true, false, false, false, false, false, null, null, 250430);
            iFragmentCoordinator.a(errorNavigationDto);
            return;
        }
        this.b.f577r.a((Boolean) false);
    }
}
