package e.a.a.a.b;

import android.content.Intent;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpActivity.kt */
public final /* synthetic */ class BaseDpActivity extends h implements Functions0<Intent, g> {
    public BaseDpActivity(BaseDpActivity4 baseDpActivity4) {
        super(1, baseDpActivity4);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [e.a.a.a.b.BaseDpActivity4, android.app.Activity] */
    public Object a(Object obj) {
        Intent intent = (Intent) obj;
        if (intent != null) {
            ((BaseDpActivity4) this.c).startActivity(intent);
            return Unit.a;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "openActivity";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpActivity4.class);
    }

    public final String i() {
        return "openActivity(Landroid/content/Intent;)V";
    }
}
