package e.a.a.a.e.p;

import android.os.Parcel;
import android.os.Parcelable;
import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ErrorNavigationDto.kt */
public final class ErrorNavigationDto implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f607e;

    /* renamed from: f  reason: collision with root package name */
    public final String f608f;
    public final String g;
    public final int h;

    /* renamed from: i  reason: collision with root package name */
    public final int f609i;

    /* renamed from: j  reason: collision with root package name */
    public final int f610j;

    /* renamed from: k  reason: collision with root package name */
    public final int f611k;

    /* renamed from: l  reason: collision with root package name */
    public final boolean f612l;

    /* renamed from: m  reason: collision with root package name */
    public final boolean f613m;

    /* renamed from: n  reason: collision with root package name */
    public final boolean f614n;

    /* renamed from: o  reason: collision with root package name */
    public final boolean f615o;

    /* renamed from: p  reason: collision with root package name */
    public final boolean f616p;

    /* renamed from: q  reason: collision with root package name */
    public final boolean f617q;

    /* renamed from: r  reason: collision with root package name */
    public final Parcelable f618r;

    /* renamed from: s  reason: collision with root package name */
    public final Parcelable f619s;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            Parcel parcel2 = parcel;
            if (parcel2 != null) {
                return new ErrorNavigationDto(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel2.readParcelable(ErrorNavigationDto.class.getClassLoader()), parcel2.readParcelable(ErrorNavigationDto.class.getClassLoader()));
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new ErrorNavigationDto[i2];
        }
    }

    public ErrorNavigationDto(String str, String str2, String str3, String str4, String str5, String str6, int i2, int i3, int i4, int i5, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Parcelable parcelable, Parcelable parcelable2) {
        String str7 = str;
        String str8 = str2;
        if (str7 == null) {
            Intrinsics.a("errorKey");
            throw null;
        } else if (str8 != null) {
            this.b = str7;
            this.c = str8;
            this.d = str3;
            this.f607e = str4;
            this.f608f = str5;
            this.g = str6;
            this.h = i2;
            this.f609i = i3;
            this.f610j = i4;
            this.f611k = i5;
            this.f612l = z;
            this.f613m = z2;
            this.f614n = z3;
            this.f615o = z4;
            this.f616p = z5;
            this.f617q = z6;
            this.f618r = parcelable;
            this.f619s = parcelable2;
        } else {
            Intrinsics.a("routerTag");
            throw null;
        }
    }

    public static /* synthetic */ ErrorNavigationDto a(ErrorNavigationDto errorNavigationDto, String str, String str2, String str3, String str4, String str5, String str6, int i2, int i3, int i4, int i5, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, Parcelable parcelable, Parcelable parcelable2, int i6) {
        ErrorNavigationDto errorNavigationDto2 = errorNavigationDto;
        int i7 = i6;
        String str7 = (i7 & 1) != 0 ? errorNavigationDto2.b : str;
        String str8 = (i7 & 2) != 0 ? errorNavigationDto2.c : str2;
        String str9 = (i7 & 4) != 0 ? errorNavigationDto2.d : str3;
        String str10 = (i7 & 8) != 0 ? errorNavigationDto2.f607e : str4;
        String str11 = (i7 & 16) != 0 ? errorNavigationDto2.f608f : str5;
        String str12 = (i7 & 32) != 0 ? errorNavigationDto2.g : str6;
        int i8 = (i7 & 64) != 0 ? errorNavigationDto2.h : i2;
        int i9 = (i7 & 128) != 0 ? errorNavigationDto2.f609i : i3;
        int i10 = (i7 & 256) != 0 ? errorNavigationDto2.f610j : i4;
        int i11 = (i7 & 512) != 0 ? errorNavigationDto2.f611k : i5;
        boolean z7 = (i7 & 1024) != 0 ? errorNavigationDto2.f612l : z;
        boolean z8 = (i7 & 2048) != 0 ? errorNavigationDto2.f613m : z2;
        boolean z9 = (i7 & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0 ? errorNavigationDto2.f614n : z3;
        boolean z10 = (i7 & 8192) != 0 ? errorNavigationDto2.f615o : z4;
        boolean z11 = (i7 & 16384) != 0 ? errorNavigationDto2.f616p : z5;
        boolean z12 = (i7 & 32768) != 0 ? errorNavigationDto2.f617q : z6;
        Parcelable parcelable3 = (i7 & LogFileManager.MAX_LOG_SIZE) != 0 ? errorNavigationDto2.f618r : parcelable;
        Parcelable parcelable4 = (i7 & 131072) != 0 ? errorNavigationDto2.f619s : parcelable2;
        if (errorNavigationDto2 == null) {
            throw null;
        } else if (str7 == null) {
            Intrinsics.a("errorKey");
            throw null;
        } else if (str8 != null) {
            return new ErrorNavigationDto(str7, str8, str9, str10, str11, str12, i8, i9, i10, i11, z7, z8, z9, z10, z11, z12, parcelable3, parcelable4);
        } else {
            Intrinsics.a("routerTag");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ErrorNavigationDto)) {
            return false;
        }
        ErrorNavigationDto errorNavigationDto = (ErrorNavigationDto) obj;
        return Intrinsics.a(this.b, errorNavigationDto.b) && Intrinsics.a(this.c, errorNavigationDto.c) && Intrinsics.a(this.d, errorNavigationDto.d) && Intrinsics.a(this.f607e, errorNavigationDto.f607e) && Intrinsics.a(this.f608f, errorNavigationDto.f608f) && Intrinsics.a(this.g, errorNavigationDto.g) && this.h == errorNavigationDto.h && this.f609i == errorNavigationDto.f609i && this.f610j == errorNavigationDto.f610j && this.f611k == errorNavigationDto.f611k && this.f612l == errorNavigationDto.f612l && this.f613m == errorNavigationDto.f613m && this.f614n == errorNavigationDto.f614n && this.f615o == errorNavigationDto.f615o && this.f616p == errorNavigationDto.f616p && this.f617q == errorNavigationDto.f617q && Intrinsics.a(this.f618r, errorNavigationDto.f618r) && Intrinsics.a(this.f619s, errorNavigationDto.f619s);
    }

    public int hashCode() {
        String str = this.b;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.f607e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f608f;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.g;
        int hashCode6 = (((((((((hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31) + this.h) * 31) + this.f609i) * 31) + this.f610j) * 31) + this.f611k) * 31;
        boolean z = this.f612l;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i3 = (hashCode6 + (z ? 1 : 0)) * 31;
        boolean z3 = this.f613m;
        if (z3) {
            z3 = true;
        }
        int i4 = (i3 + (z3 ? 1 : 0)) * 31;
        boolean z4 = this.f614n;
        if (z4) {
            z4 = true;
        }
        int i5 = (i4 + (z4 ? 1 : 0)) * 31;
        boolean z5 = this.f615o;
        if (z5) {
            z5 = true;
        }
        int i6 = (i5 + (z5 ? 1 : 0)) * 31;
        boolean z6 = this.f616p;
        if (z6) {
            z6 = true;
        }
        int i7 = (i6 + (z6 ? 1 : 0)) * 31;
        boolean z7 = this.f617q;
        if (!z7) {
            z2 = z7;
        }
        int i8 = (i7 + (z2 ? 1 : 0)) * 31;
        Parcelable parcelable = this.f618r;
        int hashCode7 = (i8 + (parcelable != null ? parcelable.hashCode() : 0)) * 31;
        Parcelable parcelable2 = this.f619s;
        if (parcelable2 != null) {
            i2 = parcelable2.hashCode();
        }
        return hashCode7 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("ErrorNavigationDto(errorKey=");
        a2.append(this.b);
        a2.append(", routerTag=");
        a2.append(this.c);
        a2.append(", title=");
        a2.append(this.d);
        a2.append(", message=");
        a2.append(this.f607e);
        a2.append(", backButtonText=");
        a2.append(this.f608f);
        a2.append(", submitButtonText=");
        a2.append(this.g);
        a2.append(", titleResId=");
        a2.append(this.h);
        a2.append(", messageResId=");
        a2.append(this.f609i);
        a2.append(", backButtonTextResId=");
        a2.append(this.f610j);
        a2.append(", submitButtonTextResId=");
        a2.append(this.f611k);
        a2.append(", showBackButton=");
        a2.append(this.f612l);
        a2.append(", showSubmitButton=");
        a2.append(this.f613m);
        a2.append(", leavePreviousScreenOnBackAction=");
        a2.append(this.f614n);
        a2.append(", leavePreviousScreenOnSubmitAction=");
        a2.append(this.f615o);
        a2.append(", stayOnScreenOnBackAction=");
        a2.append(this.f616p);
        a2.append(", hideAppOnHardwareBackPressed=");
        a2.append(this.f617q);
        a2.append(", submitEvent=");
        a2.append(this.f618r);
        a2.append(", backEvent=");
        a2.append(this.f619s);
        a2.append(")");
        return a2.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeString(this.d);
            parcel.writeString(this.f607e);
            parcel.writeString(this.f608f);
            parcel.writeString(this.g);
            parcel.writeInt(this.h);
            parcel.writeInt(this.f609i);
            parcel.writeInt(this.f610j);
            parcel.writeInt(this.f611k);
            parcel.writeInt(this.f612l ? 1 : 0);
            parcel.writeInt(this.f613m ? 1 : 0);
            parcel.writeInt(this.f614n ? 1 : 0);
            parcel.writeInt(this.f615o ? 1 : 0);
            parcel.writeInt(this.f616p ? 1 : 0);
            parcel.writeInt(this.f617q ? 1 : 0);
            parcel.writeParcelable(this.f618r, i2);
            parcel.writeParcelable(this.f619s, i2);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ErrorNavigationDto(java.lang.String r22, java.lang.String r23, java.lang.String r24, java.lang.String r25, java.lang.String r26, java.lang.String r27, int r28, int r29, int r30, int r31, boolean r32, boolean r33, boolean r34, boolean r35, boolean r36, boolean r37, android.os.Parcelable r38, android.os.Parcelable r39, int r40) {
        /*
            r21 = this;
            r0 = r40
            r1 = r0 & 2
            if (r1 == 0) goto L_0x000a
            java.lang.String r1 = ""
            r4 = r1
            goto L_0x000c
        L_0x000a:
            r4 = r23
        L_0x000c:
            r1 = r0 & 4
            r2 = 0
            if (r1 == 0) goto L_0x0013
            r5 = r2
            goto L_0x0015
        L_0x0013:
            r5 = r24
        L_0x0015:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x001b
            r6 = r2
            goto L_0x001d
        L_0x001b:
            r6 = r25
        L_0x001d:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0023
            r7 = r2
            goto L_0x0025
        L_0x0023:
            r7 = r26
        L_0x0025:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x002b
            r8 = r2
            goto L_0x002d
        L_0x002b:
            r8 = r27
        L_0x002d:
            r1 = r0 & 64
            r3 = 0
            if (r1 == 0) goto L_0x0034
            r9 = 0
            goto L_0x0036
        L_0x0034:
            r9 = r28
        L_0x0036:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x003c
            r10 = 0
            goto L_0x003e
        L_0x003c:
            r10 = r29
        L_0x003e:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0044
            r11 = 0
            goto L_0x0046
        L_0x0044:
            r11 = r30
        L_0x0046:
            r1 = r0 & 512(0x200, float:7.175E-43)
            if (r1 == 0) goto L_0x004c
            r12 = 0
            goto L_0x004e
        L_0x004c:
            r12 = r31
        L_0x004e:
            r1 = r0 & 1024(0x400, float:1.435E-42)
            if (r1 == 0) goto L_0x0054
            r13 = 0
            goto L_0x0056
        L_0x0054:
            r13 = r32
        L_0x0056:
            r1 = r0 & 2048(0x800, float:2.87E-42)
            if (r1 == 0) goto L_0x005c
            r14 = 0
            goto L_0x005e
        L_0x005c:
            r14 = r33
        L_0x005e:
            r1 = r0 & 4096(0x1000, float:5.74E-42)
            if (r1 == 0) goto L_0x0064
            r15 = 0
            goto L_0x0066
        L_0x0064:
            r15 = r34
        L_0x0066:
            r1 = r0 & 8192(0x2000, float:1.14794E-41)
            if (r1 == 0) goto L_0x006d
            r16 = 0
            goto L_0x006f
        L_0x006d:
            r16 = r35
        L_0x006f:
            r1 = r0 & 16384(0x4000, float:2.2959E-41)
            if (r1 == 0) goto L_0x0076
            r17 = 0
            goto L_0x0078
        L_0x0076:
            r17 = r36
        L_0x0078:
            r1 = 32768(0x8000, float:4.5918E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x0081
            r18 = 0
            goto L_0x0083
        L_0x0081:
            r18 = r37
        L_0x0083:
            r1 = 65536(0x10000, float:9.18355E-41)
            r1 = r1 & r0
            if (r1 == 0) goto L_0x008b
            r19 = r2
            goto L_0x008d
        L_0x008b:
            r19 = r38
        L_0x008d:
            r1 = 131072(0x20000, float:1.83671E-40)
            r0 = r0 & r1
            if (r0 == 0) goto L_0x0095
            r20 = r2
            goto L_0x0097
        L_0x0095:
            r20 = r39
        L_0x0097:
            r2 = r21
            r3 = r22
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: e.a.a.a.e.p.ErrorNavigationDto.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, boolean, boolean, boolean, boolean, boolean, boolean, android.os.Parcelable, android.os.Parcelable, int):void");
    }
}
