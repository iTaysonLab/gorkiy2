package e.a.a.a.c.c;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: BottomSheetSelectorDialogViewState.kt */
public abstract class BottomSheetSelectorDialogViewState0 extends BaseViewState0 {

    /* compiled from: BottomSheetSelectorDialogViewState.kt */
    public static final class a extends BottomSheetSelectorDialogViewState0 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: BottomSheetSelectorDialogViewState.kt */
    public static final class b extends BottomSheetSelectorDialogViewState0 {
        public final String a;
        public final List<e.a.a.a.e.p.a> b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, List<? extends e.a.a.a.e.p.a> list) {
            super(null);
            if (str == null) {
                Intrinsics.a("title");
                throw null;
            } else if (list != null) {
                this.a = str;
                this.b = list;
            } else {
                Intrinsics.a("items");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return Intrinsics.a(this.a, bVar.a) && Intrinsics.a(this.b, bVar.b);
        }

        public int hashCode() {
            String str = this.a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            List<e.a.a.a.e.p.a> list = this.b;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitEvent(title=");
            a2.append(this.a);
            a2.append(", items=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: BottomSheetSelectorDialogViewState.kt */
    public static final class c extends BottomSheetSelectorDialogViewState0 {
        public final BottomSheetSelectorNavigationDto a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto) {
            super(null);
            if (bottomSheetSelectorNavigationDto != null) {
                this.a = bottomSheetSelectorNavigationDto;
                return;
            }
            Intrinsics.a("item");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && Intrinsics.a(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = this.a;
            if (bottomSheetSelectorNavigationDto != null) {
                return bottomSheetSelectorNavigationDto.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("ItemSelectedEvent(item=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ BottomSheetSelectorDialogViewState0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
