package e.a.a.a.d;

import e.a.a.a.d.DevActivityViewState0;
import e.a.a.a.d.y;
import e.a.a.j.b.IDevMenuRepository;
import e.a.a.j.b.IDevMenuRepository0;
import l.b.Completable;
import l.b.d;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.e.a.CompletableDoFinally;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm1<T, R> implements Function<y.a, d> {
    public final /* synthetic */ DevActivityVm2 a;

    public DevActivityVm1(DevActivityVm2 devActivityVm2) {
        this.a = devActivityVm2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.d.DevActivityVm0, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Object a(Object obj) {
        IDevMenuRepository iDevMenuRepository;
        if (((DevActivityViewState0.a) obj) != null) {
            DevActivityVm2 devActivityVm2 = this.a;
            devActivityVm2.f602j.a(((DevActivityViewState) devActivityVm2.e()).b);
            DevActivityVm2 devActivityVm22 = this.a;
            IDevMenuRepository0 iDevMenuRepository0 = devActivityVm22.f602j;
            try {
                iDevMenuRepository = IDevMenuRepository.valueOf(((DevActivityViewState) devActivityVm22.e()).a);
            } catch (Exception unused) {
                iDevMenuRepository = null;
            }
            Completable a2 = iDevMenuRepository0.a(iDevMenuRepository);
            DevActivityVm0 devActivityVm0 = new DevActivityVm0(this);
            if (a2 != null) {
                ObjectHelper.a((Object) devActivityVm0, "onFinally is null");
                return new CompletableDoFinally(a2, devActivityVm0);
            }
            throw null;
        }
        Intrinsics.a("it");
        throw null;
    }
}
