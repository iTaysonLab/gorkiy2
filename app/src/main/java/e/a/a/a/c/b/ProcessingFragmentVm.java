package e.a.a.a.c.b;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.c.b.ProcessingFragmentViewState1;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.p.ProcessingNavigationDto0;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.c.c.BaseViewState1;
import n.n.c.Intrinsics;

/* compiled from: ProcessingFragmentVm.kt */
public final class ProcessingFragmentVm extends BaseDpFragmentVm1<c> {

    /* renamed from: j  reason: collision with root package name */
    public final IFragmentCoordinator f597j;

    /* renamed from: k  reason: collision with root package name */
    public final ITopLevelCoordinator f598k;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProcessingFragmentVm(IFragmentCoordinator iFragmentCoordinator, ITopLevelCoordinator iTopLevelCoordinator) {
        super(iFragmentCoordinator);
        if (iFragmentCoordinator == null) {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        } else if (iTopLevelCoordinator != null) {
            this.f597j = iFragmentCoordinator;
            this.f598k = iTopLevelCoordinator;
        } else {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        ProcessingFragmentViewState processingFragmentViewState = (ProcessingFragmentViewState) obj;
        if (processingFragmentViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (!(baseViewState1 instanceof ProcessingFragmentViewState1.a)) {
            super.a(processingFragmentViewState, baseViewState1);
            return processingFragmentViewState;
        } else {
            ProcessingFragmentViewState1.a aVar = (ProcessingFragmentViewState1.a) baseViewState1;
            throw null;
        }
    }

    public Object d() {
        return new ProcessingFragmentViewState(null, false, 3);
    }

    public void f() {
        if (((ProcessingFragmentViewState) e()).b) {
            this.f598k.b();
        } else {
            this.f597j.a(ProcessingNavigationDto0.BACK);
        }
    }
}
