package e.a.a.a.d;

import e.a.a.a.d.DevActivityViewState0;
import e.a.a.a.d.DevActivityViewState1;
import e.a.a.a.d.y;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.j.a.IAuthRepository0;
import e.a.a.j.b.IDevMenuRepository0;
import e.c.b.i.a.ICacheStorage;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseMviVm1;
import e.c.c.BaseViewState1;
import e.c.c.BaseViewState4;
import e.c.c.k;
import e.c.c.p;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Scheduler;
import l.b.r.b.AndroidSchedulers;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.d.CallbackCompletableObserver;
import l.b.u.e.a.CompletableObserveOn;
import l.b.u.e.c.ObservableFlatMapCompletableCompletable;
import l.b.w.Schedulers;
import n.Unit;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.n.c.j;
import n.p.KDeclarationContainer;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm2 extends BaseMviVm1<j> {

    /* renamed from: i  reason: collision with root package name */
    public final ITopLevelCoordinator f601i;

    /* renamed from: j  reason: collision with root package name */
    public final IDevMenuRepository0 f602j;

    /* renamed from: k  reason: collision with root package name */
    public final IAuthRepository0 f603k;

    /* renamed from: l  reason: collision with root package name */
    public final ICacheStorage f604l;

    /* compiled from: DevActivityVm.kt */
    public static final /* synthetic */ class a extends h implements Functions0<Observable<y.a>, Observable<BaseMviVm0<? extends k>>> {
        public a(DevActivityVm2 devActivityVm2) {
            super(1, devActivityVm2);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.d.DevActivityVm2$a, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return DevActivityVm2.a((DevActivityVm2) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "applyChangesProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(DevActivityVm2.class);
        }

        public final String i() {
            return "applyChangesProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            DevActivityViewState0.b bVar = (DevActivityViewState0.b) obj;
            if (bVar != null) {
                return new BaseMviVm0.a(new DevActivityViewState1.a(bVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final /* synthetic */ class c extends h implements Functions0<Observable<y.c>, Observable<BaseMviVm0<? extends k>>> {
        public c(DevActivityVm2 devActivityVm2) {
            super(1, devActivityVm2);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.d.DevActivityVm2$c, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return DevActivityVm2.b((DevActivityVm2) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "clearCacheProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(DevActivityVm2.class);
        }

        public final String i() {
            return "clearCacheProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final class d extends j implements Functions0<y.e, n.g> {
        public final /* synthetic */ DevActivityVm2 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DevActivityVm2 devActivityVm2) {
            super(1);
            this.c = devActivityVm2;
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [l.b.CompletableObserver, l.b.u.d.CallbackCompletableObserver] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.d.DevActivityVm3, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public Object a(Object obj) {
            if (((DevActivityViewState0.e) obj) != null) {
                Completable a = this.c.f603k.a();
                DevActivityVm3 devActivityVm3 = new DevActivityVm3(this);
                if (a != null) {
                    ObjectHelper.a((Object) devActivityVm3, "onComplete is null");
                    a.a((CompletableObserver) new CallbackCompletableObserver(devActivityVm3));
                    return Unit.a;
                }
                throw null;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            DevActivityViewState0.f fVar = (DevActivityViewState0.f) obj;
            if (fVar != null) {
                return new BaseMviVm0.a(new DevActivityViewState1.b(fVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final class f extends j implements Functions0<y.d, n.g> {
        public final /* synthetic */ DevActivityVm2 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DevActivityVm2 devActivityVm2) {
            super(1);
            this.c = devActivityVm2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [l.b.Scheduler, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public Object a(Object obj) {
            if (((DevActivityViewState0.d) obj) != null) {
                Completable a = this.c.f602j.a().a(Schedulers.b);
                Scheduler a2 = AndroidSchedulers.a();
                ObjectHelper.a((Object) a2, "scheduler is null");
                new CompletableObserveOn(a, a2).a(new DevActivityVm4(this), new DevActivityVm5(this));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivityVm.kt */
    public static final class g<T, R> implements Function<T, ObservableSource<? extends R>> {
        public final /* synthetic */ DevActivityVm2 a;

        public g(DevActivityVm2 devActivityVm2) {
            this.a = devActivityVm2;
        }

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                return Observable.a(Observable.d(Boolean.valueOf(this.a.f602j.b())).c((Function) DevActivityVm8.a), this.a.f602j.e().a((Function) DevActivityVm9.a).b(DevActivityVm10.a).c().c((Function) DevActivityVm11.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public DevActivityVm2(ITopLevelCoordinator iTopLevelCoordinator, IDevMenuRepository0 iDevMenuRepository0, IAuthRepository0 iAuthRepository0, ICacheStorage iCacheStorage) {
        if (iTopLevelCoordinator == null) {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        } else if (iDevMenuRepository0 == null) {
            Intrinsics.a("devMenuRepository");
            throw null;
        } else if (iAuthRepository0 == null) {
            Intrinsics.a("authRepository");
            throw null;
        } else if (iCacheStorage != null) {
            this.f601i = iTopLevelCoordinator;
            this.f602j = iDevMenuRepository0;
            this.f603k = iAuthRepository0;
            this.f604l = iCacheStorage;
        } else {
            Intrinsics.a("cacheStorage");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS
     arg types: [e.a.a.a.d.DevActivityViewState, e.c.c.BaseViewState1]
     candidates:
      e.a.a.a.d.DevActivityVm2.a(e.a.a.a.d.DevActivityVm2, l.b.Observable):l.b.Observable
      e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS */
    public Object a(Object obj, BaseViewState1 baseViewState1) {
        DevActivityViewState devActivityViewState = (DevActivityViewState) obj;
        if (devActivityViewState == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof DevActivityViewState1.a) {
            return DevActivityViewState.a(devActivityViewState, ((DevActivityViewState1.a) baseViewState1).a, false, 2);
        } else {
            if (baseViewState1 instanceof DevActivityViewState1.b) {
                return DevActivityViewState.a(devActivityViewState, null, ((DevActivityViewState1.b) baseViewState1).a, 1);
            }
            super.a((Object) devActivityViewState, (k) baseViewState1);
            return devActivityViewState;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> b(Observable<p> observable) {
        if (observable != null) {
            Observable<R> a2 = observable.a(new g(this));
            Intrinsics.a((Object) a2, "observable.flatMap {\n   …}\n            )\n        }");
            return a2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    public Object d() {
        return new DevActivityViewState(null, false, 3);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [l.b.u.e.c.ObservableFlatMapCompletableCompletable, l.b.Completable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.d.DevActivityVm7, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable b(DevActivityVm2 devActivityVm2, Observable observable) {
        if (devActivityVm2 != null) {
            DevActivityVm7 devActivityVm7 = new DevActivityVm7(devActivityVm2);
            if (observable != null) {
                ObjectHelper.a((Object) devActivityVm7, "mapper is null");
                Observable b2 = new ObservableFlatMapCompletableCompletable(observable, devActivityVm7, false).b();
                Intrinsics.a((Object) b2, "observable.flatMapComple…le<Lce<out BaseResult>>()");
                return b2;
            }
            throw null;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(DevActivityViewState0.a.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(DevActivityViewState0.b.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(DevActivityViewState0.c.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(DevActivityViewState0.e.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(DevActivityViewState0.f.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(DevActivityViewState0.d.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a8 = Observable.a(super.a(observable), BaseMviVm.b(a2, new a(this)), a3.c((Function) b.a), BaseMviVm.b(a4, new c(this)), BaseMviVm.a(a5, new d(this)), a6.c((Function) e.a), BaseMviVm.a(a7, new f(this)));
            Intrinsics.a((Object) a8, "Observable.mergeArray(\n …)\n            }\n        )");
            return a8;
        }
        Intrinsics.a("o");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [l.b.u.e.c.ObservableFlatMapCompletableCompletable, l.b.Completable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.d.DevActivityVm1, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable a(DevActivityVm2 devActivityVm2, Observable observable) {
        if (devActivityVm2 != null) {
            DevActivityVm1 devActivityVm1 = new DevActivityVm1(devActivityVm2);
            if (observable != null) {
                ObjectHelper.a((Object) devActivityVm1, "mapper is null");
                Observable b2 = new ObservableFlatMapCompletableCompletable(observable, devActivityVm1, false).b();
                Intrinsics.a((Object) b2, "observable.flatMapComple…le<Lce<out BaseResult>>()");
                return b2;
            }
            throw null;
        }
        throw null;
    }
}
