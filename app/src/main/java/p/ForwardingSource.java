package p;

import n.n.c.Intrinsics;

/* compiled from: ForwardingSource.kt */
public abstract class ForwardingSource implements Source {
    public final Source b;

    public ForwardingSource(Source source) {
        if (source != null) {
            this.b = source;
        } else {
            Intrinsics.a("delegate");
            throw null;
        }
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            return this.b.b(buffer, j2);
        }
        Intrinsics.a("sink");
        throw null;
    }

    public void close() {
        this.b.close();
    }

    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }

    public Timeout b() {
        return this.b.b();
    }
}
