package p;

import n.n.c.Intrinsics;

/* compiled from: ForwardingSink.kt */
public abstract class ForwardingSink implements Sink {
    public final Sink b;

    public ForwardingSink(Sink sink) {
        if (sink != null) {
            this.b = sink;
        } else {
            Intrinsics.a("delegate");
            throw null;
        }
    }

    public void a(Buffer buffer, long j2) {
        if (buffer != null) {
            this.b.a(buffer, j2);
        } else {
            Intrinsics.a("source");
            throw null;
        }
    }

    public Timeout b() {
        return this.b.b();
    }

    public void close() {
        this.b.close();
    }

    public void flush() {
        this.b.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }
}
