package p;

import java.util.concurrent.TimeUnit;
import n.n.c.Intrinsics;

/* compiled from: ForwardingTimeout.kt */
public class ForwardingTimeout extends Timeout {

    /* renamed from: e  reason: collision with root package name */
    public Timeout f3064e;

    public ForwardingTimeout(Timeout timeout) {
        if (timeout != null) {
            this.f3064e = super;
        } else {
            Intrinsics.a("delegate");
            throw null;
        }
    }

    public Timeout a(long j2, TimeUnit timeUnit) {
        if (timeUnit != null) {
            return this.f3064e.a(j2, timeUnit);
        }
        Intrinsics.a("unit");
        throw null;
    }

    public Timeout b() {
        return this.f3064e.b();
    }

    public long c() {
        return this.f3064e.c();
    }

    public boolean d() {
        return this.f3064e.d();
    }

    public void e() {
        this.f3064e.e();
    }

    public Timeout a(long j2) {
        return this.f3064e.a(j2);
    }

    public Timeout a() {
        return this.f3064e.a();
    }
}
