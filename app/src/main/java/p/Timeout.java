package p;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import n.n.c.Intrinsics;

/* compiled from: Timeout.kt */
public class Timeout {
    public static final Timeout d = new a();
    public boolean a;
    public long b;
    public long c;

    /* compiled from: Timeout.kt */
    public static final class a extends Timeout {
        public Timeout a(long j2) {
            return super;
        }

        public Timeout a(long j2, TimeUnit timeUnit) {
            if (timeUnit != null) {
                return super;
            }
            Intrinsics.a("unit");
            throw null;
        }

        public void e() {
        }
    }

    public Timeout a(long j2, TimeUnit timeUnit) {
        if (timeUnit != null) {
            if (j2 >= 0) {
                this.c = timeUnit.toNanos(j2);
                return this;
            }
            throw new IllegalArgumentException(("timeout < 0: " + j2).toString());
        }
        Intrinsics.a("unit");
        throw null;
    }

    public Timeout b() {
        this.c = 0;
        return this;
    }

    public long c() {
        if (this.a) {
            return this.b;
        }
        throw new IllegalStateException("No deadline".toString());
    }

    public boolean d() {
        return this.a;
    }

    public void e() {
        if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        } else if (this.a && this.b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    public Timeout a(long j2) {
        this.a = true;
        this.b = j2;
        return this;
    }

    public Timeout a() {
        this.a = false;
        return this;
    }
}
