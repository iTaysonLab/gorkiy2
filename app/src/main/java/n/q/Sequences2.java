package n.q;

import java.util.Iterator;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: Sequences.kt */
public final class Sequences2<T, R> implements Sequence<R> {
    public final Sequence<T> a;
    public final Functions0<T, R> b;

    /* compiled from: Sequences.kt */
    public static final class a implements Iterator<R>, n.n.c.t.a {
        public final Iterator<T> b;
        public final /* synthetic */ Sequences2 c;

        public a(Sequences2 sequences2) {
            this.c = sequences2;
            this.b = sequences2.a.iterator();
        }

        public boolean hasNext() {
            return this.b.hasNext();
        }

        public R next() {
            return this.c.b.a(this.b.next());
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    public Sequences2(Sequence<? extends T> sequence, Functions0<? super T, ? extends R> functions0) {
        if (sequence == null) {
            Intrinsics.a("sequence");
            throw null;
        } else if (functions0 != null) {
            this.a = sequence;
            this.b = functions0;
        } else {
            Intrinsics.a("transformer");
            throw null;
        }
    }

    public Iterator<R> iterator() {
        return new a(this);
    }
}
