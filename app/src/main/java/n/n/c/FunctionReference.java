package n.n.c;

import j.a.a.a.outline;
import n.p.KCallable;
import n.p.KFunction;

public class FunctionReference extends CallableReference implements FunctionBase, KFunction {

    /* renamed from: e  reason: collision with root package name */
    public final int f2784e;

    public FunctionReference(int i2) {
        this.f2784e = i2;
    }

    public int c() {
        return this.f2784e;
    }

    public KCallable e() {
        if (Reflection.a != null) {
            return this;
        }
        throw null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof FunctionReference) {
            FunctionReference functionReference = (FunctionReference) obj;
            if (g() != null ? g().equals(super.g()) : super.g() == null) {
                if (!f().equals(super.f()) || !i().equals(super.i()) || !Intrinsics.a(super.c, super.c)) {
                    return false;
                }
                return true;
            }
            return false;
        } else if (obj instanceof KFunction) {
            return obj.equals(d());
        } else {
            return false;
        }
    }

    public KCallable h() {
        return (KFunction) super.h();
    }

    public int hashCode() {
        return i().hashCode() + ((f().hashCode() + (g() == null ? 0 : g().hashCode() * 31)) * 31);
    }

    public String toString() {
        KCallable d = d();
        if (d != this) {
            return d.toString();
        }
        if ("<init>".equals(f())) {
            return "constructor (Kotlin reflection is not available)";
        }
        StringBuilder a = outline.a("function ");
        a.append(f());
        a.append(" (Kotlin reflection is not available)");
        return a.toString();
    }

    public FunctionReference(int i2, Object obj) {
        super(obj);
        this.f2784e = i2;
    }
}
