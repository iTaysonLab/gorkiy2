package n.n.c;

import java.util.Iterator;
import java.util.NoSuchElementException;
import n.n.c.t.a;

/* compiled from: ArrayIterator.kt */
public final class ArrayIterator<T> implements Iterator<T>, a {
    public int b;
    public final T[] c;

    public ArrayIterator(T[] tArr) {
        if (tArr != null) {
            this.c = tArr;
        } else {
            Intrinsics.a("array");
            throw null;
        }
    }

    public boolean hasNext() {
        return this.b < this.c.length;
    }

    public T next() {
        try {
            T[] tArr = this.c;
            int i2 = this.b;
            this.b = i2 + 1;
            return tArr[i2];
        } catch (ArrayIndexOutOfBoundsException e2) {
            this.b--;
            throw new NoSuchElementException(e2.getMessage());
        }
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
