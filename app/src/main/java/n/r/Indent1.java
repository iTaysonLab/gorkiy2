package n.r;

import j.a.a.a.outline;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: Indent.kt */
public final class Indent1 extends j implements Functions0<String, String> {
    public final /* synthetic */ String c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Indent1(String str) {
        super(1);
        this.c = str;
    }

    public Object a(Object obj) {
        String str = (String) obj;
        if (str != null) {
            return outline.a(new StringBuilder(), this.c, str);
        }
        Intrinsics.a("line");
        throw null;
    }
}
