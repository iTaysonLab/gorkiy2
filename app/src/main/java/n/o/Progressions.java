package n.o;

import java.util.Iterator;
import n.i.Collections;
import n.n.c.t.a;

/* compiled from: Progressions.kt */
public class Progressions implements Iterable<Integer>, a {
    public final int b;
    public final int c;
    public final int d;

    public Progressions(int i2, int i3, int i4) {
        if (i4 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i4 != Integer.MIN_VALUE) {
            this.b = i2;
            if (i4 > 0) {
                if (i2 < i3) {
                    i3 -= Collections.c(Collections.c(i3, i4) - Collections.c(i2, i4), i4);
                }
            } else if (i4 >= 0) {
                throw new IllegalArgumentException("Step is zero.");
            } else if (i2 > i3) {
                int i5 = -i4;
                i3 += Collections.c(Collections.c(i2, i5) - Collections.c(i3, i5), i5);
            }
            this.c = i3;
            this.d = i4;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof Progressions) {
            if (!isEmpty() || !((Progressions) obj).isEmpty()) {
                Progressions progressions = (Progressions) obj;
                if (!(this.b == progressions.b && this.c == progressions.c && this.d == progressions.d)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    public boolean isEmpty() {
        if (this.d > 0) {
            if (this.b > this.c) {
                return true;
            }
        } else if (this.b < this.c) {
            return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new ProgressionIterators(this.b, this.c, this.d);
    }

    public String toString() {
        int i2;
        StringBuilder sb;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i2 = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i2 = -this.d;
        }
        sb.append(i2);
        return sb.toString();
    }
}
