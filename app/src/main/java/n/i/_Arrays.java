package n.i;

import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: _Arrays.kt */
public class _Arrays extends Arrays {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> List<T> a(Object[] objArr) {
        if (objArr != null) {
            List<T> asList = Arrays.asList(objArr);
            Intrinsics.a((Object) asList, "ArraysUtilJVM.asList(this)");
            return asList;
        }
        Intrinsics.a("$this$asList");
        throw null;
    }

    public static final <T, C extends Collection<? super T>> C a(Iterable<? extends T> iterable, C c) {
        if (iterable == null) {
            Intrinsics.a("$this$toCollection");
            throw null;
        } else if (c != null) {
            for (Object add : iterable) {
                c.add(add);
            }
            return c;
        } else {
            Intrinsics.a("destination");
            throw null;
        }
    }

    public static final <T> List<T> a(Iterable iterable) {
        List list;
        if (iterable != null) {
            boolean z = iterable instanceof Collection;
            if (z) {
                Collection collection = (Collection) iterable;
                int size = collection.size();
                if (size == 0) {
                    return Collections2.b;
                }
                if (size != 1) {
                    return a(collection);
                }
                return c.c(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
            }
            if (z) {
                list = a((Collection) iterable);
            } else {
                ArrayList arrayList = new ArrayList();
                a(iterable, arrayList);
                list = arrayList;
            }
            return Collections.b(list);
        }
        Intrinsics.a("$this$toList");
        throw null;
    }

    public static final <T> List<T> a(Collection collection) {
        if (collection != null) {
            return new ArrayList(collection);
        }
        Intrinsics.a("$this$toMutableList");
        throw null;
    }

    public static /* synthetic */ void a(Object[] objArr, Object obj, int i2, int i3, int i4) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = objArr.length;
        }
        if (objArr != null) {
            Arrays.fill(objArr, i2, i3, obj);
        } else {
            Intrinsics.a("$this$fill");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.lang.Iterable<? extends T>]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean */
    public static final <T> List<T> a(Collection<? extends T> collection, Iterable<? extends T> iterable) {
        if (collection == null) {
            Intrinsics.a("$this$plus");
            throw null;
        } else if (iterable == null) {
            Intrinsics.a("elements");
            throw null;
        } else if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection2.size() + collection.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        } else {
            ArrayList arrayList2 = new ArrayList(collection);
            Collections.a((Collection) arrayList2, (Iterable) iterable);
            return arrayList2;
        }
    }

    public static final <T, A extends Appendable> A a(Iterable iterable, Appendable appendable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i2, CharSequence charSequence4, Functions0 functions0) {
        if (iterable == null) {
            Intrinsics.a("$this$joinTo");
            throw null;
        } else if (appendable == null) {
            Intrinsics.a("buffer");
            throw null;
        } else if (charSequence == null) {
            Intrinsics.a("separator");
            throw null;
        } else if (charSequence2 == null) {
            Intrinsics.a("prefix");
            throw null;
        } else if (charSequence3 == null) {
            Intrinsics.a("postfix");
            throw null;
        } else if (charSequence4 != null) {
            appendable.append(charSequence2);
            int i3 = 0;
            for (Object next : iterable) {
                i3++;
                if (i3 > 1) {
                    appendable.append(charSequence);
                }
                if (i2 >= 0 && i3 > i2) {
                    break;
                }
                Collections.a(appendable, next, functions0);
            }
            if (i2 >= 0 && i3 > i2) {
                appendable.append(charSequence4);
            }
            appendable.append(charSequence3);
            return appendable;
        } else {
            Intrinsics.a("truncated");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ String a(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i2, CharSequence charSequence4, Functions0 functions0, int i3) {
        if ((i3 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = charSequence;
        String str = (i3 & 2) != 0 ? "" : charSequence2;
        String str2 = (i3 & 4) != 0 ? "" : charSequence3;
        int i4 = (i3 & 8) != 0 ? -1 : i2;
        if ((i3 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence6 = charSequence4;
        Functions0 functions02 = (i3 & 32) != 0 ? null : functions0;
        if (iterable == null) {
            Intrinsics.a("$this$joinToString");
            throw null;
        } else if (charSequence5 == null) {
            Intrinsics.a("separator");
            throw null;
        } else if (str == null) {
            Intrinsics.a("prefix");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("postfix");
            throw null;
        } else if (charSequence6 != null) {
            StringBuilder sb = new StringBuilder();
            a(iterable, sb, charSequence5, str, str2, i4, charSequence6, functions02);
            String sb2 = sb.toString();
            Intrinsics.a((Object) sb2, "joinTo(StringBuilder(), …ed, transform).toString()");
            return sb2;
        } else {
            Intrinsics.a("truncated");
            throw null;
        }
    }
}
