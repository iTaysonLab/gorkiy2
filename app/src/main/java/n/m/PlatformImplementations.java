package n.m;

import java.lang.reflect.Method;
import n.n.c.Intrinsics;

/* compiled from: PlatformImplementations.kt */
public class PlatformImplementations {

    /* compiled from: PlatformImplementations.kt */
    public static final class a {
        public static final Method a;

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r7v3, types: [java.lang.Class[], java.lang.Object] */
        /* JADX WARN: Type inference failed for: r5v5 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.reflect.Method[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.reflect.Method, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0037, code lost:
            if (n.n.c.Intrinsics.a(r5, r0) != false) goto L_0x003b;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        static {
            /*
                java.lang.Class<java.lang.Throwable> r0 = java.lang.Throwable.class
                java.lang.reflect.Method[] r1 = r0.getMethods()
                java.lang.String r2 = "throwableClass.methods"
                n.n.c.Intrinsics.a(r1, r2)
                int r2 = r1.length
                r3 = 0
                r4 = 0
            L_0x000e:
                r5 = 0
                if (r4 >= r2) goto L_0x0042
                r6 = r1[r4]
                java.lang.String r7 = "it"
                n.n.c.Intrinsics.a(r6, r7)
                java.lang.String r7 = r6.getName()
                java.lang.String r8 = "addSuppressed"
                boolean r7 = n.n.c.Intrinsics.a(r7, r8)
                r8 = 1
                if (r7 == 0) goto L_0x003a
                java.lang.Class[] r7 = r6.getParameterTypes()
                java.lang.String r9 = "it.parameterTypes"
                n.n.c.Intrinsics.a(r7, r9)
                int r9 = r7.length
                if (r9 != r8) goto L_0x0033
                r5 = r7[r3]
            L_0x0033:
                boolean r5 = n.n.c.Intrinsics.a(r5, r0)
                if (r5 == 0) goto L_0x003a
                goto L_0x003b
            L_0x003a:
                r8 = 0
            L_0x003b:
                if (r8 == 0) goto L_0x003f
                r5 = r6
                goto L_0x0042
            L_0x003f:
                int r4 = r4 + 1
                goto L_0x000e
            L_0x0042:
                n.m.PlatformImplementations.a.a = r5
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: n.m.PlatformImplementations.a.<clinit>():void");
        }
    }

    public void a(Throwable th, Throwable th2) {
        if (th == null) {
            Intrinsics.a("cause");
            throw null;
        } else if (th2 != null) {
            Method method = a.a;
            if (method != null) {
                method.invoke(th, th2);
            }
        } else {
            Intrinsics.a("exception");
            throw null;
        }
    }
}
