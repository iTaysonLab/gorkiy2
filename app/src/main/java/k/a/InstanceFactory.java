package k.a;

public final class InstanceFactory<T> implements Factory<T> {
    public final T a;

    static {
        new InstanceFactory(null);
    }

    public InstanceFactory(T t2) {
        this.a = t2;
    }

    public T get() {
        return this.a;
    }
}
