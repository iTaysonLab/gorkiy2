package i.e;

import i.e.MapCollections;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ArrayMap<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {

    /* renamed from: i  reason: collision with root package name */
    public MapCollections<K, V> f1057i;

    public class a extends MapCollections<K, V> {
        public a() {
        }

        public Object a(int i2, int i3) {
            return ArrayMap.this.c[(i2 << 1) + i3];
        }

        public int b(Object obj) {
            return ArrayMap.this.b(obj);
        }

        public int c() {
            return ArrayMap.this.d;
        }

        public int a(Object obj) {
            return ArrayMap.this.a(obj);
        }

        public Map<K, V> b() {
            return ArrayMap.this;
        }

        public void a(K k2, V v) {
            ArrayMap.this.put(k2, v);
        }

        public V a(int i2, V v) {
            return ArrayMap.this.a(i2, v);
        }

        public void a(int i2) {
            ArrayMap.this.d(i2);
        }

        public void a() {
            ArrayMap.this.clear();
        }
    }

    public ArrayMap() {
    }

    public final MapCollections<K, V> b() {
        if (this.f1057i == null) {
            this.f1057i = new a();
        }
        return this.f1057i;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        MapCollections b = b();
        if (b.a == null) {
            b.a = new MapCollections.b();
        }
        return b.a;
    }

    public Set<K> keySet() {
        MapCollections b = b();
        if (b.b == null) {
            b.b = new MapCollections.c();
        }
        return b.b;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        b(map.size() + super.d);
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    public Collection<V> values() {
        MapCollections b = b();
        if (b.c == null) {
            b.c = new MapCollections.e();
        }
        return b.c;
    }

    public ArrayMap(int i2) {
        super(i2);
    }

    public ArrayMap(SimpleArrayMap simpleArrayMap) {
        if (simpleArrayMap != null) {
            a(super);
        }
    }
}
