package i.j.b;

import android.graphics.Rect;
import i.j.b.ExploreByTouchHelper;
import java.util.Comparator;

/* compiled from: FocusStrategy */
public class FocusStrategy1<T> implements Comparator<T> {
    public final Rect b = new Rect();
    public final Rect c = new Rect();
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final FocusStrategy<T> f1244e;

    public FocusStrategy1(boolean z, FocusStrategy<T> focusStrategy) {
        this.d = z;
        this.f1244e = focusStrategy;
    }

    public int compare(T t2, T t3) {
        Rect rect = this.b;
        Rect rect2 = this.c;
        ((ExploreByTouchHelper.a) this.f1244e).a(t2, rect);
        ((ExploreByTouchHelper.a) this.f1244e).a(t3, rect2);
        int i2 = rect.top;
        int i3 = rect2.top;
        if (i2 < i3) {
            return -1;
        }
        if (i2 > i3) {
            return 1;
        }
        int i4 = rect.left;
        int i5 = rect2.left;
        if (i4 < i5) {
            if (this.d) {
                return 1;
            }
            return -1;
        } else if (i4 <= i5) {
            int i6 = rect.bottom;
            int i7 = rect2.bottom;
            if (i6 < i7) {
                return -1;
            }
            if (i6 > i7) {
                return 1;
            }
            int i8 = rect.right;
            int i9 = rect2.right;
            if (i8 < i9) {
                if (this.d) {
                    return 1;
                }
                return -1;
            } else if (i8 <= i9) {
                return 0;
            } else {
                if (this.d) {
                    return -1;
                }
                return 1;
            }
        } else if (this.d) {
            return -1;
        } else {
            return 1;
        }
    }
}
