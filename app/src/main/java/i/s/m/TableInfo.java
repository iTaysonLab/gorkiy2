package i.s.m;

import android.database.Cursor;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.s.m.b;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.e.FrameworkSQLiteDatabase0;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TableInfo {
    public final String a;
    public final Map<String, b.a> b;
    public final Set<b.b> c;
    public final Set<b.d> d;

    public static class a {
        public final String a;
        public final String b;
        public final int c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final int f1441e;

        /* renamed from: f  reason: collision with root package name */
        public final String f1442f;
        public final int g;

        public a(String str, String str2, boolean z, int i2, String str3, int i3) {
            this.a = str;
            this.b = str2;
            this.d = z;
            this.f1441e = i2;
            int i4 = 5;
            if (str2 != null) {
                String upperCase = str2.toUpperCase(Locale.US);
                if (upperCase.contains("INT")) {
                    i4 = 3;
                } else if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                    i4 = 2;
                } else if (!upperCase.contains("BLOB")) {
                    i4 = (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
                }
            }
            this.c = i4;
            this.f1442f = str3;
            this.g = i3;
        }

        public boolean equals(Object obj) {
            String str;
            String str2;
            String str3;
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f1441e != aVar.f1441e || !this.a.equals(aVar.a) || this.d != aVar.d) {
                return false;
            }
            if (this.g == 1 && aVar.g == 2 && (str3 = this.f1442f) != null && !str3.equals(aVar.f1442f)) {
                return false;
            }
            if (this.g == 2 && aVar.g == 1 && (str2 = aVar.f1442f) != null && !str2.equals(this.f1442f)) {
                return false;
            }
            int i2 = this.g;
            if ((i2 == 0 || i2 != aVar.g || ((str = this.f1442f) == null ? aVar.f1442f == null : str.equals(aVar.f1442f))) && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.c) * 31) + (this.d ? 1231 : 1237)) * 31) + this.f1441e;
        }

        public String toString() {
            StringBuilder a2 = outline.a("Column{name='");
            a2.append(this.a);
            a2.append('\'');
            a2.append(", type='");
            a2.append(this.b);
            a2.append('\'');
            a2.append(", affinity='");
            a2.append(this.c);
            a2.append('\'');
            a2.append(", notNull=");
            a2.append(this.d);
            a2.append(", primaryKeyPosition=");
            a2.append(this.f1441e);
            a2.append(", defaultValue='");
            a2.append(this.f1442f);
            a2.append('\'');
            a2.append('}');
            return a2.toString();
        }
    }

    public static class b {
        public final String a;
        public final String b;
        public final String c;
        public final List<String> d;

        /* renamed from: e  reason: collision with root package name */
        public final List<String> f1443e;

        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.f1443e = Collections.unmodifiableList(list2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.equals(bVar.a) && this.b.equals(bVar.b) && this.c.equals(bVar.c) && this.d.equals(bVar.d)) {
                return this.f1443e.equals(bVar.f1443e);
            }
            return false;
        }

        public int hashCode() {
            int hashCode = this.b.hashCode();
            int hashCode2 = this.c.hashCode();
            int hashCode3 = this.d.hashCode();
            return this.f1443e.hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31)) * 31);
        }

        public String toString() {
            StringBuilder a2 = outline.a("ForeignKey{referenceTable='");
            a2.append(this.a);
            a2.append('\'');
            a2.append(", onDelete='");
            a2.append(this.b);
            a2.append('\'');
            a2.append(", onUpdate='");
            a2.append(this.c);
            a2.append('\'');
            a2.append(", columnNames=");
            a2.append(this.d);
            a2.append(", referenceColumnNames=");
            a2.append(this.f1443e);
            a2.append('}');
            return a2.toString();
        }
    }

    public static class c implements Comparable<b.c> {
        public final int b;
        public final int c;
        public final String d;

        /* renamed from: e  reason: collision with root package name */
        public final String f1444e;

        public c(int i2, int i3, String str, String str2) {
            this.b = i2;
            this.c = i3;
            this.d = str;
            this.f1444e = str2;
        }

        public int compareTo(Object obj) {
            c cVar = (c) obj;
            int i2 = this.b - cVar.b;
            return i2 == 0 ? this.c - cVar.c : i2;
        }
    }

    public static class d {
        public final String a;
        public final boolean b;
        public final List<String> c;

        public d(String str, boolean z, List<String> list) {
            this.a = str;
            this.b = z;
            this.c = list;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.b != dVar.b || !this.c.equals(dVar.c)) {
                return false;
            }
            if (this.a.startsWith("index_")) {
                return dVar.a.startsWith("index_");
            }
            return this.a.equals(dVar.a);
        }

        public int hashCode() {
            int i2;
            if (this.a.startsWith("index_")) {
                i2 = -1184239155;
            } else {
                i2 = this.a.hashCode();
            }
            return this.c.hashCode() + (((i2 * 31) + (this.b ? 1 : 0)) * 31);
        }

        public String toString() {
            StringBuilder a2 = outline.a("Index{name='");
            a2.append(this.a);
            a2.append('\'');
            a2.append(", unique=");
            a2.append(this.b);
            a2.append(", columns=");
            a2.append(this.c);
            a2.append('}');
            return a2.toString();
        }
    }

    public TableInfo(String str, Map<String, b.a> map, Set<b.b> set, Set<b.d> set2) {
        Set<b.d> set3;
        this.a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = Collections.unmodifiableSet(set2);
        }
        this.d = set3;
    }

    public static List<b.c> a(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < count; i2++) {
            cursor.moveToPosition(i2);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    public boolean equals(Object obj) {
        Set<b.d> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || TableInfo.class != obj.getClass()) {
            return false;
        }
        TableInfo tableInfo = (TableInfo) obj;
        String str = this.a;
        if (str == null ? tableInfo.a != null : !str.equals(tableInfo.a)) {
            return false;
        }
        Map<String, b.a> map = this.b;
        if (map == null ? tableInfo.b != null : !map.equals(tableInfo.b)) {
            return false;
        }
        Set<b.b> set2 = this.c;
        if (set2 == null ? tableInfo.c != null : !set2.equals(tableInfo.c)) {
            return false;
        }
        Set<b.d> set3 = this.d;
        if (set3 == null || (set = tableInfo.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, b.a> map = this.b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<b.b> set = this.c;
        if (set != null) {
            i2 = set.hashCode();
        }
        return hashCode2 + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("TableInfo{name='");
        a2.append(this.a);
        a2.append('\'');
        a2.append(", columns=");
        a2.append(this.b);
        a2.append(", foreignKeys=");
        a2.append(this.c);
        a2.append(", indices=");
        a2.append(this.d);
        a2.append('}');
        return a2.toString();
    }

    public static d a(SupportSQLiteDatabase supportSQLiteDatabase, String str, boolean z) {
        Cursor b2 = ((FrameworkSQLiteDatabase0) supportSQLiteDatabase).b(outline.a("PRAGMA index_xinfo(`", str, "`)"));
        try {
            int columnIndex = b2.getColumnIndex("seqno");
            int columnIndex2 = b2.getColumnIndex("cid");
            int columnIndex3 = b2.getColumnIndex(DefaultAppMeasurementEventListenerRegistrar.NAME);
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    TreeMap treeMap = new TreeMap();
                    while (b2.moveToNext()) {
                        if (b2.getInt(columnIndex2) >= 0) {
                            int i2 = b2.getInt(columnIndex);
                            treeMap.put(Integer.valueOf(i2), b2.getString(columnIndex3));
                        }
                    }
                    ArrayList arrayList = new ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    d dVar = new d(str, z, arrayList);
                    b2.close();
                    return dVar;
                }
            }
            return null;
        } finally {
            b2.close();
        }
    }
}
