package i.s;

import android.os.Looper;
import i.s.h;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.SupportSQLiteOpenHelper;
import i.v.a.e.FrameworkSQLiteDatabase0;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import ru.covid19.droid.data.storage.db.DpDatabase_Impl;

public abstract class RoomDatabase {
    @Deprecated
    public volatile SupportSQLiteDatabase a;
    public Executor b;
    public SupportSQLiteOpenHelper c;
    public final InvalidationTracker d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1434e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1435f;
    @Deprecated
    public List<h.a> g;
    public final ReentrantReadWriteLock h = new ReentrantReadWriteLock();

    /* renamed from: i  reason: collision with root package name */
    public final ThreadLocal<Integer> f1436i = new ThreadLocal<>();

    public static abstract class a {
        public void a() {
        }

        public void b() {
        }

        public void c() {
        }
    }

    public enum b {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING
    }

    public static class c {
        public HashMap<Integer, TreeMap<Integer, i.s.l.a>> a = new HashMap<>();
    }

    public RoomDatabase() {
        new ConcurrentHashMap();
        this.d = new InvalidationTracker((DpDatabase_Impl) this, new HashMap(0), new HashMap(0), "cacheMap");
    }

    public void b() {
        if (!this.f1434e) {
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
            }
        }
    }

    public void c() {
        if (!e() && this.f1436i.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }

    @Deprecated
    public void d() {
        ((FrameworkSQLiteDatabase0) this.c.a()).b.endTransaction();
        if (!e()) {
            InvalidationTracker invalidationTracker = this.d;
            if (invalidationTracker.f1423e.compareAndSet(false, true)) {
                invalidationTracker.d.b.execute(invalidationTracker.f1426j);
            }
        }
    }

    public boolean e() {
        return ((FrameworkSQLiteDatabase0) this.c.a()).b.inTransaction();
    }

    public boolean f() {
        SupportSQLiteDatabase supportSQLiteDatabase = this.a;
        return supportSQLiteDatabase != null && ((FrameworkSQLiteDatabase0) supportSQLiteDatabase).b.isOpen();
    }
}
