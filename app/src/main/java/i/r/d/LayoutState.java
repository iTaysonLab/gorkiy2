package i.r.d;

import j.a.a.a.outline;

public class LayoutState {
    public boolean a = true;
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f1399e;

    /* renamed from: f  reason: collision with root package name */
    public int f1400f = 0;
    public int g = 0;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f1401i;

    public String toString() {
        StringBuilder a2 = outline.a("LayoutState{mAvailable=");
        a2.append(this.b);
        a2.append(", mCurrentPosition=");
        a2.append(this.c);
        a2.append(", mItemDirection=");
        a2.append(this.d);
        a2.append(", mLayoutDirection=");
        a2.append(this.f1399e);
        a2.append(", mStartLine=");
        a2.append(this.f1400f);
        a2.append(", mEndLine=");
        a2.append(this.g);
        a2.append('}');
        return a2.toString();
    }
}
