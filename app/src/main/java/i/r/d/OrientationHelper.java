package i.r.d;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class OrientationHelper {
    public final RecyclerView.o a;
    public int b = RecyclerView.UNDEFINED_DURATION;
    public final Rect c = new Rect();

    public /* synthetic */ OrientationHelper(RecyclerView.o oVar, OrientationHelper0 orientationHelper0) {
        this.a = oVar;
    }

    public static OrientationHelper a(RecyclerView.o oVar, int i2) {
        if (i2 == 0) {
            return new OrientationHelper0(oVar);
        }
        if (i2 == 1) {
            return new OrientationHelper1(oVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public abstract int a();

    public abstract int a(View view);

    public abstract void a(int i2);

    public abstract int b();

    public abstract int b(View view);

    public abstract int c();

    public abstract int c(View view);

    public abstract int d();

    public abstract int d(View view);

    public abstract int e();

    public abstract int e(View view);

    public abstract int f();

    public abstract int f(View view);

    public abstract int g();

    public int h() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return g() - this.b;
    }
}
