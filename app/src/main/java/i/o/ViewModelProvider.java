package i.o;

import android.app.Application;
import j.a.a.a.outline;
import java.lang.reflect.InvocationTargetException;

public class ViewModelProvider {
    public final b a;
    public final ViewModelStore b;

    public static class a extends d {
        public static a b;
        public Application a;

        public a(Application application) {
            this.a = application;
        }

        public <T extends v> T a(Class<T> cls) {
            if (!AndroidViewModel.class.isAssignableFrom(cls)) {
                return super.a(cls);
            }
            try {
                return (ViewModel) cls.getConstructor(Application.class).newInstance(this.a);
            } catch (NoSuchMethodException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Cannot create an instance of " + cls, e3);
            } catch (InstantiationException e4) {
                throw new RuntimeException("Cannot create an instance of " + cls, e4);
            } catch (InvocationTargetException e5) {
                throw new RuntimeException("Cannot create an instance of " + cls, e5);
            }
        }
    }

    public interface b {
        <T extends v> T a(Class<T> cls);
    }

    public static abstract class c implements b {
        public <T extends v> T a(Class<T> cls) {
            throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        public abstract <T extends v> T a(String str, Class<T> cls);
    }

    public static class d implements b {
        public <T extends v> T a(Class<T> cls) {
            try {
                return (ViewModel) cls.newInstance();
            } catch (InstantiationException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            } catch (IllegalAccessException e3) {
                throw new RuntimeException("Cannot create an instance of " + cls, e3);
            }
        }
    }

    public ViewModelProvider(ViewModelStore viewModelStore, b bVar) {
        this.a = bVar;
        this.b = viewModelStore;
    }

    public <T extends v> T a(Class<T> cls) {
        T t2;
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            String a2 = outline.a("androidx.lifecycle.ViewModelProvider.DefaultKey:", canonicalName);
            T t3 = (ViewModel) this.b.a.get(a2);
            if (!cls.isInstance(t3)) {
                b bVar = this.a;
                if (bVar instanceof c) {
                    t2 = ((c) bVar).a(a2, cls);
                } else {
                    t2 = bVar.a(cls);
                }
                t3 = t2;
                ViewModel put = this.b.a.put(a2, t3);
                if (put != null) {
                    put.b();
                }
            }
            return t3;
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
