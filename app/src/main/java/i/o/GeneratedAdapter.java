package i.o;

import i.o.Lifecycle;

public interface GeneratedAdapter {
    void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar, boolean z, MethodCallsLogger methodCallsLogger);
}
