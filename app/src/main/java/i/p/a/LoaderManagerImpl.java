package i.p.a;

import i.b.k.ResourcesFlusher;
import i.e.SparseArrayCompat;
import i.o.LifecycleOwner;
import i.o.MutableLiveData;
import i.o.Observer;
import i.o.ViewModel;
import i.o.ViewModelProvider;
import i.o.ViewModelStore;
import i.o.k;
import i.o.v;
import i.p.a.b;
import j.a.a.a.outline;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class LoaderManagerImpl extends LoaderManager {
    public final LifecycleOwner a;
    public final c b;

    public static class a<D> extends MutableLiveData<D> {

        /* renamed from: j  reason: collision with root package name */
        public LifecycleOwner f1362j;

        /* renamed from: k  reason: collision with root package name */
        public b<D> f1363k;

        public void a() {
            throw null;
        }

        public void b() {
            throw null;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(0);
            sb.append(" : ");
            ResourcesFlusher.a((Object) null, sb);
            sb.append("}}");
            return sb.toString();
        }

        public void a(Observer<? super D> observer) {
            super.a((Observer<? super k>) observer);
            this.f1362j = null;
            this.f1363k = null;
        }

        public void a(D d) {
            super.a((k) d);
        }
    }

    public static class b<D> implements Observer<D> {
    }

    public static class c extends ViewModel {
        public static final ViewModelProvider.b c = new a();
        public SparseArrayCompat<b.a> b = new SparseArrayCompat<>(10);

        public static class a implements ViewModelProvider.b {
            public <T extends v> T a(Class<T> cls) {
                return new c();
            }
        }

        public void b() {
            if (this.b.c() <= 0) {
                SparseArrayCompat<b.a> sparseArrayCompat = this.b;
                int i2 = sparseArrayCompat.f1073e;
                Object[] objArr = sparseArrayCompat.d;
                for (int i3 = 0; i3 < i2; i3++) {
                    objArr[i3] = null;
                }
                sparseArrayCompat.f1073e = 0;
                sparseArrayCompat.b = false;
            } else if (this.b.d(0) != null) {
                throw null;
            } else {
                throw null;
            }
        }
    }

    public LoaderManagerImpl(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        ViewModel viewModel;
        this.a = lifecycleOwner;
        ViewModelProvider.b bVar = c.c;
        Class<c> cls = c.class;
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            String a2 = outline.a("androidx.lifecycle.ViewModelProvider.DefaultKey:", canonicalName);
            ViewModel viewModel2 = viewModelStore.a.get(a2);
            if (!cls.isInstance(viewModel2)) {
                if (bVar instanceof ViewModelProvider.c) {
                    viewModel = ((ViewModelProvider.c) bVar).a(a2, cls);
                } else {
                    viewModel = bVar.a(cls);
                }
                viewModel2 = viewModel;
                ViewModel put = viewModelStore.a.put(a2, viewModel2);
                if (put != null) {
                    put.b();
                }
            }
            this.b = (c) viewModel2;
            return;
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        c cVar = this.b;
        if (cVar.b.c() > 0) {
            printWriter.print(str);
            printWriter.println("Loaders:");
            String str2 = str + "    ";
            if (cVar.b.c() > 0) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(cVar.b.b(0));
                printWriter.print(": ");
                printWriter.println(cVar.b.d(0).toString());
                printWriter.print(str2);
                printWriter.print("mId=");
                printWriter.print(0);
                printWriter.print(" mArgs=");
                printWriter.println((Object) null);
                printWriter.print(str2);
                printWriter.print("mLoader=");
                printWriter.println((Object) null);
                throw null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
     arg types: [i.o.LifecycleOwner, java.lang.StringBuilder]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void */
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        ResourcesFlusher.a((Object) this.a, sb);
        sb.append("}}");
        return sb.toString();
    }
}
