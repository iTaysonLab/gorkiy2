package i.t;

import android.os.Bundle;
import androidx.savedstate.Recreator;
import androidx.savedstate.SavedStateRegistry$1;
import i.c.a.b.SafeIterableMap;
import i.o.Lifecycle;
import i.o.LifecycleRegistry;
import i.t.SavedStateRegistry;
import java.util.Map;

public final class SavedStateRegistryController {
    public final SavedStateRegistryOwner a;
    public final SavedStateRegistry b = new SavedStateRegistry();

    public SavedStateRegistryController(SavedStateRegistryOwner savedStateRegistryOwner) {
        this.a = savedStateRegistryOwner;
    }

    public void a(Bundle bundle) {
        Lifecycle a2 = this.a.a();
        if (((LifecycleRegistry) a2).b == Lifecycle.b.INITIALIZED) {
            a2.a(new Recreator(this.a));
            SavedStateRegistry savedStateRegistry = this.b;
            if (!savedStateRegistry.c) {
                if (bundle != null) {
                    savedStateRegistry.b = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
                }
                a2.a(new SavedStateRegistry$1(savedStateRegistry));
                savedStateRegistry.c = true;
                return;
            }
            throw new IllegalStateException("SavedStateRegistry was already restored.");
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage");
    }

    public void b(Bundle bundle) {
        SavedStateRegistry savedStateRegistry = this.b;
        if (savedStateRegistry != null) {
            Bundle bundle2 = new Bundle();
            Bundle bundle3 = savedStateRegistry.b;
            if (bundle3 != null) {
                bundle2.putAll(bundle3);
            }
            SafeIterableMap<K, V>.d c = savedStateRegistry.a.c();
            while (c.hasNext()) {
                Map.Entry entry = (Map.Entry) c.next();
                bundle2.putBundle((String) entry.getKey(), ((SavedStateRegistry.b) entry.getValue()).a());
            }
            bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
            return;
        }
        throw null;
    }
}
