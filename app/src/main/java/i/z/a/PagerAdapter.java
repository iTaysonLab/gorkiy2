package i.z.a;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.view.ViewGroup;

public abstract class PagerAdapter {
    public final DataSetObservable a = new DataSetObservable();

    public abstract int a();

    public void a(DataSetObserver dataSetObserver) {
        synchronized (this) {
        }
    }

    public abstract void a(ViewGroup viewGroup, int i2, Object obj);
}
