package i.x.a.a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;

public class AnimatedVectorDrawableCompat extends VectorDrawableCommon implements Animatable2Compat {
    public b c;
    public Context d;

    /* renamed from: e  reason: collision with root package name */
    public ArgbEvaluator f1480e;

    /* renamed from: f  reason: collision with root package name */
    public final Drawable.Callback f1481f;

    public class a implements Drawable.Callback {
        public a() {
        }

        public void invalidateDrawable(Drawable drawable) {
            AnimatedVectorDrawableCompat.this.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
            AnimatedVectorDrawableCompat.this.scheduleSelf(runnable, j2);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            AnimatedVectorDrawableCompat.this.unscheduleSelf(runnable);
        }
    }

    public static class b extends Drawable.ConstantState {
        public int a;
        public VectorDrawableCompat b;
        public AnimatorSet c;
        public ArrayList<Animator> d;

        /* renamed from: e  reason: collision with root package name */
        public ArrayMap<Animator, String> f1482e;

        public b(b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.a = bVar.a;
                VectorDrawableCompat vectorDrawableCompat = bVar.b;
                if (vectorDrawableCompat != null) {
                    Drawable.ConstantState constantState = vectorDrawableCompat.getConstantState();
                    if (resources != null) {
                        this.b = (VectorDrawableCompat) super.newDrawable(resources);
                    } else {
                        this.b = (VectorDrawableCompat) super.newDrawable();
                    }
                    VectorDrawableCompat vectorDrawableCompat2 = this.b;
                    vectorDrawableCompat2.mutate();
                    this.b = vectorDrawableCompat2;
                    vectorDrawableCompat2.setCallback(callback);
                    this.b.setBounds(bVar.b.getBounds());
                    this.b.g = false;
                }
                ArrayList<Animator> arrayList = bVar.d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.d = new ArrayList<>(size);
                    this.f1482e = new ArrayMap<>(size);
                    for (int i2 = 0; i2 < size; i2++) {
                        Animator animator = bVar.d.get(i2);
                        Animator clone = animator.clone();
                        String orDefault = bVar.f1482e.getOrDefault(animator, null);
                        clone.setTarget(this.b.c.b.f1515p.getOrDefault(orDefault, null));
                        this.d.add(clone);
                        this.f1482e.put(clone, orDefault);
                    }
                    if (this.c == null) {
                        this.c = new AnimatorSet();
                    }
                    this.c.playTogether(this.d);
                }
            }
        }

        public int getChangingConfigurations() {
            return this.a;
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    public AnimatedVectorDrawableCompat() {
        this(null, null, null);
    }

    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.applyTheme(theme);
        }
    }

    public boolean canApplyTheme() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.canApplyTheme();
        }
        return false;
    }

    public void draw(Canvas canvas) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.c.b.draw(canvas);
        if (this.c.c.isStarted()) {
            invalidateSelf();
        }
    }

    public int getAlpha() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getAlpha();
        }
        return this.c.b.getAlpha();
    }

    public int getChangingConfigurations() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.c.a;
    }

    public ColorFilter getColorFilter() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getColorFilter();
        }
        return this.c.b.getColorFilter();
    }

    public Drawable.ConstantState getConstantState() {
        if (super.b == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new c(super.b.getConstantState());
    }

    public int getIntrinsicHeight() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.c.b.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.c.b.getIntrinsicWidth();
    }

    public int getOpacity() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.c.b.getOpacity();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ee, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f1, code lost:
        r7 = "Can't load animation resource ID #0x";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f5, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f6, code lost:
        r7 = "Can't load animation resource ID #0x";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x013c, code lost:
        r19.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ee A[ExcHandler: all (th java.lang.Throwable), Splitter:B:33:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x013c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void inflate(android.content.res.Resources r22, org.xmlpull.v1.XmlPullParser r23, android.util.AttributeSet r24, android.content.res.Resources.Theme r25) {
        /*
            r21 = this;
            r1 = r21
            r0 = r22
            r2 = r24
            r3 = r25
            android.graphics.drawable.Drawable r4 = r1.b
            if (r4 == 0) goto L_0x0012
            r5 = r23
            r4.inflate(r0, r5, r2, r3)
            return
        L_0x0012:
            r5 = r23
            int r4 = r23.getEventType()
            int r6 = r23.getDepth()
            r7 = 1
            int r6 = r6 + r7
        L_0x001e:
            if (r4 == r7) goto L_0x0155
            int r8 = r23.getDepth()
            if (r8 >= r6) goto L_0x0029
            r8 = 3
            if (r4 == r8) goto L_0x0155
        L_0x0029:
            r8 = 2
            if (r4 != r8) goto L_0x014e
            java.lang.String r4 = r23.getName()
            java.lang.String r8 = "animated-vector"
            boolean r8 = r8.equals(r4)
            r9 = 0
            r10 = 0
            if (r8 == 0) goto L_0x0063
            int[] r4 = i.x.a.a.AndroidResources.f1474e
            android.content.res.TypedArray r4 = i.b.k.ResourcesFlusher.a(r0, r3, r2, r4)
            int r8 = r4.getResourceId(r10, r10)
            if (r8 == 0) goto L_0x005e
            i.x.a.a.VectorDrawableCompat r8 = i.x.a.a.VectorDrawableCompat.a(r0, r8, r3)
            r8.g = r10
            android.graphics.drawable.Drawable$Callback r10 = r1.f1481f
            r8.setCallback(r10)
            i.x.a.a.AnimatedVectorDrawableCompat$b r10 = r1.c
            i.x.a.a.VectorDrawableCompat r10 = r10.b
            if (r10 == 0) goto L_0x005a
            r10.setCallback(r9)
        L_0x005a:
            i.x.a.a.AnimatedVectorDrawableCompat$b r9 = r1.c
            r9.b = r8
        L_0x005e:
            r4.recycle()
            goto L_0x014e
        L_0x0063:
            java.lang.String r8 = "target"
            boolean r4 = r8.equals(r4)
            if (r4 == 0) goto L_0x014e
            int[] r4 = i.x.a.a.AndroidResources.f1475f
            android.content.res.TypedArray r4 = r0.obtainAttributes(r2, r4)
            java.lang.String r8 = r4.getString(r10)
            int r10 = r4.getResourceId(r7, r10)
            if (r10 == 0) goto L_0x014b
            android.content.Context r11 = r1.d
            if (r11 == 0) goto L_0x0140
            int r12 = android.os.Build.VERSION.SDK_INT
            r13 = 24
            if (r12 < r13) goto L_0x008a
            android.animation.Animator r10 = android.animation.AnimatorInflater.loadAnimator(r11, r10)
            goto L_0x00b4
        L_0x008a:
            android.content.res.Resources r12 = r11.getResources()
            android.content.res.Resources$Theme r13 = r11.getTheme()
            java.lang.String r15 = "Can't load animation resource ID #0x"
            android.content.res.XmlResourceParser r19 = r12.getAnimation(r10)     // Catch:{ XmlPullParserException -> 0x011a, IOException -> 0x00fc }
            android.util.AttributeSet r16 = android.util.Xml.asAttributeSet(r19)     // Catch:{ XmlPullParserException -> 0x00f5, IOException -> 0x00f0, all -> 0x00ee }
            r17 = 0
            r18 = 0
            r20 = 1065353216(0x3f800000, float:1.0)
            r14 = r19
            r7 = r15
            r15 = r16
            r16 = r17
            r17 = r18
            r18 = r20
            android.animation.Animator r10 = i.b.k.ResourcesFlusher.a(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ XmlPullParserException -> 0x00ec, IOException -> 0x00ea, all -> 0x00ee }
            r19.close()
        L_0x00b4:
            i.x.a.a.AnimatedVectorDrawableCompat$b r7 = r1.c
            i.x.a.a.VectorDrawableCompat r7 = r7.b
            i.x.a.a.VectorDrawableCompat$h r7 = r7.c
            i.x.a.a.VectorDrawableCompat$g r7 = r7.b
            i.e.ArrayMap<java.lang.String, java.lang.Object> r7 = r7.f1515p
            java.lang.Object r7 = r7.getOrDefault(r8, r9)
            r10.setTarget(r7)
            i.x.a.a.AnimatedVectorDrawableCompat$b r7 = r1.c
            java.util.ArrayList<android.animation.Animator> r9 = r7.d
            if (r9 != 0) goto L_0x00db
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            r7.d = r9
            i.x.a.a.AnimatedVectorDrawableCompat$b r7 = r1.c
            i.e.ArrayMap r9 = new i.e.ArrayMap
            r9.<init>()
            r7.f1482e = r9
        L_0x00db:
            i.x.a.a.AnimatedVectorDrawableCompat$b r7 = r1.c
            java.util.ArrayList<android.animation.Animator> r7 = r7.d
            r7.add(r10)
            i.x.a.a.AnimatedVectorDrawableCompat$b r7 = r1.c
            i.e.ArrayMap<android.animation.Animator, java.lang.String> r7 = r7.f1482e
            r7.put(r10, r8)
            goto L_0x014b
        L_0x00ea:
            r0 = move-exception
            goto L_0x00f2
        L_0x00ec:
            r0 = move-exception
            goto L_0x00f7
        L_0x00ee:
            r0 = move-exception
            goto L_0x013a
        L_0x00f0:
            r0 = move-exception
            r7 = r15
        L_0x00f2:
            r9 = r19
            goto L_0x00fe
        L_0x00f5:
            r0 = move-exception
            r7 = r15
        L_0x00f7:
            r9 = r19
            goto L_0x011c
        L_0x00fa:
            r0 = move-exception
            goto L_0x0138
        L_0x00fc:
            r0 = move-exception
            r7 = r15
        L_0x00fe:
            android.content.res.Resources$NotFoundException r2 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x00fa }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fa }
            r3.<init>()     // Catch:{ all -> 0x00fa }
            r3.append(r7)     // Catch:{ all -> 0x00fa }
            java.lang.String r4 = java.lang.Integer.toHexString(r10)     // Catch:{ all -> 0x00fa }
            r3.append(r4)     // Catch:{ all -> 0x00fa }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00fa }
            r2.<init>(r3)     // Catch:{ all -> 0x00fa }
            r2.initCause(r0)     // Catch:{ all -> 0x00fa }
            throw r2     // Catch:{ all -> 0x00fa }
        L_0x011a:
            r0 = move-exception
            r7 = r15
        L_0x011c:
            android.content.res.Resources$NotFoundException r2 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x00fa }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fa }
            r3.<init>()     // Catch:{ all -> 0x00fa }
            r3.append(r7)     // Catch:{ all -> 0x00fa }
            java.lang.String r4 = java.lang.Integer.toHexString(r10)     // Catch:{ all -> 0x00fa }
            r3.append(r4)     // Catch:{ all -> 0x00fa }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00fa }
            r2.<init>(r3)     // Catch:{ all -> 0x00fa }
            r2.initCause(r0)     // Catch:{ all -> 0x00fa }
            throw r2     // Catch:{ all -> 0x00fa }
        L_0x0138:
            r19 = r9
        L_0x013a:
            if (r19 == 0) goto L_0x013f
            r19.close()
        L_0x013f:
            throw r0
        L_0x0140:
            r4.recycle()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "Context can't be null when inflating animators"
            r0.<init>(r2)
            throw r0
        L_0x014b:
            r4.recycle()
        L_0x014e:
            int r4 = r23.next()
            r7 = 1
            goto L_0x001e
        L_0x0155:
            i.x.a.a.AnimatedVectorDrawableCompat$b r0 = r1.c
            android.animation.AnimatorSet r2 = r0.c
            if (r2 != 0) goto L_0x0162
            android.animation.AnimatorSet r2 = new android.animation.AnimatorSet
            r2.<init>()
            r0.c = r2
        L_0x0162:
            android.animation.AnimatorSet r2 = r0.c
            java.util.ArrayList<android.animation.Animator> r0 = r0.d
            r2.playTogether(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.x.a.a.AnimatedVectorDrawableCompat.inflate(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):void");
    }

    public boolean isAutoMirrored() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.isAutoMirrored();
        }
        return this.c.b.isAutoMirrored();
    }

    public boolean isRunning() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.c.c.isRunning();
    }

    public boolean isStateful() {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.c.b.isStateful();
    }

    public Drawable mutate() {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    public void onBoundsChange(Rect rect) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.c.b.setBounds(rect);
        }
    }

    public boolean onLevelChange(int i2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        return this.c.b.setLevel(i2);
    }

    public boolean onStateChange(int[] iArr) {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.c.b.setState(iArr);
    }

    public void setAlpha(int i2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else {
            this.c.b.setAlpha(i2);
        }
    }

    public void setAutoMirrored(boolean z) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setAutoMirrored(z);
            return;
        }
        VectorDrawableCompat vectorDrawableCompat = this.c.b;
        Drawable drawable2 = super.b;
        if (drawable2 != null) {
            drawable2.setAutoMirrored(z);
        } else {
            vectorDrawableCompat.c.f1516e = z;
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = super.b;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        VectorDrawableCompat vectorDrawableCompat = this.c.b;
        Drawable drawable2 = super.b;
        if (drawable2 != null) {
            drawable2.setColorFilter(colorFilter);
            return;
        }
        vectorDrawableCompat.f1484e = colorFilter;
        vectorDrawableCompat.invalidateSelf();
    }

    public void setTint(int i2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.b(drawable, i2);
        } else {
            this.c.b.setTint(i2);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.a(drawable, colorStateList);
        } else {
            this.c.b.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = super.b;
        if (drawable != null) {
            ResourcesFlusher.a(drawable, mode);
        } else {
            this.c.b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = super.b;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.c.b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public void start() {
        Drawable drawable = super.b;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.c.c.isStarted()) {
            this.c.c.start();
            invalidateSelf();
        }
    }

    public void stop() {
        Drawable drawable = super.b;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.c.c.end();
        }
    }

    public AnimatedVectorDrawableCompat(Context context, b bVar, Resources resources) {
        this.f1480e = null;
        this.f1481f = new a();
        this.d = context;
        if (bVar != null) {
            this.c = bVar;
        } else {
            this.c = new b(bVar, this.f1481f, resources);
        }
    }

    public static class c extends Drawable.ConstantState {
        public final Drawable.ConstantState a;

        public c(Drawable.ConstantState constantState) {
            this.a = super;
        }

        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        public Drawable newDrawable() {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(null, null, null);
            Drawable newDrawable = this.a.newDrawable();
            animatedVectorDrawableCompat.b = newDrawable;
            newDrawable.setCallback(animatedVectorDrawableCompat.f1481f);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(null, null, null);
            Drawable newDrawable = this.a.newDrawable(resources);
            animatedVectorDrawableCompat.b = newDrawable;
            newDrawable.setCallback(animatedVectorDrawableCompat.f1481f);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(null, null, null);
            Drawable newDrawable = this.a.newDrawable(resources, theme);
            animatedVectorDrawableCompat.b = newDrawable;
            newDrawable.setCallback(animatedVectorDrawableCompat.f1481f);
            return animatedVectorDrawableCompat;
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        inflate(resources, xmlPullParser, attributeSet, null);
    }
}
