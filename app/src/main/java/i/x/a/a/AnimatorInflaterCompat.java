package i.x.a.a;

import android.animation.TypeEvaluator;
import i.b.k.ResourcesFlusher;
import i.h.f.PathParser;
import i.h.f.b;

public class AnimatorInflaterCompat implements TypeEvaluator<b[]> {
    public PathParser[] a;

    public Object evaluate(float f2, Object obj, Object obj2) {
        PathParser[] pathParserArr = (PathParser[]) obj;
        PathParser[] pathParserArr2 = (PathParser[]) obj2;
        if (ResourcesFlusher.a(pathParserArr, pathParserArr2)) {
            if (!ResourcesFlusher.a(this.a, pathParserArr)) {
                this.a = ResourcesFlusher.a(pathParserArr);
            }
            int i2 = 0;
            while (i2 < pathParserArr.length) {
                PathParser pathParser = this.a[i2];
                PathParser pathParser2 = pathParserArr[i2];
                PathParser pathParser3 = pathParserArr2[i2];
                if (pathParser != null) {
                    pathParser.a = pathParser2.a;
                    int i3 = 0;
                    while (true) {
                        float[] fArr = pathParser2.b;
                        if (i3 >= fArr.length) {
                            break;
                        }
                        pathParser.b[i3] = (pathParser3.b[i3] * f2) + ((1.0f - f2) * fArr[i3]);
                        i3++;
                    }
                    i2++;
                } else {
                    throw null;
                }
            }
            return this.a;
        }
        throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
    }
}
