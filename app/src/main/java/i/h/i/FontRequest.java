package i.h.i;

import android.util.Base64;
import j.a.a.a.outline;
import java.util.List;

public final class FontRequest {
    public final String a;
    public final String b;
    public final String c;
    public final List<List<byte[]>> d;

    /* renamed from: e  reason: collision with root package name */
    public final int f1188e;

    /* renamed from: f  reason: collision with root package name */
    public final String f1189f;

    public FontRequest(String str, String str2, String str3, List<List<byte[]>> list) {
        if (str != null) {
            this.a = str;
            if (str2 != null) {
                this.b = str2;
                if (str3 != null) {
                    this.c = str3;
                    if (list != null) {
                        this.d = list;
                        this.f1188e = 0;
                        this.f1189f = str + "-" + this.b + "-" + this.c;
                        return;
                    }
                    throw null;
                }
                throw null;
            }
            throw null;
        }
        throw null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder a2 = outline.a("FontRequest {mProviderAuthority: ");
        a2.append(this.a);
        a2.append(", mProviderPackage: ");
        a2.append(this.b);
        a2.append(", mQuery: ");
        a2.append(this.c);
        a2.append(", mCertificates:");
        sb.append(a2.toString());
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            sb.append(" [");
            List list = this.d.get(i2);
            for (int i3 = 0; i3 < list.size(); i3++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[]) list.get(i3), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.f1188e);
        return sb.toString();
    }
}
