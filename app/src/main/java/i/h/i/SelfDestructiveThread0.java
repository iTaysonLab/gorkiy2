package i.h.i;

import android.os.Handler;
import i.h.i.SelfDestructiveThread;
import java.util.concurrent.Callable;

/* compiled from: SelfDestructiveThread */
public class SelfDestructiveThread0 implements Runnable {
    public final /* synthetic */ Callable b;
    public final /* synthetic */ Handler c;
    public final /* synthetic */ SelfDestructiveThread.c d;

    /* compiled from: SelfDestructiveThread */
    public class a implements Runnable {
        public final /* synthetic */ Object b;

        public a(Object obj) {
            this.b = obj;
        }

        public void run() {
            SelfDestructiveThread0.this.d.a(this.b);
        }
    }

    public SelfDestructiveThread0(SelfDestructiveThread selfDestructiveThread, Callable callable, Handler handler, SelfDestructiveThread.c cVar) {
        this.b = callable;
        this.c = handler;
        this.d = cVar;
    }

    public void run() {
        Object obj;
        try {
            obj = this.b.call();
        } catch (Exception unused) {
            obj = null;
        }
        this.c.post(new a(obj));
    }
}
