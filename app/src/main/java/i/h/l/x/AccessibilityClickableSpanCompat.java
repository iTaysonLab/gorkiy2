package i.h.l.x;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;

public final class AccessibilityClickableSpanCompat extends ClickableSpan {
    public final int b;
    public final AccessibilityNodeInfoCompat c;
    public final int d;

    public AccessibilityClickableSpanCompat(int i2, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, int i3) {
        this.b = i2;
        this.c = accessibilityNodeInfoCompat;
        this.d = i3;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.b);
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = this.c;
        accessibilityNodeInfoCompat.a.performAction(this.d, bundle);
    }
}
