package i.a;

public interface Cancellable {
    void cancel();
}
