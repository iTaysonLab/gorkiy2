package i.a;

import java.util.concurrent.CopyOnWriteArrayList;

public abstract class OnBackPressedCallback {
    public boolean a;
    public CopyOnWriteArrayList<a> b = new CopyOnWriteArrayList<>();

    public OnBackPressedCallback(boolean z) {
        this.a = z;
    }
}
