package i.b.p.i;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.crashlytics.android.core.CodedOutputStream;
import i.b.k.ResourcesFlusher;
import i.h.e.ContextCompat;
import i.h.g.a.SupportMenuItem;
import i.h.l.ActionProvider;

public class ActionMenuItem implements SupportMenuItem {
    public final int a;
    public final int b;
    public final int c;
    public CharSequence d;

    /* renamed from: e  reason: collision with root package name */
    public CharSequence f845e;

    /* renamed from: f  reason: collision with root package name */
    public Intent f846f;
    public char g;
    public int h = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: i  reason: collision with root package name */
    public char f847i;

    /* renamed from: j  reason: collision with root package name */
    public int f848j = CodedOutputStream.DEFAULT_BUFFER_SIZE;

    /* renamed from: k  reason: collision with root package name */
    public Drawable f849k;

    /* renamed from: l  reason: collision with root package name */
    public Context f850l;

    /* renamed from: m  reason: collision with root package name */
    public CharSequence f851m;

    /* renamed from: n  reason: collision with root package name */
    public CharSequence f852n;

    /* renamed from: o  reason: collision with root package name */
    public ColorStateList f853o = null;

    /* renamed from: p  reason: collision with root package name */
    public PorterDuff.Mode f854p = null;

    /* renamed from: q  reason: collision with root package name */
    public boolean f855q = false;

    /* renamed from: r  reason: collision with root package name */
    public boolean f856r = false;

    /* renamed from: s  reason: collision with root package name */
    public int f857s = 16;

    public ActionMenuItem(Context context, int i2, int i3, int i4, int i5, CharSequence charSequence) {
        this.f850l = context;
        this.a = i3;
        this.b = i2;
        this.c = i5;
        this.d = charSequence;
    }

    public SupportMenuItem a(ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public ActionProvider a() {
        return null;
    }

    public final void b() {
        if (this.f849k == null) {
            return;
        }
        if (this.f855q || this.f856r) {
            Drawable d2 = ResourcesFlusher.d(this.f849k);
            this.f849k = d2;
            Drawable mutate = d2.mutate();
            this.f849k = mutate;
            if (this.f855q) {
                mutate.setTintList(this.f853o);
            }
            if (this.f856r) {
                this.f849k.setTintMode(this.f854p);
            }
        }
    }

    public boolean collapseActionView() {
        return false;
    }

    public boolean expandActionView() {
        return false;
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException();
    }

    public View getActionView() {
        return null;
    }

    public int getAlphabeticModifiers() {
        return this.f848j;
    }

    public char getAlphabeticShortcut() {
        return this.f847i;
    }

    public CharSequence getContentDescription() {
        return this.f851m;
    }

    public int getGroupId() {
        return this.b;
    }

    public Drawable getIcon() {
        return this.f849k;
    }

    public ColorStateList getIconTintList() {
        return this.f853o;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.f854p;
    }

    public Intent getIntent() {
        return this.f846f;
    }

    public int getItemId() {
        return this.a;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    public int getNumericModifiers() {
        return this.h;
    }

    public char getNumericShortcut() {
        return this.g;
    }

    public int getOrder() {
        return this.c;
    }

    public SubMenu getSubMenu() {
        return null;
    }

    public CharSequence getTitle() {
        return this.d;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f845e;
        return charSequence != null ? charSequence : this.d;
    }

    public CharSequence getTooltipText() {
        return this.f852n;
    }

    public boolean hasSubMenu() {
        return false;
    }

    public boolean isActionViewExpanded() {
        return false;
    }

    public boolean isCheckable() {
        return (this.f857s & 1) != 0;
    }

    public boolean isChecked() {
        return (this.f857s & 2) != 0;
    }

    public boolean isEnabled() {
        return (this.f857s & 16) != 0;
    }

    public boolean isVisible() {
        return (this.f857s & 8) == 0;
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setActionView(View view) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        this.f847i = Character.toLowerCase(c2);
        return this;
    }

    public MenuItem setCheckable(boolean z) {
        this.f857s = z | (this.f857s & true) ? 1 : 0;
        return this;
    }

    public MenuItem setChecked(boolean z) {
        this.f857s = (z ? 2 : 0) | (this.f857s & -3);
        return this;
    }

    public MenuItem setContentDescription(CharSequence charSequence) {
        this.f851m = charSequence;
        return this;
    }

    public MenuItem setEnabled(boolean z) {
        this.f857s = (z ? 16 : 0) | (this.f857s & -17);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.f849k = drawable;
        b();
        return this;
    }

    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.f853o = colorStateList;
        this.f855q = true;
        b();
        return this;
    }

    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.f854p = mode;
        this.f856r = true;
        b();
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.f846f = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        this.g = c2;
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.g = c2;
        this.f847i = Character.toLowerCase(c3);
        return this;
    }

    public void setShowAsAction(int i2) {
    }

    public MenuItem setShowAsActionFlags(int i2) {
        return this;
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.d = charSequence;
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f845e = charSequence;
        return this;
    }

    public MenuItem setTooltipText(CharSequence charSequence) {
        this.f852n = charSequence;
        return this;
    }

    public MenuItem setVisible(boolean z) {
        int i2 = 8;
        int i3 = this.f857s & 8;
        if (z) {
            i2 = 0;
        }
        this.f857s = i3 | i2;
        return this;
    }

    public MenuItem setActionView(int i2) {
        throw new UnsupportedOperationException();
    }

    public MenuItem setAlphabeticShortcut(char c2, int i2) {
        this.f847i = Character.toLowerCase(c2);
        this.f848j = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    /* renamed from: setContentDescription  reason: collision with other method in class */
    public SupportMenuItem m6setContentDescription(CharSequence charSequence) {
        this.f851m = charSequence;
        return this;
    }

    public MenuItem setNumericShortcut(char c2, int i2) {
        this.g = c2;
        this.h = KeyEvent.normalizeMetaState(i2);
        return this;
    }

    public MenuItem setTitle(int i2) {
        this.d = this.f850l.getResources().getString(i2);
        return this;
    }

    /* renamed from: setTooltipText  reason: collision with other method in class */
    public SupportMenuItem m7setTooltipText(CharSequence charSequence) {
        this.f852n = charSequence;
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.f849k = ContextCompat.c(this.f850l, i2);
        b();
        return this;
    }

    public MenuItem setShortcut(char c2, char c3, int i2, int i3) {
        this.g = c2;
        this.h = KeyEvent.normalizeMetaState(i2);
        this.f847i = Character.toLowerCase(c3);
        this.f848j = KeyEvent.normalizeMetaState(i3);
        return this;
    }
}
