package i.b.q;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import i.b.k.ResourcesFlusher;
import i.b.m.a.AnimatedStateListDrawableCompat;
import i.b.q.AppCompatDrawableManager;
import i.b.q.l0;
import i.e.ArrayMap;
import i.e.LongSparseArray;
import i.e.LruCache;
import i.e.SparseArrayCompat;
import i.x.a.a.AnimatedVectorDrawableCompat;
import i.x.a.a.VectorDrawableCompat;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import org.xmlpull.v1.XmlPullParser;

public final class ResourceManagerInternal {
    public static final PorterDuff.Mode h = PorterDuff.Mode.SRC_IN;

    /* renamed from: i  reason: collision with root package name */
    public static ResourceManagerInternal f1011i;

    /* renamed from: j  reason: collision with root package name */
    public static final c f1012j = new c(6);
    public WeakHashMap<Context, SparseArrayCompat<ColorStateList>> a;
    public ArrayMap<String, l0.d> b;
    public SparseArrayCompat<String> c;
    public final WeakHashMap<Context, LongSparseArray<WeakReference<Drawable.ConstantState>>> d = new WeakHashMap<>(0);

    /* renamed from: e  reason: collision with root package name */
    public TypedValue f1013e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1014f;
    public e g;

    public static class a implements d {
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return AnimatedStateListDrawableCompat.a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e2) {
                Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e2);
                return null;
            }
        }
    }

    public static class b implements d {
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                Resources resources = context.getResources();
                AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context, null, null);
                animatedVectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
                return animatedVectorDrawableCompat;
            } catch (Exception e2) {
                Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e2);
                return null;
            }
        }
    }

    public static class c extends LruCache<Integer, PorterDuffColorFilter> {
        public c(int i2) {
            super(i2);
        }
    }

    public interface d {
        Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme);
    }

    public interface e {
    }

    public static class f implements d {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.x.a.a.VectorDrawableCompat.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):i.x.a.a.VectorDrawableCompat
         arg types: [android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme]
         candidates:
          ClspMth{android.graphics.drawable.Drawable.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.graphics.drawable.Drawable throws java.io.IOException, org.xmlpull.v1.XmlPullParserException}
          i.x.a.a.VectorDrawableCompat.createFromXmlInner(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):i.x.a.a.VectorDrawableCompat */
        public Drawable a(Context context, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
            try {
                return VectorDrawableCompat.createFromXmlInner(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (Exception e2) {
                Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e2);
                return null;
            }
        }
    }

    public static synchronized ResourceManagerInternal a() {
        ResourceManagerInternal resourceManagerInternal;
        synchronized (ResourceManagerInternal.class) {
            if (f1011i == null) {
                ResourceManagerInternal resourceManagerInternal2 = new ResourceManagerInternal();
                f1011i = resourceManagerInternal2;
                if (Build.VERSION.SDK_INT < 24) {
                    resourceManagerInternal2.a("vector", new f());
                    resourceManagerInternal2.a("animated-vector", new b());
                    resourceManagerInternal2.a("animated-selector", new a());
                }
            }
            resourceManagerInternal = f1011i;
        }
        return resourceManagerInternal;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.ResourceManagerInternal.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      i.b.q.ResourceManagerInternal.a(android.graphics.drawable.Drawable, i.b.q.TintInfo, int[]):void
      i.b.q.ResourceManagerInternal.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      i.b.q.ResourceManagerInternal.a(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      i.b.q.ResourceManagerInternal.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public synchronized Drawable b(Context context, int i2) {
        return a(context, i2, false);
    }

    public synchronized ColorStateList c(Context context, int i2) {
        ColorStateList colorStateList;
        SparseArrayCompat sparseArrayCompat;
        WeakHashMap<Context, SparseArrayCompat<ColorStateList>> weakHashMap = this.a;
        ColorStateList colorStateList2 = null;
        colorStateList = (weakHashMap == null || (sparseArrayCompat = weakHashMap.get(context)) == null) ? null : (ColorStateList) sparseArrayCompat.b(i2, null);
        if (colorStateList == null) {
            if (this.g != null) {
                colorStateList2 = ((AppCompatDrawableManager.a) this.g).b(context, i2);
            }
            if (colorStateList2 != null) {
                if (this.a == null) {
                    this.a = new WeakHashMap<>();
                }
                SparseArrayCompat sparseArrayCompat2 = this.a.get(context);
                if (sparseArrayCompat2 == null) {
                    sparseArrayCompat2 = new SparseArrayCompat(10);
                    this.a.put(context, sparseArrayCompat2);
                }
                sparseArrayCompat2.a(i2, colorStateList2);
            }
            colorStateList = colorStateList2;
        }
        return colorStateList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007b A[Catch:{ Exception -> 0x00a9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a1 A[Catch:{ Exception -> 0x00a9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable d(android.content.Context r11, int r12) {
        /*
            r10 = this;
            i.e.ArrayMap<java.lang.String, i.b.q.l0$d> r0 = r10.b
            r1 = 0
            if (r0 == 0) goto L_0x00b9
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x00b9
            i.e.SparseArrayCompat<java.lang.String> r0 = r10.c
            java.lang.String r2 = "appcompat_skip_skip"
            if (r0 == 0) goto L_0x0028
            java.lang.Object r0 = r0.b(r12, r1)
            java.lang.String r0 = (java.lang.String) r0
            boolean r3 = r2.equals(r0)
            if (r3 != 0) goto L_0x0027
            if (r0 == 0) goto L_0x0031
            i.e.ArrayMap<java.lang.String, i.b.q.l0$d> r3 = r10.b
            java.lang.Object r0 = r3.getOrDefault(r0, r1)
            if (r0 != 0) goto L_0x0031
        L_0x0027:
            return r1
        L_0x0028:
            i.e.SparseArrayCompat r0 = new i.e.SparseArrayCompat
            r1 = 10
            r0.<init>(r1)
            r10.c = r0
        L_0x0031:
            android.util.TypedValue r0 = r10.f1013e
            if (r0 != 0) goto L_0x003c
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
            r10.f1013e = r0
        L_0x003c:
            android.util.TypedValue r0 = r10.f1013e
            android.content.res.Resources r1 = r11.getResources()
            r3 = 1
            r1.getValue(r12, r0, r3)
            int r4 = r0.assetCookie
            long r4 = (long) r4
            r6 = 32
            long r4 = r4 << r6
            int r6 = r0.data
            long r6 = (long) r6
            long r4 = r4 | r6
            android.graphics.drawable.Drawable r6 = r10.a(r11, r4)
            if (r6 == 0) goto L_0x0057
            return r6
        L_0x0057:
            java.lang.CharSequence r7 = r0.string
            if (r7 == 0) goto L_0x00b1
            java.lang.String r7 = r7.toString()
            java.lang.String r8 = ".xml"
            boolean r7 = r7.endsWith(r8)
            if (r7 == 0) goto L_0x00b1
            android.content.res.XmlResourceParser r1 = r1.getXml(r12)     // Catch:{ Exception -> 0x00a9 }
            android.util.AttributeSet r7 = android.util.Xml.asAttributeSet(r1)     // Catch:{ Exception -> 0x00a9 }
        L_0x006f:
            int r8 = r1.next()     // Catch:{ Exception -> 0x00a9 }
            r9 = 2
            if (r8 == r9) goto L_0x0079
            if (r8 == r3) goto L_0x0079
            goto L_0x006f
        L_0x0079:
            if (r8 != r9) goto L_0x00a1
            java.lang.String r3 = r1.getName()     // Catch:{ Exception -> 0x00a9 }
            i.e.SparseArrayCompat<java.lang.String> r8 = r10.c     // Catch:{ Exception -> 0x00a9 }
            r8.a(r12, r3)     // Catch:{ Exception -> 0x00a9 }
            i.e.ArrayMap<java.lang.String, i.b.q.l0$d> r8 = r10.b     // Catch:{ Exception -> 0x00a9 }
            java.lang.Object r3 = r8.get(r3)     // Catch:{ Exception -> 0x00a9 }
            i.b.q.ResourceManagerInternal$d r3 = (i.b.q.ResourceManagerInternal.d) r3     // Catch:{ Exception -> 0x00a9 }
            if (r3 == 0) goto L_0x0096
            android.content.res.Resources$Theme r8 = r11.getTheme()     // Catch:{ Exception -> 0x00a9 }
            android.graphics.drawable.Drawable r6 = r3.a(r11, r1, r7, r8)     // Catch:{ Exception -> 0x00a9 }
        L_0x0096:
            if (r6 == 0) goto L_0x00b1
            int r0 = r0.changingConfigurations     // Catch:{ Exception -> 0x00a9 }
            r6.setChangingConfigurations(r0)     // Catch:{ Exception -> 0x00a9 }
            r10.a(r11, r4, r6)     // Catch:{ Exception -> 0x00a9 }
            goto L_0x00b1
        L_0x00a1:
            org.xmlpull.v1.XmlPullParserException r11 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r0 = "No start tag found"
            r11.<init>(r0)     // Catch:{ Exception -> 0x00a9 }
            throw r11     // Catch:{ Exception -> 0x00a9 }
        L_0x00a9:
            r11 = move-exception
            java.lang.String r0 = "ResourceManagerInternal"
            java.lang.String r1 = "Exception while inflating drawable"
            android.util.Log.e(r0, r1, r11)
        L_0x00b1:
            if (r6 != 0) goto L_0x00b8
            i.e.SparseArrayCompat<java.lang.String> r11 = r10.c
            r11.a(r12, r2)
        L_0x00b8:
            return r6
        L_0x00b9:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.ResourceManagerInternal.d(android.content.Context, int):android.graphics.drawable.Drawable");
    }

    public synchronized void a(e eVar) {
        this.g = eVar;
    }

    public synchronized void a(Context context) {
        LongSparseArray longSparseArray = this.d.get(context);
        if (longSparseArray != null) {
            longSparseArray.b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public final Drawable a(Context context, int i2) {
        if (this.f1013e == null) {
            this.f1013e = new TypedValue();
        }
        TypedValue typedValue = this.f1013e;
        context.getResources().getValue(i2, typedValue, true);
        long j2 = (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
        Drawable a2 = a(context, j2);
        if (a2 != null) {
            return a2;
        }
        e eVar = this.g;
        LayerDrawable layerDrawable = null;
        if (eVar != null) {
            AppCompatDrawableManager.a aVar = (AppCompatDrawableManager.a) eVar;
            if (i2 == i.b.e.abc_cab_background_top_material) {
                layerDrawable = new LayerDrawable(new Drawable[]{b(context, i.b.e.abc_cab_background_internal_bg), b(context, i.b.e.abc_cab_background_top_mtrl_alpha)});
            }
        }
        if (layerDrawable != null) {
            layerDrawable.setChangingConfigurations(typedValue.changingConfigurations);
            a(context, j2, layerDrawable);
        }
        return layerDrawable;
    }

    public final Drawable a(Context context, int i2, boolean z, Drawable drawable) {
        ColorStateList c2 = c(context, i2);
        PorterDuff.Mode mode = null;
        if (c2 != null) {
            if (DrawableUtils.a(drawable)) {
                drawable = drawable.mutate();
            }
            Drawable d2 = ResourcesFlusher.d(drawable);
            d2.setTintList(c2);
            e eVar = this.g;
            if (eVar != null) {
                AppCompatDrawableManager.a aVar = (AppCompatDrawableManager.a) eVar;
                if (i2 == i.b.e.abc_switch_thumb_material) {
                    mode = PorterDuff.Mode.MULTIPLY;
                }
            }
            if (mode == null) {
                return d2;
            }
            d2.setTintMode(mode);
            return d2;
        }
        e eVar2 = this.g;
        if (eVar2 != null) {
            AppCompatDrawableManager.a aVar2 = (AppCompatDrawableManager.a) eVar2;
            if (aVar2 != null) {
                boolean z2 = true;
                if (i2 == i.b.e.abc_seekbar_track_material) {
                    LayerDrawable layerDrawable = (LayerDrawable) drawable;
                    aVar2.a(layerDrawable.findDrawableByLayerId(16908288), ThemeUtils.b(context, i.b.a.colorControlNormal), AppCompatDrawableManager.b);
                    aVar2.a(layerDrawable.findDrawableByLayerId(16908303), ThemeUtils.b(context, i.b.a.colorControlNormal), AppCompatDrawableManager.b);
                    aVar2.a(layerDrawable.findDrawableByLayerId(16908301), ThemeUtils.b(context, i.b.a.colorControlActivated), AppCompatDrawableManager.b);
                } else if (i2 == i.b.e.abc_ratingbar_material || i2 == i.b.e.abc_ratingbar_indicator_material || i2 == i.b.e.abc_ratingbar_small_material) {
                    LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                    aVar2.a(layerDrawable2.findDrawableByLayerId(16908288), ThemeUtils.a(context, i.b.a.colorControlNormal), AppCompatDrawableManager.b);
                    aVar2.a(layerDrawable2.findDrawableByLayerId(16908303), ThemeUtils.b(context, i.b.a.colorControlActivated), AppCompatDrawableManager.b);
                    aVar2.a(layerDrawable2.findDrawableByLayerId(16908301), ThemeUtils.b(context, i.b.a.colorControlActivated), AppCompatDrawableManager.b);
                } else {
                    z2 = false;
                }
                if (z2) {
                    return drawable;
                }
            } else {
                throw null;
            }
        }
        if (a(context, i2, drawable) || !z) {
            return drawable;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized android.graphics.drawable.Drawable a(android.content.Context r4, long r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.WeakHashMap<android.content.Context, i.e.LongSparseArray<java.lang.ref.WeakReference<android.graphics.drawable.Drawable$ConstantState>>> r0 = r3.d     // Catch:{ all -> 0x0041 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0041 }
            i.e.LongSparseArray r0 = (i.e.LongSparseArray) r0     // Catch:{ all -> 0x0041 }
            r1 = 0
            if (r0 != 0) goto L_0x000e
            monitor-exit(r3)
            return r1
        L_0x000e:
            java.lang.Object r2 = r0.b(r5, r1)     // Catch:{ all -> 0x0041 }
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x003f
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0041 }
            android.graphics.drawable.Drawable$ConstantState r2 = (android.graphics.drawable.Drawable.ConstantState) r2     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0028
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ all -> 0x0041 }
            android.graphics.drawable.Drawable r4 = r2.newDrawable(r4)     // Catch:{ all -> 0x0041 }
            monitor-exit(r3)
            return r4
        L_0x0028:
            long[] r4 = r0.c     // Catch:{ all -> 0x0041 }
            int r2 = r0.f1064e     // Catch:{ all -> 0x0041 }
            int r4 = i.e.ContainerHelpers.a(r4, r2, r5)     // Catch:{ all -> 0x0041 }
            if (r4 < 0) goto L_0x003f
            java.lang.Object[] r5 = r0.d     // Catch:{ all -> 0x0041 }
            r6 = r5[r4]     // Catch:{ all -> 0x0041 }
            java.lang.Object r2 = i.e.LongSparseArray.f1063f     // Catch:{ all -> 0x0041 }
            if (r6 == r2) goto L_0x003f
            r5[r4] = r2     // Catch:{ all -> 0x0041 }
            r4 = 1
            r0.b = r4     // Catch:{ all -> 0x0041 }
        L_0x003f:
            monitor-exit(r3)
            return r1
        L_0x0041:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.ResourceManagerInternal.a(android.content.Context, long):android.graphics.drawable.Drawable");
    }

    public final synchronized boolean a(Context context, long j2, Drawable drawable) {
        Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        LongSparseArray longSparseArray = this.d.get(context);
        if (longSparseArray == null) {
            longSparseArray = new LongSparseArray();
            this.d.put(context, longSparseArray);
        }
        longSparseArray.c(j2, new WeakReference(constantState));
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.content.Context r8, int r9, android.graphics.drawable.Drawable r10) {
        /*
            r7 = this;
            i.b.q.ResourceManagerInternal$e r0 = r7.g
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x006f
            i.b.q.AppCompatDrawableManager$a r0 = (i.b.q.AppCompatDrawableManager.a) r0
            if (r0 == 0) goto L_0x006d
            android.graphics.PorterDuff$Mode r3 = i.b.q.AppCompatDrawableManager.b
            int[] r4 = r0.a
            boolean r4 = r0.a(r4, r9)
            r5 = 16842801(0x1010031, float:2.3693695E-38)
            r6 = -1
            if (r4 == 0) goto L_0x001b
            int r5 = i.b.a.colorControlNormal
            goto L_0x0044
        L_0x001b:
            int[] r4 = r0.c
            boolean r4 = r0.a(r4, r9)
            if (r4 == 0) goto L_0x0026
            int r5 = i.b.a.colorControlActivated
            goto L_0x0044
        L_0x0026:
            int[] r4 = r0.d
            boolean r0 = r0.a(r4, r9)
            if (r0 == 0) goto L_0x0031
            android.graphics.PorterDuff$Mode r3 = android.graphics.PorterDuff.Mode.MULTIPLY
            goto L_0x0044
        L_0x0031:
            int r0 = i.b.e.abc_list_divider_mtrl_alpha
            if (r9 != r0) goto L_0x0040
            r9 = 16842800(0x1010030, float:2.3693693E-38)
            r0 = 1109603123(0x42233333, float:40.8)
            int r0 = java.lang.Math.round(r0)
            goto L_0x0046
        L_0x0040:
            int r0 = i.b.e.abc_dialog_material_background
            if (r9 != r0) goto L_0x0048
        L_0x0044:
            r9 = r5
            r0 = -1
        L_0x0046:
            r4 = 1
            goto L_0x004b
        L_0x0048:
            r9 = 0
            r0 = -1
            r4 = 0
        L_0x004b:
            if (r4 == 0) goto L_0x0069
            boolean r4 = i.b.q.DrawableUtils.a(r10)
            if (r4 == 0) goto L_0x0057
            android.graphics.drawable.Drawable r10 = r10.mutate()
        L_0x0057:
            int r8 = i.b.q.ThemeUtils.b(r8, r9)
            android.graphics.PorterDuffColorFilter r8 = i.b.q.AppCompatDrawableManager.a(r8, r3)
            r10.setColorFilter(r8)
            if (r0 == r6) goto L_0x0067
            r10.setAlpha(r0)
        L_0x0067:
            r8 = 1
            goto L_0x006a
        L_0x0069:
            r8 = 0
        L_0x006a:
            if (r8 == 0) goto L_0x006f
            goto L_0x0070
        L_0x006d:
            r8 = 0
            throw r8
        L_0x006f:
            r1 = 0
        L_0x0070:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.ResourceManagerInternal.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean");
    }

    public final void a(String str, d dVar) {
        if (this.b == null) {
            this.b = new ArrayMap<>();
        }
        this.b.put(str, dVar);
    }

    public static void a(Drawable drawable, TintInfo tintInfo, int[] iArr) {
        if (!DrawableUtils.a(drawable) || drawable.mutate() == drawable) {
            if (tintInfo.d || tintInfo.c) {
                PorterDuffColorFilter porterDuffColorFilter = null;
                ColorStateList colorStateList = tintInfo.d ? tintInfo.a : null;
                PorterDuff.Mode mode = tintInfo.c ? tintInfo.b : h;
                if (!(colorStateList == null || mode == null)) {
                    porterDuffColorFilter = a(colorStateList.getColorForState(iArr, 0), mode);
                }
                drawable.setColorFilter(porterDuffColorFilter);
            } else {
                drawable.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        Log.d("ResourceManagerInternal", "Mutated drawable is not the same instance as the input.");
    }

    public static synchronized PorterDuffColorFilter a(int i2, PorterDuff.Mode mode) {
        PorterDuffColorFilter porterDuffColorFilter;
        synchronized (ResourceManagerInternal.class) {
            c cVar = f1012j;
            if (cVar != null) {
                int i3 = (i2 + 31) * 31;
                porterDuffColorFilter = (PorterDuffColorFilter) cVar.a((String) Integer.valueOf(mode.hashCode() + i3));
                if (porterDuffColorFilter == null) {
                    porterDuffColorFilter = new PorterDuffColorFilter(i2, mode);
                    c cVar2 = f1012j;
                    if (cVar2 != null) {
                        PorterDuffColorFilter porterDuffColorFilter2 = (PorterDuffColorFilter) cVar2.a(Integer.valueOf(mode.hashCode() + i3), porterDuffColorFilter);
                    } else {
                        throw null;
                    }
                }
            } else {
                throw null;
            }
        }
        return porterDuffColorFilter;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r0 != false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.graphics.drawable.Drawable a(android.content.Context r5, int r6, boolean r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.f1014f     // Catch:{ all -> 0x0047 }
            if (r0 == 0) goto L_0x0006
            goto L_0x002a
        L_0x0006:
            r0 = 1
            r4.f1014f = r0     // Catch:{ all -> 0x0047 }
            int r1 = i.b.n.a.abc_vector_test     // Catch:{ all -> 0x0047 }
            android.graphics.drawable.Drawable r1 = r4.b(r5, r1)     // Catch:{ all -> 0x0047 }
            r2 = 0
            if (r1 == 0) goto L_0x0049
            boolean r3 = r1 instanceof i.x.a.a.VectorDrawableCompat     // Catch:{ all -> 0x0047 }
            if (r3 != 0) goto L_0x0028
            java.lang.String r3 = "android.graphics.drawable.VectorDrawable"
            java.lang.Class r1 = r1.getClass()     // Catch:{ all -> 0x0047 }
            java.lang.String r1 = r1.getName()     // Catch:{ all -> 0x0047 }
            boolean r1 = r3.equals(r1)     // Catch:{ all -> 0x0047 }
            if (r1 == 0) goto L_0x0027
            goto L_0x0028
        L_0x0027:
            r0 = 0
        L_0x0028:
            if (r0 == 0) goto L_0x0049
        L_0x002a:
            android.graphics.drawable.Drawable r0 = r4.d(r5, r6)     // Catch:{ all -> 0x0047 }
            if (r0 != 0) goto L_0x0034
            android.graphics.drawable.Drawable r0 = r4.a(r5, r6)     // Catch:{ all -> 0x0047 }
        L_0x0034:
            if (r0 != 0) goto L_0x003a
            android.graphics.drawable.Drawable r0 = i.h.e.ContextCompat.c(r5, r6)     // Catch:{ all -> 0x0047 }
        L_0x003a:
            if (r0 == 0) goto L_0x0040
            android.graphics.drawable.Drawable r0 = r4.a(r5, r6, r7, r0)     // Catch:{ all -> 0x0047 }
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            i.b.q.DrawableUtils.b(r0)     // Catch:{ all -> 0x0047 }
        L_0x0045:
            monitor-exit(r4)
            return r0
        L_0x0047:
            r5 = move-exception
            goto L_0x0053
        L_0x0049:
            r4.f1014f = r2     // Catch:{ all -> 0x0047 }
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0047 }
            java.lang.String r6 = "This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat."
            r5.<init>(r6)     // Catch:{ all -> 0x0047 }
            throw r5     // Catch:{ all -> 0x0047 }
        L_0x0053:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.q.ResourceManagerInternal.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable");
    }
}
