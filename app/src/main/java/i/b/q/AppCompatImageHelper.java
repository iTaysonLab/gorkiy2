package i.b.q;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import i.b.j;
import i.b.l.a.AppCompatResources;

public class AppCompatImageHelper {
    public final ImageView a;
    public TintInfo b;
    public TintInfo c;

    public AppCompatImageHelper(ImageView imageView) {
        this.a = imageView;
    }

    public void a(AttributeSet attributeSet, int i2) {
        Drawable drawable;
        Drawable drawable2;
        int f2;
        TintTypedArray a2 = TintTypedArray.a(this.a.getContext(), attributeSet, j.AppCompatImageView, i2, 0);
        try {
            Drawable drawable3 = this.a.getDrawable();
            if (!(drawable3 != null || (f2 = a2.f(j.AppCompatImageView_srcCompat, -1)) == -1 || (drawable3 = AppCompatResources.c(this.a.getContext(), f2)) == null)) {
                this.a.setImageDrawable(drawable3);
            }
            if (drawable3 != null) {
                DrawableUtils.b(drawable3);
            }
            if (a2.f(j.AppCompatImageView_tint)) {
                ImageView imageView = this.a;
                imageView.setImageTintList(a2.a(j.AppCompatImageView_tint));
                if (!(Build.VERSION.SDK_INT != 21 || (drawable2 = imageView.getDrawable()) == null || imageView.getImageTintList() == null)) {
                    if (drawable2.isStateful()) {
                        drawable2.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable2);
                }
            }
            if (a2.f(j.AppCompatImageView_tintMode)) {
                ImageView imageView2 = this.a;
                imageView2.setImageTintMode(DrawableUtils.a(a2.d(j.AppCompatImageView_tintMode, -1), null));
                if (!(Build.VERSION.SDK_INT != 21 || (drawable = imageView2.getDrawable()) == null || imageView2.getImageTintList() == null)) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView2.getDrawableState());
                    }
                    imageView2.setImageDrawable(drawable);
                }
            }
            a2.b.recycle();
        } catch (Throwable th) {
            a2.b.recycle();
            throw th;
        }
    }

    public void a(int i2) {
        if (i2 != 0) {
            Drawable c2 = AppCompatResources.c(this.a.getContext(), i2);
            if (c2 != null) {
                DrawableUtils.b(c2);
            }
            this.a.setImageDrawable(c2);
        } else {
            this.a.setImageDrawable(null);
        }
        a();
    }

    public void a(ColorStateList colorStateList) {
        if (this.b == null) {
            this.b = new TintInfo();
        }
        TintInfo tintInfo = this.b;
        tintInfo.a = colorStateList;
        tintInfo.d = true;
        a();
    }

    public void a(PorterDuff.Mode mode) {
        if (this.b == null) {
            this.b = new TintInfo();
        }
        TintInfo tintInfo = this.b;
        tintInfo.b = mode;
        tintInfo.c = true;
        a();
    }

    public void a() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            DrawableUtils.b(drawable);
        }
        if (drawable != null) {
            int i2 = Build.VERSION.SDK_INT;
            boolean z = true;
            if (i2 <= 21 && i2 == 21) {
                if (this.c == null) {
                    this.c = new TintInfo();
                }
                TintInfo tintInfo = this.c;
                tintInfo.a = null;
                tintInfo.d = false;
                tintInfo.b = null;
                tintInfo.c = false;
                ColorStateList imageTintList = this.a.getImageTintList();
                if (imageTintList != null) {
                    tintInfo.d = true;
                    tintInfo.a = imageTintList;
                }
                PorterDuff.Mode imageTintMode = this.a.getImageTintMode();
                if (imageTintMode != null) {
                    tintInfo.c = true;
                    tintInfo.b = imageTintMode;
                }
                if (tintInfo.d || tintInfo.c) {
                    AppCompatDrawableManager.a(drawable, tintInfo, this.a.getDrawableState());
                } else {
                    z = false;
                }
                if (z) {
                    return;
                }
            }
            TintInfo tintInfo2 = this.b;
            if (tintInfo2 != null) {
                AppCompatDrawableManager.a(drawable, tintInfo2, this.a.getDrawableState());
            }
        }
    }
}
