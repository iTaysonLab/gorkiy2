package i.b.q;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.h.e.b.ResourcesCompat;

public class TintTypedArray {
    public final Context a;
    public final TypedArray b;
    public TypedValue c;

    public TintTypedArray(Context context, TypedArray typedArray) {
        this.a = context;
        this.b = typedArray;
    }

    public static TintTypedArray a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new TintTypedArray(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public Drawable b(int i2) {
        int resourceId;
        if (!this.b.hasValue(i2) || (resourceId = this.b.getResourceId(i2, 0)) == 0) {
            return this.b.getDrawable(i2);
        }
        return AppCompatResources.c(this.a, resourceId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.AppCompatDrawableManager.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      i.b.q.AppCompatDrawableManager.a(android.graphics.drawable.Drawable, i.b.q.TintInfo, int[]):void
      i.b.q.AppCompatDrawableManager.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public Drawable c(int i2) {
        int resourceId;
        if (!this.b.hasValue(i2) || (resourceId = this.b.getResourceId(i2, 0)) == 0) {
            return null;
        }
        return AppCompatDrawableManager.a().a(this.a, resourceId, true);
    }

    public String d(int i2) {
        return this.b.getString(i2);
    }

    public CharSequence e(int i2) {
        return this.b.getText(i2);
    }

    public int f(int i2, int i3) {
        return this.b.getResourceId(i2, i3);
    }

    public static TintTypedArray a(Context context, AttributeSet attributeSet, int[] iArr, int i2, int i3) {
        return new TintTypedArray(context, context.obtainStyledAttributes(attributeSet, iArr, i2, i3));
    }

    public int d(int i2, int i3) {
        return this.b.getInt(i2, i3);
    }

    public int e(int i2, int i3) {
        return this.b.getLayoutDimension(i2, i3);
    }

    public boolean f(int i2) {
        return this.b.hasValue(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, android.os.Handler, boolean):android.graphics.Typeface
     arg types: [android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, ?[OBJECT, ARRAY], int]
     candidates:
      i.b.k.ResourcesFlusher.a(androidx.recyclerview.widget.RecyclerView$a0, i.r.d.OrientationHelper, android.view.View, android.view.View, androidx.recyclerview.widget.RecyclerView$o, boolean, boolean):int
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, android.animation.ValueAnimator, float, org.xmlpull.v1.XmlPullParser):android.animation.ValueAnimator
      i.b.k.ResourcesFlusher.a(android.content.Context, int, android.util.TypedValue, int, i.h.e.b.ResourcesCompat, android.os.Handler, boolean):android.graphics.Typeface */
    public Typeface a(int i2, int i3, ResourcesCompat resourcesCompat) {
        int resourceId = this.b.getResourceId(i2, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.c == null) {
            this.c = new TypedValue();
        }
        Context context = this.a;
        TypedValue typedValue = this.c;
        if (context.isRestricted()) {
            return null;
        }
        return ResourcesFlusher.a(context, resourceId, typedValue, i3, resourcesCompat, (Handler) null, true);
    }

    public int c(int i2, int i3) {
        return this.b.getDimensionPixelSize(i2, i3);
    }

    public int b(int i2, int i3) {
        return this.b.getDimensionPixelOffset(i2, i3);
    }

    public boolean a(int i2, boolean z) {
        return this.b.getBoolean(i2, z);
    }

    public int a(int i2, int i3) {
        return this.b.getColor(i2, i3);
    }

    public ColorStateList a(int i2) {
        int resourceId;
        ColorStateList b2;
        if (!this.b.hasValue(i2) || (resourceId = this.b.getResourceId(i2, 0)) == 0 || (b2 = AppCompatResources.b(this.a, resourceId)) == null) {
            return this.b.getColorStateList(i2);
        }
        return b2;
    }

    public float a(int i2, float f2) {
        return this.b.getDimension(i2, f2);
    }
}
