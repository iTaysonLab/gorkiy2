package i.b.m.a;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.StateSet;
import i.b.m.a.DrawableContainer;

@SuppressLint({"RestrictedAPI"})
public class StateListDrawable extends DrawableContainer {

    /* renamed from: n  reason: collision with root package name */
    public a f822n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f823o;

    public static class a extends DrawableContainer.c {
        public int[][] J;

        public a(a aVar, StateListDrawable stateListDrawable, Resources resources) {
            super(super, stateListDrawable, resources);
            if (aVar != null) {
                this.J = aVar.J;
            } else {
                this.J = new int[super.g.length][];
            }
        }

        public int a(int[] iArr) {
            int[][] iArr2 = this.J;
            int i2 = super.h;
            for (int i3 = 0; i3 < i2; i3++) {
                if (StateSet.stateSetMatches(iArr2[i3], iArr)) {
                    return i3;
                }
            }
            return -1;
        }

        public void d() {
            int[][] iArr = this.J;
            int[][] iArr2 = new int[iArr.length][];
            for (int length = iArr.length - 1; length >= 0; length--) {
                int[][] iArr3 = this.J;
                iArr2[length] = iArr3[length] != null ? (int[]) iArr3[length].clone() : null;
            }
            this.J = iArr2;
        }

        public Drawable newDrawable() {
            return new StateListDrawable(this, null);
        }

        public Drawable newDrawable(Resources resources) {
            return new StateListDrawable(this, resources);
        }
    }

    public StateListDrawable(a aVar, Resources resources) {
        a(new a(aVar, this, resources));
        onStateChange(getState());
    }

    public void applyTheme(Resources.Theme theme) {
        DrawableContainer.c cVar = super.b;
        if (cVar != null) {
            if (theme != null) {
                cVar.c();
                int i2 = cVar.h;
                Drawable[] drawableArr = cVar.g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null && drawableArr[i3].canApplyTheme()) {
                        drawableArr[i3].applyTheme(theme);
                        cVar.f808e |= drawableArr[i3].getChangingConfigurations();
                    }
                }
                cVar.a(theme.getResources());
            }
            onStateChange(getState());
            return;
        }
        throw null;
    }

    public boolean isStateful() {
        return true;
    }

    public Drawable mutate() {
        if (!this.f823o) {
            super.mutate();
            if (this == this) {
                this.f822n.d();
                this.f823o = true;
            }
        }
        return this;
    }

    public boolean onStateChange(int[] iArr) {
        boolean z;
        Drawable drawable = super.f801e;
        if (drawable != null) {
            z = drawable.setState(iArr);
        } else {
            Drawable drawable2 = super.d;
            z = drawable2 != null ? drawable2.setState(iArr) : false;
        }
        int a2 = this.f822n.a(iArr);
        if (a2 < 0) {
            a2 = this.f822n.a(StateSet.WILD_CARD);
        }
        if (a(a2) || z) {
            return true;
        }
        return false;
    }

    public a a() {
        return new a(this.f822n, this, null);
    }

    public void a(DrawableContainer.c cVar) {
        super.a(cVar);
        if (cVar instanceof a) {
            this.f822n = (a) cVar;
        }
    }

    public StateListDrawable(a aVar) {
        if (aVar != null) {
            a(aVar);
        }
    }
}
