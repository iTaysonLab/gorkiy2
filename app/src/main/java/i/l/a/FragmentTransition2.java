package i.l.a;

import android.graphics.Rect;
import android.view.View;
import androidx.fragment.app.Fragment;
import i.e.ArrayMap;
import i.l.a.FragmentTransition3;
import i.l.a.w;
import java.util.ArrayList;

/* compiled from: FragmentTransition */
public final class FragmentTransition2 implements Runnable {
    public final /* synthetic */ FragmentTransitionImpl0 b;
    public final /* synthetic */ ArrayMap c;
    public final /* synthetic */ Object d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ FragmentTransition3.a f1343e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ ArrayList f1344f;
    public final /* synthetic */ View g;
    public final /* synthetic */ Fragment h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ Fragment f1345i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ boolean f1346j;

    /* renamed from: k  reason: collision with root package name */
    public final /* synthetic */ ArrayList f1347k;

    /* renamed from: l  reason: collision with root package name */
    public final /* synthetic */ Object f1348l;

    /* renamed from: m  reason: collision with root package name */
    public final /* synthetic */ Rect f1349m;

    public FragmentTransition2(FragmentTransitionImpl0 fragmentTransitionImpl0, ArrayMap arrayMap, Object obj, FragmentTransition3.a aVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
        this.b = fragmentTransitionImpl0;
        this.c = arrayMap;
        this.d = obj;
        this.f1343e = aVar;
        this.f1344f = arrayList;
        this.g = view;
        this.h = fragment;
        this.f1345i = fragment2;
        this.f1346j = z;
        this.f1347k = arrayList2;
        this.f1348l = obj2;
        this.f1349m = rect;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View>
     arg types: [i.l.a.FragmentTransitionImpl0, i.e.ArrayMap, java.lang.Object, i.l.a.FragmentTransition3$a]
     candidates:
      i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, int]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, java.lang.Object, androidx.fragment.app.Fragment, java.util.ArrayList<android.view.View>, android.view.View):java.util.ArrayList<android.view.View>
      i.l.a.FragmentTransition3.a(i.l.a.a, i.l.a.r$a, android.util.SparseArray<i.l.a.w$a>, boolean, boolean):void
      i.l.a.FragmentTransition3.a(androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean, i.e.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View
     arg types: [i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.FragmentTransition3$a, java.lang.Object, boolean]
     candidates:
      i.l.a.FragmentTransition3.a(i.l.a.b0, i.e.ArrayMap<java.lang.String, java.lang.String>, java.lang.Object, i.l.a.w$a):i.e.ArrayMap<java.lang.String, android.view.View>
      i.l.a.FragmentTransition3.a(i.l.a.FragmentTransitionImpl0, androidx.fragment.app.Fragment, androidx.fragment.app.Fragment, boolean):java.lang.Object
      i.l.a.FragmentTransition3.a(i.e.ArrayMap<java.lang.String, android.view.View>, i.l.a.w$a, java.lang.Object, boolean):android.view.View */
    public void run() {
        ArrayMap<String, View> a = FragmentTransition3.a((b0) this.b, (ArrayMap<String, String>) this.c, this.d, (w.a) this.f1343e);
        if (a != null) {
            this.f1344f.addAll(a.values());
            this.f1344f.add(this.g);
        }
        FragmentTransition3.a(this.h, this.f1345i, this.f1346j, a, false);
        Object obj = this.d;
        if (obj != null) {
            this.b.b(obj, (ArrayList<View>) this.f1347k, (ArrayList<View>) this.f1344f);
            View a2 = FragmentTransition3.a(a, (w.a) this.f1343e, this.f1348l, this.f1346j);
            if (a2 != null) {
                this.b.a(a2, this.f1349m);
            }
        }
    }
}
