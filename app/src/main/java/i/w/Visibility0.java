package i.w;

import android.view.View;
import android.view.ViewGroup;

/* compiled from: Visibility */
public class Visibility0 extends TransitionListenerAdapter {
    public final /* synthetic */ ViewGroup a;
    public final /* synthetic */ View b;
    public final /* synthetic */ View c;
    public final /* synthetic */ Visibility d;

    public Visibility0(Visibility visibility, ViewGroup viewGroup, View view, View view2) {
        this.d = visibility;
        this.a = viewGroup;
        this.b = view;
        this.c = view2;
    }

    public void a(Transition transition) {
        this.a.getOverlay().remove(this.b);
    }

    public void b(Transition transition) {
        if (this.b.getParent() == null) {
            this.a.getOverlay().add(this.b);
        } else {
            this.d.cancel();
        }
    }

    public void e(Transition transition) {
        this.c.setTag(f.save_overlay_view, null);
        this.a.getOverlay().remove(this.b);
        transition.b(this);
    }
}
