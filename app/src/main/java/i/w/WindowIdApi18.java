package i.w;

import android.view.View;
import android.view.WindowId;

public class WindowIdApi18 implements WindowIdImpl {
    public final WindowId a;

    public WindowIdApi18(View view) {
        this.a = view.getWindowId();
    }

    public boolean equals(Object obj) {
        return (obj instanceof WindowIdApi18) && ((WindowIdApi18) obj).a.equals(this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
