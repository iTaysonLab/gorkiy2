package i.i.a;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ResourceCursorAdapter extends CursorAdapter {

    /* renamed from: j  reason: collision with root package name */
    public int f1231j;

    /* renamed from: k  reason: collision with root package name */
    public int f1232k;

    /* renamed from: l  reason: collision with root package name */
    public LayoutInflater f1233l;

    @Deprecated
    public ResourceCursorAdapter(Context context, int i2, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.f1232k = i2;
        this.f1231j = i2;
        this.f1233l = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f1233l.inflate(this.f1231j, viewGroup, false);
    }
}
