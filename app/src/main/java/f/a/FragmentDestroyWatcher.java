package f.a;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import androidx.annotation.RecentlyNonNull;
import com.crashlytics.android.answers.SessionEvent;
import java.lang.reflect.Proxy;
import java.util.List;
import kotlin.TypeCastException;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: FragmentDestroyWatcher.kt */
public final class FragmentDestroyWatcher implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ Application.ActivityLifecycleCallbacks b;
    public final /* synthetic */ List c;

    public FragmentDestroyWatcher(List list) {
        this.c = list;
        InternalAppWatcher internalAppWatcher = InternalAppWatcher.f732i;
        Class<Application.ActivityLifecycleCallbacks> cls = Application.ActivityLifecycleCallbacks.class;
        InternalAppWatcher0 internalAppWatcher0 = InternalAppWatcher0.a;
        Object newProxyInstance = Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, internalAppWatcher0);
        if (newProxyInstance != null) {
            this.b = (Application.ActivityLifecycleCallbacks) newProxyInstance;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Application.ActivityLifecycleCallbacks");
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (activity != null) {
            for (Functions0 a : this.c) {
                a.a(activity);
            }
            return;
        }
        Intrinsics.a(SessionEvent.ACTIVITY_KEY);
        throw null;
    }

    public void onActivityDestroyed(@RecentlyNonNull Activity activity) {
        this.b.onActivityDestroyed(activity);
    }

    public void onActivityPaused(@RecentlyNonNull Activity activity) {
        this.b.onActivityPaused(activity);
    }

    public void onActivityResumed(@RecentlyNonNull Activity activity) {
        this.b.onActivityResumed(activity);
    }

    public void onActivitySaveInstanceState(@RecentlyNonNull Activity activity, @RecentlyNonNull Bundle bundle) {
        this.b.onActivitySaveInstanceState(activity, bundle);
    }

    public void onActivityStarted(@RecentlyNonNull Activity activity) {
        this.b.onActivityStarted(activity);
    }

    public void onActivityStopped(@RecentlyNonNull Activity activity) {
        this.b.onActivityStopped(activity);
    }
}
