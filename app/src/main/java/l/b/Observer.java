package l.b;

import l.b.s.Disposable;

public interface Observer<T> {
    void a();

    void a(Throwable th);

    void a(Disposable disposable);

    void b(T t2);
}
