package l.b.u.c;

import java.util.concurrent.Callable;

public interface ScalarCallable<T> extends Callable<T> {
    T call();
}
