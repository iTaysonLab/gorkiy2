package l.b.u.e.c;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.u.a.DisposableHelper;

public final class ObservableDoOnEach<T> extends AbstractObservableWithUpstream<T, T> {
    public final Consumer<? super T> c;
    public final Consumer<? super Throwable> d;

    /* renamed from: e  reason: collision with root package name */
    public final Action f2699e;

    /* renamed from: f  reason: collision with root package name */
    public final Action f2700f;

    public ObservableDoOnEach(ObservableSource<T> observableSource, Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, l.b.t.a aVar, l.b.t.a aVar2) {
        super(observableSource);
        this.c = consumer;
        this.d = consumer2;
        this.f2699e = aVar;
        this.f2700f = aVar2;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c, this.d, this.f2699e, this.f2700f));
    }

    public static final class a<T> implements Observer<T>, b {
        public final Observer<? super T> b;
        public final Consumer<? super T> c;
        public final Consumer<? super Throwable> d;

        /* renamed from: e  reason: collision with root package name */
        public final Action f2701e;

        /* renamed from: f  reason: collision with root package name */
        public final Action f2702f;
        public Disposable g;
        public boolean h;

        public a(Observer<? super T> observer, Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, l.b.t.a aVar, l.b.t.a aVar2) {
            this.b = observer;
            this.c = consumer;
            this.d = consumer2;
            this.f2701e = aVar;
            this.f2702f = aVar2;
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableDoOnEach$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.h) {
                try {
                    this.c.a(t2);
                    this.b.b(t2);
                } catch (Throwable th) {
                    c.c(th);
                    this.g.f();
                    a(th);
                }
            }
        }

        public void f() {
            this.g.f();
        }

        public boolean g() {
            return this.g.g();
        }

        public void a(Throwable th) {
            if (this.h) {
                c.b(th);
                return;
            }
            this.h = true;
            try {
                this.d.a(th);
            } catch (Throwable th2) {
                c.c(th2);
                th = new CompositeException(th, th2);
            }
            this.b.a(th);
            try {
                this.f2702f.run();
            } catch (Throwable th3) {
                c.c(th3);
                c.b(th3);
            }
        }

        public void a() {
            if (!this.h) {
                try {
                    this.f2701e.run();
                    this.h = true;
                    this.b.a();
                    try {
                        this.f2702f.run();
                    } catch (Throwable th) {
                        c.c(th);
                        c.b(th);
                    }
                } catch (Throwable th2) {
                    c.c(th2);
                    a(th2);
                }
            }
        }
    }
}
