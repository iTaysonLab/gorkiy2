package l.b.u.e.a;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.d;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.u.a.DisposableHelper;
import l.b.u.a.EmptyDisposable;

public final class CompletablePeek extends Completable {
    public final CompletableSource a;
    public final Consumer<? super b> b;
    public final Consumer<? super Throwable> c;
    public final Action d;

    /* renamed from: e  reason: collision with root package name */
    public final Action f2682e;

    /* renamed from: f  reason: collision with root package name */
    public final Action f2683f;
    public final Action g;

    public CompletablePeek(d dVar, Consumer<? super b> consumer, Consumer<? super Throwable> consumer2, l.b.t.a aVar, l.b.t.a aVar2, l.b.t.a aVar3, l.b.t.a aVar4) {
        this.a = dVar;
        this.b = consumer;
        this.c = consumer2;
        this.d = aVar;
        this.f2682e = aVar2;
        this.f2683f = aVar3;
        this.g = aVar4;
    }

    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver));
    }

    public final class a implements CompletableObserver, Disposable {
        public final CompletableObserver b;
        public Disposable c;

        public a(CompletableObserver completableObserver) {
            this.b = completableObserver;
        }

        /* JADX WARN: Type inference failed for: r3v1, types: [l.b.s.Disposable, l.b.u.a.DisposableHelper] */
        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        public void a(Disposable disposable) {
            try {
                CompletablePeek.this.b.a(disposable);
                if (DisposableHelper.a(this.c, disposable)) {
                    this.c = disposable;
                    this.b.a(this);
                }
            } catch (Throwable th) {
                c.c(th);
                disposable.f();
                this.c = DisposableHelper.DISPOSED;
                CompletableObserver completableObserver = this.b;
                completableObserver.a((Disposable) EmptyDisposable.INSTANCE);
                completableObserver.a(th);
            }
        }

        public void f() {
            try {
                CompletablePeek.this.g.run();
            } catch (Throwable th) {
                c.c(th);
                c.b(th);
            }
            this.c.f();
        }

        public boolean g() {
            return this.c.g();
        }

        public void a(Throwable th) {
            if (this.c == DisposableHelper.DISPOSED) {
                c.b(th);
                return;
            }
            try {
                CompletablePeek.this.c.a(th);
                CompletablePeek.this.f2682e.run();
            } catch (Throwable th2) {
                c.c(th2);
                th = new CompositeException(th, th2);
            }
            this.b.a(th);
            try {
                CompletablePeek.this.f2683f.run();
            } catch (Throwable th3) {
                c.c(th3);
                c.b(th3);
            }
        }

        public void a() {
            if (this.c != DisposableHelper.DISPOSED) {
                try {
                    CompletablePeek.this.d.run();
                    CompletablePeek.this.f2682e.run();
                    this.b.a();
                    try {
                        CompletablePeek.this.f2683f.run();
                    } catch (Throwable th) {
                        c.c(th);
                        c.b(th);
                    }
                } catch (Throwable th2) {
                    c.c(th2);
                    this.b.a(th2);
                }
            }
        }
    }
}
