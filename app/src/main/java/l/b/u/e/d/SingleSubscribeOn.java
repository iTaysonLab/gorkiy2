package l.b.u.e.d;

import java.util.concurrent.atomic.AtomicReference;
import l.b.Scheduler;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.l;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;
import l.b.u.a.SequentialDisposable;

public final class SingleSubscribeOn<T> extends Single<T> {
    public final SingleSource<? extends T> a;
    public final Scheduler b;

    public static final class a<T> extends AtomicReference<b> implements SingleObserver<T>, b, Runnable {
        public final SingleObserver<? super T> b;
        public final SequentialDisposable c = new SequentialDisposable();
        public final SingleSource<? extends T> d;

        public a(SingleObserver<? super T> singleObserver, SingleSource<? extends T> singleSource) {
            this.b = singleObserver;
            this.d = singleSource;
        }

        public void a(Disposable disposable) {
            DisposableHelper.b(super, disposable);
        }

        public void f() {
            DisposableHelper.a(super);
            SequentialDisposable sequentialDisposable = this.c;
            if (sequentialDisposable != null) {
                DisposableHelper.a(super);
                return;
            }
            throw null;
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public void run() {
            this.d.a(this);
        }

        public void a(T t2) {
            this.b.a((Object) t2);
        }

        public void a(Throwable th) {
            this.b.a(th);
        }
    }

    public SingleSubscribeOn(SingleSource<? extends T> singleSource, l lVar) {
        this.a = singleSource;
        this.b = lVar;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.e.d.SingleSubscribeOn$a, l.b.s.Disposable, java.lang.Runnable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
     arg types: [l.b.u.a.SequentialDisposable, l.b.s.Disposable]
     candidates:
      l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
      l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
    public void b(SingleObserver<? super T> singleObserver) {
        ? aVar = new a(singleObserver, this.a);
        singleObserver.a((Disposable) aVar);
        Disposable a2 = this.b.a(aVar);
        SequentialDisposable sequentialDisposable = aVar.c;
        if (sequentialDisposable != null) {
            DisposableHelper.a((AtomicReference<b>) sequentialDisposable, (b) a2);
            return;
        }
        throw null;
    }
}
