package l.b.u.e.c;

import l.b.ObservableSource;
import l.b.Observer;
import l.b.t.BiPredicate;
import l.b.t.Function;
import l.b.u.b.ObjectHelper;
import l.b.u.d.BasicFuseableObserver;

public final class ObservableDistinctUntilChanged<T, K> extends AbstractObservableWithUpstream<T, T> {
    public final Function<? super T, K> c;
    public final BiPredicate<? super K, ? super K> d;

    public static final class a<T, K> extends BasicFuseableObserver<T, T> {
        public final Function<? super T, K> g;
        public final BiPredicate<? super K, ? super K> h;

        /* renamed from: i  reason: collision with root package name */
        public K f2697i;

        /* renamed from: j  reason: collision with root package name */
        public boolean f2698j;

        public a(Observer<? super T> observer, Function<? super T, K> function, BiPredicate<? super K, ? super K> biPredicate) {
            super(observer);
            this.g = function;
            this.h = biPredicate;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public int a(int r1) {
            /*
                r0 = this;
                int r1 = r0.b(r1)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableDistinctUntilChanged.a.a(int):int");
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void b(T r5) {
            /*
                r4 = this;
                boolean r0 = r4.f2677e
                if (r0 == 0) goto L_0x0005
                return
            L_0x0005:
                int r0 = r4.f2678f
                if (r0 == 0) goto L_0x000f
                l.b.Observer<? super R> r0 = r4.b
                r0.b(r5)
                return
            L_0x000f:
                l.b.t.Function<? super T, K> r0 = r4.g     // Catch:{ all -> 0x003f }
                java.lang.Object r0 = r0.a(r5)     // Catch:{ all -> 0x003f }
                boolean r1 = r4.f2698j     // Catch:{ all -> 0x003f }
                r2 = 1
                if (r1 == 0) goto L_0x0035
                l.b.t.BiPredicate<? super K, ? super K> r1 = r4.h     // Catch:{ all -> 0x003f }
                K r3 = r4.f2697i     // Catch:{ all -> 0x003f }
                l.b.u.b.ObjectHelper$a r1 = (l.b.u.b.ObjectHelper.a) r1     // Catch:{ all -> 0x003f }
                if (r1 == 0) goto L_0x0033
                if (r3 == r0) goto L_0x002e
                if (r3 == 0) goto L_0x002d
                boolean r1 = r3.equals(r0)     // Catch:{ all -> 0x003f }
                if (r1 == 0) goto L_0x002d
                goto L_0x002e
            L_0x002d:
                r2 = 0
            L_0x002e:
                r4.f2697i = r0     // Catch:{ all -> 0x003f }
                if (r2 == 0) goto L_0x0039
                return
            L_0x0033:
                r5 = 0
                throw r5     // Catch:{ all -> 0x003f }
            L_0x0035:
                r4.f2698j = r2     // Catch:{ all -> 0x003f }
                r4.f2697i = r0     // Catch:{ all -> 0x003f }
            L_0x0039:
                l.b.Observer<? super R> r0 = r4.b
                r0.b(r5)
                return
            L_0x003f:
                r5 = move-exception
                r4.b(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableDistinctUntilChanged.a.b(java.lang.Object):void");
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.c.QueueDisposable<T>, l.b.u.c.SimpleQueue] */
        public T poll() {
            while (true) {
                T poll = super.d.poll();
                if (poll == null) {
                    return null;
                }
                K a = this.g.a(poll);
                boolean z = true;
                if (!this.f2698j) {
                    this.f2698j = true;
                    this.f2697i = a;
                    return poll;
                }
                BiPredicate<? super K, ? super K> biPredicate = this.h;
                K k2 = this.f2697i;
                if (((ObjectHelper.a) biPredicate) != null) {
                    if (k2 != a && (k2 == null || !k2.equals(a))) {
                        z = false;
                    }
                    if (!z) {
                        this.f2697i = a;
                        return poll;
                    }
                    this.f2697i = a;
                } else {
                    throw null;
                }
            }
        }
    }

    public ObservableDistinctUntilChanged(ObservableSource<T> observableSource, Function<? super T, K> function, BiPredicate<? super K, ? super K> biPredicate) {
        super(observableSource);
        this.c = function;
        this.d = biPredicate;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c, this.d));
    }
}
