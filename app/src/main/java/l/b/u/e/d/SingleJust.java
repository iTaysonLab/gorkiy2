package l.b.u.e.d;

import l.b.Single;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;

public final class SingleJust<T> extends Single<T> {
    public final T a;

    public SingleJust(T t2) {
        this.a = t2;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    public void b(SingleObserver<? super T> singleObserver) {
        singleObserver.a((Disposable) EmptyDisposable.INSTANCE);
        singleObserver.a((Object) this.a);
    }
}
