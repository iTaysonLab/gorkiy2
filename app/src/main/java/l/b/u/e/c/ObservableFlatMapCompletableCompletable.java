package l.b.u.e.c;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.b;
import l.b.c;
import l.b.d;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;
import l.b.u.c.FuseToObservable;
import l.b.u.h.AtomicThrowable;
import l.b.u.h.ExceptionHelper;

public final class ObservableFlatMapCompletableCompletable<T> extends b implements FuseToObservable<T> {
    public final ObservableSource<T> a;
    public final Function<? super T, ? extends d> b;
    public final boolean c;

    public ObservableFlatMapCompletableCompletable(ObservableSource<T> observableSource, Function<? super T, ? extends d> function, boolean z) {
        this.a = observableSource;
        this.b = function;
        this.c = z;
    }

    public Observable<T> a() {
        return new ObservableFlatMapCompletable(this.a, this.b, this.c);
    }

    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver, this.b, this.c));
    }

    public static final class a<T> extends AtomicInteger implements l.b.s.b, Observer<T> {
        public final CompletableObserver b;
        public final AtomicThrowable c = new AtomicThrowable();
        public final Function<? super T, ? extends d> d;

        /* renamed from: e  reason: collision with root package name */
        public final boolean f2727e;

        /* renamed from: f  reason: collision with root package name */
        public final CompositeDisposable f2728f = new CompositeDisposable();
        public Disposable g;
        public volatile boolean h;

        /* renamed from: l.b.u.e.c.ObservableFlatMapCompletableCompletable$a$a  reason: collision with other inner class name */
        public final class C0036a extends AtomicReference<l.b.s.b> implements c, l.b.s.b {
            public C0036a() {
            }

            public void a(Disposable disposable) {
                DisposableHelper.b(super, disposable);
            }

            public void f() {
                DisposableHelper.a(super);
            }

            public boolean g() {
                return DisposableHelper.a((Disposable) get());
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletableCompletable$a$a] */
            public void a() {
                a aVar = a.this;
                aVar.f2728f.a((Disposable) this);
                aVar.a();
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletableCompletable$a$a] */
            public void a(Throwable th) {
                a aVar = a.this;
                aVar.f2728f.a((Disposable) this);
                aVar.a(th);
            }
        }

        public a(c cVar, Function<? super T, ? extends d> function, boolean z) {
            this.b = cVar;
            this.d = function;
            this.f2727e = z;
            lazySet(1);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletableCompletable$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                this.b.a((Disposable) this);
            }
        }

        /* JADX WARN: Type inference failed for: r0v3, types: [l.b.CompletableObserver, l.b.s.Disposable, l.b.u.e.c.ObservableFlatMapCompletableCompletable$a$a] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.d, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void b(T t2) {
            try {
                Object a = this.d.a(t2);
                ObjectHelper.a((Object) a, "The mapper returned a null CompletableSource");
                CompletableSource completableSource = (CompletableSource) a;
                getAndIncrement();
                ? aVar = new C0036a();
                if (!this.h && this.f2728f.c(aVar)) {
                    completableSource.a(aVar);
                }
            } catch (Throwable th) {
                j.c.a.a.c.n.c.c(th);
                this.g.f();
                a(th);
            }
        }

        public void f() {
            this.h = true;
            this.g.f();
            this.f2728f.f();
        }

        public boolean g() {
            return this.g.g();
        }

        public void a(Throwable th) {
            AtomicThrowable atomicThrowable = this.c;
            if (atomicThrowable == null) {
                throw null;
            } else if (!ExceptionHelper.a(atomicThrowable, th)) {
                j.c.a.a.c.n.c.b(th);
            } else if (!this.f2727e) {
                f();
                if (getAndSet(0) > 0) {
                    AtomicThrowable atomicThrowable2 = this.c;
                    if (atomicThrowable2 != null) {
                        this.b.a(ExceptionHelper.a(atomicThrowable2));
                        return;
                    }
                    throw null;
                }
            } else if (decrementAndGet() == 0) {
                AtomicThrowable atomicThrowable3 = this.c;
                if (atomicThrowable3 != null) {
                    this.b.a(ExceptionHelper.a(atomicThrowable3));
                    return;
                }
                throw null;
            }
        }

        public void a() {
            if (decrementAndGet() == 0) {
                AtomicThrowable atomicThrowable = this.c;
                if (atomicThrowable != null) {
                    Throwable a = ExceptionHelper.a(atomicThrowable);
                    if (a != null) {
                        this.b.a(a);
                    } else {
                        this.b.a();
                    }
                } else {
                    throw null;
                }
            }
        }
    }
}
