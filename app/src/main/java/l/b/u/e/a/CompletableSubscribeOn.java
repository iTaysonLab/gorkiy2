package l.b.u.e.a;

import java.util.concurrent.atomic.AtomicReference;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.Scheduler;
import l.b.c;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;
import l.b.u.a.SequentialDisposable;

public final class CompletableSubscribeOn extends Completable {
    public final CompletableSource a;
    public final Scheduler b;

    public static final class a extends AtomicReference<b> implements c, b, Runnable {
        public final CompletableObserver b;
        public final SequentialDisposable c = new SequentialDisposable();
        public final CompletableSource d;

        public a(CompletableObserver completableObserver, CompletableSource completableSource) {
            this.b = completableObserver;
            this.d = completableSource;
        }

        public void a(Disposable disposable) {
            DisposableHelper.b(super, disposable);
        }

        public void f() {
            DisposableHelper.a(super);
            SequentialDisposable sequentialDisposable = this.c;
            if (sequentialDisposable != null) {
                DisposableHelper.a(super);
                return;
            }
            throw null;
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.CompletableObserver, l.b.u.e.a.CompletableSubscribeOn$a] */
        public void run() {
            this.d.a(this);
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }

    public CompletableSubscribeOn(CompletableSource completableSource, Scheduler scheduler) {
        this.a = completableSource;
        this.b = scheduler;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.u.e.a.CompletableSubscribeOn$a, java.lang.Runnable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
     arg types: [l.b.u.a.SequentialDisposable, l.b.s.Disposable]
     candidates:
      l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
      l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
    public void b(CompletableObserver completableObserver) {
        ? aVar = new a(completableObserver, this.a);
        completableObserver.a((Disposable) aVar);
        Disposable a2 = this.b.a(aVar);
        SequentialDisposable sequentialDisposable = aVar.c;
        if (sequentialDisposable != null) {
            DisposableHelper.a((AtomicReference<b>) sequentialDisposable, (b) a2);
            return;
        }
        throw null;
    }
}
