package l.b.u.e.c;

import java.util.concurrent.atomic.AtomicInteger;
import l.b.Observer;
import l.b.u.c.QueueDisposable;

public final class ObservableScalarXMap<T> extends AtomicInteger implements QueueDisposable<T>, Runnable {
    public final Observer<? super T> b;
    public final T c;

    public ObservableScalarXMap(Observer<? super T> observer, T t2) {
        this.b = observer;
        this.c = t2;
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public int a(int r2) {
        /*
            r1 = this;
            r0 = 1
            r2 = r2 & r0
            if (r2 == 0) goto L_0x0008
            r1.lazySet(r0)
            return r0
        L_0x0008:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableScalarXMap.a(int):int");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void clear() {
        /*
            r1 = this;
            r0 = 3
            r1.lazySet(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableScalarXMap.clear():void");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void f() {
        /*
            r1 = this;
            r0 = 3
            r1.set(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableScalarXMap.f():void");
    }

    public boolean g() {
        return get() == 3;
    }

    public boolean isEmpty() {
        return get() != 1;
    }

    public boolean offer(T t2) {
        throw new UnsupportedOperationException("Should not be called!");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public T poll() {
        /*
            r2 = this;
            int r0 = r2.get()
            r1 = 1
            if (r0 != r1) goto L_0x000e
            r0 = 3
            r2.lazySet(r0)
            T r0 = r2.c
            return r0
        L_0x000e:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableScalarXMap.poll():java.lang.Object");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void run() {
        /*
            r3 = this;
            int r0 = r3.get()
            if (r0 != 0) goto L_0x0024
            r0 = 0
            r1 = 2
            boolean r0 = r3.compareAndSet(r0, r1)
            if (r0 == 0) goto L_0x0024
            l.b.Observer<? super T> r0 = r3.b
            T r2 = r3.c
            r0.b(r2)
            int r0 = r3.get()
            if (r0 != r1) goto L_0x0024
            r0 = 3
            r3.lazySet(r0)
            l.b.Observer<? super T> r0 = r3.b
            r0.a()
        L_0x0024:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableScalarXMap.run():void");
    }
}
