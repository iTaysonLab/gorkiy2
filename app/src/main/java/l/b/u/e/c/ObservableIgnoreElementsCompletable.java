package l.b.u.e.c;

import l.b.CompletableObserver;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.b;
import l.b.s.Disposable;
import l.b.u.c.FuseToObservable;

public final class ObservableIgnoreElementsCompletable<T> extends b implements FuseToObservable<T> {
    public final ObservableSource<T> a;

    public ObservableIgnoreElementsCompletable(ObservableSource<T> observableSource) {
        this.a = observableSource;
    }

    public Observable<T> a() {
        return new ObservableIgnoreElements(this.a);
    }

    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver));
    }

    public static final class a<T> implements Observer<T>, l.b.s.b {
        public final CompletableObserver b;
        public Disposable c;

        public a(CompletableObserver completableObserver) {
            this.b = completableObserver;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableIgnoreElementsCompletable$a] */
        public void a(Disposable disposable) {
            this.c = disposable;
            this.b.a((Disposable) this);
        }

        public void b(T t2) {
        }

        public void f() {
            this.c.f();
        }

        public boolean g() {
            return this.c.g();
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a() {
            this.b.a();
        }
    }
}
