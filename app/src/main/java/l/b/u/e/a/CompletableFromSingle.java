package l.b.u.e.a;

import l.b.CompletableObserver;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.b;
import l.b.s.Disposable;

public final class CompletableFromSingle<T> extends b {
    public final SingleSource<T> a;

    public static final class a<T> implements SingleObserver<T> {
        public final CompletableObserver b;

        public a(CompletableObserver completableObserver) {
            this.b = completableObserver;
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        public void a(T t2) {
            this.b.a();
        }
    }

    public CompletableFromSingle(SingleSource<T> singleSource) {
        this.a = singleSource;
    }

    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver));
    }
}
