package l.b.u.b;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.t.Predicate;

public final class Functions {
    public static final Function<Object, Object> a = new f();
    public static final Runnable b = new e();
    public static final Action c = new c();
    public static final Consumer<Object> d = new d();

    /* renamed from: e  reason: collision with root package name */
    public static final Consumer<Throwable> f2676e = new g();

    public static final class a<T, U> implements Function<T, U> {
        public final Class<U> a;

        public a(Class<U> cls) {
            this.a = cls;
        }

        public U a(T t2) {
            return this.a.cast(t2);
        }
    }

    public static final class b<T, U> implements Predicate<T> {
        public final Class<U> b;

        public b(Class<U> cls) {
            this.b = cls;
        }

        public boolean a(T t2) {
            return this.b.isInstance(t2);
        }
    }

    public static final class c implements Action {
        public void run() {
        }

        public String toString() {
            return "EmptyAction";
        }
    }

    public static final class d implements Consumer<Object> {
        public void a(Object obj) {
        }

        public String toString() {
            return "EmptyConsumer";
        }
    }

    public static final class e implements Runnable {
        public void run() {
        }

        public String toString() {
            return "EmptyRunnable";
        }
    }

    public static final class f implements Function<Object, Object> {
        public Object a(Object obj) {
            return obj;
        }

        public String toString() {
            return "IdentityFunction";
        }
    }

    public static final class g implements Consumer<Throwable> {
        public void a(Object obj) {
            j.c.a.a.c.n.c.b((Throwable) new OnErrorNotImplementedException((Throwable) obj));
        }
    }

    public static <T, U> Function<T, U> a(Class<U> cls) {
        return new a(cls);
    }

    public static <T, U> Predicate<T> b(Class<U> cls) {
        return new b(cls);
    }
}
