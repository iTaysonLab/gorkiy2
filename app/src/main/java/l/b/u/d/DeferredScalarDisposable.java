package l.b.u.d;

import l.b.Observer;

public class DeferredScalarDisposable<T> extends BasicIntQueueDisposable<T> {
    public final Observer<? super T> b;
    public T c;

    public DeferredScalarDisposable(Observer<? super T> observer) {
        this.b = observer;
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public final int a(int r2) {
        /*
            r1 = this;
            r0 = 2
            r2 = r2 & r0
            if (r2 == 0) goto L_0x000a
            r2 = 8
            r1.lazySet(r2)
            return r0
        L_0x000a:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.DeferredScalarDisposable.a(int):int");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public final void clear() {
        /*
            r1 = this;
            r0 = 32
            r1.lazySet(r0)
            r0 = 0
            r1.c = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.DeferredScalarDisposable.clear():void");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void f() {
        /*
            r1 = this;
            r0 = 4
            r1.set(r0)
            r0 = 0
            r1.c = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.DeferredScalarDisposable.f():void");
    }

    public final boolean g() {
        return get() == 4;
    }

    public final boolean isEmpty() {
        return get() != 16;
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public final T poll() {
        /*
            r3 = this;
            int r0 = r3.get()
            r1 = 0
            r2 = 16
            if (r0 != r2) goto L_0x0013
            T r0 = r3.c
            r3.c = r1
            r1 = 32
            r3.lazySet(r1)
            return r0
        L_0x0013:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.DeferredScalarDisposable.poll():java.lang.Object");
    }
}
