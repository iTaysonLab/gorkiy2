package l.b.u.d;

import j.c.a.a.c.n.c;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.a;
import l.b.u.a.DisposableHelper;

public final class DisposableLambdaObserver<T> implements Observer<T>, b {
    public final Observer<? super T> b;
    public final Consumer<? super b> c;
    public final Action d;

    /* renamed from: e  reason: collision with root package name */
    public Disposable f2680e;

    public DisposableLambdaObserver(Observer<? super T> observer, Consumer<? super b> consumer, a aVar) {
        this.b = observer;
        this.c = consumer;
        this.d = aVar;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [l.b.s.Disposable, l.b.u.a.DisposableHelper] */
    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void a(l.b.s.Disposable r2) {
        /*
            r1 = this;
            l.b.t.Consumer<? super l.b.s.b> r0 = r1.c     // Catch:{ all -> 0x0015 }
            r0.a(r2)     // Catch:{ all -> 0x0015 }
            l.b.s.Disposable r0 = r1.f2680e
            boolean r0 = l.b.u.a.DisposableHelper.a(r0, r2)
            if (r0 == 0) goto L_0x0014
            r1.f2680e = r2
            l.b.Observer<? super T> r2 = r1.b
            r2.a(r1)
        L_0x0014:
            return
        L_0x0015:
            r0 = move-exception
            j.c.a.a.c.n.c.c(r0)
            r2.f()
            l.b.u.a.DisposableHelper r2 = l.b.u.a.DisposableHelper.DISPOSED
            r1.f2680e = r2
            l.b.Observer<? super T> r2 = r1.b
            l.b.u.a.EmptyDisposable.a(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.DisposableLambdaObserver.a(l.b.s.Disposable):void");
    }

    public void b(T t2) {
        this.b.b(t2);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.a.DisposableHelper] */
    public void f() {
        Disposable disposable = this.f2680e;
        ? r1 = DisposableHelper.DISPOSED;
        if (disposable != r1) {
            this.f2680e = r1;
            try {
                this.d.run();
            } catch (Throwable th) {
                c.c(th);
                c.b(th);
            }
            disposable.f();
        }
    }

    public boolean g() {
        return this.f2680e.g();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.a.DisposableHelper] */
    public void a(Throwable th) {
        Disposable disposable = this.f2680e;
        ? r1 = DisposableHelper.DISPOSED;
        if (disposable != r1) {
            this.f2680e = r1;
            this.b.a(th);
            return;
        }
        c.b(th);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.a.DisposableHelper] */
    public void a() {
        Disposable disposable = this.f2680e;
        ? r1 = DisposableHelper.DISPOSED;
        if (disposable != r1) {
            this.f2680e = r1;
            this.b.a();
        }
    }
}
