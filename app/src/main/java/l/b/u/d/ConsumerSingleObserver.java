package l.b.u.d;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Consumer;
import l.b.u.a.DisposableHelper;

public final class ConsumerSingleObserver<T> extends AtomicReference<b> implements SingleObserver<T>, b {
    public final Consumer<? super T> b;
    public final Consumer<? super Throwable> c;

    public ConsumerSingleObserver(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2) {
        this.b = consumer;
        this.c = consumer2;
    }

    public void a(Throwable th) {
        lazySet(DisposableHelper.DISPOSED);
        try {
            this.c.a(th);
        } catch (Throwable th2) {
            c.c(th2);
            c.b((Throwable) new CompositeException(th, th2));
        }
    }

    public void f() {
        DisposableHelper.a(super);
    }

    public boolean g() {
        return get() == DisposableHelper.DISPOSED;
    }

    public void a(Disposable disposable) {
        DisposableHelper.b(super, disposable);
    }

    public void a(T t2) {
        lazySet(DisposableHelper.DISPOSED);
        try {
            this.b.a(t2);
        } catch (Throwable th) {
            c.c(th);
            c.b(th);
        }
    }
}
