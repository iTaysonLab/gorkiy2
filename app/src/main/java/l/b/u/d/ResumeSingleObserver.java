package l.b.u.d;

import java.util.concurrent.atomic.AtomicReference;
import l.b.SingleObserver;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class ResumeSingleObserver<T> implements SingleObserver<T> {
    public final AtomicReference<b> b;
    public final SingleObserver<? super T> c;

    public ResumeSingleObserver(AtomicReference<b> atomicReference, SingleObserver<? super T> singleObserver) {
        this.b = atomicReference;
        this.c = singleObserver;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
     arg types: [java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.Disposable]
     candidates:
      l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
      l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
    public void a(Disposable disposable) {
        DisposableHelper.a(this.b, (b) disposable);
    }

    public void a(T t2) {
        this.c.a((Object) t2);
    }

    public void a(Throwable th) {
        this.c.a(th);
    }
}
