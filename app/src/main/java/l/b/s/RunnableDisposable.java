package l.b.s;

import j.a.a.a.outline;
import l.b.u.b.ObjectHelper;

public final class RunnableDisposable extends RunnableDisposable<Runnable> implements Disposable {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RunnableDisposable(Runnable runnable) {
        super((Object) runnable);
        ObjectHelper.a((Object) runnable, "value is null");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.RunnableDisposable, java.util.concurrent.atomic.AtomicReference] */
    public final void f() {
        Object andSet;
        if (get() != null && (andSet = getAndSet(null)) != null) {
            ((Runnable) andSet).run();
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.RunnableDisposable, java.util.concurrent.atomic.AtomicReference] */
    public final boolean g() {
        return get() == null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [l.b.s.RunnableDisposable, java.util.concurrent.atomic.AtomicReference] */
    public String toString() {
        StringBuilder a = outline.a("RunnableDisposable(disposed=");
        a.append(g());
        a.append(", ");
        a.append(get());
        a.append(")");
        return a.toString();
    }
}
