package l.b.r;

import android.os.Looper;
import java.util.concurrent.atomic.AtomicBoolean;
import l.b.r.b.AndroidSchedulers;
import l.b.s.Disposable;

public abstract class MainThreadDisposable implements Disposable {
    public final AtomicBoolean b = new AtomicBoolean();

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            MainThreadDisposable.this.a();
        }
    }

    public abstract void a();

    public final void f() {
        if (!this.b.compareAndSet(false, true)) {
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            a();
        } else {
            AndroidSchedulers.a().a(new a());
        }
    }

    public final boolean g() {
        return this.b.get();
    }
}
