package l.a.a.a.o.g;

public class FeaturesSettingsData {
    public final boolean a;
    public final boolean b;
    public final boolean c;
    public final boolean d;

    public FeaturesSettingsData(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.a = z;
        this.b = z3;
        this.c = z4;
        this.d = z5;
    }
}
