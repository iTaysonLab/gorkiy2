package l.a.a.a.o.b;

import android.os.SystemClock;
import android.util.Log;

public class TimingMetric {
    public final String a;
    public final String b;
    public final boolean c;
    public long d;

    /* renamed from: e  reason: collision with root package name */
    public long f2642e;

    public TimingMetric(String str, String str2) {
        this.a = str;
        this.b = str2;
        this.c = !Log.isLoggable(str2, 2);
    }

    public synchronized void a() {
        if (!this.c) {
            this.d = SystemClock.elapsedRealtime();
            this.f2642e = 0;
        }
    }

    public synchronized void b() {
        if (!this.c) {
            if (this.f2642e == 0) {
                this.f2642e = SystemClock.elapsedRealtime() - this.d;
                String str = this.b;
                Log.v(str, this.a + ": " + this.f2642e + "ms");
            }
        }
    }
}
