package l.a.a.a.o.b;

import com.crashlytics.android.core.CodedOutputStream;
import j.a.a.a.outline;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueueFile implements Closeable {
    public static final Logger h = Logger.getLogger(QueueFile.class.getName());
    public final RandomAccessFile b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public b f2640e;

    /* renamed from: f  reason: collision with root package name */
    public b f2641f;
    public final byte[] g = new byte[16];

    public class a implements d {
        public boolean a = true;
        public final /* synthetic */ StringBuilder b;

        public a(QueueFile queueFile, StringBuilder sb) {
            this.b = sb;
        }

        public void read(InputStream inputStream, int i2) {
            if (this.a) {
                this.a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i2);
        }
    }

    public static class b {
        public static final b c = new b(0, 0);
        public final int a;
        public final int b;

        public b(int i2, int i3) {
            this.a = i2;
            this.b = i3;
        }

        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.a + ", length = " + this.b + "]";
        }
    }

    public interface d {
        void read(InputStream inputStream, int i2);
    }

    /* JADX INFO: finally extract failed */
    public QueueFile(File file) {
        if (!file.exists()) {
            File file2 = new File(file.getPath() + ".tmp");
            RandomAccessFile randomAccessFile = new RandomAccessFile(file2, "rwd");
            try {
                randomAccessFile.setLength(4096);
                randomAccessFile.seek(0);
                byte[] bArr = new byte[16];
                int[] iArr = {4096, 0, 0, 0};
                int i2 = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    b(bArr, i2, iArr[i3]);
                    i2 += 4;
                }
                randomAccessFile.write(bArr);
                randomAccessFile.close();
                if (!file2.renameTo(file)) {
                    throw new IOException("Rename failed!");
                }
            } catch (Throwable th) {
                randomAccessFile.close();
                throw th;
            }
        }
        RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rwd");
        this.b = randomAccessFile2;
        randomAccessFile2.seek(0);
        this.b.readFully(this.g);
        int a2 = a(this.g, 0);
        this.c = a2;
        if (((long) a2) <= this.b.length()) {
            this.d = a(this.g, 4);
            int a3 = a(this.g, 8);
            int a4 = a(this.g, 12);
            this.f2640e = b(a3);
            this.f2641f = b(a4);
            return;
        }
        StringBuilder a5 = outline.a("File is truncated. Expected length: ");
        a5.append(this.c);
        a5.append(", Actual length: ");
        a5.append(this.b.length());
        throw new IOException(a5.toString());
    }

    public static int a(byte[] bArr, int i2) {
        return ((bArr[i2] & 255) << 24) + ((bArr[i2 + 1] & 255) << 16) + ((bArr[i2 + 2] & 255) << 8) + (bArr[i2 + 3] & 255);
    }

    public static void b(byte[] bArr, int i2, int i3) {
        bArr[i2] = (byte) (i3 >> 24);
        bArr[i2 + 1] = (byte) (i3 >> 16);
        bArr[i2 + 2] = (byte) (i3 >> 8);
        bArr[i2 + 3] = (byte) i3;
    }

    public final int c(int i2) {
        int i3 = this.c;
        return i2 < i3 ? i2 : (i2 + 16) - i3;
    }

    public synchronized void close() {
        this.b.close();
    }

    public synchronized boolean f() {
        return this.d == 0;
    }

    public synchronized void g() {
        if (f()) {
            throw new NoSuchElementException();
        } else if (this.d == 1) {
            a();
        } else {
            int c2 = c(this.f2640e.a + 4 + this.f2640e.b);
            a(c2, this.g, 0, 4);
            int a2 = a(this.g, 0);
            a(this.c, this.d - 1, c2, this.f2641f.a);
            this.d--;
            this.f2640e = new b(c2, a2);
        }
    }

    public int h() {
        if (this.d == 0) {
            return 16;
        }
        b bVar = this.f2641f;
        int i2 = bVar.a;
        int i3 = this.f2640e.a;
        if (i2 >= i3) {
            return (i2 - i3) + 4 + bVar.b + 16;
        }
        return (((i2 + 4) + bVar.b) + this.c) - i3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(QueueFile.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.c);
        sb.append(", size=");
        sb.append(this.d);
        sb.append(", first=");
        sb.append(this.f2640e);
        sb.append(", last=");
        sb.append(this.f2641f);
        sb.append(", element lengths=[");
        try {
            a(new a(this, sb));
        } catch (IOException e2) {
            h.log(Level.WARNING, "read error", (Throwable) e2);
        }
        sb.append("]]");
        return sb.toString();
    }

    public final void a(int i2, int i3, int i4, int i5) {
        byte[] bArr = this.g;
        int[] iArr = {i2, i3, i4, i5};
        int i6 = 0;
        for (int i7 = 0; i7 < 4; i7++) {
            b(bArr, i6, iArr[i7]);
            i6 += 4;
        }
        this.b.seek(0);
        this.b.write(this.g);
    }

    public final b b(int i2) {
        if (i2 == 0) {
            return b.c;
        }
        this.b.seek((long) i2);
        return new b(i2, this.b.readInt());
    }

    public static /* synthetic */ int a(QueueFile queueFile, int i2) {
        int i3 = queueFile.c;
        return i2 < i3 ? i2 : (i2 + 16) - i3;
    }

    public final class c extends InputStream {
        public int b;
        public int c;

        public /* synthetic */ c(b bVar, a aVar) {
            int i2 = bVar.a + 4;
            int i3 = QueueFile.this.c;
            this.b = i2 >= i3 ? (i2 + 16) - i3 : i2;
            this.c = bVar.b;
        }

        public int read(byte[] bArr, int i2, int i3) {
            if (bArr == null) {
                throw new NullPointerException("buffer");
            } else if ((i2 | i3) < 0 || i3 > bArr.length - i2) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                int i4 = this.c;
                if (i4 <= 0) {
                    return -1;
                }
                if (i3 > i4) {
                    i3 = i4;
                }
                QueueFile.this.a(this.b, bArr, i2, i3);
                this.b = QueueFile.a(QueueFile.this, this.b + i3);
                this.c -= i3;
                return i3;
            }
        }

        public int read() {
            if (this.c == 0) {
                return -1;
            }
            QueueFile.this.b.seek((long) this.b);
            int read = QueueFile.this.b.read();
            this.b = QueueFile.a(QueueFile.this, this.b + 1);
            this.c--;
            return read;
        }
    }

    public final void a(int i2, byte[] bArr, int i3, int i4) {
        int i5 = this.c;
        if (i2 >= i5) {
            i2 = (i2 + 16) - i5;
        }
        int i6 = i2 + i4;
        int i7 = this.c;
        if (i6 <= i7) {
            this.b.seek((long) i2);
            this.b.readFully(bArr, i3, i4);
            return;
        }
        int i8 = i7 - i2;
        this.b.seek((long) i2);
        this.b.readFully(bArr, i3, i8);
        this.b.seek(16);
        this.b.readFully(bArr, i3 + i8, i4 - i8);
    }

    public final void b(int i2, byte[] bArr, int i3, int i4) {
        int i5 = this.c;
        if (i2 >= i5) {
            i2 = (i2 + 16) - i5;
        }
        int i6 = i2 + i4;
        int i7 = this.c;
        if (i6 <= i7) {
            this.b.seek((long) i2);
            this.b.write(bArr, i3, i4);
            return;
        }
        int i8 = i7 - i2;
        this.b.seek((long) i2);
        this.b.write(bArr, i3, i8);
        this.b.seek(16);
        this.b.write(bArr, i3 + i8, i4 - i8);
    }

    public synchronized void a(byte[] bArr, int i2, int i3) {
        int i4;
        if (bArr == null) {
            throw new NullPointerException("buffer");
        } else if ((i2 | i3) < 0 || i3 > bArr.length - i2) {
            throw new IndexOutOfBoundsException();
        } else {
            a(i3);
            boolean f2 = f();
            if (f2) {
                i4 = 16;
            } else {
                i4 = c(this.f2641f.a + 4 + this.f2641f.b);
            }
            b bVar = new b(i4, i3);
            b(this.g, 0, i3);
            b(bVar.a, this.g, 0, 4);
            b(bVar.a + 4, bArr, i2, i3);
            a(this.c, this.d + 1, f2 ? bVar.a : this.f2640e.a, bVar.a);
            this.f2641f = bVar;
            this.d++;
            if (f2) {
                this.f2640e = bVar;
            }
        }
    }

    public final void a(int i2) {
        int i3 = i2 + 4;
        int h2 = this.c - h();
        if (h2 < i3) {
            int i4 = this.c;
            do {
                h2 += i4;
                i4 <<= 1;
            } while (h2 < i3);
            this.b.setLength((long) i4);
            this.b.getChannel().force(true);
            b bVar = this.f2641f;
            int c2 = c(bVar.a + 4 + bVar.b);
            if (c2 < this.f2640e.a) {
                FileChannel channel = this.b.getChannel();
                channel.position((long) this.c);
                long j2 = (long) (c2 - 4);
                if (channel.transferTo(16, j2, channel) != j2) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i5 = this.f2641f.a;
            int i6 = this.f2640e.a;
            if (i5 < i6) {
                int i7 = (this.c + i5) - 16;
                a(i4, this.d, i6, i7);
                this.f2641f = new b(i7, this.f2641f.b);
            } else {
                a(i4, this.d, i6, i5);
            }
            this.c = i4;
        }
    }

    public synchronized void a(d dVar) {
        int i2 = this.f2640e.a;
        for (int i3 = 0; i3 < this.d; i3++) {
            b b2 = b(i2);
            dVar.read(new c(b2, null), b2.b);
            i2 = c(b2.a + 4 + b2.b);
        }
    }

    public synchronized void a() {
        a((int) CodedOutputStream.DEFAULT_BUFFER_SIZE, 0, 0, 0);
        this.d = 0;
        this.f2640e = b.c;
        this.f2641f = b.c;
        if (this.c > 4096) {
            this.b.setLength((long) CodedOutputStream.DEFAULT_BUFFER_SIZE);
            this.b.getChannel().force(true);
        }
        this.c = CodedOutputStream.DEFAULT_BUFFER_SIZE;
    }
}
