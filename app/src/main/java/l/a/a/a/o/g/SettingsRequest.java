package l.a.a.a.o.g;

public class SettingsRequest {
    public final String a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final String f2672e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2673f;
    public final String g;
    public final String h;

    /* renamed from: i  reason: collision with root package name */
    public final int f2674i;

    /* renamed from: j  reason: collision with root package name */
    public final String f2675j;

    public SettingsRequest(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, int i2, String str9) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.f2672e = str5;
        this.f2673f = str6;
        this.g = str7;
        this.h = str8;
        this.f2674i = i2;
        this.f2675j = str9;
    }
}
