package l.a.a.a.o.b;

import java.util.Map;
import l.a.a.a.o.b.s;

public interface DeviceIdentifierProvider {
    Map<s.a, String> getDeviceIdentifiers();
}
