package l.a.a.a.o.g;

public class AppSettingsData {
    public final String a;
    public final String b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f2665e;

    public AppSettingsData(String str, String str2, String str3, String str4, String str5, boolean z, AppIconSettingsData appIconSettingsData) {
        this.a = str2;
        this.b = str3;
        this.c = str4;
        this.d = str5;
        this.f2665e = z;
    }
}
