package l.a.a.a.o.e;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
