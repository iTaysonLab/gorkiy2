package com.crashlytics.android.answers;

import com.crashlytics.android.answers.SessionEvent;
import l.a.a.a.o.d.FileRollOverManager;
import l.a.a.a.o.g.AnalyticsSettingsData;

public interface SessionAnalyticsManagerStrategy extends FileRollOverManager {
    void deleteAllEvents();

    void processEvent(SessionEvent.Builder builder);

    void sendEvents();

    void setAnalyticsSettingsData(AnalyticsSettingsData analyticsSettingsData, String str);
}
