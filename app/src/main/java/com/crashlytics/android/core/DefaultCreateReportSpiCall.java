package com.crashlytics.android.core;

import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.io.File;
import java.util.Map;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.AbstractSpiCall;
import l.a.a.a.o.e.HttpMethod;
import l.a.a.a.o.e.HttpRequestFactory;

public class DefaultCreateReportSpiCall extends AbstractSpiCall implements CreateReportSpiCall {
    public static final String FILE_CONTENT_TYPE = "application/octet-stream";
    public static final String FILE_PARAM = "report[file]";
    public static final String IDENTIFIER_PARAM = "report[identifier]";
    public static final String MULTI_FILE_PARAM = "report[file";

    public DefaultCreateReportSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, CreateReportRequest createReportRequest) {
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_API_KEY, createReportRequest.apiKey);
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_VERSION, super.kit.getVersion());
        for (Map.Entry next : createReportRequest.report.getCustomHeaders().entrySet()) {
            httpRequest.b((String) next.getKey(), (String) next.getValue());
        }
        return httpRequest;
    }

    private HttpRequest applyMultipartDataTo(HttpRequest httpRequest, Report report) {
        HttpRequest httpRequest2 = httpRequest;
        httpRequest2.c(IDENTIFIER_PARAM, report.getIdentifier());
        if (report.getFiles().length == 1) {
            DefaultLogger a = Fabric.a();
            StringBuilder a2 = outline.a("Adding single file ");
            a2.append(report.getFileName());
            a2.append(" to report ");
            a2.append(report.getIdentifier());
            String sb = a2.toString();
            if (a.a(CrashlyticsCore.TAG, 3)) {
                Log.d(CrashlyticsCore.TAG, sb, null);
            }
            httpRequest2.a(FILE_PARAM, report.getFileName(), "application/octet-stream", report.getFile());
            return httpRequest2;
        }
        int i2 = 0;
        for (File file : report.getFiles()) {
            DefaultLogger a3 = Fabric.a();
            StringBuilder a4 = outline.a("Adding file ");
            a4.append(file.getName());
            a4.append(" to report ");
            a4.append(report.getIdentifier());
            String sb2 = a4.toString();
            if (a3.a(CrashlyticsCore.TAG, 3)) {
                Log.d(CrashlyticsCore.TAG, sb2, null);
            }
            httpRequest2.a(outline.b(MULTI_FILE_PARAM, i2, "]"), file.getName(), "application/octet-stream", file);
            i2++;
        }
        return httpRequest2;
    }

    public boolean invoke(CreateReportRequest createReportRequest) {
        HttpRequest applyMultipartDataTo = applyMultipartDataTo(applyHeadersTo(getHttpRequest(), createReportRequest), createReportRequest.report);
        DefaultLogger a = Fabric.a();
        StringBuilder a2 = outline.a("Sending report to: ");
        a2.append(getUrl());
        String sb = a2.toString();
        if (a.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, sb, null);
        }
        int c = applyMultipartDataTo.c();
        DefaultLogger a3 = Fabric.a();
        StringBuilder a4 = outline.a("Create report request ID: ");
        a4.append(applyMultipartDataTo.a(AbstractSpiCall.HEADER_REQUEST_ID));
        String sb2 = a4.toString();
        if (a3.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, sb2, null);
        }
        DefaultLogger a5 = Fabric.a();
        String b = outline.b("Result was: ", c);
        if (a5.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, b, null);
        }
        return c.c(c) == 0;
    }

    public DefaultCreateReportSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, HttpMethod httpMethod) {
        super(kit, str, str2, httpRequestFactory, httpMethod);
    }
}
