package com.bumptech.glide.load.engine;

import android.util.Log;
import j.a.a.a.outline;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class GlideException extends Exception {
    public static final StackTraceElement[] g = new StackTraceElement[0];
    public final List<Throwable> b;
    public Key c;
    public DataSource d;

    /* renamed from: e  reason: collision with root package name */
    public Class<?> f372e;

    /* renamed from: f  reason: collision with root package name */
    public String f373f;

    public GlideException(String str) {
        List<Throwable> emptyList = Collections.emptyList();
        this.f373f = str;
        setStackTrace(g);
        this.b = emptyList;
    }

    public void a(String str) {
        ArrayList arrayList = new ArrayList();
        a(this, arrayList);
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            StringBuilder a2 = outline.a("Root cause (");
            int i3 = i2 + 1;
            a2.append(i3);
            a2.append(" of ");
            a2.append(size);
            a2.append(")");
            Log.i(str, a2.toString(), (Throwable) arrayList.get(i2));
            i2 = i3;
        }
    }

    public Throwable fillInStackTrace() {
        return this;
    }

    public String getMessage() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder(71);
        sb.append(this.f373f);
        String str3 = "";
        if (this.f372e != null) {
            StringBuilder a2 = outline.a(", ");
            a2.append(this.f372e);
            str = a2.toString();
        } else {
            str = str3;
        }
        sb.append(str);
        if (this.d != null) {
            StringBuilder a3 = outline.a(", ");
            a3.append(this.d);
            str2 = a3.toString();
        } else {
            str2 = str3;
        }
        sb.append(str2);
        if (this.c != null) {
            StringBuilder a4 = outline.a(", ");
            a4.append(this.c);
            str3 = a4.toString();
        }
        sb.append(str3);
        ArrayList arrayList = new ArrayList();
        a(this, arrayList);
        if (arrayList.isEmpty()) {
            return sb.toString();
        }
        if (arrayList.size() == 1) {
            sb.append("\nThere was 1 cause:");
        } else {
            sb.append("\nThere were ");
            sb.append(arrayList.size());
            sb.append(" causes:");
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Throwable th = (Throwable) it.next();
            sb.append(10);
            sb.append(th.getClass().getName());
            sb.append('(');
            sb.append(th.getMessage());
            sb.append(')');
        }
        sb.append("\n call GlideException#logRootCauses(String) for more detail");
        return sb.toString();
    }

    public void printStackTrace() {
        a(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        a(printStream);
    }

    public void printStackTrace(PrintWriter printWriter) {
        a(printWriter);
    }

    public static final class a implements Appendable {
        public final Appendable b;
        public boolean c = true;

        public a(Appendable appendable) {
            this.b = appendable;
        }

        public Appendable append(char c2) {
            boolean z = false;
            if (this.c) {
                this.c = false;
                this.b.append("  ");
            }
            if (c2 == 10) {
                z = true;
            }
            this.c = z;
            this.b.append(c2);
            return this;
        }

        public Appendable append(CharSequence charSequence) {
            if (charSequence == null) {
                charSequence = "";
            }
            append(charSequence, 0, charSequence.length());
            return this;
        }

        public Appendable append(CharSequence charSequence, int i2, int i3) {
            if (charSequence == null) {
                charSequence = "";
            }
            boolean z = false;
            if (this.c) {
                this.c = false;
                this.b.append("  ");
            }
            if (charSequence.length() > 0 && charSequence.charAt(i3 - 1) == 10) {
                z = true;
            }
            this.c = z;
            this.b.append(charSequence, i2, i3);
            return this;
        }
    }

    public final void a(Throwable th, List<Throwable> list) {
        if (th instanceof GlideException) {
            for (Throwable a2 : ((GlideException) th).b) {
                a(a2, list);
            }
            return;
        }
        list.add(th);
    }

    public GlideException(String str, Throwable th) {
        List<Throwable> singletonList = Collections.singletonList(th);
        this.f373f = str;
        setStackTrace(g);
        this.b = singletonList;
    }

    public GlideException(String str, List<Throwable> list) {
        this.f373f = str;
        setStackTrace(g);
        this.b = list;
    }

    public final void a(Appendable appendable) {
        a(this, appendable);
        try {
            a(this.b, new a(appendable));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static void a(Throwable th, Appendable appendable) {
        try {
            appendable.append(th.getClass().toString()).append(": ").append(th.getMessage()).append(10);
        } catch (IOException unused) {
            throw new RuntimeException(th);
        }
    }

    public static void a(List<Throwable> list, Appendable appendable) {
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            int i3 = i2 + 1;
            appendable.append("Cause (").append(String.valueOf(i3)).append(" of ").append(String.valueOf(size)).append("): ");
            Throwable th = list.get(i2);
            if (th instanceof GlideException) {
                ((GlideException) th).a(appendable);
            } else {
                a(th, appendable);
            }
            i2 = i3;
        }
    }
}
