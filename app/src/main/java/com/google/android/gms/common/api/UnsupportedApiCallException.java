package com.google.android.gms.common.api;

import j.c.a.a.c.d;

public final class UnsupportedApiCallException extends UnsupportedOperationException {
    public final d b;

    public UnsupportedApiCallException(d dVar) {
        this.b = dVar;
    }

    public final String getMessage() {
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(valueOf.length() + 8);
        sb.append("Missing ");
        sb.append(valueOf);
        return sb.toString();
    }
}
