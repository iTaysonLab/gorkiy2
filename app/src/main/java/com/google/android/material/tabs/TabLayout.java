package com.google.android.material.tabs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import e.a.a.a.a.LoginPagerAdapter;
import e.a.a.a.a.LoginPagerAdapter1;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.h.k.Pools;
import i.h.k.Pools0;
import i.h.k.Pools1;
import i.h.l.PointerIconCompat;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.z.a.PagerAdapter;
import j.c.a.b.b0.ThemeEnforcement;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.j0.TabItem;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.m.AnimationUtils;
import j.c.a.b.m0.a.MaterialThemeOverlay;
import j.c.a.b.o.BadgeDrawable;
import j.c.a.b.o.BadgeUtils;
import j.c.a.b.y.ElevationOverlayProvider;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ViewPager.e
public class TabLayout extends HorizontalScrollView {
    public static final int P = k.Widget_Design_TabLayout;
    public static final Pools<g> Q = new Pools1(16);
    public int A;
    public boolean B;
    public boolean C;
    public boolean D;
    public c E;
    public final ArrayList<c> F;
    public c G;
    public ValueAnimator H;
    public ViewPager I;
    public PagerAdapter J;
    public DataSetObserver K;
    public h L;
    public b M;
    public boolean N;
    public final Pools<i> O;
    public final ArrayList<g> b;
    public g c;
    public final RectF d;

    /* renamed from: e  reason: collision with root package name */
    public final f f499e;

    /* renamed from: f  reason: collision with root package name */
    public int f500f;
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int f501i;

    /* renamed from: j  reason: collision with root package name */
    public int f502j;

    /* renamed from: k  reason: collision with root package name */
    public ColorStateList f503k;

    /* renamed from: l  reason: collision with root package name */
    public ColorStateList f504l;

    /* renamed from: m  reason: collision with root package name */
    public ColorStateList f505m;

    /* renamed from: n  reason: collision with root package name */
    public Drawable f506n;

    /* renamed from: o  reason: collision with root package name */
    public PorterDuff.Mode f507o;

    /* renamed from: p  reason: collision with root package name */
    public float f508p;

    /* renamed from: q  reason: collision with root package name */
    public float f509q;

    /* renamed from: r  reason: collision with root package name */
    public final int f510r;

    /* renamed from: s  reason: collision with root package name */
    public int f511s;

    /* renamed from: t  reason: collision with root package name */
    public final int f512t;
    public final int u;
    public final int v;
    public int w;
    public int x;
    public int y;
    public int z;

    public class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            TabLayout.this.scrollTo(((Integer) valueAnimator.getAnimatedValue()).intValue(), 0);
        }
    }

    public class b implements ViewPager.i {
        public boolean a;

        public b() {
        }

        public void a(ViewPager viewPager, PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
            TabLayout tabLayout = TabLayout.this;
            if (tabLayout.I == viewPager) {
                tabLayout.a(pagerAdapter2, this.a);
            }
        }
    }

    @Deprecated
    public interface c<T extends g> {
        void a(T t2);

        void b(T t2);

        void c(T t2);
    }

    public interface d extends c<g> {
    }

    public class e extends DataSetObserver {
        public e() {
        }

        public void onChanged() {
            TabLayout.this.d();
        }

        public void onInvalidated() {
            TabLayout.this.d();
        }
    }

    public static class j implements d {
        public final ViewPager a;

        public j(ViewPager viewPager) {
            this.a = viewPager;
        }

        public void a(g gVar) {
        }

        public void b(g gVar) {
            this.a.setCurrentItem(gVar.d);
        }

        public void c(g gVar) {
        }
    }

    public TabLayout(Context context) {
        this(context, null);
    }

    private int getDefaultHeight() {
        int size = this.b.size();
        boolean z2 = false;
        int i2 = 0;
        while (true) {
            if (i2 < size) {
                g gVar = this.b.get(i2);
                if (gVar != null && gVar.a != null && !TextUtils.isEmpty(gVar.b)) {
                    z2 = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        return (!z2 || this.B) ? 48 : 72;
    }

    private int getTabMinWidth() {
        int i2 = this.f512t;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A;
        if (i3 == 0 || i3 == 2) {
            return this.v;
        }
        return 0;
    }

    private int getTabScrollRange() {
        return Math.max(0, ((this.f499e.getWidth() - getWidth()) - getPaddingLeft()) - getPaddingRight());
    }

    private void setSelectedTabView(int i2) {
        int childCount = this.f499e.getChildCount();
        if (i2 < childCount) {
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = this.f499e.getChildAt(i3);
                boolean z2 = true;
                childAt.setSelected(i3 == i2);
                if (i3 != i2) {
                    z2 = false;
                }
                childAt.setActivated(z2);
                i3++;
            }
        }
    }

    public void a(int i2, float f2, boolean z2, boolean z3) {
        int round = Math.round(((float) i2) + f2);
        if (round >= 0 && round < this.f499e.getChildCount()) {
            if (z3) {
                f fVar = this.f499e;
                ValueAnimator valueAnimator = fVar.f516j;
                if (valueAnimator != null && valueAnimator.isRunning()) {
                    fVar.f516j.cancel();
                }
                fVar.f513e = i2;
                fVar.f514f = f2;
                fVar.a();
            }
            ValueAnimator valueAnimator2 = this.H;
            if (valueAnimator2 != null && valueAnimator2.isRunning()) {
                this.H.cancel();
            }
            scrollTo(a(i2, f2), 0);
            if (z2) {
                setSelectedTabView(round);
            }
        }
    }

    public void addView(View view) {
        a(view);
    }

    public g b(int i2) {
        if (i2 < 0 || i2 >= getTabCount()) {
            return null;
        }
        return this.b.get(i2);
    }

    public g c() {
        g a2 = Q.a();
        if (a2 == null) {
            a2 = new g();
        }
        a2.g = this;
        Pools<i> pools = this.O;
        i a3 = pools != null ? pools.a() : null;
        if (a3 == null) {
            a3 = new i(getContext());
        }
        a3.setTab(a2);
        a3.setFocusable(true);
        a3.setMinimumWidth(getTabMinWidth());
        if (TextUtils.isEmpty(a2.c)) {
            a3.setContentDescription(a2.b);
        } else {
            a3.setContentDescription(a2.c);
        }
        a2.h = a3;
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.tabs.TabLayout.a(com.google.android.material.tabs.TabLayout$g, boolean):void
     arg types: [com.google.android.material.tabs.TabLayout$g, int]
     candidates:
      com.google.android.material.tabs.TabLayout.a(int, float):int
      com.google.android.material.tabs.TabLayout.a(i.z.a.PagerAdapter, boolean):void
      com.google.android.material.tabs.TabLayout.a(com.google.android.material.tabs.TabLayout$g, boolean):void */
    public void d() {
        int currentItem;
        for (int childCount = this.f499e.getChildCount() - 1; childCount >= 0; childCount--) {
            i iVar = (i) this.f499e.getChildAt(childCount);
            this.f499e.removeViewAt(childCount);
            if (iVar != null) {
                iVar.setTab(null);
                iVar.setSelected(false);
                this.O.a(iVar);
            }
            requestLayout();
        }
        Iterator<g> it = this.b.iterator();
        while (it.hasNext()) {
            g next = it.next();
            it.remove();
            next.g = null;
            next.h = null;
            next.a = null;
            next.b = null;
            next.c = null;
            next.d = -1;
            next.f520e = null;
            Q.a(next);
        }
        this.c = null;
        PagerAdapter pagerAdapter = this.J;
        if (pagerAdapter != null) {
            int a2 = pagerAdapter.a();
            int i2 = 0;
            while (i2 < a2) {
                g c2 = c();
                LoginPagerAdapter1 loginPagerAdapter1 = (LoginPagerAdapter1) this.J;
                if (loginPagerAdapter1 != null) {
                    c2.a(loginPagerAdapter1.f583j.getString(LoginPagerAdapter.values()[i2].titleResId));
                    a(c2, false);
                    i2++;
                } else {
                    throw null;
                }
            }
            ViewPager viewPager = this.I;
            if (viewPager != null && a2 > 0 && (currentItem = viewPager.getCurrentItem()) != getSelectedTabPosition() && currentItem < getTabCount()) {
                b(b(currentItem), true);
            }
        }
    }

    public final void e() {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.get(i2).a();
        }
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    public int getSelectedTabPosition() {
        g gVar = this.c;
        if (gVar != null) {
            return gVar.d;
        }
        return -1;
    }

    public int getTabCount() {
        return this.b.size();
    }

    public int getTabGravity() {
        return this.x;
    }

    public ColorStateList getTabIconTint() {
        return this.f504l;
    }

    public int getTabIndicatorGravity() {
        return this.z;
    }

    public int getTabMaxWidth() {
        return this.f511s;
    }

    public int getTabMode() {
        return this.A;
    }

    public ColorStateList getTabRippleColor() {
        return this.f505m;
    }

    public Drawable getTabSelectedIndicator() {
        return this.f506n;
    }

    public ColorStateList getTabTextColors() {
        return this.f503k;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Drawable background = getBackground();
        if (background instanceof MaterialShapeDrawable) {
            j.c.a.a.c.n.c.a(this, (MaterialShapeDrawable) background);
        }
        if (this.I == null) {
            ViewParent parent = getParent();
            if (parent instanceof ViewPager) {
                a((ViewPager) parent, true, true);
            }
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.N) {
            setupWithViewPager(null);
            this.N = false;
        }
    }

    public void onDraw(Canvas canvas) {
        i iVar;
        Drawable drawable;
        for (int i2 = 0; i2 < this.f499e.getChildCount(); i2++) {
            View childAt = this.f499e.getChildAt(i2);
            if ((childAt instanceof i) && (drawable = (iVar = (i) childAt).f525j) != null) {
                drawable.setBounds(iVar.getLeft(), iVar.getTop(), iVar.getRight(), iVar.getBottom());
                iVar.f525j.draw(canvas);
            }
        }
        super.onDraw(canvas);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        new AccessibilityNodeInfoCompat(accessibilityNodeInfo).a(AccessibilityNodeInfoCompat.b.a(1, getTabCount(), false, 1));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        if (r0 != 2) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007e, code lost:
        if (r7.getMeasuredWidth() != getMeasuredWidth()) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0080, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008a, code lost:
        if (r7.getMeasuredWidth() < getMeasuredWidth()) goto L_0x0080;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r7, int r8) {
        /*
            r6 = this;
            android.content.Context r0 = r6.getContext()
            int r1 = r6.getDefaultHeight()
            float r0 = j.c.a.a.c.n.c.a(r0, r1)
            int r0 = java.lang.Math.round(r0)
            int r1 = android.view.View.MeasureSpec.getMode(r8)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 1073741824(0x40000000, float:2.0)
            r4 = 0
            r5 = 1
            if (r1 == r2) goto L_0x002e
            if (r1 == 0) goto L_0x001f
            goto L_0x0041
        L_0x001f:
            int r8 = r6.getPaddingTop()
            int r8 = r8 + r0
            int r0 = r6.getPaddingBottom()
            int r0 = r0 + r8
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r3)
            goto L_0x0041
        L_0x002e:
            int r1 = r6.getChildCount()
            if (r1 != r5) goto L_0x0041
            int r1 = android.view.View.MeasureSpec.getSize(r8)
            if (r1 < r0) goto L_0x0041
            android.view.View r1 = r6.getChildAt(r4)
            r1.setMinimumHeight(r0)
        L_0x0041:
            int r0 = android.view.View.MeasureSpec.getSize(r7)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            if (r1 == 0) goto L_0x005f
            int r1 = r6.u
            if (r1 <= 0) goto L_0x0050
            goto L_0x005d
        L_0x0050:
            float r0 = (float) r0
            android.content.Context r1 = r6.getContext()
            r2 = 56
            float r1 = j.c.a.a.c.n.c.a(r1, r2)
            float r0 = r0 - r1
            int r1 = (int) r0
        L_0x005d:
            r6.f511s = r1
        L_0x005f:
            super.onMeasure(r7, r8)
            int r7 = r6.getChildCount()
            if (r7 != r5) goto L_0x00ad
            android.view.View r7 = r6.getChildAt(r4)
            int r0 = r6.A
            if (r0 == 0) goto L_0x0082
            if (r0 == r5) goto L_0x0076
            r1 = 2
            if (r0 == r1) goto L_0x0082
            goto L_0x008d
        L_0x0076:
            int r0 = r7.getMeasuredWidth()
            int r1 = r6.getMeasuredWidth()
            if (r0 == r1) goto L_0x008d
        L_0x0080:
            r4 = 1
            goto L_0x008d
        L_0x0082:
            int r0 = r7.getMeasuredWidth()
            int r1 = r6.getMeasuredWidth()
            if (r0 >= r1) goto L_0x008d
            goto L_0x0080
        L_0x008d:
            if (r4 == 0) goto L_0x00ad
            int r0 = r6.getPaddingTop()
            int r1 = r6.getPaddingBottom()
            int r1 = r1 + r0
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            int r0 = r0.height
            int r8 = android.widget.HorizontalScrollView.getChildMeasureSpec(r8, r1, r0)
            int r0 = r6.getMeasuredWidth()
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r3)
            r7.measure(r0, r8)
        L_0x00ad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.onMeasure(int, int):void");
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        j.c.a.a.c.n.c.a(this, f2);
    }

    public void setInlineLabel(boolean z2) {
        if (this.B != z2) {
            this.B = z2;
            for (int i2 = 0; i2 < this.f499e.getChildCount(); i2++) {
                View childAt = this.f499e.getChildAt(i2);
                if (childAt instanceof i) {
                    i iVar = (i) childAt;
                    iVar.setOrientation(TabLayout.this.B ^ true ? 1 : 0);
                    if (iVar.h == null && iVar.f524i == null) {
                        iVar.a(iVar.c, iVar.d);
                    } else {
                        iVar.a(iVar.h, iVar.f524i);
                    }
                }
            }
            a();
        }
    }

    public void setInlineLabelResource(int i2) {
        setInlineLabel(getResources().getBoolean(i2));
    }

    @Deprecated
    public void setOnTabSelectedListener(d dVar) {
        setOnTabSelectedListener((c) dVar);
    }

    public void setScrollAnimatorListener(Animator.AnimatorListener animatorListener) {
        b();
        this.H.addListener(animatorListener);
    }

    public void setSelectedTabIndicator(Drawable drawable) {
        if (this.f506n != drawable) {
            this.f506n = drawable;
            ViewCompat.A(this.f499e);
        }
    }

    public void setSelectedTabIndicatorColor(int i2) {
        f fVar = this.f499e;
        if (fVar.c.getColor() != i2) {
            fVar.c.setColor(i2);
            ViewCompat.A(fVar);
        }
    }

    public void setSelectedTabIndicatorGravity(int i2) {
        if (this.z != i2) {
            this.z = i2;
            ViewCompat.A(this.f499e);
        }
    }

    @Deprecated
    public void setSelectedTabIndicatorHeight(int i2) {
        f fVar = this.f499e;
        if (fVar.b != i2) {
            fVar.b = i2;
            ViewCompat.A(fVar);
        }
    }

    public void setTabGravity(int i2) {
        if (this.x != i2) {
            this.x = i2;
            a();
        }
    }

    public void setTabIconTint(ColorStateList colorStateList) {
        if (this.f504l != colorStateList) {
            this.f504l = colorStateList;
            e();
        }
    }

    public void setTabIconTintResource(int i2) {
        setTabIconTint(AppCompatResources.b(getContext(), i2));
    }

    public void setTabIndicatorFullWidth(boolean z2) {
        this.C = z2;
        ViewCompat.A(this.f499e);
    }

    public void setTabMode(int i2) {
        if (i2 != this.A) {
            this.A = i2;
            a();
        }
    }

    public void setTabRippleColor(ColorStateList colorStateList) {
        if (this.f505m != colorStateList) {
            this.f505m = colorStateList;
            for (int i2 = 0; i2 < this.f499e.getChildCount(); i2++) {
                View childAt = this.f499e.getChildAt(i2);
                if (childAt instanceof i) {
                    ((i) childAt).a(getContext());
                }
            }
        }
    }

    public void setTabRippleColorResource(int i2) {
        setTabRippleColor(AppCompatResources.b(getContext(), i2));
    }

    public void setTabTextColors(ColorStateList colorStateList) {
        if (this.f503k != colorStateList) {
            this.f503k = colorStateList;
            e();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.tabs.TabLayout.a(i.z.a.PagerAdapter, boolean):void
     arg types: [i.z.a.PagerAdapter, int]
     candidates:
      com.google.android.material.tabs.TabLayout.a(int, float):int
      com.google.android.material.tabs.TabLayout.a(com.google.android.material.tabs.TabLayout$g, boolean):void
      com.google.android.material.tabs.TabLayout.a(i.z.a.PagerAdapter, boolean):void */
    @Deprecated
    public void setTabsFromPagerAdapter(PagerAdapter pagerAdapter) {
        a(pagerAdapter, false);
    }

    public void setUnboundedRipple(boolean z2) {
        if (this.D != z2) {
            this.D = z2;
            for (int i2 = 0; i2 < this.f499e.getChildCount(); i2++) {
                View childAt = this.f499e.getChildAt(i2);
                if (childAt instanceof i) {
                    ((i) childAt).a(getContext());
                }
            }
        }
    }

    public void setUnboundedRippleResource(int i2) {
        setUnboundedRipple(getResources().getBoolean(i2));
    }

    public void setupWithViewPager(ViewPager viewPager) {
        a(viewPager, true, false);
    }

    public boolean shouldDelayChildPressedState() {
        return getTabScrollRange() > 0;
    }

    public static class h implements ViewPager.j {
        public final WeakReference<TabLayout> a;
        public int b;
        public int c;

        public h(TabLayout tabLayout) {
            this.a = new WeakReference<>(tabLayout);
        }

        public void a(int i2) {
            this.b = this.c;
            this.c = i2;
        }

        public void b(int i2) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i2 && i2 < tabLayout.getTabCount()) {
                int i3 = this.c;
                tabLayout.b(tabLayout.b(i2), i3 == 0 || (i3 == 2 && this.b == 0));
            }
        }

        public void a(int i2, float f2, int i3) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null) {
                boolean z = false;
                boolean z2 = this.c != 2 || this.b == 1;
                if (!(this.c == 2 && this.b == 0)) {
                    z = true;
                }
                tabLayout.a(i2, f2, z2, z);
            }
        }
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.tabStyle);
    }

    public void addView(View view, int i2) {
        a(view);
    }

    public final void b() {
        if (this.H == null) {
            ValueAnimator valueAnimator = new ValueAnimator();
            this.H = valueAnimator;
            valueAnimator.setInterpolator(AnimationUtils.b);
            this.H.setDuration((long) this.y);
            this.H.addUpdateListener(new a());
        }
    }

    /* renamed from: generateLayoutParams  reason: collision with other method in class */
    public FrameLayout.LayoutParams m4generateLayoutParams(AttributeSet attributeSet) {
        return generateDefaultLayoutParams();
    }

    @Deprecated
    public void setOnTabSelectedListener(c cVar) {
        c cVar2 = this.E;
        if (cVar2 != null) {
            this.F.remove(cVar2);
        }
        this.E = cVar;
        if (cVar != null && !this.F.contains(cVar)) {
            this.F.add(cVar);
        }
    }

    public final class i extends LinearLayout {
        public g b;
        public TextView c;
        public ImageView d;

        /* renamed from: e  reason: collision with root package name */
        public View f522e;

        /* renamed from: f  reason: collision with root package name */
        public BadgeDrawable f523f;
        public View g;
        public TextView h;

        /* renamed from: i  reason: collision with root package name */
        public ImageView f524i;

        /* renamed from: j  reason: collision with root package name */
        public Drawable f525j;

        /* renamed from: k  reason: collision with root package name */
        public int f526k = 2;

        public i(Context context) {
            super(context);
            PointerIconCompat pointerIconCompat;
            a(context);
            setPaddingRelative(TabLayout.this.f500f, TabLayout.this.g, TabLayout.this.h, TabLayout.this.f501i);
            setGravity(17);
            setOrientation(TabLayout.this.B ^ true ? 1 : 0);
            setClickable(true);
            Context context2 = getContext();
            if (Build.VERSION.SDK_INT >= 24) {
                pointerIconCompat = new PointerIconCompat(PointerIcon.getSystemIcon(context2, 1002));
            } else {
                pointerIconCompat = new PointerIconCompat(null);
            }
            if (Build.VERSION.SDK_INT >= 24) {
                setPointerIcon((PointerIcon) pointerIconCompat.a);
            }
        }

        private BadgeDrawable getBadge() {
            return this.f523f;
        }

        /* access modifiers changed from: private */
        public int getContentWidth() {
            View[] viewArr = {this.c, this.d, this.g};
            int i2 = 0;
            int i3 = 0;
            boolean z = false;
            for (int i4 = 0; i4 < 3; i4++) {
                View view = viewArr[i4];
                if (view != null && view.getVisibility() == 0) {
                    i3 = z ? Math.min(i3, view.getLeft()) : view.getLeft();
                    i2 = z ? Math.max(i2, view.getRight()) : view.getRight();
                    z = true;
                }
            }
            return i2 - i3;
        }

        private BadgeDrawable getOrCreateBadge() {
            if (this.f523f == null) {
                Context context = getContext();
                int i2 = BadgeDrawable.f2306s;
                int i3 = BadgeDrawable.f2305r;
                BadgeDrawable badgeDrawable = new BadgeDrawable(context);
                TypedArray b2 = ThemeEnforcement.b(context, null, l.Badge, i2, i3, new int[0]);
                int i4 = b2.getInt(l.Badge_maxCharacterCount, 4);
                BadgeDrawable.a aVar = badgeDrawable.f2309i;
                if (aVar.f2319f != i4) {
                    aVar.f2319f = i4;
                    badgeDrawable.f2312l = ((int) Math.pow(10.0d, ((double) i4) - 1.0d)) - 1;
                    badgeDrawable.d.d = true;
                    badgeDrawable.e();
                    badgeDrawable.invalidateSelf();
                }
                if (b2.hasValue(l.Badge_number)) {
                    int max = Math.max(0, b2.getInt(l.Badge_number, 0));
                    BadgeDrawable.a aVar2 = badgeDrawable.f2309i;
                    if (aVar2.f2318e != max) {
                        aVar2.f2318e = max;
                        badgeDrawable.d.d = true;
                        badgeDrawable.e();
                        badgeDrawable.invalidateSelf();
                    }
                }
                int defaultColor = j.c.a.a.c.n.c.a(context, b2, l.Badge_backgroundColor).getDefaultColor();
                badgeDrawable.f2309i.b = defaultColor;
                ColorStateList valueOf = ColorStateList.valueOf(defaultColor);
                MaterialShapeDrawable materialShapeDrawable = badgeDrawable.c;
                if (materialShapeDrawable.b.d != valueOf) {
                    materialShapeDrawable.a(valueOf);
                    badgeDrawable.invalidateSelf();
                }
                if (b2.hasValue(l.Badge_badgeTextColor)) {
                    int defaultColor2 = j.c.a.a.c.n.c.a(context, b2, l.Badge_badgeTextColor).getDefaultColor();
                    badgeDrawable.f2309i.c = defaultColor2;
                    if (badgeDrawable.d.a.getColor() != defaultColor2) {
                        badgeDrawable.d.a.setColor(defaultColor2);
                        badgeDrawable.invalidateSelf();
                    }
                }
                int i5 = b2.getInt(l.Badge_badgeGravity, 8388661);
                BadgeDrawable.a aVar3 = badgeDrawable.f2309i;
                if (aVar3.f2320i != i5) {
                    aVar3.f2320i = i5;
                    WeakReference<View> weakReference = badgeDrawable.f2316p;
                    if (!(weakReference == null || weakReference.get() == null)) {
                        View view = badgeDrawable.f2316p.get();
                        WeakReference<ViewGroup> weakReference2 = badgeDrawable.f2317q;
                        ViewGroup viewGroup = weakReference2 != null ? weakReference2.get() : null;
                        badgeDrawable.f2316p = new WeakReference<>(view);
                        badgeDrawable.f2317q = new WeakReference<>(viewGroup);
                        badgeDrawable.e();
                        badgeDrawable.invalidateSelf();
                    }
                }
                badgeDrawable.f2309i.f2321j = b2.getDimensionPixelOffset(l.Badge_horizontalOffset, 0);
                badgeDrawable.e();
                badgeDrawable.f2309i.f2322k = b2.getDimensionPixelOffset(l.Badge_verticalOffset, 0);
                badgeDrawable.e();
                b2.recycle();
                this.f523f = badgeDrawable;
            }
            c();
            BadgeDrawable badgeDrawable2 = this.f523f;
            if (badgeDrawable2 != null) {
                return badgeDrawable2;
            }
            throw new IllegalStateException("Unable to create badge");
        }

        public final void b() {
            if (a() && this.f522e != null) {
                setClipChildren(true);
                setClipToPadding(true);
                BadgeDrawable badgeDrawable = this.f523f;
                View view = this.f522e;
                if (badgeDrawable != null) {
                    view.getOverlay().remove(badgeDrawable);
                }
                this.f522e = null;
            }
        }

        public final void c() {
            g gVar;
            g gVar2;
            if (a()) {
                if (this.g != null) {
                    b();
                    return;
                }
                ImageView imageView = this.d;
                if (imageView == null || (gVar2 = this.b) == null || gVar2.a == null) {
                    TextView textView = this.c;
                    if (textView == null || (gVar = this.b) == null || gVar.f521f != 1) {
                        b();
                    } else if (this.f522e != textView) {
                        b();
                        a(this.c);
                    } else {
                        b(textView);
                    }
                } else if (this.f522e != imageView) {
                    b();
                    a(this.d);
                } else {
                    b(imageView);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, com.google.android.material.tabs.TabLayout$i, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final void d() {
            Drawable drawable;
            g gVar = this.b;
            Drawable drawable2 = null;
            View view = gVar != null ? gVar.f520e : null;
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(view);
                    }
                    addView(view);
                }
                this.g = view;
                TextView textView = this.c;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.d;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.d.setImageDrawable(null);
                }
                TextView textView2 = (TextView) view.findViewById(16908308);
                this.h = textView2;
                if (textView2 != null) {
                    this.f526k = textView2.getMaxLines();
                }
                this.f524i = (ImageView) view.findViewById(16908294);
            } else {
                View view2 = this.g;
                if (view2 != null) {
                    removeView(view2);
                    this.g = null;
                }
                this.h = null;
                this.f524i = null;
            }
            boolean z = false;
            if (this.g == null) {
                if (this.d == null) {
                    ImageView imageView2 = (ImageView) LayoutInflater.from(getContext()).inflate(j.c.a.b.h.design_layout_tab_icon, (ViewGroup) this, false);
                    this.d = imageView2;
                    addView(imageView2, 0);
                }
                if (!(gVar == null || (drawable = gVar.a) == null)) {
                    drawable2 = ResourcesFlusher.d(drawable).mutate();
                }
                if (drawable2 != null) {
                    drawable2.setTintList(TabLayout.this.f504l);
                    PorterDuff.Mode mode = TabLayout.this.f507o;
                    if (mode != null) {
                        drawable2.setTintMode(mode);
                    }
                }
                if (this.c == null) {
                    TextView textView3 = (TextView) LayoutInflater.from(getContext()).inflate(j.c.a.b.h.design_layout_tab_text, (ViewGroup) this, false);
                    this.c = textView3;
                    addView(textView3);
                    this.f526k = this.c.getMaxLines();
                }
                ResourcesFlusher.d(this.c, TabLayout.this.f502j);
                ColorStateList colorStateList = TabLayout.this.f503k;
                if (colorStateList != null) {
                    this.c.setTextColor(colorStateList);
                }
                a(this.c, this.d);
                c();
                ImageView imageView3 = this.d;
                if (imageView3 != null) {
                    imageView3.addOnLayoutChangeListener(new j.c.a.b.j0.TabLayout(this, imageView3));
                }
                TextView textView4 = this.c;
                if (textView4 != null) {
                    textView4.addOnLayoutChangeListener(new j.c.a.b.j0.TabLayout(this, textView4));
                }
            } else if (!(this.h == null && this.f524i == null)) {
                a(this.h, this.f524i);
            }
            if (gVar != null && !TextUtils.isEmpty(gVar.c)) {
                setContentDescription(gVar.c);
            }
            if (gVar != null) {
                TabLayout tabLayout = gVar.g;
                if (tabLayout != null) {
                    if (tabLayout.getSelectedTabPosition() == gVar.d) {
                        z = true;
                    }
                } else {
                    throw new IllegalArgumentException("Tab not attached to a TabLayout");
                }
            }
            setSelected(z);
        }

        public void drawableStateChanged() {
            super.drawableStateChanged();
            int[] drawableState = getDrawableState();
            Drawable drawable = this.f525j;
            boolean z = false;
            if (drawable != null && drawable.isStateful()) {
                z = false | this.f525j.setState(drawableState);
            }
            if (z) {
                invalidate();
                TabLayout.this.invalidate();
            }
        }

        public g getTab() {
            return this.b;
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            Context context;
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            BadgeDrawable badgeDrawable = this.f523f;
            if (badgeDrawable != null && badgeDrawable.isVisible()) {
                CharSequence contentDescription = getContentDescription();
                StringBuilder sb = new StringBuilder();
                sb.append((Object) contentDescription);
                sb.append(", ");
                BadgeDrawable badgeDrawable2 = this.f523f;
                Object obj = null;
                if (badgeDrawable2.isVisible()) {
                    if (!badgeDrawable2.d()) {
                        obj = badgeDrawable2.f2309i.g;
                    } else if (badgeDrawable2.f2309i.h > 0 && (context = badgeDrawable2.b.get()) != null) {
                        obj = context.getResources().getQuantityString(badgeDrawable2.f2309i.h, badgeDrawable2.c(), Integer.valueOf(badgeDrawable2.c()));
                    }
                }
                sb.append(obj);
                accessibilityNodeInfo.setContentDescription(sb.toString());
            }
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
            accessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.c.a(0, 1, this.b.d, 1, false, isSelected()));
            if (isSelected()) {
                accessibilityNodeInfoCompat.a.setClickable(false);
                accessibilityNodeInfoCompat.a.removeAction((AccessibilityNodeInfo.AccessibilityAction) AccessibilityNodeInfoCompat.a.f1202e.a);
            }
            accessibilityNodeInfoCompat.a.getExtras().putCharSequence("AccessibilityNodeInfo.roleDescription", "Tab");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0094, code lost:
            if (((r0 / r2.getPaint().getTextSize()) * r2.getLineWidth(0)) > ((float) ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()))) goto L_0x0096;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onMeasure(int r8, int r9) {
            /*
                r7 = this;
                int r0 = android.view.View.MeasureSpec.getSize(r8)
                int r1 = android.view.View.MeasureSpec.getMode(r8)
                com.google.android.material.tabs.TabLayout r2 = com.google.android.material.tabs.TabLayout.this
                int r2 = r2.getTabMaxWidth()
                if (r2 <= 0) goto L_0x001e
                if (r1 == 0) goto L_0x0014
                if (r0 <= r2) goto L_0x001e
            L_0x0014:
                com.google.android.material.tabs.TabLayout r8 = com.google.android.material.tabs.TabLayout.this
                int r8 = r8.f511s
                r0 = -2147483648(0xffffffff80000000, float:-0.0)
                int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r8, r0)
            L_0x001e:
                super.onMeasure(r8, r9)
                android.widget.TextView r0 = r7.c
                if (r0 == 0) goto L_0x00a6
                com.google.android.material.tabs.TabLayout r0 = com.google.android.material.tabs.TabLayout.this
                float r0 = r0.f508p
                int r1 = r7.f526k
                android.widget.ImageView r2 = r7.d
                r3 = 1
                if (r2 == 0) goto L_0x0038
                int r2 = r2.getVisibility()
                if (r2 != 0) goto L_0x0038
                r1 = 1
                goto L_0x0046
            L_0x0038:
                android.widget.TextView r2 = r7.c
                if (r2 == 0) goto L_0x0046
                int r2 = r2.getLineCount()
                if (r2 <= r3) goto L_0x0046
                com.google.android.material.tabs.TabLayout r0 = com.google.android.material.tabs.TabLayout.this
                float r0 = r0.f509q
            L_0x0046:
                android.widget.TextView r2 = r7.c
                float r2 = r2.getTextSize()
                android.widget.TextView r4 = r7.c
                int r4 = r4.getLineCount()
                android.widget.TextView r5 = r7.c
                int r5 = r5.getMaxLines()
                int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r2 != 0) goto L_0x0060
                if (r5 < 0) goto L_0x00a6
                if (r1 == r5) goto L_0x00a6
            L_0x0060:
                com.google.android.material.tabs.TabLayout r5 = com.google.android.material.tabs.TabLayout.this
                int r5 = r5.A
                r6 = 0
                if (r5 != r3) goto L_0x0097
                if (r2 <= 0) goto L_0x0097
                if (r4 != r3) goto L_0x0097
                android.widget.TextView r2 = r7.c
                android.text.Layout r2 = r2.getLayout()
                if (r2 == 0) goto L_0x0096
                float r4 = r2.getLineWidth(r6)
                android.text.TextPaint r2 = r2.getPaint()
                float r2 = r2.getTextSize()
                float r2 = r0 / r2
                float r2 = r2 * r4
                int r4 = r7.getMeasuredWidth()
                int r5 = r7.getPaddingLeft()
                int r4 = r4 - r5
                int r5 = r7.getPaddingRight()
                int r4 = r4 - r5
                float r4 = (float) r4
                int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r2 <= 0) goto L_0x0097
            L_0x0096:
                r3 = 0
            L_0x0097:
                if (r3 == 0) goto L_0x00a6
                android.widget.TextView r2 = r7.c
                r2.setTextSize(r6, r0)
                android.widget.TextView r0 = r7.c
                r0.setMaxLines(r1)
                super.onMeasure(r8, r9)
            L_0x00a6:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.i.onMeasure(int, int):void");
        }

        public boolean performClick() {
            boolean performClick = super.performClick();
            if (this.b == null) {
                return performClick;
            }
            if (!performClick) {
                playSoundEffect(0);
            }
            g gVar = this.b;
            TabLayout tabLayout = gVar.g;
            if (tabLayout != null) {
                tabLayout.b(gVar, true);
                return true;
            }
            throw new IllegalArgumentException("Tab not attached to a TabLayout");
        }

        public void setSelected(boolean z) {
            if (isSelected() != z) {
            }
            super.setSelected(z);
            TextView textView = this.c;
            if (textView != null) {
                textView.setSelected(z);
            }
            ImageView imageView = this.d;
            if (imageView != null) {
                imageView.setSelected(z);
            }
            View view = this.g;
            if (view != null) {
                view.setSelected(z);
            }
        }

        public void setTab(g gVar) {
            if (gVar != this.b) {
                this.b = gVar;
                d();
            }
        }

        /* JADX WARN: Type inference failed for: r3v5, types: [android.graphics.drawable.RippleDrawable] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(android.content.Context r8) {
            /*
                r7 = this;
                com.google.android.material.tabs.TabLayout r0 = com.google.android.material.tabs.TabLayout.this
                int r0 = r0.f510r
                r1 = 0
                if (r0 == 0) goto L_0x001f
                android.graphics.drawable.Drawable r8 = i.b.l.a.AppCompatResources.c(r8, r0)
                r7.f525j = r8
                if (r8 == 0) goto L_0x0021
                boolean r8 = r8.isStateful()
                if (r8 == 0) goto L_0x0021
                android.graphics.drawable.Drawable r8 = r7.f525j
                int[] r0 = r7.getDrawableState()
                r8.setState(r0)
                goto L_0x0021
            L_0x001f:
                r7.f525j = r1
            L_0x0021:
                android.graphics.drawable.GradientDrawable r8 = new android.graphics.drawable.GradientDrawable
                r8.<init>()
                r0 = 0
                r8.setColor(r0)
                com.google.android.material.tabs.TabLayout r2 = com.google.android.material.tabs.TabLayout.this
                android.content.res.ColorStateList r2 = r2.f505m
                if (r2 == 0) goto L_0x007b
                android.graphics.drawable.GradientDrawable r2 = new android.graphics.drawable.GradientDrawable
                r2.<init>()
                r3 = 925353388(0x3727c5ac, float:1.0E-5)
                r2.setCornerRadius(r3)
                r3 = -1
                r2.setColor(r3)
                com.google.android.material.tabs.TabLayout r3 = com.google.android.material.tabs.TabLayout.this
                android.content.res.ColorStateList r3 = r3.f505m
                r4 = 2
                int[][] r5 = new int[r4][]
                int[] r4 = new int[r4]
                int[] r6 = j.c.a.b.e0.RippleUtils.f2205j
                r5[r0] = r6
                int[] r6 = j.c.a.b.e0.RippleUtils.f2203f
                int r6 = j.c.a.b.e0.RippleUtils.a(r3, r6)
                r4[r0] = r6
                r0 = 1
                int[] r6 = android.util.StateSet.NOTHING
                r5[r0] = r6
                int[] r6 = j.c.a.b.e0.RippleUtils.b
                int r3 = j.c.a.b.e0.RippleUtils.a(r3, r6)
                r4[r0] = r3
                android.content.res.ColorStateList r0 = new android.content.res.ColorStateList
                r0.<init>(r5, r4)
                android.graphics.drawable.RippleDrawable r3 = new android.graphics.drawable.RippleDrawable
                com.google.android.material.tabs.TabLayout r4 = com.google.android.material.tabs.TabLayout.this
                boolean r4 = r4.D
                if (r4 == 0) goto L_0x006f
                r8 = r1
            L_0x006f:
                com.google.android.material.tabs.TabLayout r4 = com.google.android.material.tabs.TabLayout.this
                boolean r4 = r4.D
                if (r4 == 0) goto L_0x0076
                goto L_0x0077
            L_0x0076:
                r1 = r2
            L_0x0077:
                r3.<init>(r0, r8, r1)
                r8 = r3
            L_0x007b:
                i.h.l.ViewCompat.a(r7, r8)
                com.google.android.material.tabs.TabLayout r8 = com.google.android.material.tabs.TabLayout.this
                r8.invalidate()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.i.a(android.content.Context):void");
        }

        public final void b(View view) {
            if (a() && view == this.f522e) {
                BadgeDrawable badgeDrawable = this.f523f;
                ImageView imageView = this.d;
                BadgeUtils.a(badgeDrawable, view, null);
            }
        }

        public final void a(View view) {
            if (a() && view != null) {
                setClipChildren(false);
                setClipToPadding(false);
                BadgeDrawable badgeDrawable = this.f523f;
                ImageView imageView = this.d;
                BadgeUtils.a(badgeDrawable, view, null);
                view.getOverlay().add(badgeDrawable);
                this.f522e = view;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
         arg types: [com.google.android.material.tabs.TabLayout$i, java.lang.CharSequence]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void */
        public final void a(TextView textView, ImageView imageView) {
            Drawable drawable;
            g gVar = this.b;
            CharSequence charSequence = null;
            Drawable mutate = (gVar == null || (drawable = gVar.a) == null) ? null : ResourcesFlusher.d(drawable).mutate();
            g gVar2 = this.b;
            CharSequence charSequence2 = gVar2 != null ? gVar2.b : null;
            if (imageView != null) {
                if (mutate != null) {
                    imageView.setImageDrawable(mutate);
                    imageView.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView.setVisibility(8);
                    imageView.setImageDrawable(null);
                }
            }
            boolean z = !TextUtils.isEmpty(charSequence2);
            if (textView != null) {
                if (z) {
                    textView.setText(charSequence2);
                    if (this.b.f521f == 1) {
                        textView.setVisibility(0);
                    } else {
                        textView.setVisibility(8);
                    }
                    setVisibility(0);
                } else {
                    textView.setVisibility(8);
                    textView.setText((CharSequence) null);
                }
            }
            if (imageView != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) imageView.getLayoutParams();
                int a = (!z || imageView.getVisibility() != 0) ? 0 : (int) j.c.a.a.c.n.c.a(getContext(), 8);
                if (TabLayout.this.B) {
                    if (a != marginLayoutParams.getMarginEnd()) {
                        marginLayoutParams.setMarginEnd(a);
                        marginLayoutParams.bottomMargin = 0;
                        imageView.setLayoutParams(marginLayoutParams);
                        imageView.requestLayout();
                    }
                } else if (a != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = a;
                    marginLayoutParams.setMarginEnd(0);
                    imageView.setLayoutParams(marginLayoutParams);
                    imageView.requestLayout();
                }
            }
            g gVar3 = this.b;
            CharSequence charSequence3 = gVar3 != null ? gVar3.c : null;
            if (!z) {
                charSequence = charSequence3;
            }
            ResourcesFlusher.a((View) this, charSequence);
        }

        public final boolean a() {
            return this.f523f != null;
        }
    }

    /* JADX INFO: finally extract failed */
    public TabLayout(Context context, AttributeSet attributeSet, int i2) {
        super(MaterialThemeOverlay.a(context, attributeSet, i2, P), attributeSet, i2);
        this.b = new ArrayList<>();
        this.d = new RectF();
        this.f511s = Integer.MAX_VALUE;
        this.F = new ArrayList<>();
        this.O = new Pools0(12);
        Context context2 = getContext();
        setHorizontalScrollBarEnabled(false);
        f fVar = new f(context2);
        this.f499e = fVar;
        super.addView(fVar, 0, new FrameLayout.LayoutParams(-2, -1));
        TypedArray b2 = ThemeEnforcement.b(context2, attributeSet, l.TabLayout, i2, P, l.TabLayout_tabTextAppearance);
        if (getBackground() instanceof ColorDrawable) {
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable();
            materialShapeDrawable.a(ColorStateList.valueOf(((ColorDrawable) getBackground()).getColor()));
            materialShapeDrawable.b.b = new ElevationOverlayProvider(context2);
            materialShapeDrawable.j();
            materialShapeDrawable.a(ViewCompat.g(this));
            setBackground(materialShapeDrawable);
        }
        f fVar2 = this.f499e;
        int dimensionPixelSize = b2.getDimensionPixelSize(l.TabLayout_tabIndicatorHeight, -1);
        if (fVar2.b != dimensionPixelSize) {
            fVar2.b = dimensionPixelSize;
            ViewCompat.A(fVar2);
        }
        f fVar3 = this.f499e;
        int color = b2.getColor(l.TabLayout_tabIndicatorColor, 0);
        if (fVar3.c.getColor() != color) {
            fVar3.c.setColor(color);
            ViewCompat.A(fVar3);
        }
        setSelectedTabIndicator(j.c.a.a.c.n.c.b(context2, b2, l.TabLayout_tabIndicator));
        setSelectedTabIndicatorGravity(b2.getInt(l.TabLayout_tabIndicatorGravity, 0));
        setTabIndicatorFullWidth(b2.getBoolean(l.TabLayout_tabIndicatorFullWidth, true));
        int dimensionPixelSize2 = b2.getDimensionPixelSize(l.TabLayout_tabPadding, 0);
        this.f501i = dimensionPixelSize2;
        this.h = dimensionPixelSize2;
        this.g = dimensionPixelSize2;
        this.f500f = dimensionPixelSize2;
        this.f500f = b2.getDimensionPixelSize(l.TabLayout_tabPaddingStart, dimensionPixelSize2);
        this.g = b2.getDimensionPixelSize(l.TabLayout_tabPaddingTop, this.g);
        this.h = b2.getDimensionPixelSize(l.TabLayout_tabPaddingEnd, this.h);
        this.f501i = b2.getDimensionPixelSize(l.TabLayout_tabPaddingBottom, this.f501i);
        int resourceId = b2.getResourceId(l.TabLayout_tabTextAppearance, k.TextAppearance_Design_Tab);
        this.f502j = resourceId;
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(resourceId, i.b.j.TextAppearance);
        try {
            this.f508p = (float) obtainStyledAttributes.getDimensionPixelSize(i.b.j.TextAppearance_android_textSize, 0);
            this.f503k = j.c.a.a.c.n.c.a(context2, obtainStyledAttributes, i.b.j.TextAppearance_android_textColor);
            obtainStyledAttributes.recycle();
            if (b2.hasValue(l.TabLayout_tabTextColor)) {
                this.f503k = j.c.a.a.c.n.c.a(context2, b2, l.TabLayout_tabTextColor);
            }
            if (b2.hasValue(l.TabLayout_tabSelectedTextColor)) {
                int color2 = b2.getColor(l.TabLayout_tabSelectedTextColor, 0);
                int defaultColor = this.f503k.getDefaultColor();
                this.f503k = new ColorStateList(new int[][]{HorizontalScrollView.SELECTED_STATE_SET, HorizontalScrollView.EMPTY_STATE_SET}, new int[]{color2, defaultColor});
            }
            this.f504l = j.c.a.a.c.n.c.a(context2, b2, l.TabLayout_tabIconTint);
            this.f507o = j.c.a.a.c.n.c.a(b2.getInt(l.TabLayout_tabIconTintMode, -1), (PorterDuff.Mode) null);
            this.f505m = j.c.a.a.c.n.c.a(context2, b2, l.TabLayout_tabRippleColor);
            this.y = b2.getInt(l.TabLayout_tabIndicatorAnimationDuration, 300);
            this.f512t = b2.getDimensionPixelSize(l.TabLayout_tabMinWidth, -1);
            this.u = b2.getDimensionPixelSize(l.TabLayout_tabMaxWidth, -1);
            this.f510r = b2.getResourceId(l.TabLayout_tabBackground, 0);
            this.w = b2.getDimensionPixelSize(l.TabLayout_tabContentStart, 0);
            this.A = b2.getInt(l.TabLayout_tabMode, 1);
            this.x = b2.getInt(l.TabLayout_tabGravity, 0);
            this.B = b2.getBoolean(l.TabLayout_tabInlineLabel, false);
            this.D = b2.getBoolean(l.TabLayout_tabUnboundedRipple, false);
            b2.recycle();
            Resources resources = getResources();
            this.f509q = (float) resources.getDimensionPixelSize(j.c.a.b.d.design_tab_text_size_2line);
            this.v = resources.getDimensionPixelSize(j.c.a.b.d.design_tab_scrollable_min_width);
            a();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    public static class g {
        public Drawable a;
        public CharSequence b;
        public CharSequence c;
        public int d = -1;

        /* renamed from: e  reason: collision with root package name */
        public View f520e;

        /* renamed from: f  reason: collision with root package name */
        public int f521f = 1;
        public TabLayout g;
        public i h;

        public g a(CharSequence charSequence) {
            if (TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(charSequence)) {
                this.h.setContentDescription(charSequence);
            }
            this.b = charSequence;
            a();
            return this;
        }

        public void a() {
            i iVar = this.h;
            if (iVar != null) {
                iVar.d();
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        a(view);
    }

    public void setSelectedTabIndicator(int i2) {
        if (i2 != 0) {
            setSelectedTabIndicator(AppCompatResources.c(getContext(), i2));
        } else {
            setSelectedTabIndicator((Drawable) null);
        }
    }

    public void b(g gVar, boolean z2) {
        g gVar2 = this.c;
        if (gVar2 != gVar) {
            int i2 = gVar != null ? gVar.d : -1;
            if (z2) {
                if ((gVar2 == null || gVar2.d == -1) && i2 != -1) {
                    a(i2, 0.0f, true, true);
                } else {
                    a(i2);
                }
                if (i2 != -1) {
                    setSelectedTabView(i2);
                }
            }
            this.c = gVar;
            if (gVar2 != null) {
                for (int size = this.F.size() - 1; size >= 0; size--) {
                    this.F.get(size).a(gVar2);
                }
            }
            if (gVar != null) {
                for (int size2 = this.F.size() - 1; size2 >= 0; size2--) {
                    this.F.get(size2).b(gVar);
                }
            }
        } else if (gVar2 != null) {
            for (int size3 = this.F.size() - 1; size3 >= 0; size3--) {
                this.F.get(size3).c(gVar);
            }
            a(gVar.d);
        }
    }

    public void a(g gVar, boolean z2) {
        int size = this.b.size();
        if (gVar.g == this) {
            gVar.d = size;
            this.b.add(size, gVar);
            int size2 = this.b.size();
            while (true) {
                size++;
                if (size >= size2) {
                    break;
                }
                this.b.get(size).d = size;
            }
            i iVar = gVar.h;
            iVar.setSelected(false);
            iVar.setActivated(false);
            f fVar = this.f499e;
            int i2 = gVar.d;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
            a(layoutParams);
            fVar.addView(iVar, i2, layoutParams);
            if (z2) {
                TabLayout tabLayout = gVar.g;
                if (tabLayout != null) {
                    tabLayout.b(gVar, true);
                    return;
                }
                throw new IllegalArgumentException("Tab not attached to a TabLayout");
            }
            return;
        }
        throw new IllegalArgumentException("Tab belongs to a different TabLayout.");
    }

    public class f extends LinearLayout {
        public int b;
        public final Paint c;
        public final GradientDrawable d;

        /* renamed from: e  reason: collision with root package name */
        public int f513e = -1;

        /* renamed from: f  reason: collision with root package name */
        public float f514f;
        public int g = -1;
        public int h = -1;

        /* renamed from: i  reason: collision with root package name */
        public int f515i = -1;

        /* renamed from: j  reason: collision with root package name */
        public ValueAnimator f516j;

        /* renamed from: k  reason: collision with root package name */
        public int f517k = -1;

        /* renamed from: l  reason: collision with root package name */
        public int f518l = -1;

        public class a implements ValueAnimator.AnimatorUpdateListener {
            public final /* synthetic */ int a;
            public final /* synthetic */ int b;

            public a(int i2, int i3) {
                this.a = i2;
                this.b = i3;
            }

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                f fVar = f.this;
                int a2 = AnimationUtils.a(fVar.f517k, this.a, animatedFraction);
                int i2 = f.this.f518l;
                int round = Math.round(animatedFraction * ((float) (this.b - i2))) + i2;
                if (a2 != fVar.h || round != fVar.f515i) {
                    fVar.h = a2;
                    fVar.f515i = round;
                    ViewCompat.A(fVar);
                }
            }
        }

        public class b extends AnimatorListenerAdapter {
            public final /* synthetic */ int a;

            public b(int i2) {
                this.a = i2;
            }

            public void onAnimationEnd(Animator animator) {
                f fVar = f.this;
                fVar.f513e = this.a;
                fVar.f514f = 0.0f;
            }

            public void onAnimationStart(Animator animator) {
                f.this.f513e = this.a;
            }
        }

        public f(Context context) {
            super(context);
            setWillNotDraw(false);
            this.c = new Paint();
            this.d = new GradientDrawable();
        }

        public final void a() {
            int i2;
            View childAt = getChildAt(this.f513e);
            int i3 = -1;
            if (childAt == null || childAt.getWidth() <= 0) {
                i2 = -1;
            } else {
                int left = childAt.getLeft();
                int right = childAt.getRight();
                TabLayout tabLayout = TabLayout.this;
                if (!tabLayout.C && (childAt instanceof i)) {
                    a((i) childAt, tabLayout.d);
                    RectF rectF = TabLayout.this.d;
                    left = (int) rectF.left;
                    right = (int) rectF.right;
                }
                if (this.f514f <= 0.0f || this.f513e >= getChildCount() - 1) {
                    i3 = left;
                    i2 = right;
                } else {
                    View childAt2 = getChildAt(this.f513e + 1);
                    int left2 = childAt2.getLeft();
                    int right2 = childAt2.getRight();
                    TabLayout tabLayout2 = TabLayout.this;
                    if (!tabLayout2.C && (childAt2 instanceof i)) {
                        a((i) childAt2, tabLayout2.d);
                        RectF rectF2 = TabLayout.this.d;
                        left2 = (int) rectF2.left;
                        right2 = (int) rectF2.right;
                    }
                    float f2 = this.f514f;
                    float f3 = 1.0f - f2;
                    i3 = (int) ((((float) left) * f3) + (((float) left2) * f2));
                    i2 = (int) ((f3 * ((float) right)) + (((float) right2) * f2));
                }
            }
            if (i3 != this.h || i2 != this.f515i) {
                this.h = i3;
                this.f515i = i2;
                ViewCompat.A(this);
            }
        }

        public void draw(Canvas canvas) {
            Drawable drawable = TabLayout.this.f506n;
            int i2 = 0;
            int intrinsicHeight = drawable != null ? drawable.getIntrinsicHeight() : 0;
            int i3 = this.b;
            if (i3 >= 0) {
                intrinsicHeight = i3;
            }
            int i4 = TabLayout.this.z;
            if (i4 == 0) {
                i2 = getHeight() - intrinsicHeight;
                intrinsicHeight = getHeight();
            } else if (i4 == 1) {
                i2 = (getHeight() - intrinsicHeight) / 2;
                intrinsicHeight = (getHeight() + intrinsicHeight) / 2;
            } else if (i4 != 2) {
                if (i4 != 3) {
                    intrinsicHeight = 0;
                } else {
                    intrinsicHeight = getHeight();
                }
            }
            int i5 = this.h;
            if (i5 >= 0 && this.f515i > i5) {
                Drawable drawable2 = TabLayout.this.f506n;
                if (drawable2 == null) {
                    drawable2 = this.d;
                }
                Drawable d2 = ResourcesFlusher.d(drawable2);
                d2.setBounds(this.h, i2, this.f515i, intrinsicHeight);
                Paint paint = this.c;
                if (paint != null) {
                    if (Build.VERSION.SDK_INT == 21) {
                        d2.setColorFilter(paint.getColor(), PorterDuff.Mode.SRC_IN);
                    } else {
                        d2.setTint(paint.getColor());
                    }
                }
                d2.draw(canvas);
            }
            super.draw(canvas);
        }

        public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
            super.onLayout(z, i2, i3, i4, i5);
            ValueAnimator valueAnimator = this.f516j;
            if (valueAnimator == null || !valueAnimator.isRunning()) {
                a();
            } else {
                a(false, this.f513e, -1);
            }
        }

        public void onMeasure(int i2, int i3) {
            super.onMeasure(i2, i3);
            if (View.MeasureSpec.getMode(i2) == 1073741824) {
                TabLayout tabLayout = TabLayout.this;
                boolean z = true;
                if (tabLayout.x == 1 || tabLayout.A == 2) {
                    int childCount = getChildCount();
                    int i4 = 0;
                    for (int i5 = 0; i5 < childCount; i5++) {
                        View childAt = getChildAt(i5);
                        if (childAt.getVisibility() == 0) {
                            i4 = Math.max(i4, childAt.getMeasuredWidth());
                        }
                    }
                    if (i4 > 0) {
                        if (i4 * childCount <= getMeasuredWidth() - (((int) j.c.a.a.c.n.c.a(getContext(), 16)) * 2)) {
                            boolean z2 = false;
                            for (int i6 = 0; i6 < childCount; i6++) {
                                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i6).getLayoutParams();
                                if (layoutParams.width != i4 || layoutParams.weight != 0.0f) {
                                    layoutParams.width = i4;
                                    layoutParams.weight = 0.0f;
                                    z2 = true;
                                }
                            }
                            z = z2;
                        } else {
                            TabLayout tabLayout2 = TabLayout.this;
                            tabLayout2.x = 0;
                            tabLayout2.a(false);
                        }
                        if (z) {
                            super.onMeasure(i2, i3);
                        }
                    }
                }
            }
        }

        public void onRtlPropertiesChanged(int i2) {
            super.onRtlPropertiesChanged(i2);
            if (Build.VERSION.SDK_INT < 23 && this.g != i2) {
                requestLayout();
                this.g = i2;
            }
        }

        public final void a(boolean z, int i2, int i3) {
            View childAt = getChildAt(i2);
            if (childAt == null) {
                a();
                return;
            }
            int left = childAt.getLeft();
            int right = childAt.getRight();
            TabLayout tabLayout = TabLayout.this;
            if (!tabLayout.C && (childAt instanceof i)) {
                a((i) childAt, tabLayout.d);
                RectF rectF = TabLayout.this.d;
                left = (int) rectF.left;
                right = (int) rectF.right;
            }
            int i4 = this.h;
            int i5 = this.f515i;
            if (i4 != left || i5 != right) {
                if (z) {
                    this.f517k = i4;
                    this.f518l = i5;
                }
                a aVar = new a(left, right);
                if (z) {
                    ValueAnimator valueAnimator = new ValueAnimator();
                    this.f516j = valueAnimator;
                    valueAnimator.setInterpolator(AnimationUtils.b);
                    valueAnimator.setDuration((long) i3);
                    valueAnimator.setFloatValues(0.0f, 1.0f);
                    valueAnimator.addUpdateListener(aVar);
                    valueAnimator.addListener(new b(i2));
                    valueAnimator.start();
                    return;
                }
                this.f516j.removeAllUpdateListeners();
                this.f516j.addUpdateListener(aVar);
            }
        }

        public final void a(i iVar, RectF rectF) {
            int a2 = iVar.getContentWidth();
            int a3 = (int) j.c.a.a.c.n.c.a(getContext(), 24);
            if (a2 < a3) {
                a2 = a3;
            }
            int right = (super.getRight() + super.getLeft()) / 2;
            int i2 = a2 / 2;
            rectF.set((float) (right - i2), 0.0f, (float) (right + i2), 0.0f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.tabs.TabLayout.a(i.z.a.PagerAdapter, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.material.tabs.TabLayout.a(int, float):int
      com.google.android.material.tabs.TabLayout.a(com.google.android.material.tabs.TabLayout$g, boolean):void
      com.google.android.material.tabs.TabLayout.a(i.z.a.PagerAdapter, boolean):void */
    public final void a(ViewPager viewPager, boolean z2, boolean z3) {
        List<ViewPager.i> list;
        List<ViewPager.j> list2;
        ViewPager viewPager2 = this.I;
        if (viewPager2 != null) {
            h hVar = this.L;
            if (!(hVar == null || (list2 = viewPager2.S) == null)) {
                list2.remove(hVar);
            }
            b bVar = this.M;
            if (!(bVar == null || (list = this.I.U) == null)) {
                list.remove(bVar);
            }
        }
        c cVar = this.G;
        if (cVar != null) {
            this.F.remove(cVar);
            this.G = null;
        }
        if (viewPager != null) {
            this.I = viewPager;
            if (this.L == null) {
                this.L = new h(this);
            }
            h hVar2 = this.L;
            hVar2.c = 0;
            hVar2.b = 0;
            if (viewPager.S == null) {
                viewPager.S = new ArrayList();
            }
            viewPager.S.add(hVar2);
            j jVar = new j(viewPager);
            this.G = jVar;
            if (!this.F.contains(jVar)) {
                this.F.add(jVar);
            }
            PagerAdapter adapter = viewPager.getAdapter();
            if (adapter != null) {
                a(adapter, z2);
            }
            if (this.M == null) {
                this.M = new b();
            }
            b bVar2 = this.M;
            bVar2.a = z2;
            if (viewPager.U == null) {
                viewPager.U = new ArrayList();
            }
            viewPager.U.add(bVar2);
            a(viewPager.getCurrentItem(), 0.0f, true, true);
        } else {
            this.I = null;
            a((PagerAdapter) null, false);
        }
        this.N = z3;
    }

    public void a(PagerAdapter pagerAdapter, boolean z2) {
        DataSetObserver dataSetObserver;
        PagerAdapter pagerAdapter2 = this.J;
        if (!(pagerAdapter2 == null || (dataSetObserver = this.K) == null)) {
            pagerAdapter2.a.unregisterObserver(dataSetObserver);
        }
        this.J = pagerAdapter;
        if (z2 && pagerAdapter != null) {
            if (this.K == null) {
                this.K = new e();
            }
            pagerAdapter.a.registerObserver(this.K);
        }
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.google.android.material.tabs.TabLayout$i, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a(View view) {
        if (view instanceof TabItem) {
            TabItem tabItem = (TabItem) view;
            g c2 = c();
            CharSequence charSequence = tabItem.b;
            if (charSequence != null) {
                c2.a(charSequence);
            }
            Drawable drawable = tabItem.c;
            if (drawable != null) {
                c2.a = drawable;
                TabLayout tabLayout = c2.g;
                if (tabLayout.x == 1 || tabLayout.A == 2) {
                    c2.g.a(true);
                }
                c2.a();
            }
            int i2 = tabItem.d;
            if (i2 != 0) {
                c2.f520e = LayoutInflater.from(c2.h.getContext()).inflate(i2, (ViewGroup) c2.h, false);
                c2.a();
            }
            if (!TextUtils.isEmpty(tabItem.getContentDescription())) {
                c2.c = tabItem.getContentDescription();
                c2.a();
            }
            a(c2, this.b.isEmpty());
            return;
        }
        throw new IllegalArgumentException("Only TabItem instances can be added to TabLayout");
    }

    public final void a(LinearLayout.LayoutParams layoutParams) {
        if (this.A == 1 && this.x == 0) {
            layoutParams.width = 0;
            layoutParams.weight = 1.0f;
            return;
        }
        layoutParams.width = -2;
        layoutParams.weight = 0.0f;
    }

    public final void a(int i2) {
        boolean z2;
        if (i2 != -1) {
            if (getWindowToken() != null && ViewCompat.w(this)) {
                f fVar = this.f499e;
                int childCount = fVar.getChildCount();
                int i3 = 0;
                while (true) {
                    if (i3 >= childCount) {
                        z2 = false;
                        break;
                    } else if (fVar.getChildAt(i3).getWidth() <= 0) {
                        z2 = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (!z2) {
                    int scrollX = getScrollX();
                    int a2 = a(i2, 0.0f);
                    if (scrollX != a2) {
                        b();
                        this.H.setIntValues(scrollX, a2);
                        this.H.start();
                    }
                    f fVar2 = this.f499e;
                    int i4 = this.y;
                    ValueAnimator valueAnimator = fVar2.f516j;
                    if (valueAnimator != null && valueAnimator.isRunning()) {
                        fVar2.f516j.cancel();
                    }
                    fVar2.a(true, i2, i4);
                    return;
                }
            }
            a(i2, 0.0f, true, true);
        }
    }

    public final int a(int i2, float f2) {
        int i3 = this.A;
        int i4 = 0;
        if (i3 != 0 && i3 != 2) {
            return 0;
        }
        View childAt = this.f499e.getChildAt(i2);
        int i5 = i2 + 1;
        View childAt2 = i5 < this.f499e.getChildCount() ? this.f499e.getChildAt(i5) : null;
        int width = childAt != null ? childAt.getWidth() : 0;
        if (childAt2 != null) {
            i4 = childAt2.getWidth();
        }
        int left = ((width / 2) + childAt.getLeft()) - (getWidth() / 2);
        int i6 = (int) (((float) (width + i4)) * 0.5f * f2);
        return ViewCompat.k(this) == 0 ? left + i6 : left - i6;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
        if (r0 != 2) goto L_0x0050;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r4 = this;
            int r0 = r4.A
            r1 = 2
            r2 = 0
            if (r0 == 0) goto L_0x000b
            if (r0 != r1) goto L_0x0009
            goto L_0x000b
        L_0x0009:
            r0 = 0
            goto L_0x0014
        L_0x000b:
            int r0 = r4.w
            int r3 = r4.f500f
            int r0 = r0 - r3
            int r0 = java.lang.Math.max(r2, r0)
        L_0x0014:
            com.google.android.material.tabs.TabLayout$f r3 = r4.f499e
            i.h.l.ViewCompat.a(r3, r0, r2, r2, r2)
            int r0 = r4.A
            java.lang.String r2 = "TabLayout"
            r3 = 1
            if (r0 == 0) goto L_0x0034
            if (r0 == r3) goto L_0x0025
            if (r0 == r1) goto L_0x0025
            goto L_0x0050
        L_0x0025:
            int r0 = r4.x
            if (r0 != r1) goto L_0x002e
            java.lang.String r0 = "GRAVITY_START is not supported with the current tab mode, GRAVITY_CENTER will be used instead"
            android.util.Log.w(r2, r0)
        L_0x002e:
            com.google.android.material.tabs.TabLayout$f r0 = r4.f499e
            r0.setGravity(r3)
            goto L_0x0050
        L_0x0034:
            int r0 = r4.x
            if (r0 == 0) goto L_0x0043
            if (r0 == r3) goto L_0x003d
            if (r0 == r1) goto L_0x0048
            goto L_0x0050
        L_0x003d:
            com.google.android.material.tabs.TabLayout$f r0 = r4.f499e
            r0.setGravity(r3)
            goto L_0x0050
        L_0x0043:
            java.lang.String r0 = "MODE_SCROLLABLE + GRAVITY_FILL is not supported, GRAVITY_START will be used instead"
            android.util.Log.w(r2, r0)
        L_0x0048:
            com.google.android.material.tabs.TabLayout$f r0 = r4.f499e
            r1 = 8388611(0x800003, float:1.1754948E-38)
            r0.setGravity(r1)
        L_0x0050:
            r4.a(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.tabs.TabLayout.a():void");
    }

    public void a(boolean z2) {
        for (int i2 = 0; i2 < this.f499e.getChildCount(); i2++) {
            View childAt = this.f499e.getChildAt(i2);
            childAt.setMinimumWidth(getTabMinWidth());
            a((LinearLayout.LayoutParams) childAt.getLayoutParams());
            if (z2) {
                childAt.requestLayout();
            }
        }
    }
}
