package com.google.android.material.internal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Checkable;
import android.widget.ImageButton;
import i.b.q.AppCompatImageButton;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.a.AbsSavedState;

public class CheckableImageButton extends AppCompatImageButton implements Checkable {
    public static final int[] g = {16842912};
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f480e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f481f;

    public class a extends AccessibilityDelegateCompat {
        public a() {
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            accessibilityNodeInfoCompat.a.setCheckable(CheckableImageButton.this.f480e);
            accessibilityNodeInfoCompat.a.setChecked(CheckableImageButton.this.d);
        }

        public void b(View view, AccessibilityEvent accessibilityEvent) {
            super.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setChecked(CheckableImageButton.this.d);
        }
    }

    public static class b extends AbsSavedState {
        public static final Parcelable.Creator<b> CREATOR = new a();
        public boolean d;

        public static class a implements Parcelable.ClassLoaderCreator<b> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new b(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new b[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new b(parcel, null);
            }
        }

        public b(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeInt(this.d ? 1 : 0);
        }

        public b(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = parcel.readInt() != 1 ? false : true;
        }
    }

    public CheckableImageButton(Context context) {
        this(context, null);
    }

    public boolean isChecked() {
        return this.d;
    }

    public int[] onCreateDrawableState(int i2) {
        if (this.d) {
            return ImageButton.mergeDrawableStates(super.onCreateDrawableState(i2 + g.length), g);
        }
        return super.onCreateDrawableState(i2);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof b)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.b);
        setChecked(bVar.d);
    }

    public Parcelable onSaveInstanceState() {
        b bVar = new b(super.onSaveInstanceState());
        bVar.d = this.d;
        return bVar;
    }

    public void setCheckable(boolean z) {
        if (this.f480e != z) {
            this.f480e = z;
            sendAccessibilityEvent(0);
        }
    }

    public void setChecked(boolean z) {
        if (this.f480e && this.d != z) {
            this.d = z;
            refreshDrawableState();
            sendAccessibilityEvent(2048);
        }
    }

    public void setPressable(boolean z) {
        this.f481f = z;
    }

    public void setPressed(boolean z) {
        if (this.f481f) {
            super.setPressed(z);
        }
    }

    public void toggle() {
        setChecked(!this.d);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.b.a.imageButtonStyle);
    }

    public CheckableImageButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f480e = true;
        this.f481f = true;
        ViewCompat.a(this, new a());
    }
}
