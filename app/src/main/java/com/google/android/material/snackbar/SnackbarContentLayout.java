package com.google.android.material.snackbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import i.h.l.ViewCompat;
import j.c.a.b.f;
import j.c.a.b.h0.ContentViewCallback;
import j.c.a.b.l;

public class SnackbarContentLayout extends LinearLayout implements ContentViewCallback {
    public TextView b;
    public Button c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f498e;

    public SnackbarContentLayout(Context context) {
        this(context, null);
    }

    public final boolean a(int i2, int i3, int i4) {
        boolean z;
        if (i2 != getOrientation()) {
            setOrientation(i2);
            z = true;
        } else {
            z = false;
        }
        if (this.b.getPaddingTop() == i3 && this.b.getPaddingBottom() == i4) {
            return z;
        }
        TextView textView = this.b;
        if (ViewCompat.y(textView)) {
            textView.setPaddingRelative(textView.getPaddingStart(), i3, textView.getPaddingEnd(), i4);
            return true;
        }
        textView.setPadding(textView.getPaddingLeft(), i3, textView.getPaddingRight(), i4);
        return true;
    }

    public void b(int i2, int i3) {
        this.b.setAlpha(0.0f);
        long j2 = (long) i3;
        long j3 = (long) i2;
        this.b.animate().alpha(1.0f).setDuration(j2).setStartDelay(j3).start();
        if (this.c.getVisibility() == 0) {
            this.c.setAlpha(0.0f);
            this.c.animate().alpha(1.0f).setDuration(j2).setStartDelay(j3).start();
        }
    }

    public Button getActionView() {
        return this.c;
    }

    public TextView getMessageView() {
        return this.b;
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.b = (TextView) findViewById(f.snackbar_text);
        this.c = (Button) findViewById(f.snackbar_action);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (a(1, r0, r0 - r1) != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (a(0, r0, r0) != false) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            super.onMeasure(r8, r9)
            int r0 = r7.d
            if (r0 <= 0) goto L_0x0018
            int r0 = r7.getMeasuredWidth()
            int r1 = r7.d
            if (r0 <= r1) goto L_0x0018
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r8)
            super.onMeasure(r8, r9)
        L_0x0018:
            android.content.res.Resources r0 = r7.getResources()
            int r1 = j.c.a.b.d.design_snackbar_padding_vertical_2lines
            int r0 = r0.getDimensionPixelSize(r1)
            android.content.res.Resources r1 = r7.getResources()
            int r2 = j.c.a.b.d.design_snackbar_padding_vertical
            int r1 = r1.getDimensionPixelSize(r2)
            android.widget.TextView r2 = r7.b
            android.text.Layout r2 = r2.getLayout()
            int r2 = r2.getLineCount()
            r3 = 0
            r4 = 1
            if (r2 <= r4) goto L_0x003c
            r2 = 1
            goto L_0x003d
        L_0x003c:
            r2 = 0
        L_0x003d:
            if (r2 == 0) goto L_0x0056
            int r5 = r7.f498e
            if (r5 <= 0) goto L_0x0056
            android.widget.Button r5 = r7.c
            int r5 = r5.getMeasuredWidth()
            int r6 = r7.f498e
            if (r5 <= r6) goto L_0x0056
            int r1 = r0 - r1
            boolean r0 = r7.a(r4, r0, r1)
            if (r0 == 0) goto L_0x0061
            goto L_0x0060
        L_0x0056:
            if (r2 == 0) goto L_0x0059
            goto L_0x005a
        L_0x0059:
            r0 = r1
        L_0x005a:
            boolean r0 = r7.a(r3, r0, r0)
            if (r0 == 0) goto L_0x0061
        L_0x0060:
            r3 = 1
        L_0x0061:
            if (r3 == 0) goto L_0x0066
            super.onMeasure(r8, r9)
        L_0x0066:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.snackbar.SnackbarContentLayout.onMeasure(int, int):void");
    }

    public void setMaxInlineActionWidth(int i2) {
        this.f498e = i2;
    }

    public SnackbarContentLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.SnackbarLayout);
        this.d = obtainStyledAttributes.getDimensionPixelSize(l.SnackbarLayout_android_maxWidth, -1);
        this.f498e = obtainStyledAttributes.getDimensionPixelSize(l.SnackbarLayout_maxActionInlineWidth, -1);
        obtainStyledAttributes.recycle();
    }

    public void a(int i2, int i3) {
        this.b.setAlpha(1.0f);
        long j2 = (long) i3;
        long j3 = (long) i2;
        this.b.animate().alpha(0.0f).setDuration(j2).setStartDelay(j3).start();
        if (this.c.getVisibility() == 0) {
            this.c.setAlpha(1.0f);
            this.c.animate().alpha(0.0f).setDuration(j2).setStartDelay(j3).start();
        }
    }
}
