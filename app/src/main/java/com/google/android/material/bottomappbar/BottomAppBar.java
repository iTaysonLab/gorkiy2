package com.google.android.material.bottomappbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import i.h.l.ViewCompat;
import i.j.a.AbsSavedState;
import j.c.a.a.c.n.c;
import j.c.a.b.m.TransformationCallback;
import j.c.a.b.q.BottomAppBar0;
import j.c.a.b.q.BottomAppBar1;
import j.c.a.b.q.BottomAppBar2;
import j.c.a.b.q.BottomAppBar3;
import j.c.a.b.q.BottomAppBarTopEdgeTreatment;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BottomAppBar extends Toolbar implements CoordinatorLayout.b {
    public Animator Q;
    public Animator R;
    public int S;
    public int T;
    public boolean U;
    public int V;
    public boolean W;
    public Behavior a0;
    public AnimatorListenerAdapter b0;

    public static class a extends AbsSavedState {
        public static final Parcelable.Creator<a> CREATOR = new C0004a();
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f409e;

        /* renamed from: com.google.android.material.bottomappbar.BottomAppBar$a$a  reason: collision with other inner class name */
        public static class C0004a implements Parcelable.ClassLoaderCreator<a> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new a(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new a[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new a(parcel, null);
            }
        }

        public a(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeInt(this.d);
            parcel.writeInt(this.f409e ? 1 : 0);
        }

        public a(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = parcel.readInt();
            this.f409e = parcel.readInt() != 0;
        }
    }

    private ActionMenuView getActionMenuView() {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt instanceof ActionMenuView) {
                return (ActionMenuView) childAt;
            }
        }
        return null;
    }

    private int getBottomInset() {
        return 0;
    }

    /* access modifiers changed from: private */
    public float getFabTranslationX() {
        return b(this.S);
    }

    private float getFabTranslationY() {
        return -getTopEdgeTreatment().d;
    }

    private int getLeftInset() {
        return 0;
    }

    private int getRightInset() {
        return 0;
    }

    private BottomAppBarTopEdgeTreatment getTopEdgeTreatment() {
        throw null;
    }

    public ColorStateList getBackgroundTint() {
        throw null;
    }

    public float getCradleVerticalOffset() {
        return getTopEdgeTreatment().d;
    }

    public int getFabAlignmentMode() {
        return this.S;
    }

    public int getFabAnimationMode() {
        return this.T;
    }

    public float getFabCradleMargin() {
        return getTopEdgeTreatment().c;
    }

    public float getFabCradleRoundedCornerRadius() {
        return getTopEdgeTreatment().b;
    }

    public boolean getHideOnScroll() {
        return this.U;
    }

    public final FloatingActionButton h() {
        View i2 = i();
        if (i2 instanceof FloatingActionButton) {
            return (FloatingActionButton) i2;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.view.View i() {
        /*
            r4 = this;
            android.view.ViewParent r0 = r4.getParent()
            boolean r0 = r0 instanceof androidx.coordinatorlayout.widget.CoordinatorLayout
            r1 = 0
            if (r0 != 0) goto L_0x000a
            return r1
        L_0x000a:
            android.view.ViewParent r0 = r4.getParent()
            androidx.coordinatorlayout.widget.CoordinatorLayout r0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) r0
            java.util.List r0 = r0.b(r4)
            java.util.Iterator r0 = r0.iterator()
        L_0x0018:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x002d
            java.lang.Object r2 = r0.next()
            android.view.View r2 = (android.view.View) r2
            boolean r3 = r2 instanceof com.google.android.material.floatingactionbutton.FloatingActionButton
            if (r3 != 0) goto L_0x002c
            boolean r3 = r2 instanceof com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
            if (r3 == 0) goto L_0x0018
        L_0x002c:
            return r2
        L_0x002d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomappbar.BottomAppBar.i():android.view.View");
    }

    public final boolean j() {
        FloatingActionButton h = h();
        return h != null && h.c();
    }

    public final void k() {
        getTopEdgeTreatment().f2324e = getFabTranslationX();
        i();
        if (this.W) {
            boolean j2 = j();
        }
        throw null;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        throw null;
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (z) {
            Animator animator = this.R;
            if (animator != null) {
                animator.cancel();
            }
            Animator animator2 = this.Q;
            if (animator2 != null) {
                animator2.cancel();
            }
            k();
            throw null;
        }
        ActionMenuView actionMenuView = getActionMenuView();
        if (actionMenuView != null) {
            actionMenuView.setAlpha(1.0f);
            if (!j()) {
                actionMenuView.setTranslationX((float) a(actionMenuView, 0, false));
            } else {
                actionMenuView.setTranslationX((float) a(actionMenuView, this.S, this.W));
            }
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof a)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.b);
        this.S = aVar.d;
        this.W = aVar.f409e;
    }

    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.d = this.S;
        aVar.f409e = this.W;
        return aVar;
    }

    public void setBackgroundTint(ColorStateList colorStateList) {
        throw null;
    }

    public void setCradleVerticalOffset(float f2) {
        if (f2 != getCradleVerticalOffset()) {
            BottomAppBarTopEdgeTreatment topEdgeTreatment = getTopEdgeTreatment();
            if (topEdgeTreatment == null) {
                throw null;
            } else if (f2 >= 0.0f) {
                topEdgeTreatment.d = f2;
                throw null;
            } else {
                throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
            }
        }
    }

    public void setElevation(float f2) {
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void
     arg types: [j.c.a.b.q.BottomAppBar3, int]
     candidates:
      com.google.android.material.floatingactionbutton.FloatingActionButton.a(int, int):int
      com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton, android.graphics.drawable.Drawable):void
      j.c.a.b.b0.VisibilityAwareImageButton.a(int, boolean):void
      com.google.android.material.floatingactionbutton.FloatingActionButton.a(com.google.android.material.floatingactionbutton.FloatingActionButton$a, boolean):void */
    public void setFabAlignmentMode(int i2) {
        int i3;
        if (this.S != i2 && ViewCompat.w(this)) {
            Animator animator = this.Q;
            if (animator != null) {
                animator.cancel();
            }
            ArrayList arrayList = new ArrayList();
            if (this.T == 1) {
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(h(), "translationX", b(i2));
                ofFloat.setDuration(300L);
                arrayList.add(ofFloat);
            } else {
                FloatingActionButton h = h();
                if (h != null && !h.b()) {
                    this.V++;
                    h.a((FloatingActionButton.a) new BottomAppBar3(this, i2), true);
                }
            }
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(arrayList);
            this.Q = animatorSet;
            animatorSet.addListener(new j.c.a.b.q.BottomAppBar(this));
            this.Q.start();
        }
        boolean z = this.W;
        if (ViewCompat.w(this)) {
            Animator animator2 = this.R;
            if (animator2 != null) {
                animator2.cancel();
            }
            ArrayList arrayList2 = new ArrayList();
            if (!j()) {
                z = false;
                i3 = 0;
            } else {
                i3 = i2;
            }
            ActionMenuView actionMenuView = getActionMenuView();
            if (actionMenuView != null) {
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(actionMenuView, "alpha", 1.0f);
                if (Math.abs(actionMenuView.getTranslationX() - ((float) a(actionMenuView, i3, z))) > 1.0f) {
                    ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(actionMenuView, "alpha", 0.0f);
                    ofFloat3.addListener(new BottomAppBar1(this, actionMenuView, i3, z));
                    AnimatorSet animatorSet2 = new AnimatorSet();
                    animatorSet2.setDuration(150L);
                    animatorSet2.playSequentially(ofFloat3, ofFloat2);
                    arrayList2.add(animatorSet2);
                } else if (actionMenuView.getAlpha() < 1.0f) {
                    arrayList2.add(ofFloat2);
                }
            }
            AnimatorSet animatorSet3 = new AnimatorSet();
            animatorSet3.playTogether(arrayList2);
            this.R = animatorSet3;
            animatorSet3.addListener(new BottomAppBar0(this));
            this.R.start();
        }
        this.S = i2;
    }

    public void setFabAnimationMode(int i2) {
        this.T = i2;
    }

    public void setFabCradleMargin(float f2) {
        if (f2 != getFabCradleMargin()) {
            getTopEdgeTreatment().c = f2;
            throw null;
        }
    }

    public void setFabCradleRoundedCornerRadius(float f2) {
        if (f2 != getFabCradleRoundedCornerRadius()) {
            getTopEdgeTreatment().b = f2;
            throw null;
        }
    }

    public void setHideOnScroll(boolean z) {
        this.U = z;
    }

    public void setSubtitle(CharSequence charSequence) {
    }

    public void setTitle(CharSequence charSequence) {
    }

    public static /* synthetic */ void a(BottomAppBar bottomAppBar) {
        bottomAppBar.V--;
    }

    public final float b(int i2) {
        boolean b = c.b((View) this);
        int i3 = 1;
        if (i2 != 1) {
            return 0.0f;
        }
        int measuredWidth = (getMeasuredWidth() / 2) + 0;
        if (b) {
            i3 = -1;
        }
        return (float) (measuredWidth * i3);
    }

    public Behavior getBehavior() {
        if (this.a0 == null) {
            this.a0 = new Behavior();
        }
        return this.a0;
    }

    public static class Behavior extends HideBottomViewOnScrollBehavior<BottomAppBar> {

        /* renamed from: e  reason: collision with root package name */
        public final Rect f407e = new Rect();

        /* renamed from: f  reason: collision with root package name */
        public WeakReference<BottomAppBar> f408f;
        public int g;
        public final View.OnLayoutChangeListener h = new a();

        public class a implements View.OnLayoutChangeListener {
            public a() {
            }

            public void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
                if (Behavior.this.f408f.get() == null || !(view instanceof FloatingActionButton)) {
                    view.removeOnLayoutChangeListener(this);
                    return;
                }
                FloatingActionButton floatingActionButton = (FloatingActionButton) view;
                Behavior.this.f407e.set(0, 0, floatingActionButton.getMeasuredWidth(), floatingActionButton.getMeasuredHeight());
                throw null;
            }
        }

        public Behavior() {
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, int i2) {
            BottomAppBar bottomAppBar = (BottomAppBar) view;
            this.f408f = new WeakReference<>(bottomAppBar);
            View c = bottomAppBar.i();
            if (c == null || ViewCompat.w(c)) {
                coordinatorLayout.b(bottomAppBar, i2);
                super.a = bottomAppBar.getMeasuredHeight() + ((ViewGroup.MarginLayoutParams) bottomAppBar.getLayoutParams()).bottomMargin;
                return false;
            }
            CoordinatorLayout.f fVar = (CoordinatorLayout.f) c.getLayoutParams();
            fVar.d = 49;
            this.g = fVar.bottomMargin;
            if (c instanceof FloatingActionButton) {
                FloatingActionButton floatingActionButton = (FloatingActionButton) c;
                floatingActionButton.addOnLayoutChangeListener(this.h);
                if (bottomAppBar != null) {
                    floatingActionButton.a((Animator.AnimatorListener) null);
                    floatingActionButton.b(new BottomAppBar2(bottomAppBar));
                    floatingActionButton.a((TransformationCallback<? extends FloatingActionButton>) null);
                } else {
                    throw null;
                }
            }
            bottomAppBar.k();
            throw null;
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public boolean a(CoordinatorLayout coordinatorLayout, View view, View view2, View view3, int i2, int i3) {
            if (!((BottomAppBar) view).getHideOnScroll()) {
                return false;
            }
            return i2 == 2;
        }
    }

    public int a(ActionMenuView actionMenuView, int i2, boolean z) {
        if (i2 != 1 || !z) {
            return 0;
        }
        boolean b = c.b((View) this);
        int measuredWidth = b ? getMeasuredWidth() : 0;
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            View childAt = getChildAt(i3);
            if ((childAt.getLayoutParams() instanceof Toolbar.e) && (((Toolbar.e) childAt.getLayoutParams()).a & 8388615) == 8388611) {
                if (b) {
                    measuredWidth = Math.min(measuredWidth, childAt.getLeft());
                } else {
                    measuredWidth = Math.max(measuredWidth, childAt.getRight());
                }
            }
        }
        return measuredWidth - ((b ? actionMenuView.getRight() : actionMenuView.getLeft()) + 0);
    }
}
