package androidx.constraintlayout.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import i.f.b.ConstraintHelper;

public class Group extends ConstraintHelper {
    public Group(Context context) {
        super(context);
    }

    public void a(AttributeSet attributeSet) {
        super.a(attributeSet);
        super.f1138f = false;
    }

    public void b(ConstraintLayout constraintLayout) {
        int visibility = getVisibility();
        float elevation = getElevation();
        for (int i2 = 0; i2 < super.c; i2++) {
            View view = constraintLayout.b.get(super.b[i2]);
            if (view != null) {
                view.setVisibility(visibility);
                if (elevation > 0.0f) {
                    view.setElevation(elevation);
                }
            }
        }
    }

    public Group(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public Group(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public void a(ConstraintLayout constraintLayout) {
        ConstraintLayout.a aVar = (ConstraintLayout.a) getLayoutParams();
        aVar.k0.f(0);
        aVar.k0.e(0);
    }
}
