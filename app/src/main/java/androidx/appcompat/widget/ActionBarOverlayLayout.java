package androidx.appcompat.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.OverScroller;
import androidx.recyclerview.widget.RecyclerView;
import i.b.f;
import i.b.k.WindowDecorActionBar;
import i.b.p.ViewPropertyAnimatorCompatSet;
import i.b.p.i.MenuPresenter;
import i.b.q.DecorContentParent;
import i.b.q.DecorToolbar;
import i.b.q.ViewUtils;
import i.h.l.NestedScrollingParent2;
import i.h.l.NestedScrollingParent3;
import i.h.l.NestedScrollingParentHelper;
import i.h.l.ViewCompat;
import j.a.a.a.outline;

public class ActionBarOverlayLayout extends ViewGroup implements DecorContentParent, NestedScrollingParent2, NestedScrollingParent3 {
    public static final int[] C = {i.b.a.actionBarSize, 16842841};
    public final Runnable A;
    public final NestedScrollingParentHelper B;
    public int b;
    public int c;
    public ContentFrameLayout d;

    /* renamed from: e  reason: collision with root package name */
    public ActionBarContainer f72e;

    /* renamed from: f  reason: collision with root package name */
    public DecorToolbar f73f;
    public Drawable g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f74i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f75j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f76k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f77l;

    /* renamed from: m  reason: collision with root package name */
    public int f78m;

    /* renamed from: n  reason: collision with root package name */
    public int f79n;

    /* renamed from: o  reason: collision with root package name */
    public final Rect f80o;

    /* renamed from: p  reason: collision with root package name */
    public final Rect f81p;

    /* renamed from: q  reason: collision with root package name */
    public final Rect f82q;

    /* renamed from: r  reason: collision with root package name */
    public final Rect f83r;

    /* renamed from: s  reason: collision with root package name */
    public final Rect f84s;

    /* renamed from: t  reason: collision with root package name */
    public final Rect f85t;
    public final Rect u;
    public d v;
    public OverScroller w;
    public ViewPropertyAnimator x;
    public final AnimatorListenerAdapter y;
    public final Runnable z;

    public class a extends AnimatorListenerAdapter {
        public a() {
        }

        public void onAnimationCancel(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.x = null;
            actionBarOverlayLayout.f77l = false;
        }

        public void onAnimationEnd(Animator animator) {
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.x = null;
            actionBarOverlayLayout.f77l = false;
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            ActionBarOverlayLayout.this.c();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.x = actionBarOverlayLayout.f72e.animate().translationY(0.0f).setListener(ActionBarOverlayLayout.this.y);
        }
    }

    public class c implements Runnable {
        public c() {
        }

        public void run() {
            ActionBarOverlayLayout.this.c();
            ActionBarOverlayLayout actionBarOverlayLayout = ActionBarOverlayLayout.this;
            actionBarOverlayLayout.x = actionBarOverlayLayout.f72e.animate().translationY((float) (-ActionBarOverlayLayout.this.f72e.getHeight())).setListener(ActionBarOverlayLayout.this.y);
        }
    }

    public interface d {
    }

    public static class e extends ViewGroup.MarginLayoutParams {
        public e(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public e(int i2, int i3) {
            super(i2, i3);
        }

        public e(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public ActionBarOverlayLayout(Context context) {
        this(context, null);
    }

    public final void a(Context context) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(C);
        boolean z2 = false;
        this.b = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        Drawable drawable = obtainStyledAttributes.getDrawable(1);
        this.g = drawable;
        setWillNotDraw(drawable == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion < 19) {
            z2 = true;
        }
        this.h = z2;
        this.w = new OverScroller(context);
    }

    public void a(View view, int i2, int i3, int[] iArr, int i4) {
    }

    public boolean b(View view, View view2, int i2, int i3) {
        return i3 == 0 && onStartNestedScroll(view, view2, i2);
    }

    public void c() {
        removeCallbacks(this.z);
        removeCallbacks(this.A);
        ViewPropertyAnimator viewPropertyAnimator = this.x;
        if (viewPropertyAnimator != null) {
            viewPropertyAnimator.cancel();
        }
    }

    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof e;
    }

    public boolean d() {
        i();
        return this.f73f.d();
    }

    public void draw(Canvas canvas) {
        int i2;
        super.draw(canvas);
        if (this.g != null && !this.h) {
            if (this.f72e.getVisibility() == 0) {
                i2 = (int) (this.f72e.getTranslationY() + ((float) this.f72e.getBottom()) + 0.5f);
            } else {
                i2 = 0;
            }
            this.g.setBounds(0, i2, getWidth(), this.g.getIntrinsicHeight() + i2);
            this.g.draw(canvas);
        }
    }

    public boolean e() {
        i();
        return this.f73f.e();
    }

    public void f() {
        i();
        this.f73f.f();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean
     arg types: [androidx.appcompat.widget.ActionBarContainer, android.graphics.Rect, int, int, int, int]
     candidates:
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, int, int, int, int, int):void
      i.h.l.NestedScrollingParent2.a(android.view.View, int, int, int, int, int):void
      i.h.l.NestedScrollingParent2.a(android.view.View, int, int, int, int, int):void
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean */
    public boolean fitSystemWindows(Rect rect) {
        i();
        int q2 = ViewCompat.q(this) & 256;
        boolean a2 = a((View) this.f72e, rect, true, true, false, true);
        this.f83r.set(rect);
        ViewUtils.a(this, this.f83r, this.f80o);
        if (!this.f84s.equals(this.f83r)) {
            this.f84s.set(this.f83r);
            a2 = true;
        }
        if (!this.f81p.equals(this.f80o)) {
            this.f81p.set(this.f80o);
            a2 = true;
        }
        if (a2) {
            requestLayout();
        }
        return true;
    }

    public boolean g() {
        i();
        return this.f73f.g();
    }

    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new e(-1, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new e(getContext(), attributeSet);
    }

    public int getActionBarHideOffset() {
        ActionBarContainer actionBarContainer = this.f72e;
        if (actionBarContainer != null) {
            return -((int) actionBarContainer.getTranslationY());
        }
        return 0;
    }

    public int getNestedScrollAxes() {
        return this.B.a();
    }

    public CharSequence getTitle() {
        i();
        return this.f73f.getTitle();
    }

    public void h() {
        i();
        this.f73f.h();
    }

    public void i() {
        DecorToolbar decorToolbar;
        if (this.d == null) {
            this.d = (ContentFrameLayout) findViewById(f.action_bar_activity_content);
            this.f72e = (ActionBarContainer) findViewById(f.action_bar_container);
            View findViewById = findViewById(f.action_bar);
            if (findViewById instanceof DecorToolbar) {
                decorToolbar = (DecorToolbar) findViewById;
            } else if (findViewById instanceof Toolbar) {
                decorToolbar = ((Toolbar) findViewById).getWrapper();
            } else {
                StringBuilder a2 = outline.a("Can't make a decor toolbar out of ");
                a2.append(findViewById.getClass().getSimpleName());
                throw new IllegalStateException(a2.toString());
            }
            this.f73f = decorToolbar;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a(getContext());
        ViewCompat.B(this);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        c();
    }

    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        getPaddingRight();
        int paddingTop = getPaddingTop();
        getPaddingBottom();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                e eVar = (e) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i7 = eVar.leftMargin + paddingLeft;
                int i8 = eVar.topMargin + paddingTop;
                childAt.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean
     arg types: [androidx.appcompat.widget.ContentFrameLayout, android.graphics.Rect, int, int, int, int]
     candidates:
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, int, int, int, int, int):void
      i.h.l.NestedScrollingParent2.a(android.view.View, int, int, int, int, int):void
      i.h.l.NestedScrollingParent2.a(android.view.View, int, int, int, int, int):void
      androidx.appcompat.widget.ActionBarOverlayLayout.a(android.view.View, android.graphics.Rect, boolean, boolean, boolean, boolean):boolean */
    public void onMeasure(int i2, int i3) {
        int i4;
        i();
        measureChildWithMargins(this.f72e, i2, 0, i3, 0);
        e eVar = (e) this.f72e.getLayoutParams();
        int max = Math.max(0, this.f72e.getMeasuredWidth() + eVar.leftMargin + eVar.rightMargin);
        int max2 = Math.max(0, this.f72e.getMeasuredHeight() + eVar.topMargin + eVar.bottomMargin);
        int combineMeasuredStates = View.combineMeasuredStates(0, this.f72e.getMeasuredState());
        boolean z2 = (ViewCompat.q(this) & 256) != 0;
        if (z2) {
            i4 = this.b;
            if (this.f75j && this.f72e.getTabContainer() != null) {
                i4 += this.b;
            }
        } else {
            i4 = this.f72e.getVisibility() != 8 ? this.f72e.getMeasuredHeight() : 0;
        }
        this.f82q.set(this.f80o);
        this.f85t.set(this.f83r);
        if (this.f74i || z2) {
            Rect rect = this.f85t;
            rect.top += i4;
            rect.bottom += 0;
        } else {
            Rect rect2 = this.f82q;
            rect2.top += i4;
            rect2.bottom += 0;
        }
        a((View) this.d, this.f82q, true, true, true, true);
        if (!this.u.equals(this.f85t)) {
            this.u.set(this.f85t);
            this.d.a(this.f85t);
        }
        measureChildWithMargins(this.d, i2, 0, i3, 0);
        e eVar2 = (e) this.d.getLayoutParams();
        int max3 = Math.max(max, this.d.getMeasuredWidth() + eVar2.leftMargin + eVar2.rightMargin);
        int max4 = Math.max(max2, this.d.getMeasuredHeight() + eVar2.topMargin + eVar2.bottomMargin);
        int combineMeasuredStates2 = View.combineMeasuredStates(combineMeasuredStates, this.d.getMeasuredState());
        setMeasuredDimension(View.resolveSizeAndState(Math.max(getPaddingRight() + getPaddingLeft() + max3, getSuggestedMinimumWidth()), i2, combineMeasuredStates2), View.resolveSizeAndState(Math.max(getPaddingBottom() + getPaddingTop() + max4, getSuggestedMinimumHeight()), i3, combineMeasuredStates2 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        boolean z3 = false;
        if (!this.f76k || !z2) {
            return false;
        }
        this.w.fling(0, 0, 0, (int) f3, 0, 0, RecyclerView.UNDEFINED_DURATION, Integer.MAX_VALUE);
        if (this.w.getFinalY() > this.f72e.getHeight()) {
            z3 = true;
        }
        if (z3) {
            c();
            this.A.run();
        } else {
            c();
            this.z.run();
        }
        this.f77l = true;
        return true;
    }

    public boolean onNestedPreFling(View view, float f2, float f3) {
        return false;
    }

    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        int i6 = this.f78m + i3;
        this.f78m = i6;
        setActionBarHideOffset(i6);
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        WindowDecorActionBar windowDecorActionBar;
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        this.B.a = i2;
        this.f78m = getActionBarHideOffset();
        c();
        d dVar = this.v;
        if (dVar != null && (viewPropertyAnimatorCompatSet = (windowDecorActionBar = (WindowDecorActionBar) dVar).u) != null) {
            viewPropertyAnimatorCompatSet.a();
            windowDecorActionBar.u = null;
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        if ((i2 & 2) == 0 || this.f72e.getVisibility() != 0) {
            return false;
        }
        return this.f76k;
    }

    public void onStopNestedScroll(View view) {
        if (this.f76k && !this.f77l) {
            if (this.f78m <= this.f72e.getHeight()) {
                c();
                postDelayed(this.z, 600);
            } else {
                c();
                postDelayed(this.A, 600);
            }
        }
        d dVar = this.v;
        if (dVar != null && ((WindowDecorActionBar) dVar) == null) {
            throw null;
        }
    }

    public void onWindowSystemUiVisibilityChanged(int i2) {
        super.onWindowSystemUiVisibilityChanged(i2);
        i();
        int i3 = this.f79n ^ i2;
        this.f79n = i2;
        boolean z2 = (i2 & 4) == 0;
        boolean z3 = (i2 & 256) != 0;
        d dVar = this.v;
        if (dVar != null) {
            ((WindowDecorActionBar) dVar).f789p = !z3;
            if (z2 || !z3) {
                WindowDecorActionBar windowDecorActionBar = (WindowDecorActionBar) this.v;
                if (windowDecorActionBar.f791r) {
                    windowDecorActionBar.f791r = false;
                    windowDecorActionBar.g(true);
                }
            } else {
                WindowDecorActionBar windowDecorActionBar2 = (WindowDecorActionBar) dVar;
                if (!windowDecorActionBar2.f791r) {
                    windowDecorActionBar2.f791r = true;
                    windowDecorActionBar2.g(true);
                }
            }
        }
        if ((i3 & 256) != 0 && this.v != null) {
            ViewCompat.B(this);
        }
    }

    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        this.c = i2;
        d dVar = this.v;
        if (dVar != null) {
            ((WindowDecorActionBar) dVar).f788o = i2;
        }
    }

    public void setActionBarHideOffset(int i2) {
        c();
        this.f72e.setTranslationY((float) (-Math.max(0, Math.min(i2, this.f72e.getHeight()))));
    }

    public void setActionBarVisibilityCallback(d dVar) {
        this.v = dVar;
        if (getWindowToken() != null) {
            ((WindowDecorActionBar) this.v).f788o = this.c;
            int i2 = this.f79n;
            if (i2 != 0) {
                onWindowSystemUiVisibilityChanged(i2);
                ViewCompat.B(this);
            }
        }
    }

    public void setHasNonEmbeddedTabs(boolean z2) {
        this.f75j = z2;
    }

    public void setHideOnContentScrollEnabled(boolean z2) {
        if (z2 != this.f76k) {
            this.f76k = z2;
            if (!z2) {
                c();
                setActionBarHideOffset(0);
            }
        }
    }

    public void setIcon(int i2) {
        i();
        this.f73f.setIcon(i2);
    }

    public void setLogo(int i2) {
        i();
        this.f73f.b(i2);
    }

    public void setOverlayMode(boolean z2) {
        this.f74i = z2;
        this.h = z2 && getContext().getApplicationInfo().targetSdkVersion < 19;
    }

    public void setShowingForActionMode(boolean z2) {
    }

    public void setUiOptions(int i2) {
    }

    public void setWindowCallback(Window.Callback callback) {
        i();
        this.f73f.setWindowCallback(callback);
    }

    public void setWindowTitle(CharSequence charSequence) {
        i();
        this.f73f.setWindowTitle(charSequence);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0;
        this.f80o = new Rect();
        this.f81p = new Rect();
        this.f82q = new Rect();
        this.f83r = new Rect();
        this.f84s = new Rect();
        this.f85t = new Rect();
        this.u = new Rect();
        this.y = new a();
        this.z = new b();
        this.A = new c();
        a(context);
        this.B = new NestedScrollingParentHelper();
    }

    public boolean b() {
        i();
        return this.f73f.b();
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new e(layoutParams);
    }

    public void setIcon(Drawable drawable) {
        i();
        this.f73f.setIcon(drawable);
    }

    public final boolean a(View view, Rect rect, boolean z2, boolean z3, boolean z4, boolean z5) {
        boolean z6;
        int i2;
        int i3;
        int i4;
        int i5;
        e eVar = (e) view.getLayoutParams();
        if (!z2 || eVar.leftMargin == (i5 = rect.left)) {
            z6 = false;
        } else {
            eVar.leftMargin = i5;
            z6 = true;
        }
        if (z3 && eVar.topMargin != (i4 = rect.top)) {
            eVar.topMargin = i4;
            z6 = true;
        }
        if (z5 && eVar.rightMargin != (i3 = rect.right)) {
            eVar.rightMargin = i3;
            z6 = true;
        }
        if (!z4 || eVar.bottomMargin == (i2 = rect.bottom)) {
            return z6;
        }
        eVar.bottomMargin = i2;
        return true;
    }

    public void a(View view, View view2, int i2, int i3) {
        if (i3 == 0) {
            onNestedScrollAccepted(view, view2, i2);
        }
    }

    public void a(View view, int i2) {
        if (i2 == 0) {
            onStopNestedScroll(view);
        }
    }

    public void a(View view, int i2, int i3, int i4, int i5, int i6) {
        if (i6 == 0) {
            int i7 = this.f78m + i3;
            this.f78m = i7;
            setActionBarHideOffset(i7);
        }
    }

    public void a(View view, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
        if (i6 == 0) {
            int i7 = this.f78m + i3;
            this.f78m = i7;
            setActionBarHideOffset(i7);
        }
    }

    public void a(int i2) {
        i();
        if (i2 == 2) {
            this.f73f.m();
        } else if (i2 == 5) {
            this.f73f.o();
        } else if (i2 == 109) {
            setOverlayMode(true);
        }
    }

    public boolean a() {
        i();
        return this.f73f.a();
    }

    public void a(Menu menu, MenuPresenter.a aVar) {
        i();
        this.f73f.a(menu, aVar);
    }
}
