package androidx.cardview.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import i.d.b;
import i.d.c;
import i.d.d;
import i.d.e.CardViewApi21Impl;
import i.d.e.CardViewDelegate;
import i.d.e.CardViewImpl;
import i.d.e.RoundRectDrawable;

public class CardView extends FrameLayout {

    /* renamed from: i  reason: collision with root package name */
    public static final int[] f144i = {16842801};

    /* renamed from: j  reason: collision with root package name */
    public static final CardViewImpl f145j = new CardViewApi21Impl();
    public boolean b;
    public boolean c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f146e;

    /* renamed from: f  reason: collision with root package name */
    public final Rect f147f;
    public final Rect g;
    public final CardViewDelegate h;

    public class a implements CardViewDelegate {
        public Drawable a;

        public a() {
        }

        public boolean a() {
            return CardView.this.getPreventCornerOverlap();
        }

        public void a(int i2, int i3, int i4, int i5) {
            CardView.this.g.set(i2, i3, i4, i5);
            CardView cardView = CardView.this;
            Rect rect = cardView.f147f;
            CardView.super.setPadding(i2 + rect.left, i3 + rect.top, i4 + rect.right, i5 + rect.bottom);
        }
    }

    public CardView(Context context) {
        this(context, null);
    }

    public ColorStateList getCardBackgroundColor() {
        return ((CardViewApi21Impl) f145j).a(this.h).h;
    }

    public float getCardElevation() {
        return CardView.this.getElevation();
    }

    public int getContentPaddingBottom() {
        return this.f147f.bottom;
    }

    public int getContentPaddingLeft() {
        return this.f147f.left;
    }

    public int getContentPaddingRight() {
        return this.f147f.right;
    }

    public int getContentPaddingTop() {
        return this.f147f.top;
    }

    public float getMaxCardElevation() {
        return ((CardViewApi21Impl) f145j).b(this.h);
    }

    public boolean getPreventCornerOverlap() {
        return this.c;
    }

    public float getRadius() {
        return ((CardViewApi21Impl) f145j).c(this.h);
    }

    public boolean getUseCompatPadding() {
        return this.b;
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void setCardBackgroundColor(int i2) {
        CardViewImpl cardViewImpl = f145j;
        CardViewDelegate cardViewDelegate = this.h;
        ColorStateList valueOf = ColorStateList.valueOf(i2);
        RoundRectDrawable a2 = ((CardViewApi21Impl) cardViewImpl).a(cardViewDelegate);
        a2.a(valueOf);
        a2.invalidateSelf();
    }

    public void setCardElevation(float f2) {
        CardView.this.setElevation(f2);
    }

    public void setMaxCardElevation(float f2) {
        ((CardViewApi21Impl) f145j).a(this.h, f2);
    }

    public void setMinimumHeight(int i2) {
        this.f146e = i2;
        super.setMinimumHeight(i2);
    }

    public void setMinimumWidth(int i2) {
        this.d = i2;
        super.setMinimumWidth(i2);
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
    }

    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
    }

    public void setPreventCornerOverlap(boolean z) {
        if (z != this.c) {
            this.c = z;
            CardViewImpl cardViewImpl = f145j;
            CardViewDelegate cardViewDelegate = this.h;
            CardViewApi21Impl cardViewApi21Impl = (CardViewApi21Impl) cardViewImpl;
            cardViewApi21Impl.a(cardViewDelegate, cardViewApi21Impl.a(cardViewDelegate).f1052e);
        }
    }

    public void setRadius(float f2) {
        RoundRectDrawable a2 = ((CardViewApi21Impl) f145j).a(this.h);
        if (f2 != a2.a) {
            a2.a = f2;
            a2.a((Rect) null);
            a2.invalidateSelf();
        }
    }

    public void setUseCompatPadding(boolean z) {
        if (this.b != z) {
            this.b = z;
            CardViewImpl cardViewImpl = f145j;
            CardViewDelegate cardViewDelegate = this.h;
            CardViewApi21Impl cardViewApi21Impl = (CardViewApi21Impl) cardViewImpl;
            cardViewApi21Impl.a(cardViewDelegate, cardViewApi21Impl.a(cardViewDelegate).f1052e);
        }
    }

    public CardView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.d.a.cardViewStyle);
    }

    public CardView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        ColorStateList colorStateList;
        int i3;
        this.f147f = new Rect();
        this.g = new Rect();
        this.h = new a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.CardView, i2, c.CardView);
        if (obtainStyledAttributes.hasValue(d.CardView_cardBackgroundColor)) {
            colorStateList = obtainStyledAttributes.getColorStateList(d.CardView_cardBackgroundColor);
        } else {
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(f144i);
            int color = obtainStyledAttributes2.getColor(0, 0);
            obtainStyledAttributes2.recycle();
            float[] fArr = new float[3];
            Color.colorToHSV(color, fArr);
            if (fArr[2] > 0.5f) {
                i3 = getResources().getColor(b.cardview_light_background);
            } else {
                i3 = getResources().getColor(b.cardview_dark_background);
            }
            colorStateList = ColorStateList.valueOf(i3);
        }
        float dimension = obtainStyledAttributes.getDimension(d.CardView_cardCornerRadius, 0.0f);
        float dimension2 = obtainStyledAttributes.getDimension(d.CardView_cardElevation, 0.0f);
        float dimension3 = obtainStyledAttributes.getDimension(d.CardView_cardMaxElevation, 0.0f);
        this.b = obtainStyledAttributes.getBoolean(d.CardView_cardUseCompatPadding, false);
        this.c = obtainStyledAttributes.getBoolean(d.CardView_cardPreventCornerOverlap, true);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPadding, 0);
        this.f147f.left = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingLeft, dimensionPixelSize);
        this.f147f.top = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingTop, dimensionPixelSize);
        this.f147f.right = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingRight, dimensionPixelSize);
        this.f147f.bottom = obtainStyledAttributes.getDimensionPixelSize(d.CardView_contentPaddingBottom, dimensionPixelSize);
        dimension3 = dimension2 > dimension3 ? dimension2 : dimension3;
        this.d = obtainStyledAttributes.getDimensionPixelSize(d.CardView_android_minWidth, 0);
        this.f146e = obtainStyledAttributes.getDimensionPixelSize(d.CardView_android_minHeight, 0);
        obtainStyledAttributes.recycle();
        CardViewImpl cardViewImpl = f145j;
        CardViewDelegate cardViewDelegate = this.h;
        RoundRectDrawable roundRectDrawable = new RoundRectDrawable(colorStateList, dimension);
        a aVar = (a) cardViewDelegate;
        aVar.a = roundRectDrawable;
        CardView.this.setBackgroundDrawable(roundRectDrawable);
        CardView cardView = CardView.this;
        cardView.setClipToOutline(true);
        cardView.setElevation(dimension2);
        ((CardViewApi21Impl) cardViewImpl).a(cardViewDelegate, dimension3);
    }

    public void setCardBackgroundColor(ColorStateList colorStateList) {
        RoundRectDrawable a2 = ((CardViewApi21Impl) f145j).a(this.h);
        a2.a(colorStateList);
        a2.invalidateSelf();
    }
}
