package androidx.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import androidx.preference.Preference;
import i.b.k.ResourcesFlusher;
import i.q.c;
import i.q.d;

public class ListPreference extends DialogPreference {

    /* renamed from: q  reason: collision with root package name */
    public CharSequence[] f253q;

    /* renamed from: r  reason: collision with root package name */
    public CharSequence[] f254r;

    /* renamed from: s  reason: collision with root package name */
    public String f255s;

    /* renamed from: t  reason: collision with root package name */
    public String f256t;

    public static final class a implements Preference.a<ListPreference> {
        public static a a;

        public CharSequence a(Preference preference) {
            ListPreference listPreference = (ListPreference) preference;
            if (TextUtils.isEmpty(listPreference.j())) {
                return listPreference.b.getString(c.not_set);
            }
            return listPreference.j();
        }
    }

    public ListPreference(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.ListPreference, i2, i3);
        this.f253q = ResourcesFlusher.b(obtainStyledAttributes, d.ListPreference_entries, d.ListPreference_android_entries);
        int i4 = d.ListPreference_entryValues;
        int i5 = d.ListPreference_android_entryValues;
        CharSequence[] textArray = obtainStyledAttributes.getTextArray(i4);
        this.f254r = textArray == null ? obtainStyledAttributes.getTextArray(i5) : textArray;
        int i6 = d.ListPreference_useSimpleSummaryProvider;
        if (obtainStyledAttributes.getBoolean(i6, obtainStyledAttributes.getBoolean(i6, false))) {
            if (a.a == null) {
                a.a = new a();
            }
            this.f265o = a.a;
            h();
        }
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, d.Preference, i2, i3);
        this.f256t = ResourcesFlusher.a(obtainStyledAttributes2, d.Preference_summary, d.Preference_android_summary);
        obtainStyledAttributes2.recycle();
    }

    public Object a(TypedArray typedArray, int i2) {
        return typedArray.getString(i2);
    }

    public CharSequence f() {
        Preference.a aVar = this.f265o;
        if (aVar != null) {
            return aVar.a(this);
        }
        Object j2 = j();
        CharSequence f2 = super.f();
        String str = this.f256t;
        if (str == null) {
            return f2;
        }
        Object[] objArr = new Object[1];
        if (j2 == null) {
            j2 = "";
        }
        objArr[0] = j2;
        String format = String.format(str, objArr);
        if (TextUtils.equals(format, f2)) {
            return f2;
        }
        Log.w("ListPreference", "Setting a summary with a String formatting marker is no longer supported. You should use a SummaryProvider instead.");
        return format;
    }

    public CharSequence j() {
        CharSequence[] charSequenceArr;
        CharSequence[] charSequenceArr2;
        String str = this.f255s;
        int i2 = -1;
        if (str != null && (charSequenceArr2 = this.f254r) != null) {
            int length = charSequenceArr2.length - 1;
            while (true) {
                if (length < 0) {
                    break;
                } else if (this.f254r[length].equals(str)) {
                    i2 = length;
                    break;
                } else {
                    length--;
                }
            }
        }
        if (i2 < 0 || (charSequenceArr = this.f253q) == null) {
            return null;
        }
        return charSequenceArr[i2];
    }

    public ListPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, ResourcesFlusher.a(context, i.q.a.dialogPreferenceStyle, 16842897), 0);
    }
}
