package androidx.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

public abstract class TwoStatePreference extends Preference {

    /* renamed from: p  reason: collision with root package name */
    public boolean f273p;

    /* renamed from: q  reason: collision with root package name */
    public CharSequence f274q;

    /* renamed from: r  reason: collision with root package name */
    public CharSequence f275r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f276s;

    /* renamed from: t  reason: collision with root package name */
    public boolean f277t;

    public TwoStatePreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0, 0);
    }

    public void a(boolean z) {
        boolean z2 = this.f273p != z;
        if (z2 || !this.f276s) {
            this.f273p = z;
            this.f276s = true;
            if (z2) {
                i();
            }
        }
    }

    public boolean i() {
        if ((this.f277t ? this.f273p : !this.f273p) || super.i()) {
            return true;
        }
        return false;
    }

    public TwoStatePreference(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
    }

    public Object a(TypedArray typedArray, int i2) {
        return Boolean.valueOf(typedArray.getBoolean(i2, false));
    }
}
