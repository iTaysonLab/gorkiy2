package androidx.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.q.b;
import i.q.d;

public class Preference implements Comparable<Preference> {
    public Context b;
    public int c;
    public CharSequence d;

    /* renamed from: e  reason: collision with root package name */
    public CharSequence f257e;

    /* renamed from: f  reason: collision with root package name */
    public String f258f;
    public String g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f259i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f260j;

    /* renamed from: k  reason: collision with root package name */
    public Object f261k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f262l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f263m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f264n;

    /* renamed from: o  reason: collision with root package name */
    public a f265o;

    public interface a<T extends Preference> {
        CharSequence a(T t2);
    }

    public Preference(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.c = Integer.MAX_VALUE;
        this.h = true;
        this.f259i = true;
        this.f260j = true;
        this.f262l = true;
        this.f263m = true;
        this.b = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, d.Preference, i2, i3);
        ResourcesFlusher.a(obtainStyledAttributes, d.Preference_icon, d.Preference_android_icon, 0);
        int i4 = d.Preference_key;
        int i5 = d.Preference_android_key;
        String string = obtainStyledAttributes.getString(i4);
        this.f258f = string == null ? obtainStyledAttributes.getString(i5) : string;
        int i6 = d.Preference_title;
        int i7 = d.Preference_android_title;
        CharSequence text = obtainStyledAttributes.getText(i6);
        this.d = text == null ? obtainStyledAttributes.getText(i7) : text;
        int i8 = d.Preference_summary;
        int i9 = d.Preference_android_summary;
        CharSequence text2 = obtainStyledAttributes.getText(i8);
        this.f257e = text2 == null ? obtainStyledAttributes.getText(i9) : text2;
        this.c = obtainStyledAttributes.getInt(d.Preference_order, obtainStyledAttributes.getInt(d.Preference_android_order, Integer.MAX_VALUE));
        int i10 = d.Preference_fragment;
        int i11 = d.Preference_android_fragment;
        String string2 = obtainStyledAttributes.getString(i10);
        this.g = string2 == null ? obtainStyledAttributes.getString(i11) : string2;
        obtainStyledAttributes.getResourceId(d.Preference_layout, obtainStyledAttributes.getResourceId(d.Preference_android_layout, b.preference));
        obtainStyledAttributes.getResourceId(d.Preference_widgetLayout, obtainStyledAttributes.getResourceId(d.Preference_android_widgetLayout, 0));
        this.h = obtainStyledAttributes.getBoolean(d.Preference_enabled, obtainStyledAttributes.getBoolean(d.Preference_android_enabled, true));
        this.f259i = obtainStyledAttributes.getBoolean(d.Preference_selectable, obtainStyledAttributes.getBoolean(d.Preference_android_selectable, true));
        this.f260j = obtainStyledAttributes.getBoolean(d.Preference_persistent, obtainStyledAttributes.getBoolean(d.Preference_android_persistent, true));
        int i12 = d.Preference_dependency;
        int i13 = d.Preference_android_dependency;
        if (obtainStyledAttributes.getString(i12) == null) {
            obtainStyledAttributes.getString(i13);
        }
        int i14 = d.Preference_allowDividerAbove;
        obtainStyledAttributes.getBoolean(i14, obtainStyledAttributes.getBoolean(i14, this.f259i));
        int i15 = d.Preference_allowDividerBelow;
        obtainStyledAttributes.getBoolean(i15, obtainStyledAttributes.getBoolean(i15, this.f259i));
        if (obtainStyledAttributes.hasValue(d.Preference_defaultValue)) {
            this.f261k = a(obtainStyledAttributes, d.Preference_defaultValue);
        } else if (obtainStyledAttributes.hasValue(d.Preference_android_defaultValue)) {
            this.f261k = a(obtainStyledAttributes, d.Preference_android_defaultValue);
        }
        obtainStyledAttributes.getBoolean(d.Preference_shouldDisableView, obtainStyledAttributes.getBoolean(d.Preference_android_shouldDisableView, true));
        boolean hasValue = obtainStyledAttributes.hasValue(d.Preference_singleLineTitle);
        this.f264n = hasValue;
        if (hasValue) {
            obtainStyledAttributes.getBoolean(d.Preference_singleLineTitle, obtainStyledAttributes.getBoolean(d.Preference_android_singleLineTitle, true));
        }
        obtainStyledAttributes.getBoolean(d.Preference_iconSpaceReserved, obtainStyledAttributes.getBoolean(d.Preference_android_iconSpaceReserved, false));
        int i16 = d.Preference_isPreferenceVisible;
        obtainStyledAttributes.getBoolean(i16, obtainStyledAttributes.getBoolean(i16, true));
        int i17 = d.Preference_enableCopying;
        obtainStyledAttributes.getBoolean(i17, obtainStyledAttributes.getBoolean(i17, false));
        obtainStyledAttributes.recycle();
    }

    public Object a(TypedArray typedArray, int i2) {
        return null;
    }

    public int compareTo(Object obj) {
        Preference preference = (Preference) obj;
        int i2 = this.c;
        int i3 = preference.c;
        if (i2 != i3) {
            return i2 - i3;
        }
        CharSequence charSequence = this.d;
        CharSequence charSequence2 = preference.d;
        if (charSequence == charSequence2) {
            return 0;
        }
        if (charSequence == null) {
            return 1;
        }
        if (charSequence2 == null) {
            return -1;
        }
        return charSequence.toString().compareToIgnoreCase(preference.d.toString());
    }

    public CharSequence f() {
        a aVar = this.f265o;
        if (aVar != null) {
            return aVar.a(this);
        }
        return this.f257e;
    }

    public boolean g() {
        return this.h && this.f262l && this.f263m;
    }

    public void h() {
    }

    public boolean i() {
        return !g();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        CharSequence charSequence = this.d;
        if (!TextUtils.isEmpty(charSequence)) {
            sb.append(charSequence);
            sb.append(' ');
        }
        CharSequence f2 = f();
        if (!TextUtils.isEmpty(f2)) {
            sb.append(f2);
            sb.append(' ');
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public Preference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, ResourcesFlusher.a(context, i.q.a.preferenceStyle, 16842894), 0);
    }
}
