package androidx.versionedparcelable;

import android.os.Parcelable;
import android.text.TextUtils;
import i.e.ArrayMap;
import i.y.VersionedParcelParcel;
import i.y.VersionedParcelable;
import i.y.b;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class VersionedParcel {
    public final ArrayMap<String, Method> a;
    public final ArrayMap<String, Method> b;
    public final ArrayMap<String, Class> c;

    public static class ParcelException extends RuntimeException {
    }

    public VersionedParcel(ArrayMap<String, Method> arrayMap, ArrayMap<String, Method> arrayMap2, ArrayMap<String, Class> arrayMap3) {
        this.a = arrayMap;
        this.b = arrayMap2;
        this.c = arrayMap3;
    }

    public abstract void a();

    public abstract boolean a(int i2);

    public boolean a(boolean z, int i2) {
        if (!a(i2)) {
            return z;
        }
        return ((VersionedParcelParcel) this).f1522e.readInt() != 0;
    }

    public abstract VersionedParcel b();

    public abstract void b(int i2);

    public void b(int i2, int i3) {
        b(i3);
        ((VersionedParcelParcel) this).f1522e.writeInt(i2);
    }

    public <T extends b> T c() {
        String readString = ((VersionedParcelParcel) this).f1522e.readString();
        if (readString == null) {
            return null;
        }
        VersionedParcel b2 = b();
        try {
            return (VersionedParcelable) a(readString).invoke(null, b2);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e2);
        } catch (InvocationTargetException e3) {
            if (e3.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e3.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e3);
        } catch (NoSuchMethodException e4) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e4);
        } catch (ClassNotFoundException e5) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e5);
        }
    }

    public int a(int i2, int i3) {
        if (!a(i3)) {
            return i2;
        }
        return ((VersionedParcelParcel) this).f1522e.readInt();
    }

    public void b(Parcelable parcelable, int i2) {
        b(i2);
        ((VersionedParcelParcel) this).f1522e.writeParcelable(parcelable, 0);
    }

    public <T extends Parcelable> T a(Parcelable parcelable, int i2) {
        if (!a(i2)) {
            return parcelable;
        }
        return ((VersionedParcelParcel) this).f1522e.readParcelable(VersionedParcelParcel.class.getClassLoader());
    }

    public final Method b(Class cls) {
        Method orDefault = this.b.getOrDefault(cls.getName(), null);
        if (orDefault != null) {
            return orDefault;
        }
        Class a2 = a(cls);
        System.currentTimeMillis();
        Method declaredMethod = a2.getDeclaredMethod("write", cls, VersionedParcel.class);
        this.b.put(cls.getName(), declaredMethod);
        return declaredMethod;
    }

    public CharSequence a(CharSequence charSequence, int i2) {
        if (!a(i2)) {
            return charSequence;
        }
        return (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(((VersionedParcelParcel) this).f1522e);
    }

    public void a(VersionedParcelable versionedParcelable) {
        if (versionedParcelable == null) {
            ((VersionedParcelParcel) this).f1522e.writeString(null);
            return;
        }
        try {
            ((VersionedParcelParcel) this).f1522e.writeString(a((Class<? extends b>) versionedParcelable.getClass()).getName());
            VersionedParcel b2 = b();
            try {
                b(versionedParcelable.getClass()).invoke(null, versionedParcelable, b2);
                b2.a();
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e2);
            } catch (InvocationTargetException e3) {
                if (e3.getCause() instanceof RuntimeException) {
                    throw ((RuntimeException) e3.getCause());
                }
                throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e3);
            } catch (NoSuchMethodException e4) {
                throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e4);
            } catch (ClassNotFoundException e5) {
                throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e5);
            }
        } catch (ClassNotFoundException e6) {
            throw new RuntimeException(versionedParcelable.getClass().getSimpleName() + " does not have a Parcelizer", e6);
        }
    }

    public final Method a(String str) {
        Class<VersionedParcel> cls = VersionedParcel.class;
        Method orDefault = this.a.getOrDefault(str, null);
        if (orDefault != null) {
            return orDefault;
        }
        System.currentTimeMillis();
        Method declaredMethod = Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod("read", cls);
        this.a.put(str, declaredMethod);
        return declaredMethod;
    }

    public final Class a(Class<? extends b> cls) {
        Class orDefault = this.c.getOrDefault(cls.getName(), null);
        if (orDefault != null) {
            return orDefault;
        }
        Class<?> cls2 = Class.forName(String.format("%s.%sParcelizer", cls.getPackage().getName(), cls.getSimpleName()), false, cls.getClassLoader());
        this.c.put(cls.getName(), cls2);
        return cls2;
    }
}
