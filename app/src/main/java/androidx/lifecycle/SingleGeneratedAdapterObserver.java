package androidx.lifecycle;

import i.o.GeneratedAdapter;
import i.o.Lifecycle;
import i.o.LifecycleEventObserver;
import i.o.LifecycleOwner;

public class SingleGeneratedAdapterObserver implements LifecycleEventObserver {
    public final GeneratedAdapter a;

    public SingleGeneratedAdapterObserver(GeneratedAdapter generatedAdapter) {
        this.a = generatedAdapter;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        this.a.a(lifecycleOwner, aVar, false, null);
        this.a.a(lifecycleOwner, aVar, true, null);
    }
}
