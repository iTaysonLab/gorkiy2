package o.m0.d;

import j.a.a.a.outline;
import java.net.UnknownServiceException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLSocket;
import n.j.Comparisons;
import n.n.c.Intrinsics;
import o.CipherSuite;
import o.ConnectionSpec;
import o.m;
import o.m0.Util;

/* compiled from: ConnectionSpecSelector.kt */
public final class ConnectionSpecSelector {
    public int a;
    public boolean b;
    public boolean c;
    public final List<m> d;

    public ConnectionSpecSelector(List<m> list) {
        if (list != null) {
            this.d = list;
        } else {
            Intrinsics.a("connectionSpecs");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Object[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final ConnectionSpec a(SSLSocket sSLSocket) {
        ConnectionSpec connectionSpec;
        boolean z;
        String[] strArr;
        String[] strArr2;
        if (sSLSocket != null) {
            int i2 = this.a;
            int size = this.d.size();
            while (true) {
                if (i2 >= size) {
                    connectionSpec = null;
                    break;
                }
                connectionSpec = (ConnectionSpec) this.d.get(i2);
                if (connectionSpec.a(sSLSocket)) {
                    this.a = i2 + 1;
                    break;
                }
                i2++;
            }
            if (connectionSpec == null) {
                StringBuilder a2 = outline.a("Unable to find acceptable protocols. isFallback=");
                a2.append(this.c);
                a2.append(',');
                a2.append(" modes=");
                a2.append(this.d);
                a2.append(',');
                a2.append(" supported protocols=");
                String[] enabledProtocols = sSLSocket.getEnabledProtocols();
                if (enabledProtocols == null) {
                    Intrinsics.a();
                    throw null;
                }
                String arrays = Arrays.toString(enabledProtocols);
                Intrinsics.a((Object) arrays, "java.util.Arrays.toString(this)");
                a2.append(arrays);
                throw new UnknownServiceException(a2.toString());
            }
            int i3 = this.a;
            int size2 = this.d.size();
            while (true) {
                if (i3 >= size2) {
                    z = false;
                    break;
                } else if (this.d.get(i3).a(sSLSocket)) {
                    z = true;
                    break;
                } else {
                    i3++;
                }
            }
            this.b = z;
            boolean z2 = this.c;
            if (connectionSpec.c != null) {
                String[] enabledCipherSuites = sSLSocket.getEnabledCipherSuites();
                Intrinsics.a((Object) enabledCipherSuites, "sslSocket.enabledCipherSuites");
                String[] strArr3 = connectionSpec.c;
                CipherSuite.b bVar = CipherSuite.f2834t;
                strArr = Util.b(enabledCipherSuites, strArr3, CipherSuite.b);
            } else {
                strArr = sSLSocket.getEnabledCipherSuites();
            }
            if (connectionSpec.d != null) {
                String[] enabledProtocols2 = sSLSocket.getEnabledProtocols();
                Intrinsics.a((Object) enabledProtocols2, "sslSocket.enabledProtocols");
                strArr2 = Util.b(enabledProtocols2, connectionSpec.d, Comparisons.b);
            } else {
                strArr2 = sSLSocket.getEnabledProtocols();
            }
            String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
            Intrinsics.a((Object) supportedCipherSuites, "supportedCipherSuites");
            CipherSuite.b bVar2 = CipherSuite.f2834t;
            int a3 = Util.a(supportedCipherSuites, "TLS_FALLBACK_SCSV", CipherSuite.b);
            if (z2 && a3 != -1) {
                Intrinsics.a((Object) strArr, "cipherSuitesIntersection");
                String str = supportedCipherSuites[a3];
                Intrinsics.a((Object) str, "supportedCipherSuites[indexOfFallbackScsv]");
                Object[] copyOf = Arrays.copyOf(strArr, strArr.length + 1);
                Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
                strArr = (String[]) copyOf;
                strArr[strArr.length - 1] = str;
            }
            ConnectionSpec.a aVar = new ConnectionSpec.a(connectionSpec);
            Intrinsics.a((Object) strArr, "cipherSuitesIntersection");
            aVar.a((String[]) Arrays.copyOf(strArr, strArr.length));
            Intrinsics.a((Object) strArr2, "tlsVersionsIntersection");
            aVar.b((String[]) Arrays.copyOf(strArr2, strArr2.length));
            ConnectionSpec a4 = aVar.a();
            if (a4.b() != null) {
                sSLSocket.setEnabledProtocols(a4.d);
            }
            if (a4.a() != null) {
                sSLSocket.setEnabledCipherSuites(a4.c);
            }
            return connectionSpec;
        }
        Intrinsics.a("sslSocket");
        throw null;
    }
}
