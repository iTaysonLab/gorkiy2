package o.m0.d;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.b.Functions;
import n.n.c.Intrinsics;
import n.n.c.j;
import o.Handshake;

/* compiled from: RealConnection.kt */
public final class RealConnection1 extends j implements Functions<List<? extends X509Certificate>> {
    public final /* synthetic */ RealConnection c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RealConnection1(RealConnection realConnection) {
        super(0);
        this.c = realConnection;
    }

    public Object b() {
        Handshake handshake = this.c.d;
        if (handshake != null) {
            List<Certificate> a = handshake.a();
            ArrayList arrayList = new ArrayList(Collections.a(a, 10));
            for (Certificate certificate : a) {
                if (certificate != null) {
                    arrayList.add((X509Certificate) certificate);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
                }
            }
            return arrayList;
        }
        Intrinsics.a();
        throw null;
    }
}
