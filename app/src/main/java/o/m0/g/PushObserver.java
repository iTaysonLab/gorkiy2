package o.m0.g;

import java.util.List;
import p.BufferedSource;

/* compiled from: PushObserver.kt */
public interface PushObserver {
    public static final PushObserver a = new n$a();

    void a(int i2, ErrorCode errorCode);

    boolean a(int i2, List<b> list);

    boolean a(int i2, List<b> list, boolean z);

    boolean a(int i2, BufferedSource bufferedSource, int i3, boolean z);
}
