package o.m0.g;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import n.n.c.Intrinsics;
import p.ByteString;

/* compiled from: Header.kt */
public final class Header {
    public static final ByteString d = ByteString.f3063f.b(":");

    /* renamed from: e  reason: collision with root package name */
    public static final ByteString f2988e = ByteString.f3063f.b(":status");

    /* renamed from: f  reason: collision with root package name */
    public static final ByteString f2989f = ByteString.f3063f.b(":method");
    public static final ByteString g = ByteString.f3063f.b(":path");
    public static final ByteString h = ByteString.f3063f.b(":scheme");

    /* renamed from: i  reason: collision with root package name */
    public static final ByteString f2990i = ByteString.f3063f.b(":authority");
    public final int a;
    public final ByteString b;
    public final ByteString c;

    public Header(ByteString byteString, ByteString byteString2) {
        if (byteString == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (byteString2 != null) {
            this.b = byteString;
            this.c = byteString2;
            this.a = byteString.g() + 32 + this.c.g();
        } else {
            Intrinsics.a("value");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Header)) {
            return false;
        }
        Header header = (Header) obj;
        return Intrinsics.a(this.b, header.b) && Intrinsics.a(this.c, header.c);
    }

    public int hashCode() {
        ByteString byteString = this.b;
        int i2 = 0;
        int hashCode = (byteString != null ? byteString.hashCode() : 0) * 31;
        ByteString byteString2 = this.c;
        if (byteString2 != null) {
            i2 = byteString2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        ByteString byteString = this.b;
        if (byteString != null) {
            sb.append(p.b0.ByteString.h(byteString));
            sb.append(": ");
            ByteString byteString2 = this.c;
            if (byteString2 != null) {
                sb.append(p.b0.ByteString.h(byteString2));
                return sb.toString();
            }
            throw null;
        }
        throw null;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Header(String str, String str2) {
        this(ByteString.f3063f.b(str), ByteString.f3063f.b(str2));
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str2 != null) {
        } else {
            Intrinsics.a("value");
            throw null;
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Header(ByteString byteString, String str) {
        this(byteString, ByteString.f3063f.b(str));
        if (byteString == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str != null) {
        } else {
            Intrinsics.a("value");
            throw null;
        }
    }
}
