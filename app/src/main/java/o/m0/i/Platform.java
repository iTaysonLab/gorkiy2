package o.m0.i;

import j.a.a.a.outline;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.OkHttpClient;
import o.Protocol;
import o.c0;
import o.m0.k.BasicCertificateChainCleaner;
import o.m0.k.BasicTrustRootIndex;
import o.m0.k.CertificateChainCleaner;
import o.m0.k.TrustRootIndex;
import p.Buffer;

/* compiled from: Platform.kt */
public class Platform {
    public static volatile Platform a;
    public static final Logger b = Logger.getLogger(OkHttpClient.class.getName());
    public static final a c = new a(null);

    /* compiled from: Platform.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final List<String> a(List<? extends c0> list) {
            if (list != null) {
                ArrayList<Protocol> arrayList = new ArrayList<>();
                for (T next : list) {
                    if (((Protocol) next) != Protocol.HTTP_1_0) {
                        arrayList.add(next);
                    }
                }
                ArrayList arrayList2 = new ArrayList(Collections.a(arrayList, 10));
                for (Protocol protocol : arrayList) {
                    arrayList2.add(protocol.protocol);
                }
                return arrayList2;
            }
            Intrinsics.a("protocols");
            throw null;
        }

        public final byte[] b(List<? extends c0> list) {
            if (list != null) {
                Buffer buffer = new Buffer();
                Iterator it = ((ArrayList) a(list)).iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    buffer.writeByte(str.length());
                    buffer.a(str);
                }
                return buffer.i(buffer.c);
            }
            Intrinsics.a("protocols");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.Provider, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Method, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003e, code lost:
        if (r0 != null) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0064, code lost:
        if (r0 != null) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00aa, code lost:
        if (java.lang.Integer.parseInt(r3) >= 9) goto L_0x010e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0112  */
    static {
        /*
            o.m0.i.Platform$a r0 = new o.m0.i.Platform$a
            r1 = 0
            r0.<init>(r1)
            o.m0.i.Platform.c = r0
            o.m0.i.AndroidPlatform$b r0 = o.m0.i.AndroidPlatform.g
            boolean r0 = o.m0.i.AndroidPlatform.f3044f
            if (r0 == 0) goto L_0x0014
            o.m0.i.AndroidPlatform r0 = new o.m0.i.AndroidPlatform
            r0.<init>()
            goto L_0x0015
        L_0x0014:
            r0 = r1
        L_0x0015:
            if (r0 == 0) goto L_0x0019
            goto L_0x0117
        L_0x0019:
            java.security.Provider[] r0 = java.security.Security.getProviders()
            r2 = 0
            r0 = r0[r2]
            java.lang.String r3 = "Security.getProviders()[0]"
            n.n.c.Intrinsics.a(r0, r3)
            java.lang.String r0 = r0.getName()
            java.lang.String r4 = "Conscrypt"
            boolean r0 = n.n.c.Intrinsics.a(r4, r0)
            if (r0 == 0) goto L_0x0042
            o.m0.i.ConscryptPlatform$a r0 = o.m0.i.ConscryptPlatform.f3047f
            boolean r0 = o.m0.i.ConscryptPlatform.f3046e
            if (r0 == 0) goto L_0x003d
            o.m0.i.ConscryptPlatform r0 = new o.m0.i.ConscryptPlatform
            r0.<init>()
            goto L_0x003e
        L_0x003d:
            r0 = r1
        L_0x003e:
            if (r0 == 0) goto L_0x0042
            goto L_0x0117
        L_0x0042:
            java.security.Provider[] r0 = java.security.Security.getProviders()
            r0 = r0[r2]
            n.n.c.Intrinsics.a(r0, r3)
            java.lang.String r0 = r0.getName()
            java.lang.String r3 = "OpenJSSE"
            boolean r0 = n.n.c.Intrinsics.a(r3, r0)
            if (r0 == 0) goto L_0x0068
            o.m0.i.OpenJSSEPlatform r0 = o.m0.i.OpenJSSEPlatform.f3052f
            boolean r0 = o.m0.i.OpenJSSEPlatform.f3051e
            if (r0 == 0) goto L_0x0063
            o.m0.i.OpenJSSEPlatform r0 = new o.m0.i.OpenJSSEPlatform
            r0.<init>()
            goto L_0x0064
        L_0x0063:
            r0 = r1
        L_0x0064:
            if (r0 == 0) goto L_0x0068
            goto L_0x0117
        L_0x0068:
            r0 = 1
            java.lang.Class<javax.net.ssl.SSLParameters> r3 = javax.net.ssl.SSLParameters.class
            java.lang.String r4 = "setApplicationProtocols"
            java.lang.Class[] r5 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.Class<java.lang.String[]> r6 = java.lang.String[].class
            r5[r2] = r6     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.Class<javax.net.ssl.SSLSocket> r4 = javax.net.ssl.SSLSocket.class
            java.lang.String r5 = "getApplicationProtocol"
            java.lang.Class[] r6 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.reflect.Method r4 = r4.getMethod(r5, r6)     // Catch:{ NoSuchMethodException -> 0x0091 }
            o.m0.i.Jdk9Platform r5 = new o.m0.i.Jdk9Platform     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.String r6 = "setProtocolMethod"
            n.n.c.Intrinsics.a(r3, r6)     // Catch:{ NoSuchMethodException -> 0x0091 }
            java.lang.String r6 = "getProtocolMethod"
            n.n.c.Intrinsics.a(r4, r6)     // Catch:{ NoSuchMethodException -> 0x0091 }
            r5.<init>(r3, r4)     // Catch:{ NoSuchMethodException -> 0x0091 }
            goto L_0x0092
        L_0x0091:
            r5 = r1
        L_0x0092:
            if (r5 == 0) goto L_0x0097
            r0 = r5
            goto L_0x0117
        L_0x0097:
            java.lang.String r3 = "java.specification.version"
            java.lang.String r4 = "unknown"
            java.lang.String r3 = java.lang.System.getProperty(r3, r4)
            java.lang.String r4 = "jvmVersion"
            n.n.c.Intrinsics.a(r3, r4)     // Catch:{ NumberFormatException -> 0x00ad }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x00ad }
            r4 = 9
            if (r3 < r4) goto L_0x00ad
            goto L_0x010e
        L_0x00ad:
            java.lang.String r3 = "org.eclipse.jetty.alpn.ALPN"
            java.lang.Class r3 = java.lang.Class.forName(r3, r0, r1)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r4 = "org.eclipse.jetty.alpn.ALPN$Provider"
            java.lang.Class r4 = java.lang.Class.forName(r4, r0, r1)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r5 = "org.eclipse.jetty.alpn.ALPN$ClientProvider"
            java.lang.Class r10 = java.lang.Class.forName(r5, r0, r1)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r5 = "org.eclipse.jetty.alpn.ALPN$ServerProvider"
            java.lang.Class r11 = java.lang.Class.forName(r5, r0, r1)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r5 = "put"
            r6 = 2
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.Class<javax.net.ssl.SSLSocket> r7 = javax.net.ssl.SSLSocket.class
            r6[r2] = r7     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            r6[r0] = r4     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.reflect.Method r7 = r3.getMethod(r5, r6)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r4 = "get"
            java.lang.Class[] r5 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.Class<javax.net.ssl.SSLSocket> r6 = javax.net.ssl.SSLSocket.class
            r5[r2] = r6     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.reflect.Method r8 = r3.getMethod(r4, r5)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r4 = "remove"
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.Class<javax.net.ssl.SSLSocket> r5 = javax.net.ssl.SSLSocket.class
            r0[r2] = r5     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.reflect.Method r9 = r3.getMethod(r4, r0)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            o.m0.i.Jdk8WithJettyBootPlatform r0 = new o.m0.i.Jdk8WithJettyBootPlatform     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r2 = "putMethod"
            n.n.c.Intrinsics.a(r7, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r2 = "getMethod"
            n.n.c.Intrinsics.a(r8, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r2 = "removeMethod"
            n.n.c.Intrinsics.a(r9, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r2 = "clientProviderClass"
            n.n.c.Intrinsics.a(r10, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            java.lang.String r2 = "serverProviderClass"
            n.n.c.Intrinsics.a(r11, r2)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            r6 = r0
            r6.<init>(r7, r8, r9, r10, r11)     // Catch:{ ClassNotFoundException | NoSuchMethodException -> 0x010d }
            r1 = r0
            goto L_0x010e
        L_0x010d:
        L_0x010e:
            if (r1 == 0) goto L_0x0112
            r0 = r1
            goto L_0x0117
        L_0x0112:
            o.m0.i.Platform r0 = new o.m0.i.Platform
            r0.<init>()
        L_0x0117:
            o.m0.i.Platform.a = r0
            java.lang.Class<o.OkHttpClient> r0 = o.OkHttpClient.class
            java.lang.String r0 = r0.getName()
            java.util.logging.Logger r0 = java.util.logging.Logger.getLogger(r0)
            o.m0.i.Platform.b = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.i.Platform.<clinit>():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.SSLContext, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SSLContext a() {
        SSLContext instance = SSLContext.getInstance("TLS");
        Intrinsics.a((Object) instance, "SSLContext.getInstance(\"TLS\")");
        return instance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.TrustManagerFactory, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public X509TrustManager b() {
        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        instance.init((KeyStore) null);
        Intrinsics.a((Object) instance, "factory");
        TrustManager[] trustManagers = instance.getTrustManagers();
        if (trustManagers != null) {
            boolean z = true;
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                z = false;
            }
            if (z) {
                TrustManager trustManager = trustManagers[0];
                if (trustManager != null) {
                    return (X509TrustManager) trustManager;
                }
                throw new TypeCastException("null cannot be cast to non-null type javax.net.ssl.X509TrustManager");
            }
            StringBuilder a2 = outline.a("Unexpected default trust managers: ");
            String arrays = Arrays.toString(trustManagers);
            Intrinsics.a((Object) arrays, "java.util.Arrays.toString(this)");
            a2.append(arrays);
            throw new IllegalStateException(a2.toString().toString());
        }
        Intrinsics.a();
        throw null;
    }

    public void c(X509TrustManager x509TrustManager) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        String simpleName = getClass().getSimpleName();
        Intrinsics.a((Object) simpleName, "javaClass.simpleName");
        return simpleName;
    }

    public void a(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        }
    }

    public void a(SSLSocketFactory sSLSocketFactory) {
        if (sSLSocketFactory == null) {
            Intrinsics.a("socketFactory");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        }
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i2) {
        if (socket == null) {
            Intrinsics.a("socket");
            throw null;
        } else if (inetSocketAddress != null) {
            socket.connect(inetSocketAddress, i2);
        } else {
            Intrinsics.a("address");
            throw null;
        }
    }

    public void a(int i2, String str, Throwable th) {
        if (str != null) {
            b.log(i2 == 5 ? Level.WARNING : Level.INFO, str, th);
        } else {
            Intrinsics.a("message");
            throw null;
        }
    }

    public Object a(String str) {
        if (str == null) {
            Intrinsics.a("closer");
            throw null;
        } else if (b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        } else {
            return null;
        }
    }

    public String b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            return null;
        }
        Intrinsics.a("sslSocket");
        throw null;
    }

    public boolean b(String str) {
        if (str != null) {
            return true;
        }
        Intrinsics.a("hostname");
        throw null;
    }

    public void a(String str, Object obj) {
        if (str != null) {
            if (obj == null) {
                str = outline.a(str, " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);");
            }
            a(5, str, (Throwable) obj);
            return;
        }
        Intrinsics.a("message");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.cert.X509Certificate[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public TrustRootIndex b(X509TrustManager x509TrustManager) {
        if (x509TrustManager != null) {
            X509Certificate[] acceptedIssuers = x509TrustManager.getAcceptedIssuers();
            Intrinsics.a((Object) acceptedIssuers, "trustManager.acceptedIssuers");
            return new BasicTrustRootIndex((X509Certificate[]) Arrays.copyOf(acceptedIssuers, acceptedIssuers.length));
        }
        Intrinsics.a("trustManager");
        throw null;
    }

    public CertificateChainCleaner a(X509TrustManager x509TrustManager) {
        if (x509TrustManager != null) {
            return new BasicCertificateChainCleaner(b(x509TrustManager));
        }
        Intrinsics.a("trustManager");
        throw null;
    }
}
