package o.m0.i;

import android.os.Build;
import j.a.a.a.outline;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.c0;
import o.m0.i.ConscryptPlatform;
import o.m0.i.g.CloseGuard;
import o.m0.i.g.ConscryptSocketAdapter;
import o.m0.i.g.DeferredSocketAdapter;
import o.m0.i.g.SocketAdapter;
import o.m0.i.g.StandardAndroidSocketAdapter;
import o.m0.i.g.e;
import o.m0.k.BasicCertificateChainCleaner;
import o.m0.k.CertificateChainCleaner;
import o.m0.k.TrustRootIndex;

/* compiled from: AndroidPlatform.kt */
public final class AndroidPlatform extends Platform {

    /* renamed from: f  reason: collision with root package name */
    public static final boolean f3044f;
    public static final b g = new b(null);
    public final List<e> d;

    /* renamed from: e  reason: collision with root package name */
    public final CloseGuard f3045e;

    /* compiled from: AndroidPlatform.kt */
    public static final class a extends CertificateChainCleaner {
        public final Object a;
        public final Method b;

        public a(Object obj, Method method) {
            if (obj == null) {
                Intrinsics.a("x509TrustManagerExtensions");
                throw null;
            } else if (method != null) {
                this.a = obj;
                this.b = method;
            } else {
                Intrinsics.a("checkServerTrusted");
                throw null;
            }
        }

        public List<Certificate> a(List<? extends Certificate> list, String str) {
            if (list == null) {
                Intrinsics.a("chain");
                throw null;
            } else if (str != null) {
                try {
                    Object[] array = list.toArray(new X509Certificate[0]);
                    if (array != null) {
                        Object invoke = this.b.invoke(this.a, (X509Certificate[]) array, "RSA", str);
                        if (invoke != null) {
                            return (List) invoke;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<java.security.cert.Certificate>");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                } catch (InvocationTargetException e2) {
                    SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e2.getMessage());
                    sSLPeerUnverifiedException.initCause(e2);
                    throw sSLPeerUnverifiedException;
                } catch (IllegalAccessException e3) {
                    throw new AssertionError(e3);
                }
            } else {
                Intrinsics.a("hostname");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            return obj instanceof a;
        }

        public int hashCode() {
            return 0;
        }
    }

    /* compiled from: AndroidPlatform.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: AndroidPlatform.kt */
    public static final class c implements TrustRootIndex {
        public final X509TrustManager a;
        public final Method b;

        public c(X509TrustManager x509TrustManager, Method method) {
            if (x509TrustManager == null) {
                Intrinsics.a("trustManager");
                throw null;
            } else if (method != null) {
                this.a = x509TrustManager;
                this.b = method;
            } else {
                Intrinsics.a("findByIssuerAndSignatureMethod");
                throw null;
            }
        }

        public X509Certificate a(X509Certificate x509Certificate) {
            if (x509Certificate != null) {
                try {
                    Object invoke = this.b.invoke(this.a, x509Certificate);
                    if (invoke != null) {
                        return ((TrustAnchor) invoke).getTrustedCert();
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.security.cert.TrustAnchor");
                } catch (IllegalAccessException e2) {
                    throw new AssertionError("unable to get issues and signature", e2);
                } catch (InvocationTargetException unused) {
                    return null;
                }
            } else {
                Intrinsics.a("cert");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return Intrinsics.a(this.a, cVar.a) && Intrinsics.a(this.b, cVar.b);
        }

        public int hashCode() {
            X509TrustManager x509TrustManager = this.a;
            int i2 = 0;
            int hashCode = (x509TrustManager != null ? x509TrustManager.hashCode() : 0) * 31;
            Method method = this.b;
            if (method != null) {
                i2 = method.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("CustomTrustRootIndex(trustManager=");
            a2.append(this.a);
            a2.append(", findByIssuerAndSignatureMethod=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    static {
        boolean z = true;
        try {
            Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
            if (1 != 0) {
                f3044f = z;
                return;
            }
            throw new IllegalStateException(("Expected Android API level 21+ but was " + Build.VERSION.SDK_INT).toString());
        } catch (ClassNotFoundException unused) {
            z = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public AndroidPlatform() {
        StandardAndroidSocketAdapter standardAndroidSocketAdapter;
        Method method;
        Method method2;
        SocketAdapter[] socketAdapterArr = new SocketAdapter[3];
        StandardAndroidSocketAdapter.a aVar = StandardAndroidSocketAdapter.f3054f;
        Method method3 = null;
        try {
            Class<?> cls = Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
            Class<?> cls2 = Class.forName("com.android.org.conscrypt" + ".OpenSSLSocketFactoryImpl");
            Class<?> cls3 = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
            Intrinsics.a((Object) cls3, "paramsClass");
            standardAndroidSocketAdapter = new StandardAndroidSocketAdapter(cls, cls2, cls3);
        } catch (Exception e2) {
            Collections.a(5, "unable to load android socket classes", e2);
            standardAndroidSocketAdapter = null;
        }
        socketAdapterArr[0] = standardAndroidSocketAdapter;
        ConscryptPlatform.a aVar2 = ConscryptPlatform.f3047f;
        socketAdapterArr[1] = ConscryptPlatform.f3046e ? ConscryptSocketAdapter.a : null;
        socketAdapterArr[2] = new DeferredSocketAdapter("com.google.android.gms.org.conscrypt");
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < 3; i2++) {
            SocketAdapter socketAdapter = socketAdapterArr[i2];
            if (socketAdapter != null) {
                arrayList.add(socketAdapter);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (((SocketAdapter) next).a()) {
                arrayList2.add(next);
            }
        }
        this.d = arrayList2;
        try {
            Class<?> cls4 = Class.forName("dalvik.system.CloseGuard");
            Method method4 = cls4.getMethod("get", new Class[0]);
            method = cls4.getMethod("open", String.class);
            method2 = cls4.getMethod("warnIfOpen", new Class[0]);
            method3 = method4;
        } catch (Exception unused) {
            method2 = null;
            method = null;
        }
        this.f3045e = new CloseGuard(method3, method, method2);
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i2) {
        if (socket == null) {
            Intrinsics.a("socket");
            throw null;
        } else if (inetSocketAddress != null) {
            try {
                socket.connect(inetSocketAddress, i2);
            } catch (ClassCastException e2) {
                if (Build.VERSION.SDK_INT == 26) {
                    throw new IOException("Exception in connect", e2);
                }
                throw e2;
            }
        } else {
            Intrinsics.a("address");
            throw null;
        }
    }

    public String b(SSLSocket sSLSocket) {
        T t2;
        if (sSLSocket != null) {
            Iterator<T> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (((SocketAdapter) t2).b(sSLSocket)) {
                    break;
                }
            }
            SocketAdapter socketAdapter = (SocketAdapter) t2;
            if (socketAdapter != null) {
                return socketAdapter.a(sSLSocket);
            }
            return null;
        }
        Intrinsics.a("sslSocket");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public boolean b(String str) {
        if (str != null) {
            try {
                Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
                Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                Intrinsics.a((Object) cls, "networkPolicyClass");
                Intrinsics.a(invoke, "networkSecurityPolicy");
                return a(str, cls, invoke);
            } catch (ClassNotFoundException unused) {
                super.b(str);
                return true;
            } catch (NoSuchMethodException unused2) {
                super.b(str);
                return true;
            } catch (IllegalAccessException e2) {
                throw new AssertionError("unable to determine cleartext support", e2);
            } catch (IllegalArgumentException e3) {
                throw new AssertionError("unable to determine cleartext support", e3);
            } catch (InvocationTargetException e4) {
                throw new AssertionError("unable to determine cleartext support", e4);
            }
        } else {
            Intrinsics.a("hostname");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        T t2 = null;
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list != null) {
            Iterator<T> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (((SocketAdapter) next).b(sSLSocket)) {
                    t2 = next;
                    break;
                }
            }
            SocketAdapter socketAdapter = (SocketAdapter) t2;
            if (socketAdapter != null) {
                socketAdapter.a(sSLSocket, str, list);
            }
        } else {
            Intrinsics.a("protocols");
            throw null;
        }
    }

    public void a(int i2, String str, Throwable th) {
        if (str != null) {
            Collections.a(i2, str, th);
        } else {
            Intrinsics.a("message");
            throw null;
        }
    }

    public Object a(String str) {
        if (str != null) {
            CloseGuard closeGuard = this.f3045e;
            Method method = closeGuard.a;
            if (method == null) {
                return null;
            }
            try {
                Object invoke = method.invoke(null, new Object[0]);
                Method method2 = closeGuard.b;
                if (method2 != null) {
                    method2.invoke(invoke, str);
                    return invoke;
                }
                Intrinsics.a();
                throw null;
            } catch (Exception unused) {
                return null;
            }
        } else {
            Intrinsics.a("closer");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Method, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public TrustRootIndex b(X509TrustManager x509TrustManager) {
        if (x509TrustManager != null) {
            try {
                Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", X509Certificate.class);
                Intrinsics.a((Object) declaredMethod, "method");
                declaredMethod.setAccessible(true);
                return new c(x509TrustManager, declaredMethod);
            } catch (NoSuchMethodException unused) {
                return super.b(x509TrustManager);
            }
        } else {
            Intrinsics.a("trustManager");
            throw null;
        }
    }

    public void a(String str, Object obj) {
        if (str != null) {
            CloseGuard closeGuard = this.f3045e;
            if (closeGuard != null) {
                boolean z = false;
                if (obj != null) {
                    try {
                        Method method = closeGuard.c;
                        if (method != null) {
                            method.invoke(obj, new Object[0]);
                            z = true;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } catch (Exception unused) {
                    }
                }
                if (!z) {
                    a(5, str, (Throwable) null);
                    return;
                }
                return;
            }
            throw null;
        }
        Intrinsics.a("message");
        throw null;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|(3:10|15|16)(2:11|12)) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r3 = ((java.lang.Boolean) r8).booleanValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        super.b(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r8 = r8.getMethod("isCleartextTrafficPermitted", new java.lang.Class[0]).invoke(r9, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0033, code lost:
        if (r8 != null) goto L_0x0035;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0027 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r7, java.lang.Class<?> r8, java.lang.Object r9) {
        /*
            r6 = this;
            java.lang.String r0 = "null cannot be cast to non-null type kotlin.Boolean"
            java.lang.String r1 = "isCleartextTrafficPermitted"
            r2 = 0
            r3 = 1
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x0027 }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r4[r2] = r5     // Catch:{ NoSuchMethodException -> 0x0027 }
            java.lang.reflect.Method r4 = r8.getMethod(r1, r4)     // Catch:{ NoSuchMethodException -> 0x0027 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ NoSuchMethodException -> 0x0027 }
            r5[r2] = r7     // Catch:{ NoSuchMethodException -> 0x0027 }
            java.lang.Object r4 = r4.invoke(r9, r5)     // Catch:{ NoSuchMethodException -> 0x0027 }
            if (r4 == 0) goto L_0x0021
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ NoSuchMethodException -> 0x0027 }
            boolean r7 = r4.booleanValue()     // Catch:{ NoSuchMethodException -> 0x0027 }
            goto L_0x0046
        L_0x0021:
            kotlin.TypeCastException r4 = new kotlin.TypeCastException     // Catch:{ NoSuchMethodException -> 0x0027 }
            r4.<init>(r0)     // Catch:{ NoSuchMethodException -> 0x0027 }
            throw r4     // Catch:{ NoSuchMethodException -> 0x0027 }
        L_0x0027:
            java.lang.Class[] r4 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x0042 }
            java.lang.reflect.Method r8 = r8.getMethod(r1, r4)     // Catch:{ NoSuchMethodException -> 0x0042 }
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ NoSuchMethodException -> 0x0042 }
            java.lang.Object r8 = r8.invoke(r9, r1)     // Catch:{ NoSuchMethodException -> 0x0042 }
            if (r8 == 0) goto L_0x003c
            java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ NoSuchMethodException -> 0x0042 }
            boolean r3 = r8.booleanValue()     // Catch:{ NoSuchMethodException -> 0x0042 }
            goto L_0x0045
        L_0x003c:
            kotlin.TypeCastException r8 = new kotlin.TypeCastException     // Catch:{ NoSuchMethodException -> 0x0042 }
            r8.<init>(r0)     // Catch:{ NoSuchMethodException -> 0x0042 }
            throw r8     // Catch:{ NoSuchMethodException -> 0x0042 }
        L_0x0042:
            super.b(r7)
        L_0x0045:
            r7 = r3
        L_0x0046:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.i.AndroidPlatform.a(java.lang.String, java.lang.Class, java.lang.Object):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [?, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Method, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public CertificateChainCleaner a(X509TrustManager x509TrustManager) {
        Class<String> cls = String.class;
        if (x509TrustManager != null) {
            try {
                Class<?> cls2 = Class.forName("android.net.http.X509TrustManagerExtensions");
                Object newInstance = cls2.getConstructor(X509TrustManager.class).newInstance(x509TrustManager);
                Method method = cls2.getMethod("checkServerTrusted", X509Certificate[].class, cls, cls);
                Intrinsics.a((Object) newInstance, "extensions");
                Intrinsics.a((Object) method, "checkServerTrusted");
                return new a(newInstance, method);
            } catch (Exception unused) {
                return new BasicCertificateChainCleaner(b(x509TrustManager));
            }
        } else {
            Intrinsics.a("trustManager");
            throw null;
        }
    }
}
