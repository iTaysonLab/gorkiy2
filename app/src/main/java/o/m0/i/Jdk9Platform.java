package o.m0.i;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import o.c0;

/* compiled from: Jdk9Platform.kt */
public final class Jdk9Platform extends Platform {
    public final Method d;

    /* renamed from: e  reason: collision with root package name */
    public final Method f3050e;

    public Jdk9Platform(Method method, Method method2) {
        if (method == null) {
            Intrinsics.a("setProtocolMethod");
            throw null;
        } else if (method2 != null) {
            this.d = method;
            this.f3050e = method2;
        } else {
            Intrinsics.a("getProtocolMethod");
            throw null;
        }
    }

    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list != null) {
            try {
                SSLParameters sSLParameters = sSLSocket.getSSLParameters();
                List<String> a = Platform.c.a(list);
                Method method = this.d;
                Object[] objArr = new Object[1];
                Object[] array = a.toArray(new String[0]);
                if (array != null) {
                    objArr[0] = array;
                    method.invoke(sSLParameters, objArr);
                    sSLSocket.setSSLParameters(sSLParameters);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } catch (IllegalAccessException e2) {
                throw new AssertionError("failed to set SSL parameters", e2);
            } catch (InvocationTargetException e3) {
                throw new AssertionError("failed to set SSL parameters", e3);
            }
        } else {
            Intrinsics.a("protocols");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public String b(SSLSocket sSLSocket) {
        if (sSLSocket != null) {
            try {
                String str = (String) this.f3050e.invoke(sSLSocket, new Object[0]);
                if (str == null) {
                    return null;
                }
                if (Intrinsics.a((Object) str, (Object) "")) {
                    return null;
                }
                return str;
            } catch (IllegalAccessException e2) {
                throw new AssertionError("failed to get ALPN selected protocol", e2);
            } catch (InvocationTargetException e3) {
                throw new AssertionError("failed to get ALPN selected protocol", e3);
            }
        } else {
            Intrinsics.a("socket");
            throw null;
        }
    }
}
