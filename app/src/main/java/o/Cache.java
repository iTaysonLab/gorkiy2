package o;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import n.i.Collections;
import n.i.Collections2;
import n.i.Sets;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Headers;
import o.MediaType;
import o.m0.Util;
import o.m0.c.CacheRequest;
import o.m0.c.CacheStrategy;
import o.m0.c.DiskLruCache;
import o.m0.c.e;
import o.m0.e.StatusLine;
import o.m0.h.FileSystem;
import o.m0.i.Platform;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.ByteString;
import p.ForwardingSink;
import p.ForwardingSource;
import p.Sink;
import p.Source;
import p.h;
import p.i;
import p.x;
import p.z;

/* compiled from: Cache.kt */
public final class Cache implements Closeable, Flushable {
    public final DiskLruCache b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f2801e;

    /* renamed from: f  reason: collision with root package name */
    public int f2802f;
    public int g;

    /* compiled from: Cache.kt */
    public static final class a extends ResponseBody {
        public final BufferedSource d;

        /* renamed from: e  reason: collision with root package name */
        public final DiskLruCache.c f2803e;

        /* renamed from: f  reason: collision with root package name */
        public final String f2804f;
        public final String g;

        /* renamed from: o.Cache$a$a  reason: collision with other inner class name */
        /* compiled from: Cache.kt */
        public static final class C0039a extends ForwardingSource {
            public final /* synthetic */ a c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0039a(a aVar, z zVar, z zVar2) {
                super(zVar2);
                this.c = aVar;
            }

            public void close() {
                this.c.f2803e.close();
                super.b.close();
            }
        }

        public a(DiskLruCache.c cVar, String str, String str2) {
            if (cVar != null) {
                this.f2803e = cVar;
                this.f2804f = str;
                this.g = str2;
                Source source = cVar.d.get(1);
                this.d = Collections.a((Source) new C0039a(this, source, source));
                return;
            }
            Intrinsics.a("snapshot");
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: o.m0.Util.a(java.lang.String, long):long
         arg types: [java.lang.String, int]
         candidates:
          o.m0.Util.a(byte, int):int
          o.m0.Util.a(java.lang.String, int):int
          o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
          o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
          o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
          o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
          o.m0.Util.a(java.lang.Object, long):void
          o.m0.Util.a(p.BufferedSink, int):void
          o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
          o.m0.Util.a(java.lang.String, long):long */
        public long a() {
            String str = this.g;
            if (str != null) {
                return Util.a(str, -1L);
            }
            return -1;
        }

        public MediaType f() {
            String str = this.f2804f;
            if (str == null) {
                return null;
            }
            MediaType.a aVar = MediaType.f2859f;
            return MediaType.a.b(str);
        }

        public BufferedSource g() {
            return this.d;
        }
    }

    /* compiled from: Cache.kt */
    public final class c implements CacheRequest {
        public final Sink a;
        public final Sink b;
        public boolean c;
        public final DiskLruCache.a d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ Cache f2811e;

        /* compiled from: Cache.kt */
        public static final class a extends ForwardingSink {
            public final /* synthetic */ c c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, x xVar) {
                super(xVar);
                this.c = cVar;
            }

            public void close() {
                synchronized (this.c.f2811e) {
                    if (!this.c.c) {
                        this.c.c = true;
                        this.c.f2811e.c++;
                        super.b.close();
                        this.c.d.b();
                    }
                }
            }
        }

        public c(Cache cache, e.a aVar) {
            if (aVar != null) {
                this.f2811e = cache;
                this.d = aVar;
                Sink a2 = aVar.a(1);
                this.a = a2;
                this.b = new a(this, a2);
                return;
            }
            Intrinsics.a("editor");
            throw null;
        }

        public void a() {
            synchronized (this.f2811e) {
                if (!this.c) {
                    this.c = true;
                    this.f2811e.d++;
                    Util.a(this.a);
                    try {
                        this.d.a();
                    } catch (IOException unused) {
                    }
                }
            }
        }

        public Sink b() {
            return this.b;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      o.m0.Util.a(byte, int):int
      o.m0.Util.a(java.lang.String, int):int
      o.m0.Util.a(java.lang.String, long):long
      o.m0.Util.a(java.lang.String, java.lang.Object[]):java.lang.String
      o.m0.Util.a(o.HttpUrl, boolean):java.lang.String
      o.m0.Util.a(p.BufferedSource, java.nio.charset.Charset):java.nio.charset.Charset
      o.m0.Util.a(java.lang.Object, long):void
      o.m0.Util.a(p.BufferedSink, int):void
      o.m0.Util.a(o.HttpUrl, o.HttpUrl):boolean
      o.m0.Util.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public Cache(File file, long j2) {
        if (file != null) {
            FileSystem fileSystem = FileSystem.a;
            DiskLruCache diskLruCache = DiskLruCache.A;
            if (j2 > 0) {
                this.b = new DiskLruCache(fileSystem, file, 201105, 2, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), Util.a("OkHttp DiskLruCache", true)));
                return;
            }
            throw new IllegalArgumentException("maxSize <= 0".toString());
        }
        Intrinsics.a("directory");
        throw null;
    }

    public final void a(Request request) {
        if (request != null) {
            this.b.d(a(request.b));
        } else {
            Intrinsics.a("request");
            throw null;
        }
    }

    public void close() {
        this.b.close();
    }

    public void flush() {
        this.b.flush();
    }

    public final synchronized void a(CacheStrategy cacheStrategy) {
        if (cacheStrategy != null) {
            this.g++;
            if (cacheStrategy.a != null) {
                this.f2801e++;
            } else if (cacheStrategy.b != null) {
                this.f2802f++;
            }
        } else {
            Intrinsics.a("cacheStrategy");
            throw null;
        }
    }

    public final synchronized void a() {
        this.f2802f++;
    }

    public static final String a(HttpUrl httpUrl) {
        if (httpUrl != null) {
            return ByteString.f3063f.b(httpUrl.f2854j).a("MD5").h();
        }
        Intrinsics.a("url");
        throw null;
    }

    public static final int a(BufferedSource bufferedSource) {
        if (bufferedSource != null) {
            try {
                long d2 = bufferedSource.d();
                String e2 = bufferedSource.e();
                if (d2 >= 0 && d2 <= ((long) Integer.MAX_VALUE)) {
                    if (!(e2.length() > 0)) {
                        return (int) d2;
                    }
                }
                throw new IOException("expected an int but was \"" + d2 + e2 + '\"');
            } catch (NumberFormatException e3) {
                throw new IOException(e3.getMessage());
            }
        } else {
            Intrinsics.a("source");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Comparator, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
     arg types: [java.lang.String, char[], int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List */
    public static final Set<String> a(w wVar) {
        int size = wVar.size();
        TreeSet treeSet = null;
        for (int i2 = 0; i2 < size; i2++) {
            if (Indent.a("Vary", wVar.c(i2), true)) {
                String d2 = wVar.d(i2);
                if (treeSet == null) {
                    Comparator comparator = String.CASE_INSENSITIVE_ORDER;
                    Intrinsics.a((Object) comparator, "java.lang.String.CASE_INSENSITIVE_ORDER");
                    treeSet = new TreeSet(comparator);
                }
                for (String str : Indent.a((CharSequence) d2, new char[]{','}, false, 0, 6)) {
                    if (str != null) {
                        treeSet.add(Indent.c(str).toString());
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                continue;
            }
        }
        return treeSet != null ? treeSet : Sets.b;
    }

    /* compiled from: Cache.kt */
    public static final class b {

        /* renamed from: k  reason: collision with root package name */
        public static final String f2805k = "OkHttp-Sent-Millis";

        /* renamed from: l  reason: collision with root package name */
        public static final String f2806l = "OkHttp-Received-Millis";
        public final String a;
        public final Headers b;
        public final String c;
        public final Protocol d;

        /* renamed from: e  reason: collision with root package name */
        public final int f2807e;

        /* renamed from: f  reason: collision with root package name */
        public final String f2808f;
        public final Headers g;
        public final Handshake h;

        /* renamed from: i  reason: collision with root package name */
        public final long f2809i;

        /* renamed from: j  reason: collision with root package name */
        public final long f2810j;

        static {
            Platform.a aVar = Platform.c;
            if (Platform.a != null) {
                Platform.a aVar2 = Platform.c;
                if (Platform.a != null) {
                    return;
                }
                throw null;
            }
            throw null;
        }

        public b(Source source) {
            TlsVersion tlsVersion;
            if (source != null) {
                try {
                    BufferedSource a2 = Collections.a(source);
                    this.a = a2.e();
                    this.c = a2.e();
                    Headers.a aVar = new Headers.a();
                    int a3 = Cache.a(a2);
                    boolean z = false;
                    for (int i2 = 0; i2 < a3; i2++) {
                        aVar.a(a2.e());
                    }
                    this.b = aVar.a();
                    StatusLine a4 = StatusLine.a(a2.e());
                    this.d = a4.a;
                    this.f2807e = a4.b;
                    this.f2808f = a4.c;
                    Headers.a aVar2 = new Headers.a();
                    int a5 = Cache.a(a2);
                    for (int i3 = 0; i3 < a5; i3++) {
                        aVar2.a(a2.e());
                    }
                    String b2 = aVar2.b(f2805k);
                    String b3 = aVar2.b(f2806l);
                    aVar2.c(f2805k);
                    aVar2.c(f2806l);
                    long j2 = 0;
                    this.f2809i = b2 != null ? Long.parseLong(b2) : 0;
                    this.f2810j = b3 != null ? Long.parseLong(b3) : j2;
                    this.g = aVar2.a();
                    if (Indent.b(this.a, "https://", false, 2)) {
                        String e2 = a2.e();
                        if (!(e2.length() > 0 ? true : z)) {
                            CipherSuite a6 = CipherSuite.f2834t.a(a2.e());
                            List<Certificate> a7 = a((i) a2);
                            List<Certificate> a8 = a((i) a2);
                            if (!a2.j()) {
                                tlsVersion = TlsVersion.Companion.a(a2.e());
                            } else {
                                tlsVersion = TlsVersion.SSL_3_0;
                            }
                            this.h = Handshake.f2848f.a(tlsVersion, a6, a7, a8);
                        } else {
                            throw new IOException("expected \"\" but was \"" + e2 + '\"');
                        }
                    } else {
                        this.h = null;
                    }
                } finally {
                    source.close();
                }
            } else {
                Intrinsics.a("rawSource");
                throw null;
            }
        }

        public final void a(DiskLruCache.a aVar) {
            if (aVar != null) {
                BufferedSink a2 = Collections.a(aVar.a(0));
                a2.a(this.a).writeByte(10);
                a2.a(this.c).writeByte(10);
                a2.h((long) this.b.size()).writeByte(10);
                int size = this.b.size();
                for (int i2 = 0; i2 < size; i2++) {
                    a2.a(this.b.c(i2)).a(": ").a(this.b.d(i2)).writeByte(10);
                }
                a2.a(new StatusLine(this.d, this.f2807e, this.f2808f).toString()).writeByte(10);
                a2.h((long) (this.g.size() + 2)).writeByte(10);
                int size2 = this.g.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    a2.a(this.g.c(i3)).a(": ").a(this.g.d(i3)).writeByte(10);
                }
                a2.a(f2805k).a(": ").h(this.f2809i).writeByte(10);
                a2.a(f2806l).a(": ").h(this.f2810j).writeByte(10);
                if (Indent.b(this.a, "https://", false, 2)) {
                    a2.writeByte(10);
                    Handshake handshake = this.h;
                    if (handshake != null) {
                        a2.a(handshake.c.a).writeByte(10);
                        a(a2, this.h.a());
                        a(a2, this.h.d);
                        a2.a(this.h.b.javaName).writeByte(10);
                    } else {
                        Intrinsics.a();
                        throw null;
                    }
                }
                a2.close();
                return;
            }
            Intrinsics.a("editor");
            throw null;
        }

        public b(Response response) {
            Headers headers;
            if (response != null) {
                this.a = response.c.b.f2854j;
                Response response2 = response.f2903j;
                if (response2 != null) {
                    Headers headers2 = response2.c.d;
                    Set<String> a2 = Cache.a((w) response.h);
                    if (a2.isEmpty()) {
                        headers = Util.b;
                    } else {
                        Headers.a aVar = new Headers.a();
                        int size = headers2.size();
                        for (int i2 = 0; i2 < size; i2++) {
                            String c2 = headers2.c(i2);
                            if (a2.contains(c2)) {
                                aVar.a(c2, headers2.d(i2));
                            }
                        }
                        headers = aVar.a();
                    }
                    this.b = headers;
                    this.c = response.c.c;
                    this.d = response.d;
                    this.f2807e = response.f2901f;
                    this.f2808f = response.f2900e;
                    this.g = response.h;
                    this.h = response.g;
                    this.f2809i = response.f2906m;
                    this.f2810j = response.f2907n;
                    return;
                }
                Intrinsics.a();
                throw null;
            }
            Intrinsics.a("response");
            throw null;
        }

        public final List<Certificate> a(i iVar) {
            int a2 = Cache.a((BufferedSource) iVar);
            if (a2 == -1) {
                return Collections2.b;
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(a2);
                int i2 = 0;
                while (i2 < a2) {
                    String e2 = iVar.e();
                    Buffer buffer = new Buffer();
                    ByteString.a aVar = ByteString.f3063f;
                    if (e2 != null) {
                        ByteString a3 = p.b0.ByteString.a(e2);
                        if (a3 != null) {
                            buffer.a(a3);
                            arrayList.add(instance.generateCertificate(new Buffer.a(buffer)));
                            i2++;
                        } else {
                            Intrinsics.a();
                            throw null;
                        }
                    } else {
                        Intrinsics.a("$receiver");
                        throw null;
                    }
                }
                return arrayList;
            } catch (CertificateException e3) {
                throw new IOException(e3.getMessage());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final void a(h hVar, List<? extends Certificate> list) {
            try {
                hVar.h((long) list.size()).writeByte(10);
                int size = list.size();
                for (int i2 = 0; i2 < size; i2++) {
                    byte[] encoded = ((Certificate) list.get(i2)).getEncoded();
                    ByteString.a aVar = ByteString.f3063f;
                    Intrinsics.a((Object) encoded, "bytes");
                    hVar.a(p.b0.ByteString.a(ByteString.a.a(aVar, encoded, 0, 0, 3))).writeByte(10);
                }
            } catch (CertificateEncodingException e2) {
                throw new IOException(e2.getMessage());
            }
        }
    }
}
