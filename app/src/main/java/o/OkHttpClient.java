package o;

import java.net.Proxy;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import l.a.a.a.o.b.AbstractSpiCall;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import o.Call;
import o.EventListener;
import o.m0.Util;
import o.m0.Util0;
import o.m0.k.CertificateChainCleaner;
import o.m0.k.OkHostnameVerifier;

/* compiled from: OkHttpClient.kt */
public class OkHttpClient implements Cloneable, Call.a {
    public static final List<c0> D = Util.a(Protocol.HTTP_2, Protocol.HTTP_1_1);
    public static final List<m> E = Util.a(ConnectionSpec.g, ConnectionSpec.h);
    public static final b F = new b(null);
    public final int A;
    public final int B;
    public final int C;
    public final Dispatcher b;
    public final ConnectionPool c;
    public final List<y> d;

    /* renamed from: e  reason: collision with root package name */
    public final List<y> f2866e;

    /* renamed from: f  reason: collision with root package name */
    public final EventListener.b f2867f;
    public final boolean g;
    public final Authenticator h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f2868i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f2869j;

    /* renamed from: k  reason: collision with root package name */
    public final CookieJar f2870k;

    /* renamed from: l  reason: collision with root package name */
    public final Cache f2871l;

    /* renamed from: m  reason: collision with root package name */
    public final Dns f2872m;

    /* renamed from: n  reason: collision with root package name */
    public final Proxy f2873n;

    /* renamed from: o  reason: collision with root package name */
    public final ProxySelector f2874o;

    /* renamed from: p  reason: collision with root package name */
    public final Authenticator f2875p;

    /* renamed from: q  reason: collision with root package name */
    public final SocketFactory f2876q;

    /* renamed from: r  reason: collision with root package name */
    public final SSLSocketFactory f2877r;

    /* renamed from: s  reason: collision with root package name */
    public final X509TrustManager f2878s;

    /* renamed from: t  reason: collision with root package name */
    public final List<m> f2879t;
    public final List<c0> u;
    public final HostnameVerifier v;
    public final CertificatePinner w;
    public final CertificateChainCleaner x;
    public final int y;
    public final int z;

    /* compiled from: OkHttpClient.kt */
    public static final class a {
        public Dispatcher a = new Dispatcher();
        public ConnectionPool b = new ConnectionPool();
        public final List<y> c = new ArrayList();
        public final List<y> d = new ArrayList();

        /* renamed from: e  reason: collision with root package name */
        public EventListener.b f2880e = new Util0(EventListener.a);

        /* renamed from: f  reason: collision with root package name */
        public boolean f2881f = true;
        public Authenticator g = Authenticator.a;
        public boolean h = true;

        /* renamed from: i  reason: collision with root package name */
        public boolean f2882i = true;

        /* renamed from: j  reason: collision with root package name */
        public CookieJar f2883j = CookieJar.a;

        /* renamed from: k  reason: collision with root package name */
        public Cache f2884k;

        /* renamed from: l  reason: collision with root package name */
        public Dns f2885l = Dns.a;

        /* renamed from: m  reason: collision with root package name */
        public Authenticator f2886m = Authenticator.a;

        /* renamed from: n  reason: collision with root package name */
        public SocketFactory f2887n;

        /* renamed from: o  reason: collision with root package name */
        public List<m> f2888o;

        /* renamed from: p  reason: collision with root package name */
        public List<? extends c0> f2889p;

        /* renamed from: q  reason: collision with root package name */
        public HostnameVerifier f2890q;

        /* renamed from: r  reason: collision with root package name */
        public CertificatePinner f2891r;

        /* renamed from: s  reason: collision with root package name */
        public int f2892s;

        /* renamed from: t  reason: collision with root package name */
        public int f2893t;
        public int u;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [javax.net.SocketFactory, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public a() {
            SocketFactory socketFactory = SocketFactory.getDefault();
            Intrinsics.a((Object) socketFactory, "SocketFactory.getDefault()");
            this.f2887n = socketFactory;
            b bVar = OkHttpClient.F;
            this.f2888o = OkHttpClient.E;
            b bVar2 = OkHttpClient.F;
            this.f2889p = OkHttpClient.D;
            this.f2890q = OkHostnameVerifier.a;
            this.f2891r = CertificatePinner.c;
            this.f2892s = AbstractSpiCall.DEFAULT_TIMEOUT;
            this.f2893t = AbstractSpiCall.DEFAULT_TIMEOUT;
            this.u = AbstractSpiCall.DEFAULT_TIMEOUT;
        }
    }

    /* compiled from: OkHttpClient.kt */
    public static final class b {
        public /* synthetic */ b(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.SSLSocketFactory, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OkHttpClient(o.OkHttpClient.a r7) {
        /*
            r6 = this;
            r0 = 0
            if (r7 == 0) goto L_0x0167
            r6.<init>()
            o.Dispatcher r1 = r7.a
            r6.b = r1
            o.ConnectionPool r1 = r7.b
            r6.c = r1
            java.util.List<o.y> r1 = r7.c
            java.util.List r1 = o.m0.Util.b(r1)
            r6.d = r1
            java.util.List<o.y> r1 = r7.d
            java.util.List r1 = o.m0.Util.b(r1)
            r6.f2866e = r1
            o.EventListener$b r1 = r7.f2880e
            r6.f2867f = r1
            boolean r1 = r7.f2881f
            r6.g = r1
            o.Authenticator r1 = r7.g
            r6.h = r1
            boolean r1 = r7.h
            r6.f2868i = r1
            boolean r1 = r7.f2882i
            r6.f2869j = r1
            o.CookieJar r1 = r7.f2883j
            r6.f2870k = r1
            o.Cache r1 = r7.f2884k
            r6.f2871l = r1
            o.Dns r1 = r7.f2885l
            r6.f2872m = r1
            r6.f2873n = r0
            java.net.ProxySelector r1 = java.net.ProxySelector.getDefault()
            if (r1 == 0) goto L_0x0047
            goto L_0x0049
        L_0x0047:
            o.m0.j.NullProxySelector r1 = o.m0.j.NullProxySelector.a
        L_0x0049:
            r6.f2874o = r1
            o.Authenticator r1 = r7.f2886m
            r6.f2875p = r1
            javax.net.SocketFactory r1 = r7.f2887n
            r6.f2876q = r1
            java.util.List<o.m> r1 = r7.f2888o
            r6.f2879t = r1
            java.util.List<? extends o.c0> r2 = r7.f2889p
            r6.u = r2
            javax.net.ssl.HostnameVerifier r2 = r7.f2890q
            r6.v = r2
            r2 = 0
            r6.y = r2
            int r3 = r7.f2892s
            r6.z = r3
            int r3 = r7.f2893t
            r6.A = r3
            int r3 = r7.u
            r6.B = r3
            r6.C = r2
            boolean r3 = r1 instanceof java.util.Collection
            r4 = 1
            if (r3 == 0) goto L_0x007c
            boolean r3 = r1.isEmpty()
            if (r3 == 0) goto L_0x007c
            goto L_0x0092
        L_0x007c:
            java.util.Iterator r1 = r1.iterator()
        L_0x0080:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x0092
            java.lang.Object r3 = r1.next()
            o.ConnectionSpec r3 = (o.ConnectionSpec) r3
            boolean r3 = r3.a
            if (r3 == 0) goto L_0x0080
            r1 = 0
            goto L_0x0093
        L_0x0092:
            r1 = 1
        L_0x0093:
            if (r1 == 0) goto L_0x009c
            r6.f2877r = r0
            r6.x = r0
            r6.f2878s = r0
            goto L_0x00db
        L_0x009c:
            o.m0.i.Platform$a r1 = o.m0.i.Platform.c
            o.m0.i.Platform r1 = o.m0.i.Platform.a
            javax.net.ssl.X509TrustManager r1 = r1.b()
            r6.f2878s = r1
            o.m0.i.Platform$a r1 = o.m0.i.Platform.c
            o.m0.i.Platform r1 = o.m0.i.Platform.a
            javax.net.ssl.X509TrustManager r3 = r6.f2878s
            r1.c(r3)
            javax.net.ssl.X509TrustManager r1 = r6.f2878s
            if (r1 == 0) goto L_0x0163
            o.m0.i.Platform$a r3 = o.m0.i.Platform.c     // Catch:{ GeneralSecurityException -> 0x015a }
            o.m0.i.Platform r3 = o.m0.i.Platform.a     // Catch:{ GeneralSecurityException -> 0x015a }
            javax.net.ssl.SSLContext r3 = r3.a()     // Catch:{ GeneralSecurityException -> 0x015a }
            javax.net.ssl.TrustManager[] r5 = new javax.net.ssl.TrustManager[r4]     // Catch:{ GeneralSecurityException -> 0x015a }
            r5[r2] = r1     // Catch:{ GeneralSecurityException -> 0x015a }
            r3.init(r0, r5, r0)     // Catch:{ GeneralSecurityException -> 0x015a }
            javax.net.ssl.SSLSocketFactory r1 = r3.getSocketFactory()     // Catch:{ GeneralSecurityException -> 0x015a }
            java.lang.String r2 = "sslContext.socketFactory"
            n.n.c.Intrinsics.a(r1, r2)     // Catch:{ GeneralSecurityException -> 0x015a }
            r6.f2877r = r1
            javax.net.ssl.X509TrustManager r1 = r6.f2878s
            if (r1 == 0) goto L_0x0156
            o.m0.i.Platform$a r2 = o.m0.i.Platform.c
            o.m0.i.Platform r2 = o.m0.i.Platform.a
            o.m0.k.CertificateChainCleaner r1 = r2.a(r1)
            r6.x = r1
        L_0x00db:
            javax.net.ssl.SSLSocketFactory r1 = r6.f2877r
            if (r1 == 0) goto L_0x00e8
            o.m0.i.Platform$a r1 = o.m0.i.Platform.c
            o.m0.i.Platform r1 = o.m0.i.Platform.a
            javax.net.ssl.SSLSocketFactory r2 = r6.f2877r
            r1.a(r2)
        L_0x00e8:
            o.CertificatePinner r7 = r7.f2891r
            o.m0.k.CertificateChainCleaner r1 = r6.x
            o.m0.k.CertificateChainCleaner r2 = r7.b
            boolean r2 = n.n.c.Intrinsics.a(r2, r1)
            if (r2 == 0) goto L_0x00f5
            goto L_0x00fd
        L_0x00f5:
            o.CertificatePinner r2 = new o.CertificatePinner
            java.util.Set<o.h$b> r7 = r7.a
            r2.<init>(r7, r1)
            r7 = r2
        L_0x00fd:
            r6.w = r7
            java.util.List<o.y> r7 = r6.d
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>"
            if (r7 == 0) goto L_0x0150
            boolean r7 = r7.contains(r0)
            r7 = r7 ^ r4
            if (r7 == 0) goto L_0x0137
            java.util.List<o.y> r7 = r6.f2866e
            if (r7 == 0) goto L_0x0131
            boolean r7 = r7.contains(r0)
            r7 = r7 ^ r4
            if (r7 == 0) goto L_0x0118
            return
        L_0x0118:
            java.lang.String r7 = "Null network interceptor: "
            java.lang.StringBuilder r7 = j.a.a.a.outline.a(r7)
            java.util.List<o.y> r0 = r6.f2866e
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r7 = r7.toString()
            r0.<init>(r7)
            throw r0
        L_0x0131:
            kotlin.TypeCastException r7 = new kotlin.TypeCastException
            r7.<init>(r1)
            throw r7
        L_0x0137:
            java.lang.String r7 = "Null interceptor: "
            java.lang.StringBuilder r7 = j.a.a.a.outline.a(r7)
            java.util.List<o.y> r0 = r6.d
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r7 = r7.toString()
            r0.<init>(r7)
            throw r0
        L_0x0150:
            kotlin.TypeCastException r7 = new kotlin.TypeCastException
            r7.<init>(r1)
            throw r7
        L_0x0156:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x015a:
            r7 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            java.lang.String r1 = "No System TLS"
            r0.<init>(r1, r7)
            throw r0
        L_0x0163:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x0167:
            java.lang.String r7 = "builder"
            n.n.c.Intrinsics.a(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.OkHttpClient.<init>(o.OkHttpClient$a):void");
    }

    public Call a(Request request) {
        if (request != null) {
            return RealCall.a(this, request, false);
        }
        Intrinsics.a("request");
        throw null;
    }

    public Object clone() {
        return super.clone();
    }

    public OkHttpClient() {
        this(new a());
    }
}
